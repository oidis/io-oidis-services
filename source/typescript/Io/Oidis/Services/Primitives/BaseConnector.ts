/*! ******************************************************************************************************** *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { WebServiceClientEventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/WebServiceClientEventType.js";
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { EventsManager } from "@io-oidis-commons/Io/Oidis/Commons/Events/EventsManager.js";
import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { HttpManager } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/HttpManager.js";
import { HttpRequestParser } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/HttpRequestParser.js";
import { IWebServiceClientEvents } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Events/IWebServiceClientEvents.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { WebSocketsClient } from "@io-oidis-commons/Io/Oidis/Commons/WebServiceApi/Clients/WebSocketsClient.js";
import { WebServiceConfiguration } from "@io-oidis-commons/Io/Oidis/Commons/WebServiceApi/WebServiceConfiguration.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { Resources } from "../DAO/Resources.js";
import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { ILiveContentFormatterPromise } from "../Interfaces/ILiveContentFormatterPromise.js";
import { ILiveContentErrorPromise, ILiveContentPromise } from "../Interfaces/ILiveContentPromise.js";
import { IWebServiceClient } from "../Interfaces/IWebServiceClient.js";
import { Loader } from "../Loader.js";
import { ForwardingClient } from "../WebServiceApi/Clients/ForwardingClient.js";
import { LiveContentWrapper } from "../WebServiceApi/LiveContentWrapper.js";
import { WebServiceClientFactory } from "../WebServiceApi/WebServiceClientFactory.js";

/**
 * BaseConnector class provides generic entry point for back-end connectors.
 */
export abstract class BaseConnector extends BaseObject {
    protected httpManager : HttpManager;
    protected options : IConnectorOptions;
    private serverClassName : string;
    private client : IWebServiceClient;
    private propagateError : boolean;
    private token : string;

    /**
     * @param {IConnectorOptions} [$options] Specify BaseConnector options.
     */
    constructor($options? : IConnectorOptions);
    /**
     * @param {boolean|number} [$reconnect = false] Specify default behaviour on client timeout.
     * If number is passed it will be used as max reconnect limit.
     * @param {string|WebServiceConfiguration} [$serverConfigurationSource] Specify source for configuration of
     * server-client communication.
     * @param {WebServiceClientType} [$clientType] Specify client type.
     */
    constructor($reconnect : boolean | number, $serverConfigurationSource? : string | WebServiceConfiguration,
                $clientType? : WebServiceClientType);
    constructor($optionsOrReconnect? : any, $serverConfigurationSource? : string | WebServiceConfiguration,
                $clientType? : WebServiceClientType) {
        super();
        if (ObjectValidator.IsEmptyOrNull($optionsOrReconnect) || ObjectValidator.IsObject($optionsOrReconnect)) {
            this.options = this.resolveDefaultOptions($optionsOrReconnect);
            if (this.options.reconnect) {
                this.options.maxReconnectCount = -1;
            }
        } else {
            this.options = this.resolveDefaultOptions({
                clientType               : $clientType,
                serverConfigurationSource: $serverConfigurationSource
            });

            if (ObjectValidator.IsBoolean($optionsOrReconnect)) {
                this.options.maxReconnectCount = $optionsOrReconnect ? -1 : 0;
            } else {
                this.options.maxReconnectCount = <number>$optionsOrReconnect;
            }

            if (this.options.maxReconnectCount !== 0) {
                this.options.reconnect = true;
            }
        }

        this.httpManager = Loader.getInstance().getHttpManager();
        this.propagateError = false;

        if (!this.options.suppressInit) {
            this.init();
        }
    }

    /**
     * @returns {string} Returns token used for communication protocol override.
     */
    public get Token() {
        return this.token;
    }

    /**
     * @param {string} $value Set token used for communication protocol override.
     */
    public set Token($value : string) {
        this.token = $value;
    }

    /**
     * @returns {number} Returns connector id suitable for events handling and connectors factory.
     */
    public getId() : number {
        return this.client.getId();
    }

    /**
     * @returns {IWebServiceClientEvents} Returns events interface connected with connector instance.
     */
    public getEvents() : IWebServiceClientEvents {
        return this.client.getEvents();
    }

    /**
     * @param {string} $name Specify name of remote agent which should be looked up by Oidis Hub.
     * @param {string|WebServiceConfiguration} [$serverConfigurationSource] Specify source for configuration of
     * server-client communication.
     * @returns {void}
     */
    public setAgent($name : string, $serverConfigurationSource? : string | WebServiceConfiguration) : void {
        EventsManager.getInstanceSingleton().Clear("" + this.getId());
        this.client = null;
        if (!ObjectValidator.IsEmptyOrNull($serverConfigurationSource)) {
            if (ObjectValidator.IsString($serverConfigurationSource)) {
                if (!StringUtils.EndsWith(<string>$serverConfigurationSource, "/connector.config.jsonp")) {
                    $serverConfigurationSource += "/connector.config.jsonp";
                }
                this.options.serverConfigurationSource = $serverConfigurationSource;
            } else {
                this.options.serverConfigurationSource = $serverConfigurationSource;
            }
        }
        this.options.clientType = WebServiceClientType.FORWARD_CLIENT;
        this.init();

        this.setAgentName($name);
    }

    public setAgentName($name : string) : void {
        if (!ObjectValidator.IsEmptyOrNull(this.getClient()) && (this.options.clientType === WebServiceClientType.FORWARD_CLIENT)) {
            (<ForwardingClient>this.getClient()).AgentName($name);
        }
    }

    /**
     * Overrides method used for forwarding client processing on Hub.
     * Default value is "Io.Oidis.Hub.Utils.ConnectorHub.ForwardRequest".
     * @param $method Specify full namespace for method in class inherited from Io.Oidis.Hub.Primitives.AgentsRegister.
     * @returns {void}
     */
    public setForwardingMethod($method : string) : void {
        const client : ForwardingClient = <ForwardingClient>this.getClient();
        if (client.hasOwnProperty("forwardingMethod")) {  // has ForwardingClient.forwardingMethod - field
            client.ForwardingMethod($method);
        }
    }

    /**
     * @param {boolean} $value Specify if connector should treat error as global exception or not.
     * @returns {boolean} Returns true if error should trows exception otherwise false.
     */
    public ErrorPropagation($value? : boolean) : boolean {
        return this.propagateError = Property.Boolean(this.propagateError, $value);
    }

    protected getClientType() : WebServiceClientType {
        let type : WebServiceClientType;
        const request : HttpRequestParser = this.httpManager.getRequest();
        if (!request.IsIdeaHost()) {
            if (request.IsLocalhostHosting()) {
                type = WebServiceClientType.HOST_CLIENT;
            } else if (request.IsJre() || request.IsPlugin() || request.IsConnector() || request.IsConnector(true)) {
                type = WebServiceClientType.DESKTOP_CONNECTOR;
            } else if (request.IsOnServer() || request.IsOnServer(true)) {
                type = WebServiceClientType.HUB_CONNECTOR;
            }
        }
        if (ObjectValidator.IsEmptyOrNull(type)) {
            type = WebServiceClientType.BUILDER_CONNECTOR;
        }
        return type;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.DESKTOP_CONNECTOR] = "";
        namespaces[WebServiceClientType.HUB_CONNECTOR] = "";
        namespaces[WebServiceClientType.BUILDER_CONNECTOR] = "";
        return namespaces;
    }

    protected init() : void {
        const $serverConfigurationSource = this.options.serverConfigurationSource;
        const $clientType = this.options.clientType;
        const $maxReconnectCount = this.options.maxReconnectCount;

        const type : WebServiceClientType = ObjectValidator.IsEmptyOrNull($clientType) ? this.getClientType() : $clientType;
        const namespaces : any = this.getServerNamespaces();
        if (namespaces.hasOwnProperty(<string>type)) {
            this.serverClassName = namespaces[<string>type];
            if (!ObjectValidator.IsEmptyOrNull(this.serverClassName)) {
                this.client = this.getWebServiceClientFactory().getClient(type, $serverConfigurationSource);
            }
        }
        if (!ObjectValidator.IsEmptyOrNull(this.client)) {
            if ($maxReconnectCount === -1) {
                const reconnectHandler : any = () : void => {
                    this.client.StartCommunication();
                };
                this.client.getEvents().OnClose(reconnectHandler);
                this.client.getEvents().OnTimeout(reconnectHandler);
            } else if ($maxReconnectCount >= 0 && this.client.IsMemberOf(WebSocketsClient)) {
                (<WebSocketsClient>this.client).MaxReconnectsCount($maxReconnectCount);
            }
            const onErrorHandler : any = ($eventArgs : ErrorEventArgs) : void => {
                if (!this.propagateError && Loader.getInstance().getEnvironmentArgs().IsProductionMode()) {
                    ExceptionsManager.Throw(this.getClassName(), $eventArgs.Exception());
                } else {
                    LogIt.Warning($eventArgs.Exception().ToString("", false));
                }
            };
            if ((type === WebServiceClientType.REST_CONNECTOR || type === WebServiceClientType.HUB_CONNECTOR) &&
                $maxReconnectCount === 0) {
                const timeoutHandler : any = () : void => {
                    const data : ArrayList<any> = new ArrayList<any>();
                    data.Add(this.getClassName(), "Owner");
                    data.Add(this.client, "Client");
                    this.httpManager.ReloadTo("/ServerError/Http/ConnectionLost", data, true);
                };
                this.client.getEvents().OnTimeout(timeoutHandler);
                this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                    this.httpManager.IsOnline(($status : boolean) : void => {
                        if ($status) {
                            onErrorHandler($eventArgs);
                        } else {
                            timeoutHandler();
                        }
                    });
                });
            } else {
                this.client.getEvents().OnError(onErrorHandler);
            }
        } else {
            throw new Error(type + " is not defined as WebServiceClient in getServerNamespaces for instance of " + this.getClassName());
        }
    }

    protected getClient() : IWebServiceClient {
        return this.client;
    }

    protected getServerNamespace() : string {
        return this.serverClassName;
    }

    protected invoke($method : string, ...$args : any[]) : ILiveContentFormatterPromise {
        return LiveContentWrapper.InvokeMethod.apply(LiveContentWrapper,  // eslint-disable-line prefer-spread
            (<any[]>[{client: this.client, className: this.serverClassName, name: $method, args: $args, token: this.Token}]));
    }

    protected asyncInvoke($method : any, ...$args : any[]) : Promise<any> {
        return new Promise<any>(($resolve, $reject) : void => {
            try {
                let promise : any;
                if (ObjectValidator.IsString($method)) {
                    promise = this.invoke.apply(this, [$method].concat($args));  // eslint-disable-line prefer-spread
                } else {
                    promise = $method.apply(this, $args);
                }
                if (!ObjectValidator.IsEmptyOrNull(promise.OnError)) {
                    promise.OnError(($error : ErrorEventArgs) : void => {
                        const error : Error = new Error($error.Message());
                        error.stack = $error.Exception().Stack();
                        $reject(error);
                    });
                }
                promise.Then((...$args : any[]) : void => {
                    if ($args.length === 1) {
                        $args = $args[0];
                    }
                    $resolve($args);
                });
            } catch (ex) {
                $reject(ex);
            }
        });
    }

    protected toSyncInvoke($asyncMethod : Promise<any>) : ILiveContentFormatterPromise {
        const callbacks : any = {
            dataFormatter: ($data : any) : any => {
                return $data;
            },
            onError      : null,
            then         : null
        };
        const onErrorHandler : any = ($error : Error) : void => {
            if (!ObjectValidator.IsEmptyOrNull(callbacks.onError)) {
                callbacks.onError($error);
            } else {
                throw $error;
            }
        };
        $asyncMethod
            .then(($output : any) : void => {
                try {
                    $output = callbacks.dataFormatter($output);
                    if (!ObjectValidator.IsEmptyOrNull(callbacks.then)) {
                        callbacks.then($output);
                    }
                } catch (ex) {
                    onErrorHandler(ex);
                }
            })
            .catch(($error : Error) : void => {
                onErrorHandler($error);
            });
        return {
            DataFormatter($formatter : ($data : any) => any) : ILiveContentErrorPromise {
                callbacks.dataFormatter = $formatter;
                return callbacks;
            },
            OnError($callback : ($error : ErrorEventArgs, ...$args : any[]) => void) : ILiveContentPromise {
                callbacks.onError = $callback;
                return callbacks;
            },
            Then($callback : (...$args : any[]) => void) : void {
                callbacks.then = $callback;
            }
        };
    }

    protected errorHandler($callback : () => void, $error : ErrorEventArgs, $args : any[]) : void {
        if (ObjectValidator.IsEmptyOrNull($callback)) {
            EventsManager.getInstanceSingleton().FireEvent("" + this.getId(), WebServiceClientEventType.ON_ERROR, $error);
        } else {
            $callback.apply(this, [$error].concat($args));
        }
    }

    protected getWebServiceClientFactory() : any {
        return WebServiceClientFactory;
    }

    protected resolveDefaultOptions($options : IConnectorOptions) : IConnectorOptions {
        const defaultOptions : IConnectorOptions = {
            maxReconnectCount: 0,
            reconnect        : false,
            suppressInit     : false
        };
        return Resources.Extend(defaultOptions, $options);
    }

    protected progressWrapper<TData>($method : string, $args : any[],
                                     $onChange : ($data : IFetchProgress) => void) : Promise<TData> {
        return new Promise<TData>(($resolve, $reject) : void => {
            try {
                const callbacks : any = {
                    onChange($data : IFetchProgress) : any {
                        // declare default callback
                    },
                    onError: null,
                    then($status : TData) : any {
                        // declare default callback
                    }
                };
                LiveContentWrapper.InvokeMethod.apply(this, [this.getClient(), this.getServerNamespace(), $method].concat($args))
                    .OnError(($error : ErrorEventArgs, ...$args : any[]) : void => {
                        this.errorHandler(callbacks.onError, $error, $args);
                    })
                    .Then(($result : any) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($result)) {
                            if (ObjectValidator.IsObject($result) && ObjectValidator.IsSet($result.type)) {
                                if ($result.type === EventType.ON_CHANGE || $result.type === EventType.ON_START) {
                                    callbacks.onChange($result.data);
                                } else {
                                    callbacks.then($result);
                                }
                            } else {
                                callbacks.then($result);
                            }
                        }
                    });
                const promise : IFetchProgressPromise = {
                    OnChange: ($callback : ($data : IFetchProgress) => void) : IFetchProgressPromise => {
                        callbacks.onChange = $callback;
                        return promise;
                    },
                    OnError : ($callback : ($args : ErrorEventArgs) => void) : IFetchProgressPromise => {
                        callbacks.onError = $callback;
                        return promise;
                    },
                    Then    : ($callback : ($status : TData) => void) : void => {
                        callbacks.then = $callback;
                    }
                };
                if (!ObjectValidator.IsEmptyOrNull($onChange)) {
                    promise.OnChange($onChange);
                }
                if (!ObjectValidator.IsEmptyOrNull(promise.OnError)) {
                    promise.OnError(($error : ErrorEventArgs) : void => {
                        $reject(new Error($error.Message()));
                    });
                }
                promise.Then($resolve);
            } catch (ex) {
                $reject(ex);
            }
        });
    }
}

export interface IConnectorOptions {
    clientType? : WebServiceClientType;
    maxReconnectCount? : number;
    serverConfigurationSource? : string | WebServiceConfiguration;
    suppressInit? : boolean;
    reconnect? : boolean;
}

export interface IFetchProgress {
    index? : number;
    from? : number;
    message? : string;
    warning? : string;
}

export interface IFetchProgressPromise extends ILiveContentErrorPromise {
    OnChange($callback : ($data : IFetchProgress) => void) : IFetchProgressPromise;

    OnError($callback : ($error : ErrorEventArgs) => void) : IFetchProgressPromise;

    Then($callback : ($status : any) => void) : void;
}

// generated-code-start
/* eslint-disable */
export const IConnectorOptions = globalThis.RegisterInterface(["clientType", "maxReconnectCount", "serverConfigurationSource", "suppressInit", "reconnect"]);
export const IFetchProgress = globalThis.RegisterInterface(["index", "from", "message", "warning"]);
export const IFetchProgressPromise = globalThis.RegisterInterface(["OnChange", "OnError", "Then"], <any>ILiveContentErrorPromise);
/* eslint-enable */
// generated-code-end
