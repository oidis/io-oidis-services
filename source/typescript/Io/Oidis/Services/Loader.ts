/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Loader as Parent } from "@io-oidis-gui/Io/Oidis/Gui/Loader.js";
import "./ForceCompile.js";
import { HttpManager } from "./HttpProcessor/HttpManager.js";
import { HttpResolver } from "./HttpProcessor/HttpResolver.js";

/**
 * Loader class provides handling of application content singleton.
 */
export class Loader extends Parent {

    public static getInstance() : Loader {
        return <Loader>super.getInstance();
    }

    public getHttpResolver() : HttpResolver {
        return <HttpResolver>super.getHttpResolver();
    }

    public getHttpManager() : HttpManager {
        return <HttpManager>super.getHttpManager();
    }

    protected initResolver() : HttpResolver {
        return new HttpResolver(this.getEnvironmentArgs().getProjectName());
    }
}
