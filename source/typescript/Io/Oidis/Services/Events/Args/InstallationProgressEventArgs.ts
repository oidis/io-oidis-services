/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ProgressEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ProgressEventArgs.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { InstallationProgressCode } from "../../Enums/InstallationProgressCode.js";

/**
 * InstallationProgressEventArgs class provides args connected with installation progress.
 */
export class InstallationProgressEventArgs extends ProgressEventArgs {
    private message : string;
    private status : boolean;
    private code : InstallationProgressCode;

    /**
     * @param {string} [$message] Specify message, which describes current progress.
     */
    constructor($message? : string) {
        super();

        this.status = false;
        this.code = InstallationProgressCode.GENERAL;
        this.Message($message);
    }

    /**
     * @param {string} [$value] Set message, which describes current progress.
     * @returns {string} Returns message value, which describes current progress.
     */
    public Message($value? : string) : string {
        return this.message = Property.String(this.message, $value);
    }

    /**
     * @param {boolean} [$value] Set actual status for current progress.
     * @returns {boolean} Returns status for current progress.
     */
    public Status($value? : boolean) : boolean {
        return this.status = Property.Boolean(this.status, $value);
    }

    /**
     * @param {InstallationProgressCode} [$value] Set progress code.
     * @returns {InstallationProgressCode} Returns progress code value.
     */
    public ProgressCode($value? : InstallationProgressCode) : InstallationProgressCode {
        if (ObjectValidator.IsSet($value)) {
            this.code = $value;
        }
        return this.code;
    }
}
