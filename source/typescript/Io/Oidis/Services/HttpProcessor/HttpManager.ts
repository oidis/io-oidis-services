/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { HttpManager as Parent } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/HttpManager.js";

export class HttpManager extends Parent {
    private connector : any;

    public ReloadTo($link? : string, $data? : any, $async? : boolean) : void {
        if (this.getRequest().IsJre() && !ObjectValidator.IsEmptyOrNull($link) && ObjectValidator.IsBoolean($data) && $data) {
            if (!ObjectValidator.IsSet(this.connector)) {
                import("../Connectors/TerminalConnector.js").then(($exports : any) : void => {
                    this.connector = new $exports.TerminalConnector();
                    this.connector.Open($link);
                });
            } else {
                this.connector.Open($link);
            }
        } else {
            super.ReloadTo($link, $data, $async);
        }
    }
}
