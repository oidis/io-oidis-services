/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Bootstrap/Primitives/BasePanel.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { AsyncBaseHttpController } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/AsyncBaseHttpController.js";
import { BaseViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewerArgs.js";
import { EnvironmentConnector } from "../../Connectors/EnvironmentConnector.js";
import { BasePageDAO } from "../../DAO/BasePageDAO.js";
import { IBasePageLocalization } from "../../Interfaces/DAO/IBasePageLocalization.js";

export class AsyncBasePageController extends AsyncBaseHttpController {
    private dao : BasePageDAO;

    public PreparePageContent() : void {
        super.PreparePageContent();
        if (!ObjectValidator.IsEmptyOrNull(this.dao)) {
            try {
                this.setPageTitle(StringUtils.Format(this.dao.getPageTitle(), this.getEnvironmentArgs().getProjectVersion()));
            } catch (ex) {
                LogIt.Error(ex);
            }
        }
    }

    public getPageConfiguration() : IBasePageLocalization {
        if (!ObjectValidator.IsEmptyOrNull(this.dao)) {
            return this.dao.getPageConfiguration();
        }
        return null;
    }

    public getModelArgs() : BaseViewerArgs {
        if (!ObjectValidator.IsEmptyOrNull(this.dao)) {
            return this.dao.getModelArgs();
        }
        return null;
    }

    protected isRendered($target : any) : boolean {
        return super.isRendered($target) ||
            !ObjectValidator.IsEmptyOrNull(this.dao) && ObjectValidator.IsEmptyOrNull(this.dao.getStaticConfiguration());
    }

    protected async canResolve($resolve : ($status : boolean) => void) : Promise<void> {
        if (await this.environmentCheckEnabled() &&
            !await EnvironmentConnector.getInstanceSingleton().IsEnvironmentLoaded()) {
            $resolve(false);
            this.reloadToInitPage();
        } else if (!await this.accessValidator()) {
            $resolve(false);
            this.reloadToNotAuthorized();
        } else {
            $resolve(true);
        }
    }

    protected getProcessArgs() : any[] {
        return [this.InstanceOwner(), this.getModelArgs(), this.getDao()];
    }

    protected setDao($value : BasePageDAO) : void {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.dao = $value;
        }
    }

    protected getDao() : BasePageDAO {
        return this.dao;
    }

    protected async environmentCheckEnabled() : Promise<boolean> {
        return false;
    }

    protected loadResources() : void {
        if (!ObjectValidator.IsEmptyOrNull(this.dao)) {
            this.dao.Load(this.getLanguage(), () : void => {
                this.PreparePageContent();
                this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_LOAD);
            }, true);
        } else {
            super.loadResources();
        }
    }

    protected async beforeLoad($instance? : BasePanel, $args? : BaseViewerArgs, $dao? : BasePageDAO) : Promise<void> {
        // override this method for ability to prepare instance before it is loaded
    }

    protected async afterLoad($instance? : BasePanel, $args? : BaseViewerArgs, $dao? : BasePageDAO) : Promise<void> {
        // override this method for ability to setup instance after it is loaded
    }

    protected async onSuccess($instance? : BasePanel, $args? : BaseViewerArgs, $dao? : BasePageDAO) : Promise<void> {
        // override this method for ability to handle controller state after it has been successfully loaded
    }

    protected reloadToNotAuthorized() : void {
        const postData : ArrayList<any> = new ArrayList<any>();
        postData.Add(this.getRequest(), "Referer");
        this.getHttpManager().ReloadTo("/ServerError/Http/NotAuthorized", postData, true);
    }

    protected reloadToInitPage() : void {
        const postData : ArrayList<any> = new ArrayList<any>();
        postData.Add(this.getRequest(), "Referer");
        this.getHttpManager().ReloadTo("/ServerError/NotReady", postData, true);
    }
}
