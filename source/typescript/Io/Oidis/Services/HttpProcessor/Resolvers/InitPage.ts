/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { BaseHttpResolver } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/BaseHttpResolver.js";
import { Resolver } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { StaticPageContentManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/StaticPageContentManager.js";
import { EnvironmentConnector } from "../../Connectors/EnvironmentConnector.js";

@Resolver("/ServerError/NotReady")
export class InitPage extends BaseHttpResolver {

    protected resolver() : void {
        EnvironmentConnector.getInstanceSingleton().IsEnvironmentLoaded()
            .then(($status : boolean) : void => {
                if (!$status) {
                    StaticPageContentManager.Clear();
                    StaticPageContentManager.Title("Loading");
                    StaticPageContentManager.BodyAppend("Application is not ready please wait");
                    StaticPageContentManager.Draw();

                    EnvironmentConnector.getInstanceSingleton().AddLoadingListener()
                        .OnError(($error : ErrorEventArgs) : void => {
                            LogIt.Error($error.Message());
                            StaticPageContentManager.BodyAppend("Failed to init environment");
                        })
                        .Then(() : void => {
                            window.location.href = "/";
                        });
                } else {
                    window.location.href = "/";
                }
            })
            .catch(($error : Error) : void => {
                ExceptionsManager.HandleException($error);
            });
    }
}
