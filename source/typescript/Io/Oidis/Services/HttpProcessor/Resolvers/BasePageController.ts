/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BrowserType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/BrowserType.js";
import { GeneralEventOwner } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/GeneralEventOwner.js";
import { WebServiceClientEventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/WebServiceClientEventType.js";
import { LanguageType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LanguageType.js";
import { PersistenceType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/PersistenceType.js";
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { BaseHttpResolver } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/BaseHttpResolver.js";
import { IEventsHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IEventsHandler.js";
import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { PersistenceFactory } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralCssNames } from "@io-oidis-gui/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { HttpRequestConstants } from "@io-oidis-gui/Io/Oidis/Gui/Enums/HttpRequestConstants.js";
import { ResizeableType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ResizeableType.js";
import { WindowCornerType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/WindowCornerType.js";
import { MoveEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MoveEventArgs.js";
import { ResizeBarEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeBarEventArgs.js";
import { ResizeEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeEventArgs.js";
import { ViewerManagerEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ViewerManagerEventArgs.js";
import { ElementEventsManager } from "@io-oidis-gui/Io/Oidis/Gui/Events/ElementEventsManager.js";
import { IDragBar } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IDragBar.js";
import { IResizeBar } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IResizeBar.js";
import { IBaseGuiObject } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IBaseGuiObject.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { IIcon } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IIcon.js";
import { IImageButton } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IImageButton.js";
import { ILabel } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/ILabel.js";
import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { BaseViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { BaseViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BaseViewerArgs.js";
import { GuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/GuiElement.js";
import { ElementOffset } from "@io-oidis-gui/Io/Oidis/Gui/Structures/ElementOffset.js";
import { Size } from "@io-oidis-gui/Io/Oidis/Gui/Structures/Size.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { StaticPageContentManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/StaticPageContentManager.js";
import { WindowManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/WindowManager.js";
import { ViewerCacheManager } from "@io-oidis-gui/Io/Oidis/Gui/ViewerCacheManager.js";
import { ViewerManager } from "@io-oidis-gui/Io/Oidis/Gui/ViewerManager.js";
import { FileSystemHandlerConnector } from "../../Connectors/FileSystemHandlerConnector.js";
import { WindowHandlerConnector } from "../../Connectors/WindowHandlerConnector.js";
import { BasePageDAO } from "../../DAO/BasePageDAO.js";
import { WindowStateType } from "../../Enums/WindowStateType.js";
import { IBasePageLocalization } from "../../Interfaces/DAO/IBasePageLocalization.js";
import { IWindowChangedEvent } from "../../Interfaces/Events/IWindowChangedEvent.js";

/**
 * BasePageController class provides handling of requests for page controller.
 */
export abstract class BasePageController extends BaseHttpResolver {
    protected loaderIcon : IIcon;
    protected loaderText : ILabel;
    protected loaderProgressLabel : ILabel;
    protected minimizeButton : IImageButton;
    protected maximizeButton : IImageButton;
    protected closeButton : IImageButton;
    protected appIcon : IIcon;
    protected appTitle : ILabel;
    private readonly dragBar : IDragBar;
    private readonly topResizeBar : IResizeBar;
    private readonly leftResizeBar : IResizeBar;
    private readonly topLeftResizeBar : IResizeBar;
    private readonly topRightResizeBar : IResizeBar;
    private readonly rightResizeBar : IResizeBar;
    private readonly bottomResizeBar : IResizeBar;
    private readonly bottomLeftResizeBar : IResizeBar;
    private readonly bottomRightResizeBar : IResizeBar;
    private modelClassName : string;
    private modelObject : BaseViewer;
    private pageTitle : string;
    private faviconSource : string;
    private loaderProgressText : string;
    private dao : BasePageDAO;
    private language : LanguageType;
    private cacheFilePath : string;
    private viewerManager : ViewerManager;
    private resizeableType : ResizeableType;
    private minSize : Size;

    constructor() {
        super();

        this.pageTitle = "Oidis Services v" + this.getEnvironmentArgs().getProjectVersion();
        this.faviconSource = "resource/graphics/icon.ico";
        this.modelClassName = "";
        this.modelObject = null;

        const loaderIconClass : any = this.getLoaderIconClass();
        if (!ObjectValidator.IsEmptyOrNull(loaderIconClass)) {
            this.loaderIcon = new loaderIconClass(null, "AppContentLoader_Icon");
            this.loaderIcon.DisableAsynchronousDraw();
        }
        const loaderTextClass : any = this.getLoaderTextClass();
        if (!ObjectValidator.IsEmptyOrNull(loaderTextClass)) {
            this.loaderText = new loaderTextClass("Loading, please wait ...", "AppContentLoader_Text");
            this.loaderText.DisableAsynchronousDraw();
            this.loaderProgressText = "Progress: {0}%";
            this.loaderProgressLabel = new loaderTextClass("", "AppContentLoader_ProgressText");
            this.loaderProgressLabel.StyleClassName("AppContentLoaderProgress");
            this.loaderProgressLabel.DisableAsynchronousDraw();
        }

        const appIconClass : any = this.getAppIconClass();
        if (!ObjectValidator.IsEmptyOrNull(appIconClass)) {
            this.appIcon = new appIconClass(null);
            this.appIcon.DisableAsynchronousDraw();
        }
        const appTitleTextClass : any = this.getAppTitleTextClass();
        if (!ObjectValidator.IsEmptyOrNull(appTitleTextClass)) {
            this.appTitle = new appTitleTextClass("");
            this.appTitle.DisableAsynchronousDraw();
        }
        const appImageButtonClass : any = this.getAppImageButtonClass();
        if (!ObjectValidator.IsEmptyOrNull(appImageButtonClass)) {
            this.minimizeButton = new appImageButtonClass();
            this.minimizeButton.DisableAsynchronousDraw();
            this.maximizeButton = new appImageButtonClass();
            this.maximizeButton.DisableAsynchronousDraw();
            this.closeButton = new appImageButtonClass();
            this.closeButton.DisableAsynchronousDraw();
        }
        const appDragBarClass : any = this.getAppDragBarClass();
        if (!ObjectValidator.IsEmptyOrNull(appDragBarClass)) {
            this.dragBar = new appDragBarClass();
            this.dragBar.DisableAsynchronousDraw();
            this.dragBar.StyleClassName("AppDragBar");
        }
        const appResizeBarClass : any = this.getAppResizeBarClass();
        if (!ObjectValidator.IsEmptyOrNull(appResizeBarClass)) {
            this.topLeftResizeBar = new appResizeBarClass(ResizeableType.HORIZONTAL_AND_VERTICAL);
            this.topLeftResizeBar.DisableAsynchronousDraw();
            this.topResizeBar = new appResizeBarClass(ResizeableType.VERTICAL);
            this.topResizeBar.DisableAsynchronousDraw();
            this.topRightResizeBar = new appResizeBarClass(ResizeableType.HORIZONTAL_AND_VERTICAL);
            this.topRightResizeBar.DisableAsynchronousDraw();

            this.leftResizeBar = new appResizeBarClass(ResizeableType.HORIZONTAL);
            this.leftResizeBar.DisableAsynchronousDraw();
            this.rightResizeBar = new appResizeBarClass(ResizeableType.HORIZONTAL);
            this.rightResizeBar.DisableAsynchronousDraw();

            this.bottomLeftResizeBar = new appResizeBarClass(ResizeableType.HORIZONTAL_AND_VERTICAL);
            this.bottomLeftResizeBar.DisableAsynchronousDraw();
            this.bottomResizeBar = new appResizeBarClass(ResizeableType.VERTICAL);
            this.bottomResizeBar.DisableAsynchronousDraw();
            this.bottomRightResizeBar = new appResizeBarClass(ResizeableType.HORIZONTAL_AND_VERTICAL);
            this.bottomRightResizeBar.DisableAsynchronousDraw();
        }

        this.language = LanguageType.EN;
        this.cacheFilePath = "";
        this.viewerManager = null;
        this.setResizeableType(ResizeableType.NONE);
        this.setDao(new BasePageDAO());
        this.minSize = new Size();
        this.setWindowMinimalSize(200, 200);
    }

    /**
     * @returns {ViewerManager} Returns instance of viewer manager, which is handling current page viewer instance.
     */
    public InstanceOwner() : ViewerManager {
        return this.viewerManager;
    }

    /**
     * Prepare current page content based on controller needs.
     * @returns {void}
     */
    public PreparePageContent() : void {
        this.setPageTitle(StringUtils.Format(this.dao.getPageTitle(), this.getEnvironmentArgs().getProjectVersion()));
        this.setLoaderText(this.dao.getPageLoadingText());
        this.setLoaderProgressText(this.dao.getPageLoadingProgressText());
        StaticPageContentManager.Language(this.language);
    }

    /**
     * @returns {BaseViewer} Returns controller's model, if controller has been successfully executed, otherwise null.
     */
    public getModel() : BaseViewer {
        return this.modelObject;
    }

    /**
     * @returns {IBasePageLocalization} Returns page configuration arguments loaded from configuration file.
     */
    public getPageConfiguration() : IBasePageLocalization {
        return this.dao.getPageConfiguration();
    }

    /**
     * @param {Language} [$language] Specify language variant of the arguments.
     * @param {Function} [$asyncHandler] Specify asynchronous handler,
     * if arguments should be loaded asynchronously or from the file.
     * @returns {BaseViewerArgs} Returns arguments, which has been set to the controller's model.
     */
    public getModelArgs($language? : LanguageType, $asyncHandler? : ($args : BaseViewerArgs) => void) : BaseViewerArgs {
        if (!ObjectValidator.IsSet($language)) {
            const persistence : IPersistenceHandler = PersistenceFactory.getPersistence(PersistenceType.BROWSER);
            if (persistence.Exists(LanguageType.ClassNameWithoutNamespace())) {
                this.language = <LanguageType>persistence.Variable(LanguageType.ClassNameWithoutNamespace());
            } else {
                this.language = StaticPageContentManager.Language();
            }
        } else if (this.language !== $language && LanguageType.Contains($language)) {
            this.language = $language;
        }
        this.dao.Load(this.language, () : void => {
            this.PreparePageContent();
            if (ObjectValidator.IsSet($asyncHandler)) {
                $asyncHandler(this.dao.getModelArgs());
            }
        });

        return this.dao.getModelArgs();
    }

    public Process() : void {
        if (!this.browserValidator()) {
            LogIt.Error("Unsupported browser. User Agent: " + this.getRequest().getUserAgent());
            this.getHttpManager().ReloadTo("/ServerError/Browser", null, true);
            this.stop();
        } else {
            Echo.Init();
            this.resolver();
        }
    }

    protected setModelClassName($value : string | IClassName) : void {
        if (!ObjectValidator.IsString($value) && ObjectValidator.IsSet((<any>$value).ClassName)) {
            $value = (<any>$value).ClassName();
        }
        this.modelClassName = Property.String(this.modelClassName, <string>$value);
    }

    protected setDao($value : BasePageDAO) : void {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.dao = $value;
        }
    }

    protected getDao() : BasePageDAO {
        return this.dao;
    }

    /**
     * @returns {IIcon} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.UserControls.IIcon
     */
    protected getLoaderIconClass() : any {
        return null;
    }

    /**
     * @returns {ILabel} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.UserControls.ILabel
     */
    protected getLoaderTextClass() : any {
        return null;
    }

    /**
     * @returns {ImageButton} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.UserControls.ImageButton
     */
    protected getAppImageButtonClass() : any {
        return null;
    }

    /**
     * @returns {IIcon} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.UserControls.IIcon
     */
    protected getAppIconClass() : any {
        return null;
    }

    /**
     * @returns {ILabel} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.UserControls.ILabel
     */
    protected getAppTitleTextClass() : any {
        return null;
    }

    /**
     * @returns {IResizeBar} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.Components.IResizeBar
     */
    protected getAppResizeBarClass() : any {
        return null;
    }

    /**
     * @returns {IDragBar} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.Components.IDragBar
     */
    protected getAppDragBarClass() : any {
        return null;
    }

    protected setPageTitle($value : string) : void {
        this.pageTitle = Property.String(this.pageTitle, $value);
        if (!this.getRequest().IsJre()) {
            StaticPageContentManager.Title(this.pageTitle);
        } else if (!ObjectValidator.IsEmptyOrNull(this.appTitle)) {
            this.appTitle.Text(this.pageTitle);
        }
    }

    protected setPageIconSource($value : string) : void {
        this.faviconSource = Property.String(this.faviconSource, $value);
        StaticPageContentManager.FaviconSource(this.faviconSource);
    }

    protected setCacheFilePath($value : string) : void {
        this.cacheFilePath = Property.String(this.cacheFilePath, $value);
    }

    protected loadResources() : void {
        const persistence : IPersistenceHandler = PersistenceFactory.getPersistence(PersistenceType.BROWSER);
        if (persistence.Exists(LanguageType.ClassNameWithoutNamespace())) {
            this.language = <LanguageType>persistence.Variable(LanguageType.ClassNameWithoutNamespace());
        }
        if (!ObjectValidator.IsEmptyOrNull(this.dao)) {
            this.dao.Load(this.language, () : void => {
                this.PreparePageContent();
                this.setCacheFilePath(this.dao.getCacheFilePath());
                this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_LOAD, false);
            });
        } else {
            this.PreparePageContent();
            this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_LOAD, false);
        }
    }

    protected resolver() : void {
        const isJreSimulator : boolean = this.getRequest().IsJre(true);

        const loaderResize : IEventsHandler = () : void => {
            if (ElementManager.IsVisible("AppContentLoader")) {
                const pageSize : Size = isJreSimulator ?
                    new Size("Browser", true) : WindowManager.getSize();
                const loaderSize : Size = new Size();
                loaderSize.Width(
                    ElementManager.getElement("AppContentLoader_Icon_Icon").offsetWidth +
                    ElementManager.getElement("AppContentLoader_Text").offsetWidth);
                loaderSize.Height(ElementManager.getElement("AppContentLoader_Text_Text").offsetHeight);
                ElementManager.setWidth("AppContentLoader", loaderSize.Width());
                ElementManager.setWidth("AppContentLoader_Content", loaderSize.Width());
                ElementManager.setCssProperty("AppContentLoader", "top", (pageSize.Height() - loaderSize.Height()) / 2);
                ElementManager.setCssProperty("AppContentLoader", "left", (pageSize.Width() - loaderSize.Width()) / 2);
                ElementManager.setOpacity("AppContentLoader", 100);
            }
        };
        let progressIndex : number = 0;
        if (!ElementManager.Exists("AppContentLoader", true)) {
            if (!ObjectValidator.IsEmptyOrNull(this.loaderProgressLabel)) {
                const loadingProgressHandler : IEventsHandler = () : void => {
                    if (progressIndex < 100) {
                        progressIndex++;
                        this.loaderProgressLabel.Text(StringUtils.Format(this.loaderProgressText, progressIndex));
                    }
                };
                this.getEventsManager().setEvent(ViewerManager.ClassName(), EventType.ON_CHANGE, loadingProgressHandler);
                this.getEventsManager().setEvent(BaseViewer.ClassName(), EventType.ON_CHANGE, loadingProgressHandler);
            }
            if (!ObjectValidator.IsEmptyOrNull(this.loaderText)) {
                this.loaderText.getEvents().setOnChange(loaderResize);
            }
            if (!ObjectValidator.IsEmptyOrNull(this.loaderIcon) && !ObjectValidator.IsEmptyOrNull(this.loaderText)) {
                WindowManager.getEvents().setOnResize(loaderResize);
            }
            StaticPageContentManager.Clear();
            StaticPageContentManager.FaviconSource(this.faviconSource);

            const EOL : string = StringUtils.NewLine(false);
            const cssInterfaceName : string = this.getCssInterfaceName();
            if (this.getRequest().IsJre() || isJreSimulator) {
                let windowHandler : WindowHandlerConnector;
                if (!isJreSimulator) {
                    windowHandler = new WindowHandlerConnector();
                }
                let blurFired : boolean = false;
                WindowManager.getEvents().setOnFocus(() : void => {
                    ElementManager.setClassName("Browser", BrowserType[BrowserType.JAVA_RE] + " FOCUS");
                });
                WindowManager.getEvents().setOnBlur(() : void => {
                    blurFired = true;
                    ElementManager.setClassName("Browser", BrowserType[BrowserType.JAVA_RE]);
                });

                const closeHandler : IEventsHandler = () : void => {
                    this.getHttpManager().ReloadTo("/PersistenceManager", null, true);
                    if (!isJreSimulator) {
                        windowHandler.Close();
                    }
                };

                WindowManager.getEvents().setOnLoad(() : void => {
                    if (!blurFired) {
                        ElementManager.setClassName("Browser", BrowserType[BrowserType.JAVA_RE] + " FOCUS");
                    }

                    let size : Size = WindowManager.getSize();
                    const borderOffset : number = ElementManager.getCssIntegerValue("Browser", "border-width", true) * 2 +
                        ElementManager.getElement("Browser").offsetTop +
                        ElementManager.getElement("Browser").offsetLeft;

                    const resizeResizeBars : ($size : Size) => void = ($size : Size) : void => {
                        const resizeBarClass : any = this.getAppResizeBarClass();
                        if (!ObjectValidator.IsEmptyOrNull(resizeBarClass)) {
                            let width : number = $size.Width() - borderOffset;
                            let height : number = $size.Height() - borderOffset;
                            if (width < this.minSize.Width()) {
                                width = this.minSize.Width();
                            }
                            if (height < this.minSize.Height()) {
                                height = this.minSize.Height();
                            }
                            resizeBarClass.setWidth(this.topResizeBar, width);
                            resizeBarClass.setWidth(this.bottomResizeBar, width);
                            ElementManager.setCssProperty(this.topRightResizeBar, "left", width);
                            ElementManager.setCssProperty(this.rightResizeBar, "left", width);
                            ElementManager.setCssProperty(this.bottomRightResizeBar, "left", width);
                            resizeBarClass.setHeight(this.leftResizeBar, height);
                            resizeBarClass.setHeight(this.rightResizeBar, height);
                            ElementManager.setCssProperty(this.bottomLeftResizeBar, "top", height);
                            ElementManager.setCssProperty(this.bottomResizeBar, "top", height);
                            ElementManager.setCssProperty(this.bottomRightResizeBar, "top", height);
                        }
                    };

                    resizeResizeBars(size);

                    let distanceChange : ResizeBarEventArgs;
                    const getNormalizedResizeOffset : any = ($resizeBarEventArgs : ResizeBarEventArgs) : ElementOffset => {
                        if (!ObjectValidator.IsEmptyOrNull(distanceChange) && !ObjectValidator.IsEmptyOrNull($resizeBarEventArgs)) {
                            const resize : ElementOffset = new ElementOffset(
                                distanceChange.getDistanceY(), distanceChange.getDistanceX());
                            const resizeBar : IResizeBar = $resizeBarEventArgs.Owner();
                            if (resizeBar === this.leftResizeBar || resizeBar === this.topLeftResizeBar ||
                                resizeBar === this.bottomLeftResizeBar) {
                                resize.Left(resize.Left() * -1);
                            }
                            if (resizeBar === this.topResizeBar || resizeBar === this.topLeftResizeBar ||
                                resizeBar === this.topRightResizeBar) {
                                resize.Top(resize.Top() * -1);
                            }
                            return resize;
                        }
                        return new ElementOffset();
                    };

                    let lastDistanceX : number = 0;
                    let lastDistanceY : number = 0;

                    const resizeHandler : any = ($eventArgs? : ResizeBarEventArgs) : void => {
                        distanceChange = $eventArgs;
                        const normalizedSize : ElementOffset = getNormalizedResizeOffset($eventArgs);
                        const resizeArgs : ResizeEventArgs = new ResizeEventArgs();
                        resizeArgs.Owner(this);
                        resizeArgs.Width(size.Width() + normalizedSize.Left());
                        resizeArgs.Height(size.Height() + normalizedSize.Top());
                        let width : number = resizeArgs.Width() - borderOffset;

                        const resizeEnvelop : any = () : void => {
                            let height : number = resizeArgs.Height() - borderOffset;
                            if (width < this.minSize.Width()) {
                                width = this.minSize.Width();
                            }
                            if (height < this.minSize.Height()) {
                                height = this.minSize.Height();
                            }
                            ElementManager.setCssProperty("Browser", "width", width);
                            ElementManager.setCssProperty("Browser", "height", height);

                            if (!ObjectValidator.IsEmptyOrNull(this.dragBar)) {
                                ElementManager.setCssProperty(this.dragBar, "width", width);
                                ElementManager.setCssProperty(this.dragBar, "left", (-1) * width);
                            }
                            this.getEventsManager().FireEvent("Browser", EventType.ON_RESIZE, resizeArgs);
                        };

                        if (!ObjectValidator.IsEmptyOrNull($eventArgs)) {
                            let dx : number = $eventArgs.getDistanceX() - lastDistanceX;
                            let dy : number = $eventArgs.getDistanceY() - lastDistanceY;
                            let cornerType : WindowCornerType;
                            lastDistanceX = $eventArgs.getDistanceX();
                            lastDistanceY = $eventArgs.getDistanceY();

                            if ($eventArgs.Owner() === this.bottomResizeBar ||
                                $eventArgs.Owner() === this.bottomRightResizeBar ||
                                $eventArgs.Owner() === this.rightResizeBar) {
                                cornerType = WindowCornerType.TOP_LEFT;
                            } else if ($eventArgs.Owner() === this.topResizeBar ||
                                $eventArgs.Owner() === this.topLeftResizeBar ||
                                $eventArgs.Owner() === this.leftResizeBar) {
                                cornerType = WindowCornerType.BOTTOM_RIGHT;
                            } else if ($eventArgs.Owner() === this.topRightResizeBar) {
                                cornerType = WindowCornerType.BOTTOM_LEFT;
                            } else if ($eventArgs.Owner() === this.bottomLeftResizeBar) {
                                cornerType = WindowCornerType.TOP_RIGHT;
                            }
                            if ($eventArgs.Owner() === this.topRightResizeBar ||
                                $eventArgs.Owner() === this.bottomLeftResizeBar) {
                                document.body.style.cursor = "ne-resize";
                            }
                            if (cornerType === WindowCornerType.TOP_LEFT ||
                                cornerType === WindowCornerType.BOTTOM_LEFT) {
                                if (dx < 0) {
                                    if (window.outerWidth + dx < this.minSize.Width()) {
                                        dx = 0;
                                    } else if (window.outerWidth <= this.minSize.Width()) {
                                        dx = 0;
                                    }
                                }
                            }
                            if (cornerType === WindowCornerType.BOTTOM_RIGHT ||
                                cornerType === WindowCornerType.TOP_RIGHT) {
                                if (dx > 0) {
                                    if (window.outerWidth - dx < this.minSize.Width()) {
                                        dx = 0;
                                    } else if (window.outerWidth <= this.minSize.Width()) {
                                        dx = 0;
                                    }
                                }
                            }
                            if (cornerType === WindowCornerType.TOP_LEFT ||
                                cornerType === WindowCornerType.TOP_RIGHT) {
                                if (dy < 0) {
                                    if (window.outerHeight + dy < this.minSize.Height()) {
                                        dy = 0;
                                    } else if (window.outerHeight <= this.minSize.Height()) {
                                        dy = 0;
                                    }
                                }
                            }
                            if (cornerType === WindowCornerType.BOTTOM_RIGHT ||
                                cornerType === WindowCornerType.BOTTOM_LEFT) {
                                if (dy > 0) {
                                    if (window.outerHeight - dy < this.minSize.Height()) {
                                        dy = 0;
                                    } else if (window.outerHeight <= this.minSize.Height()) {
                                        dy = 0;
                                    }
                                }
                            }
                            if (dx !== 0 || dy !== 0) {
                                if (!isJreSimulator) {
                                    windowHandler.Resize(cornerType, dx, dy).Then(resizeEnvelop);
                                } else {
                                    resizeEnvelop();
                                }
                            }
                        } else {
                            resizeEnvelop();
                        }
                    };

                    const resizeStartHandler : any = ($eventArgs? : ResizeBarEventArgs) : void => {
                        lastDistanceX = $eventArgs.getDistanceX();
                        lastDistanceY = $eventArgs.getDistanceY();
                    };

                    const resizeFinishHandler : any = ($eventArgs? : ResizeBarEventArgs) : void => {
                        const normalizedSize : ElementOffset = getNormalizedResizeOffset($eventArgs);
                        size.Width(size.Width() + normalizedSize.Left());
                        size.Height(size.Height() + normalizedSize.Top());
                        resizeResizeBars(size);
                        distanceChange = null;
                        const resizeArgs : ResizeEventArgs = new ResizeEventArgs();
                        resizeArgs.Owner(this);
                        resizeArgs.Width(size.Width());
                        resizeArgs.Height(size.Height());
                        this.getEventsManager().FireEvent("Browser", EventType.ON_RESIZE_COMPLETE, resizeArgs);
                    };

                    resizeHandler();
                    resizeFinishHandler();

                    const fitBrowserToWindow = () : void => {
                        size = WindowManager.getSize();
                        if (!isJreSimulator) {
                            size.Width(window.outerWidth);
                            size.Height(window.outerHeight);
                        }
                        resizeResizeBars(size);
                        resizeHandler();
                        resizeFinishHandler();
                    };

                    const initResizeType : ResizeableType = this.resizeableType;
                    const windowStateHandler : IEventsHandler = () : void => {
                        if (!isJreSimulator) {
                            windowHandler.getState().Then(($state : WindowStateType) : void => {
                                if ($state !== WindowStateType.MAXIMIZED) {
                                    windowHandler.Maximize().Then(() : void => {
                                        fitBrowserToWindow();
                                        this.setResizeableType(ResizeableType.NONE);
                                    });
                                } else {
                                    windowHandler.Restore().Then(() : void => {
                                        fitBrowserToWindow();
                                        this.setResizeableType(initResizeType);
                                    });
                                }
                            });
                        } else {
                            fitBrowserToWindow();
                        }
                    };

                    if (!isJreSimulator) {
                        if (ObjectValidator.IsEmptyOrNull(this.maximizeButton) ||
                            !ObjectValidator.IsEmptyOrNull(this.maximizeButton) && !this.maximizeButton.Visible() ||
                            this.resizeableType === ResizeableType.NONE) {
                            windowHandler.CanResize(false);
                        }

                        if (!ObjectValidator.IsEmptyOrNull(this.minimizeButton)) {
                            this.minimizeButton.getEvents().setOnClick(() : void => {
                                windowHandler.Minimize();
                            });
                        }
                        if (!ObjectValidator.IsEmptyOrNull(this.maximizeButton)) {
                            this.maximizeButton.getEvents().setOnClick(windowStateHandler);
                        }
                        if (!ObjectValidator.IsEmptyOrNull(this.closeButton)) {
                            this.closeButton.getEvents().setOnClick(closeHandler);
                        }
                        if (!ObjectValidator.IsEmptyOrNull(this.appIcon)) {
                            this.appIcon.getEvents().setOnDoubleClick(closeHandler);
                        }

                        if (!ObjectValidator.IsEmptyOrNull(this.dragBar)) {
                            const windowPosition : ElementOffset = new ElementOffset();
                            let skipMoveMessage : boolean = false;
                            let moveInitialized : boolean = false;
                            let maximizeOnComplete : boolean = false;
                            this.dragBar.getEvents().setOnDoubleClick(() : void => {
                                windowStateHandler();
                            });
                            const isWithinWindow : any = ($eventArgs : MoveEventArgs) : boolean => {
                                return $eventArgs.NativeEventArgs().screenX >= window.screenX &&
                                    $eventArgs.NativeEventArgs().screenX <= window.screenX + window.outerWidth &&
                                    $eventArgs.NativeEventArgs().screenY >= window.screenY &&
                                    $eventArgs.NativeEventArgs().screenY <= window.screenY + window.outerHeight;
                            };
                            this.dragBar.getEvents().setOnDragStart(() : void => {
                                if (!moveInitialized) {
                                    windowPosition.Top(window.screenY);
                                    windowPosition.Left(window.screenX);
                                    moveInitialized = true;
                                    skipMoveMessage = false;
                                }
                            });
                            this.dragBar.getEvents().setOnDragChange(($eventArgs : MoveEventArgs) : void => {
                                if (isWithinWindow($eventArgs)) {
                                    const left : number = $eventArgs.getDistanceX();
                                    const top : number = $eventArgs.getDistanceY();
                                    if (!skipMoveMessage && (left !== 0 || top !== 0)) {
                                        const move : any = () : void => {
                                            skipMoveMessage = true;
                                            windowHandler
                                                .MoveTo(windowPosition.Left() + left, windowPosition.Top() + top)
                                                .Then(() : void => {
                                                    moveInitialized = false;
                                                    skipMoveMessage = false;
                                                    fitBrowserToWindow();
                                                });
                                        };
                                        windowHandler.getState().Then(($state : WindowStateType) : void => {
                                            if ($state === WindowStateType.MAXIMIZED) {
                                                if (top > 5) {
                                                    skipMoveMessage = true;
                                                    windowPosition.Top(0);
                                                    windowHandler.Restore().Then(move);
                                                }
                                            } else {
                                                if ($eventArgs.NativeEventArgs().screenY <= 0) {
                                                    maximizeOnComplete = true;
                                                }
                                                move();
                                            }
                                        });
                                    }
                                }
                            });
                            this.dragBar.getEvents().setOnDragComplete(($eventArgs : MoveEventArgs) : void => {
                                if (maximizeOnComplete && $eventArgs.NativeEventArgs().screenY <= 0) {
                                    windowStateHandler();
                                }
                                moveInitialized = false;
                                skipMoveMessage = false;
                                maximizeOnComplete = false;
                            });
                        }

                        if (!ObjectValidator.IsEmptyOrNull(this.getAppResizeBarClass())) {
                            const originSize : Size = new Size();
                            const originPosition : ElementOffset = new ElementOffset();
                            const fireResizeEvent : any = ($width : number, $height : number) : void => {
                                const resizeArgs : ResizeEventArgs = new ResizeEventArgs();
                                resizeArgs.Owner(this);
                                resizeArgs.Width($width);
                                resizeArgs.Height($height);
                                ElementManager.setCssProperty("Browser", "height", resizeArgs.Height() - borderOffset);
                                this.getEventsManager().FireEvent("Browser", EventType.ON_RESIZE, resizeArgs);
                            };
                            const fitToHeight : any = () : void => {
                                if (window.screen.availHeight - window.outerHeight > 0) {
                                    originSize.Width(window.outerWidth);
                                    originSize.Height(window.outerHeight);
                                    originPosition.Left(window.screenX);
                                    originPosition.Top(window.screenY);
                                    windowHandler
                                        .MoveTo(window.screenX, 0)
                                        .Then(() : void => {
                                            windowHandler
                                                .Resize(WindowCornerType.TOP_LEFT, 0, window.screen.availHeight - window.outerHeight)
                                                .Then(() : void => {
                                                    fireResizeEvent(window.screen.availWidth, window.screen.availHeight);
                                                });
                                        });
                                } else {
                                    if (originSize.Width() > 0 && originSize.Height() > 0) {
                                        windowHandler
                                            .Resize(WindowCornerType.TOP_LEFT,
                                                originSize.Width() - window.outerWidth, originSize.Height() - window.outerHeight)
                                            .Then(() : void => {
                                                windowHandler
                                                    .MoveTo(originPosition.Left(), originPosition.Top())
                                                    .Then(() : void => {
                                                        fireResizeEvent(originSize.Width(), originSize.Height());
                                                    });
                                            });
                                    }
                                }
                            };
                            this.topResizeBar.getEvents().setOnDoubleClick(fitToHeight);
                            this.bottomResizeBar.getEvents().setOnDoubleClick(fitToHeight);
                        }

                        windowHandler.getEvents().OnWindowChanged(($eventArgs : IWindowChangedEvent) : void => {
                            if ($eventArgs.state !== WindowStateType.MINIMIZED) {
                                fitBrowserToWindow();
                                if ($eventArgs.state === WindowStateType.MAXIMIZED) {
                                    this.setResizeableType(ResizeableType.NONE);
                                } else {
                                    this.setResizeableType(initResizeType);
                                }
                            }
                        });
                    } else {
                        ElementManager.setCssProperty("Content", "margin", 0);
                        if (!ObjectValidator.IsEmptyOrNull(this.maximizeButton)) {
                            this.maximizeButton.getEvents().setOnClick(() : void => {
                                fitBrowserToWindow();
                            });
                        }

                        if (!ObjectValidator.IsEmptyOrNull(this.dragBar)) {
                            this.dragBar.getEvents().setOnDoubleClick(fitBrowserToWindow);
                            const windowOffset : ElementOffset = new ElementOffset();
                            this.dragBar.getEvents().setOnDragStart(() : void => {
                                windowOffset.Top(ElementManager.getElement("Browser").offsetTop);
                                windowOffset.Left(ElementManager.getElement("Browser").offsetLeft);
                            });
                            this.dragBar.getEvents().setOnDragChange(($eventArgs : MoveEventArgs) : void => {
                                ElementManager.setPosition("Browser", new ElementOffset(
                                    windowOffset.Top() + $eventArgs.getDistanceY(),
                                    windowOffset.Left() + $eventArgs.getDistanceX()));
                            });
                        }
                    }

                    if (!ObjectValidator.IsEmptyOrNull(this.appTitle)) {
                        this.appTitle.getEvents().setOnDoubleClick(windowStateHandler);
                    }

                    if (!ObjectValidator.IsEmptyOrNull(this.getAppResizeBarClass())) {
                        const barsList : any = [
                            this.topLeftResizeBar,
                            this.topResizeBar,
                            this.topRightResizeBar,
                            this.leftResizeBar,
                            this.rightResizeBar,
                            this.bottomLeftResizeBar,
                            this.bottomResizeBar,
                            this.bottomRightResizeBar
                        ];
                        barsList.forEach(($resizeBar : IResizeBar) : void => {
                            $resizeBar.getEvents().setOnResizeStart(resizeStartHandler);
                            $resizeBar.getEvents().setOnResizeChange(resizeHandler);
                            $resizeBar.getEvents().setOnResizeComplete(($eventArgs : ResizeBarEventArgs) : void => {
                                resizeFinishHandler($eventArgs);
                                this.getEventsManager().FireAsynchronousMethod(fitBrowserToWindow);
                            });
                        });
                    }
                    this.getEventsManager().FireAsynchronousMethod(fitBrowserToWindow);
                });

                const appWindow : IGuiElement = this.addGuiElement().StyleClassName(cssInterfaceName)
                    .Add(this.addGuiElement().StyleClassName("AppHeader")
                        .Add(this.appIcon)
                        .Add(this.appTitle)
                    )
                    .Add(this.addGuiElement("AppHeader")
                        .setAttribute("position", "absolute")
                        .setAttribute("top", "0")
                        .setAttribute("right", "0")
                        .Add(this.dragBar)
                        .Add(this.addGuiElement().StyleClassName("AppButtons")
                            .Add(this.minimizeButton)
                            .Add(this.maximizeButton)
                            .Add(this.closeButton)
                        )
                    )
                    .Add(this.addGuiElement().StyleClassName("AppResizeBars")
                        .Add(this.addGuiElement().StyleClassName(GeneralCssNames.TOP)
                            .Add(this.addGuiElement().StyleClassName(GeneralCssNames.LEFT).Add(this.topLeftResizeBar))
                            .Add(this.addGuiElement().StyleClassName(GeneralCssNames.CENTER).Add(this.topResizeBar))
                            .Add(this.addGuiElement().StyleClassName(GeneralCssNames.RIGHT).Add(this.topRightResizeBar))
                        )
                        .Add(this.addGuiElement().StyleClassName(GeneralCssNames.MIDDLE)
                            .Add(this.addGuiElement().StyleClassName(GeneralCssNames.LEFT).Add(this.leftResizeBar))
                            .Add(this.addGuiElement().StyleClassName(GeneralCssNames.CENTER))
                            .Add(this.addGuiElement().StyleClassName(GeneralCssNames.RIGHT).Add(this.rightResizeBar))
                        )
                        .Add(this.addGuiElement().StyleClassName(GeneralCssNames.BOTTOM)
                            .Add(this.addGuiElement().StyleClassName(GeneralCssNames.LEFT).Add(this.bottomLeftResizeBar))
                            .Add(this.addGuiElement().StyleClassName(GeneralCssNames.CENTER).Add(this.bottomResizeBar))
                            .Add(this.addGuiElement().StyleClassName(GeneralCssNames.RIGHT).Add(this.bottomRightResizeBar))
                        )
                    );
                StaticPageContentManager.BodyAppend(appWindow.Draw(EOL));
            }
            const appContentLoader : IGuiElement = this.addGuiElement("AppContentLoader")
                .StyleClassName(cssInterfaceName)
                .setAttribute("position", "absolute")
                .setAttribute("top", "100px")
                .setAttribute("left", "100px")
                .setAttribute("opacity", "0")
                .Add(this.addGuiElement("AppContentLoader_Content").StyleClassName("AppContentLoader")
                    .Add(this.loaderIcon)
                    .Add(this.loaderText)
                )
                .Add(this.loaderProgressLabel);
            StaticPageContentManager.BodyAppend(appContentLoader.Draw(EOL));

            const developerCorner : IGuiElement = this.addGuiElement().StyleClassName("DeveloperCorner").GuiTypeTag("DeveloperCorner")
                .Visible(false)
                .Add(this.addGuiElement("DeveloperCorner_EchoEnvelop").StyleClassName("Services")
                    .Add(this.addGuiElement().StyleClassName("Label").Add("Echo output"))
                    .Add(this.addGuiElement("DeveloperCorner_EchoTarget").StyleClassName("Echo"))
                );
            StaticPageContentManager.BodyAppend(developerCorner.Draw(EOL));

            const stream : string = Echo.getStream();
            if (!ObjectValidator.IsEmptyOrNull(stream)) {
                Echo.Clear();
            }

            StaticPageContentManager.Draw();
            ElementManager.setCssProperty(window.document.body, "margin", 0);
            Echo.Init("DeveloperCorner_EchoTarget", true);
            Echo.setOnPrint(($message : string) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    if (!ObjectValidator.IsEmptyOrNull($message)) {
                        LogIt.Info(StringUtils.StripTags($message));
                    }
                });
            });
            Echo.Print(stream);
            if (this.getRequest().IsJre() || isJreSimulator) {
                ElementManager.setClassName("Browser", BrowserType[BrowserType.JAVA_RE] + " FOCUS");
            }
            this.getEventsManager().FireEvent(GeneralEventOwner.BODY, EventType.ON_LOAD);
        } else {
            ViewerManager.getLoadedViewers().foreach(($viewer : BaseViewer) : void => {
                if (!ObjectValidator.IsEmptyOrNull($viewer.getInstance())) {
                    $viewer.getInstance().Visible(false);
                }
            });
            ElementManager.Show("AppContentLoader");
            if (!ObjectValidator.IsEmptyOrNull(this.loaderProgressLabel)) {
                this.loaderProgressLabel.Text("");
            }
        }
        if (!ObjectValidator.IsEmptyOrNull(this.loaderIcon) && !ObjectValidator.IsEmptyOrNull(this.loaderText)) {
            this.getEventsManager().FireAsynchronousMethod(loaderResize, 100);
        }
        Echo.Init("DeveloperCorner_EchoTarget", true);
        StaticPageContentManager.Title(this.pageTitle);
        const viewPort : HTMLMetaElement = document.createElement("meta");
        viewPort.name = "viewport";
        viewPort.content = "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0";
        StaticPageContentManager.HeadMetaDataAppend(viewPort);

        this.getEventsManager().setEvent(this.getClassName(), EventType.ON_LOAD, () : void => {
            this.getEventsManager().setEvent(ViewerManager.ClassName(), EventType.ON_COMPLETE,
                ($eventArgs : ViewerManagerEventArgs) : void => {
                    if (!$eventArgs.AsyncCallback()) {
                        ElementManager.Hide("AppContentLoader");
                        if (!ObjectValidator.IsEmptyOrNull(this.loaderProgressLabel)) {
                            this.loaderProgressLabel.Text("");
                        }
                        if (ElementManager.Exists("DeveloperCorner_ContextMenu", true)) {
                            const contextMenu : Node = ElementManager.getElement("DeveloperCorner_ContextMenu").parentNode;
                            contextMenu.parentNode.removeChild(contextMenu);
                        }
                        if (!ObjectValidator.IsEmptyOrNull($eventArgs.Result())) {
                            if ((<ViewerCacheManager>$eventArgs.Result()).IsTypeOf(<IClassName>ViewerCacheManager)) {
                                StaticPageContentManager.BodyAppend(
                                    (<ViewerCacheManager>$eventArgs.Result()).Show() + StringUtils.NewLine(false));
                            } else {
                                const view : BaseViewer = <BaseViewer>$eventArgs.Result();
                                this.modelObject = view;
                                if (!view.IsCached()) {
                                    StaticPageContentManager.BodyAppend(view.Show() + StringUtils.NewLine(false));
                                }
                                if (!ObjectValidator.IsEmptyOrNull(view.getInstance())) {
                                    const instance : IBaseGuiObject = this.getModel().getInstance();
                                    this.beforeLoad(instance, this.getModelArgs(), this.getDao());
                                    instance.getEvents().setOnComplete(() : void => {
                                        this.afterLoad(instance, this.getModelArgs(), this.getDao());
                                    });
                                    view.getInstance().Visible(true);

                                    const getBrowserSize : () => Size = () : Size => {
                                        const borderOffset : number =
                                            ElementManager.getCssIntegerValue("Browser", "border-width") * 2;
                                        const browserSize : Size = new Size("Browser", true);
                                        browserSize.Height((browserSize.Height() === 0 ?
                                                WindowManager.getSize().Height() : browserSize.Height()) -
                                            borderOffset -
                                            ElementManager.getElement("Browser").offsetTop * 2 -
                                            (!ObjectValidator.IsEmptyOrNull(ElementManager.getElement(this.dragBar)) ?
                                                ElementManager.getElement(this.dragBar).offsetHeight : 0));
                                        browserSize.Width((browserSize.Width() === 0 ?
                                                WindowManager.getSize().Width() : browserSize.Width()) -
                                            borderOffset);
                                        return browserSize;
                                    };

                                    this.getEventsManager().setEvent("Browser", EventType.ON_RESIZE,
                                        () : void => {
                                            const browserSize : Size = getBrowserSize();
                                            ElementManager.setWidth(view.getInstance().Id(), browserSize.Width());
                                            ElementManager.setHeight(view.getInstance().Id(), browserSize.Height());
                                        });

                                    this.getEventsManager().setEvent("Browser", EventType.ON_RESIZE_COMPLETE,
                                        () : void => {
                                            const browserSize : Size = getBrowserSize();
                                            const instance : IBaseGuiObject = view.getInstance();
                                            if (instance.Width() !== browserSize.Width() ||
                                                instance.Height() !== browserSize.Height()) {
                                                instance.Width(browserSize.Width());
                                                instance.Height(browserSize.Height());
                                            }
                                        });
                                }
                                /* dev:start */
                                if (!this.getEnvironmentArgs().IsProductionMode()) {
                                    StaticPageContentManager.BodyAppend(this.contextMenu());
                                }
                                /* dev:end */

                                this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_SUCCESS);
                                this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_COMPLETE);
                                this.onSuccess(this.getModel().getInstance(), this.getModelArgs(), this.getDao());
                            }
                            this.getEventsManager().FireEvent(GeneralEventOwner.BODY, EventType.ON_LOAD);
                        } else {
                            this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_ERROR);
                            this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_COMPLETE);
                            this.onError();
                        }
                    }
                });
            this.viewerManager = new ViewerManager(this.modelClassName, null, this.getModelArgs());
            this.viewerManager.setCacheFilePath(this.cacheFilePath);
            if (StringUtils.Contains(this.getRequest().getScriptPath(), HttpRequestConstants.FORCE_REFRESH)) {
                this.viewerManager.ReloadCacheEnabled(true);
                this.viewerManager.Process();
            } else {
                if (ViewerManager.Exists(this.viewerManager.getId())) {
                    ElementManager.Hide("AppContentLoader");
                    if (!ObjectValidator.IsEmptyOrNull(this.loaderProgressLabel)) {
                        this.loaderProgressLabel.Text("");
                    }
                    this.modelObject = ViewerManager.getLoadedViewers().getItem(this.viewerManager.getId());
                    this.getModel().getInstance().Visible(true);
                    this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_SUCCESS);
                    this.onSuccess(this.getModel().getInstance(), this.getModelArgs(), this.getDao());
                } else {
                    this.viewerManager.Process();
                }
            }
        });
        this.loadResources();
    }

    protected beforeLoad($instance? : IBaseGuiObject, $args? : BaseViewerArgs, $dao? : BasePageDAO) : void {
        // override this method for ability to prepare instance before it is loaded
    }

    protected afterLoad($instance? : IBaseGuiObject, $args? : BaseViewerArgs, $dao? : BasePageDAO) : void {
        // override this method for ability to setup instance after it is loaded
    }

    protected onSuccess($instance? : IBaseGuiObject, $args? : BaseViewerArgs, $dao? : BasePageDAO) : void {
        // override this method for ability to handle controller state after it has been successfully loaded
    }

    protected onError() : void {
        ExceptionsManager.Throw("Application loader",
            "Model \"" + this.modelClassName + "\" source file or persistence instance " +
            "has not been found! Please, validate correct usage of this.setModelClassName() in constructor.");
    }

    protected setResizeableType($value : ResizeableType) : void {
        if (!ObjectValidator.IsEmptyOrNull(this.getAppResizeBarClass())) {
            this.resizeableType = $value;
            switch ($value) {
            case ResizeableType.HORIZONTAL:
                this.topLeftResizeBar.Visible(false);
                this.topResizeBar.Visible(false);
                this.topRightResizeBar.Visible(false);
                this.leftResizeBar.Visible(true);
                this.rightResizeBar.Visible(true);
                this.bottomLeftResizeBar.Visible(false);
                this.bottomResizeBar.Visible(false);
                this.bottomRightResizeBar.Visible(false);
                break;
            case ResizeableType.VERTICAL:
                this.topLeftResizeBar.Visible(false);
                this.topResizeBar.Visible(true);
                this.topRightResizeBar.Visible(false);
                this.leftResizeBar.Visible(false);
                this.rightResizeBar.Visible(false);
                this.bottomLeftResizeBar.Visible(false);
                this.bottomResizeBar.Visible(true);
                this.bottomRightResizeBar.Visible(false);
                break;
            case ResizeableType.HORIZONTAL_AND_VERTICAL:
                this.topLeftResizeBar.Visible(true);
                this.topResizeBar.Visible(true);
                this.topRightResizeBar.Visible(true);
                this.leftResizeBar.Visible(true);
                this.rightResizeBar.Visible(true);
                this.bottomLeftResizeBar.Visible(true);
                this.bottomResizeBar.Visible(true);
                this.bottomRightResizeBar.Visible(true);
                break;
            default:
                this.topLeftResizeBar.Visible(false);
                this.topResizeBar.Visible(false);
                this.topRightResizeBar.Visible(false);
                this.leftResizeBar.Visible(false);
                this.rightResizeBar.Visible(false);
                this.bottomLeftResizeBar.Visible(false);
                this.bottomResizeBar.Visible(false);
                this.bottomRightResizeBar.Visible(false);
                break;
            }
        }
    }

    protected setWindowMinimalSize($width : number, $height : number) : void {
        this.minSize.Width($width);
        this.minSize.Height($height);
    }

    protected getCssInterfaceName() : string {
        return StringUtils.Remove(BasePageController.ClassName(), ".");
    }

    protected addGuiElement($id? : string) : IGuiElement {
        return new GuiElement().Id($id);
    }

    /* dev:start */
    protected contextMenu() : string {
        WindowManager.getEvents().setOnRightClick(($eventArgs : EventArgs) : void => {
            $eventArgs.PreventDefault();
            let top : number = WindowManager.getMouseY(<MouseEvent>$eventArgs.NativeEventArgs(), true) + 15;
            let left : number = WindowManager.getMouseX(<MouseEvent>$eventArgs.NativeEventArgs(), true) + 15;

            this.getEventsManager().FireAsynchronousMethod(() : void => {
                const menu : HTMLElement = ElementManager.getElement("DeveloperCorner_ContextMenu", true);
                ElementManager.Show(menu);
                const menuSize : Size = new Size(menu.id, true);
                const windowSize : Size = WindowManager.getSize();
                if (top + menuSize.Height() > windowSize.Height()) {
                    top = windowSize.Height() - menuSize.Height() - 20;
                }
                if (left + menuSize.Width() > windowSize.Width()) {
                    left = windowSize.Width() - menuSize.Width() - 20;
                }
                ElementManager.setCssProperty(menu, "top", top);
                ElementManager.setCssProperty(menu, "left", left);
            }, 100);
        });
        WindowManager.getEvents().setOnClick(() : void => {
            if (ElementManager.IsVisible("DeveloperCorner_ContextMenu")) {
                ElementManager.Hide("DeveloperCorner_ContextMenu");
            }
        });
        WindowManager.getEvents().setOnLoad(() : void => {
            const generateCacheEvents : ElementEventsManager = new ElementEventsManager("GenerateCacheLink");
            const showCacheDataLinkEvents : ElementEventsManager = new ElementEventsManager("ShowCacheDataLink");
            const showCacheReportLinkEvents : ElementEventsManager = new ElementEventsManager("ShowCacheReportLink");
            const cleanCacheLinkEvents : ElementEventsManager = new ElementEventsManager("CleanCacheLink");
            const forceRefreshLinkEvents : ElementEventsManager = new ElementEventsManager("ForceRefreshLink");
            const showEchoLinkEvents : ElementEventsManager = new ElementEventsManager("ShowEchoLink");
            const echoTargetId : string = "DeveloperCorner_EchoTarget";
            const echoTargetEvents : ElementEventsManager = new ElementEventsManager(echoTargetId);

            const showEcho : any = () : void => {
                ElementManager.Show(ElementManager.getElement("DeveloperCorner_EchoEnvelop", true));
                const windowSize : Size = WindowManager.getSize();
                const contentHeight : number = ElementManager.getElement(echoTargetId, true).offsetHeight;
                if (contentHeight > windowSize.Height()) {
                    ElementManager.setHeight(echoTargetId, windowSize.Height() - 35);
                }
            };
            generateCacheEvents.setOnClick(($args : EventArgs) : void => {
                $args.PreventDefault();
                if (this.getModel().IsMemberOf(BasePanelViewer)) {
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        const startTime : number = new Date().getTime();
                        this.viewerManager.getCache(() : void => {
                            const report : string = "<b>Cache has been created.</b> " +
                                "Process time: " + Convert.TimeToSeconds(new Date().getTime() - startTime) + " seconds";
                            showEcho();
                            Echo.Println(report);
                            this.viewerManager.getCacheRawData(($data : string) : void => {
                                const fs : FileSystemHandlerConnector = new FileSystemHandlerConnector();
                                this.getEventsManager().Clear("" + fs.getId(), WebServiceClientEventType.ON_ERROR);
                                fs.getEvents().OnError(() : void => {
                                    Echo.Println(
                                        "Unable to store cache data. " +
                                        "Please use Show Cache raw data option for manual storage.");
                                });
                                fs.getHostLocation().Then(($path : string) : void => {
                                    $path = StringUtils.Replace($path, "\\", "/");
                                    let cachePath : string = "";
                                    if (StringUtils.Contains($path, "/target/")) {
                                        if (StringUtils.Contains($path, "/build/target/target/")) {
                                            cachePath = StringUtils.Substring(
                                                StringUtils.Remove($path, "/build/target/"),
                                                0, StringUtils.IndexOf($path, "/target/") - 6);
                                        } else {
                                            cachePath = StringUtils.Substring(
                                                $path,
                                                0, StringUtils.IndexOf($path, "/target/", false) - 6);
                                        }
                                    } else {
                                        cachePath = $path;
                                    }
                                    cachePath = StringUtils.Replace(cachePath + "/" + this.cacheFilePath, "\\", "/");
                                    Echo.Println("Writing cache file \"" + cachePath + "\"");

                                    fs.Write(cachePath, "JsonpData(\"" +
                                        ObjectEncoder.Base64($data, true) +
                                        "\");").Then(($status : boolean) : void => {
                                        if ($status === true) {
                                            let targetPath : string = "/build/target/target/";
                                            if (!StringUtils.Contains($path, targetPath) &&
                                                !StringUtils.Contains(this.getRequest().getHostUrl(), targetPath)) {
                                                targetPath = "/build/target/";
                                            }
                                            targetPath = StringUtils.Replace(cachePath,
                                                this.cacheFilePath, targetPath + this.cacheFilePath);
                                            fs.Copy(cachePath, targetPath);
                                            Echo.Println("Cache data has been stored");
                                            this.viewerManager.ClearCache();
                                        } else {
                                            Echo.Println("Storage of cache data to the file has failed.");
                                        }
                                    });
                                });
                            });
                        });
                    }, 1000);
                }
            });
            showCacheDataLinkEvents.setOnClick(($args : EventArgs) : void => {
                $args.PreventDefault();
                Echo.Clear();
                this.viewerManager.getCacheRawData(($data : string) : void => {
                    const cacheViewer : HTMLTextAreaElement = document.createElement("textarea");
                    cacheViewer.innerHTML = "JsonpData(\"" +
                        ObjectEncoder.Base64($data, true) +
                        "\");";
                    ElementManager.getElement(echoTargetId).appendChild(cacheViewer);
                    const windowSize : Size = WindowManager.getSize();
                    ElementManager.setSize(cacheViewer, windowSize.Width() - 50, windowSize.Height() - 50);
                    ElementManager.setCssProperty(cacheViewer, "margin-top", 15);
                    showEcho();
                });
            });
            showCacheReportLinkEvents.setOnClick(($args : EventArgs) : void => {
                $args.PreventDefault();
                this.viewerManager.getCacheRawData(() : void => {
                    Echo.Clear();
                    Echo.Printf(this.viewerManager);
                    showEcho();
                });
            });
            cleanCacheLinkEvents.setOnClick(($args : EventArgs) : void => {
                $args.PreventDefault();
                this.viewerManager.ClearCache(false);
                this.getHttpManager().Refresh();
            });
            forceRefreshLinkEvents.setOnClick(($args : EventArgs) : void => {
                $args.PreventDefault();
                this.viewerManager.ClearCache();
                this.getHttpManager().Refresh();
            });
            showEchoLinkEvents.setOnClick(($args : EventArgs) : void => {
                $args.PreventDefault();
                showEcho();
            });
            echoTargetEvents.setOnDoubleClick(() : void => {
                ElementManager.Hide("DeveloperCorner_EchoEnvelop");
            });

            generateCacheEvents.Subscribe("DeveloperCorner_ContextMenu_GenerateCache", true);
            showCacheDataLinkEvents.Subscribe("DeveloperCorner_ContextMenu_ShowCacheData", true);
            showCacheReportLinkEvents.Subscribe("DeveloperCorner_ContextMenu_ShowCacheReport", true);
            cleanCacheLinkEvents.Subscribe("DeveloperCorner_ContextMenu_CleanCache", true);
            forceRefreshLinkEvents.Subscribe("DeveloperCorner_ContextMenu_ForceRefresh", true);
            showEchoLinkEvents.Subscribe("DeveloperCorner_ContextMenu_ShowEcho", true);
            echoTargetEvents.Subscribe(true);
        });

        return this.addGuiElement().StyleClassName("DeveloperCorner").GuiTypeTag("DeveloperCorner")
            .Add(this.addGuiElement("DeveloperCorner_ContextMenu").StyleClassName("ContextMenu").Visible(false)
                .Add(this.addGuiElement("DeveloperCorner_ContextMenu_GenerateCache").StyleClassName("Link")
                    .Add("Generate Cache")
                )
                .Add(this.addGuiElement("DeveloperCorner_ContextMenu_ShowCacheData").StyleClassName("Link")
                    .Add("Show Cache raw data")
                )
                .Add(this.addGuiElement("DeveloperCorner_ContextMenu_ShowCacheReport").StyleClassName("Link")
                    .Add("Show Cache report")
                )
                .Add(this.addGuiElement("DeveloperCorner_ContextMenu_CleanCache").StyleClassName("Link")
                    .Add("Destroy generated Cache")
                )
                .Add(this.addGuiElement("DeveloperCorner_ContextMenu_ForceRefresh").StyleClassName("Link")
                    .Add("Force refresh")
                )
                .Add(this.addGuiElement("DeveloperCorner_ContextMenu_ShowEcho").StyleClassName("Link")
                    .Add("Show Echo output")
                )
            )
            .Draw(StringUtils.NewLine(false));
    }

    /* dev:end */

    private setLoaderText($value : string) : void {
        if (!ObjectValidator.IsEmptyOrNull(this.loaderText)) {
            this.loaderText.Text($value);
        }
    }

    private setLoaderProgressText($value : string) : void {
        this.loaderProgressText = Property.String(this.loaderProgressText, $value);
    }
}
