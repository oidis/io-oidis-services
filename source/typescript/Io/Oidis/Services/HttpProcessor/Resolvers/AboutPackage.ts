/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseHttpResolver } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/BaseHttpResolver.js";
import { Index as CommonsIndex } from "@io-oidis-commons/Io/Oidis/Commons/Index.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { Index as GuiIndex } from "@io-oidis-gui/Io/Oidis/Gui/Index.js";
import { StaticPageContentManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/StaticPageContentManager.js";

/**
 * AboutPackage request resolver class provides handling of application dependencies content.
 */
export class AboutPackage extends BaseHttpResolver {

    protected resolver() : void {
        this.registerResolver("/about/Oidis/Commons", CommonsIndex);
        this.registerResolver("/about/Oidis/Gui", GuiIndex);

        let output : string =
            "<div class=\"GuiInterface\">" +
            "<h1>Oidis Services Library</h1>" +
            "<div class=\"Index\">";

        output +=
            "<H3>Library dependencies</H3>" +
            "<a href=\"#" + this.createLink("/about/Oidis/Commons") + "\">Oidis Commons library</a>" +
            StringUtils.NewLine() +
            "<a href=\"#" + this.createLink("/about/Oidis/Gui") + "\">Oidis Gui library</a>" +
            StringUtils.NewLine() +
            "</div>" +
            "</div>";

        output +=
            "<div class=\"Note\">" +
            "version: " + this.getEnvironmentArgs().getProjectVersion() +
            ", build: " + this.getEnvironmentArgs().getBuildTime() +
            "</div>";

        StaticPageContentManager.Clear();
        StaticPageContentManager.Title("Oidis Services - Package Info");
        StaticPageContentManager.BodyAppend(output);
        StaticPageContentManager.Draw();
    }
}
