/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IHttpResolverContext } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/HttpResolver.js";
import { HttpResolver as Parent } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/HttpResolver.js";
import { HttpManager } from "./HttpManager.js";

export class HttpResolver extends Parent {

    protected async getStartupResolvers($context : IHttpResolverContext) : Promise<any> {
        if ($context.isBrowser) {
            await this.registerResolver(async () => (await import("../ErrorPages/AuthHttpResolver.js")).AuthHttpResolver,
                "/ServerError/Http/NotAuthorized");
            await this.registerResolver(async () => (await import("../ErrorPages/ConnectionLostPage.js")).ConnectionLostPage,
                "/ServerError/Http/ConnectionLost");

            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const initPage : any = (await import("./Resolvers/InitPage.js")).InitPage;

            if (!$context.isProd) {
                await this.registerResolver(async () => (await import("../Index.js")).Index,
                    "/ServerError/Http/DefaultPage", "/", "/index", "/web/");
                await this.registerResolver(async () => (await import("./Resolvers/AboutPackage.js")).AboutPackage,
                    "/about/Package");
                await this.registerResolver(async () => (await import("../RuntimeTests/PageControllerTest.js")).PageControllerTest,
                    "/PageControllerTest");
            }
        }
        return super.getStartupResolvers($context);
    }

    protected getHttpManagerClass() : any {
        return HttpManager;
    }
}
