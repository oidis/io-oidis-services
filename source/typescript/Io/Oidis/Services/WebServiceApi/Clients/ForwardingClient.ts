/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { WebSocketsClient } from "@io-oidis-commons/Io/Oidis/Commons/WebServiceApi/Clients/WebSocketsClient.js";
import { WebServiceConfiguration } from "@io-oidis-commons/Io/Oidis/Commons/WebServiceApi/WebServiceConfiguration.js";

/**
 * ForwardingClient class provides client for web sockets communication forwarded over Oidis Hub agents.
 */
export class ForwardingClient extends WebSocketsClient {
    private agentName : string;
    private forwardingMethod : string;

    constructor($configuration : WebServiceConfiguration) {
        super($configuration);
        this.agentName = "OidisConnector";
        this.forwardingMethod = "Io.Oidis.Hub.Utils.ConnectorHub.ForwardRequest";
    }

    /**
     * @param {string} $value Specify agent name used for look up on Oidis Hub.
     * @returns {string} Returns agent name value.
     */
    public AgentName($value : string) : string {
        return this.agentName = Property.String(this.agentName, $value);
    }

    public ForwardingMethod($value : string) : string {
        return this.forwardingMethod = Property.String(this.forwardingMethod, $value);
    }

    protected sendRequest($data : string) : void {
        const protocol : any = JSON.parse($data);
        protocol.data = ObjectEncoder.Base64(JSON.stringify({
            args: ObjectEncoder.Base64(JSON.stringify([this.agentName, protocol])),
            name: this.forwardingMethod
        }));
        $data = JSON.stringify(protocol);
        super.sendRequest($data);
    }

    protected onResponse($event : MessageEvent) : void {
        try {
            if ($event.data !== "OK" && !ObjectValidator.IsEmptyOrNull($event.data)) {
                let data : any = JSON.parse($event.data);
                if (ObjectValidator.IsObject(data.data)) {
                    data.data.returnValue = ObjectDecoder.Base64(data.data.returnValue);
                    if (ObjectValidator.IsBoolean(data.data.returnValue) && !data.data.returnValue ||
                        data.data.returnValue === "false") {
                        this.throwError("Failed to forward request, no agent found.");
                    } else {
                        if (!ObjectValidator.IsEmptyOrNull(data.data.returnValue) && ObjectValidator.IsString(data.data.returnValue)) {
                            data = JSON.parse(data.data.returnValue);
                        }
                        if (data.type === EventType.ON_CHANGE) {
                            data.data.origin = this.getServerUrl();
                            super.onResponse(<any>{
                                data: data.data
                            });
                        }
                    }
                } else {
                    super.onResponse($event);
                }
            } else {
                super.onResponse($event);
            }
        } catch (ex) {
            this.throwError(ex);
        }
    }
}
