/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { WebServiceClientEventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/WebServiceClientEventType.js";
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { EventsManager } from "@io-oidis-commons/Io/Oidis/Commons/Events/EventsManager.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { WebSocketsClient } from "@io-oidis-commons/Io/Oidis/Commons/WebServiceApi/Clients/WebSocketsClient.js";
import { LiveContentArgumentType } from "../Enums/LiveContentArgumentType.js";
import { AuthHttpResolver } from "../ErrorPages/AuthHttpResolver.js";
import { ILiveContentFormatterPromise } from "../Interfaces/ILiveContentFormatterPromise.js";
import { ILiveContentErrorPromise, ILiveContentPromise } from "../Interfaces/ILiveContentPromise.js";
import { IWebServiceClient } from "../Interfaces/IWebServiceClient.js";
import { ILiveContentProtocol, IWebServiceException, IWebServiceProtocol } from "../Interfaces/IWebServiceProtocol.js";
import { Loader } from "../Loader.js";

/**
 * LiveContentWrapper class provides wrapper for client-server LiveContent communication.
 */
export class LiveContentWrapper extends BaseObject implements ILiveContentPromise {
    private static currentLCPversion : string = "1.0";
    private static eventHandlers : ArrayList<ArrayList<ArrayList<(...$args : any[]) => void>>>;
    private readonly serverClassName : string;
    private type : string;
    private name : string;
    private clientId : number;
    private args : any[];
    private callback : any;
    private dataFormatter : any;
    private errorHandler : any;

    /**
     * @param {ILCPConfig} $config Specify configuration object for method invoke API.
     * @returns {ILiveContentFormatterPromise} Returns promise interface for handling of invoke method event.
     */
    public static InvokeMethod($config : ILCPConfig) : ILiveContentFormatterPromise;
    /**
     * @param {IWebServiceClient} $client Specify web service client instance, which is handling the server-client connection.
     * @param {string} $className Specify class name, which should be reflected on server side.
     * @param {string} $name Specify method name, which should be invoked.
     * @param {...any[]} [$args] Specify method arguments, which should be passed to the invoked method.
     * @returns {ILiveContentFormatterPromise} Returns promise interface for handling of invoke method event.
     */
    public static InvokeMethod($client : IWebServiceClient, $className : string, $name : string,
                               ...$args : any[]) : ILiveContentFormatterPromise;
    public static InvokeMethod($clientOrOpts : IWebServiceClient | ILCPConfig, $className? : string, $name? : string,
                               ...$args : any[]) : ILiveContentFormatterPromise {
        let options : ILCPConfig = <ILCPConfig>$clientOrOpts;
        if (Reflection.getInstance().Implements(<IWebServiceClient>$clientOrOpts, IWebServiceClient)) {
            options = {
                args     : $args,
                className: $className,
                client   : <IWebServiceClient>$clientOrOpts,
                name     : $name
            };
        }
        const methodCall : LiveContentWrapper = new LiveContentWrapper(options.className);
        const args : any[] = [options.client.getId(), options.name];
        let index : number;
        for (index = 0; index < options.args.length; index++) {
            if (ObjectValidator.IsSet(options.args[index])) {
                args.push(options.args[index]);
            }
        }
        // eslint-disable-next-line prefer-spread
        const protocol : IWebServiceProtocol = methodCall.getProtocolForInvokeMethod.apply(methodCall, args);
        if (ObjectValidator.IsSet(options.token) && ObjectValidator.IsString(options.token)) {
            protocol.token = options.token;
        }
        options.client.Send(
            protocol,
            ($data : IWebServiceProtocol) : void => {
                methodCall.getResponseHandler(options.client, $data);
            });
        return methodCall;
    }

    /**
     * @param {IWebServiceClient} $client Specify web service client instance, which is handling the server-client connection.
     * @param {string} $className Specify class name, which should be reflected on server side.
     * @param {string} $name Specify property name, which should be set.
     * @param {any} $value Specify value, which should be set to the property.
     * @returns {ILiveContentPromise} Returns promise interface for handling of property set event.
     */
    public static setProperty($client : IWebServiceClient, $className : string, $name : string,
                              $value : any) : ILiveContentPromise {
        const methodCall : LiveContentWrapper = new LiveContentWrapper($className);
        $client.Send(
            methodCall.getProtocolForSetProperty($client.getId(), $name, $value),
            ($data : IWebServiceProtocol) : void => {
                methodCall.getResponseHandler($client, $data);
            });
        return methodCall;
    }

    /**
     * @param {IWebServiceClient} $client Specify web service client instance, which is handling the server-client connection.
     * @param {string} $className Specify class name, which should be reflected on server side.
     * @param {string} $name Specify property name, which should be invoked.
     * @returns {ILiveContentFormatterPromise} Returns promise interface for handling of invoke property event.
     */
    public static getProperty($client : IWebServiceClient, $className : string,
                              $name : string) : ILiveContentFormatterPromise {
        const methodCall : LiveContentWrapper = new LiveContentWrapper($className);
        $client.Send(
            methodCall.getProtocolForGetProperty($client.getId(), $name),
            ($data : IWebServiceProtocol) : void => {
                methodCall.getResponseHandler($client, $data);
            });
        return methodCall;
    }

    /**
     * @param {IWebServiceClient} $client Specify web service client instance, which is handling the server-client connection.
     * @param {string} $className Specify class name, which should be reflected on server side.
     * @param {string} $name Specify event name, which should be handled.
     * @param {Function} $handler Specify event handler callback, which should be asigned.
     * @returns {void}
     */
    public static AddEventHandler($client : IWebServiceClient, $className : string, $name : string,
                                  $handler : (...$args : any[]) => void) : void {
        const methodCall : LiveContentWrapper = new LiveContentWrapper($className);
        if (!LiveContentWrapper.eventHandlers.KeyExists($client.getId())) {
            LiveContentWrapper.eventHandlers.Add(new ArrayList<ArrayList<(...$args : any[]) => void>>(), $client.getId());
            $client.getEvents().OnClose(() : void => {
                LiveContentWrapper.eventHandlers.RemoveAt(LiveContentWrapper.eventHandlers.getKeys().indexOf($client.getId()));
            });
        }
        const clientEvents : ArrayList<ArrayList<(...$args : any[]) => void>> =
            LiveContentWrapper.eventHandlers.getItem($client.getId());
        const handlerOwner : string = $className + "." + $name;
        if (!clientEvents.KeyExists(handlerOwner)) {
            clientEvents.Add(new ArrayList<(...$args : any[]) => void>(), handlerOwner);

            $client.Send(
                methodCall.getProtocolForAddEventHandler($client.getId(), $name),
                ($data : IWebServiceProtocol) : void => {
                    if (StringUtils.StartsWith($data.type, "LiveContentWrapper.")) {
                        if (!ObjectValidator.IsEmptyOrNull($data.data)) {
                            const wrapper : ILiveContentProtocol = <ILiveContentProtocol>$data.data;
                            if (methodCall.memberName() === wrapper.name) {
                                methodCall.getEventHandler($data, $client.getId());
                            }
                        } else if (!$client.IsTypeOf(WebSocketsClient)) {
                            const eventId : number = $data.id;
                            let checkerId : number;
                            $client.getEvents().OnClose(() : void => {
                                if (!ObjectValidator.IsEmptyOrNull(checkerId)) {
                                    clearTimeout(checkerId);
                                }
                            });
                            const eventChecker : any = () : void => {
                                if (!ObjectValidator.IsEmptyOrNull($client) && $client.CommunicationIsRunning()) {
                                    checkerId = Loader.getInstance().getHttpResolver().getEvents()
                                        .FireAsynchronousMethod(() : void => {
                                            $client.Send(
                                                methodCall.getProtocolForEventChecker($name, eventId),
                                                ($data : IWebServiceProtocol) : void => {
                                                    if (StringUtils.StartsWith($data.type, "LiveContentWrapper.") &&
                                                        !ObjectValidator.IsEmptyOrNull($data.data)) {
                                                        if (methodCall.memberName() === handlerOwner) {
                                                            let events : IWebServiceProtocol[] = [];
                                                            if (ObjectValidator.IsArray($data.data)) {
                                                                events = <IWebServiceProtocol[]>$data.data;
                                                            }
                                                            let eventIndex : number;
                                                            for (eventIndex = 0; eventIndex < events.length; eventIndex++) {
                                                                methodCall.getEventHandler(events[eventIndex], $client.getId());
                                                            }
                                                        }
                                                    }
                                                    eventChecker();
                                                });
                                        }, 500);
                                }
                            };
                            eventChecker();
                        }
                    } else {
                        methodCall.getResponseHandler($client, $data);
                    }
                });
        }
        clientEvents.getItem(handlerOwner).Add($handler);
    }

    /**
     * @param {string} $forClassName Specify class name, which should be reflected on the server side.
     */
    constructor($forClassName : string) {
        super();

        this.serverClassName = $forClassName;
        this.args = [];
        this.callback = () : void => {
            // default callback
        };
        this.errorHandler = null;
        if (!ObjectValidator.IsSet(LiveContentWrapper.eventHandlers)) {
            LiveContentWrapper.eventHandlers = new ArrayList<ArrayList<ArrayList<(...$args : any[]) => void>>>();
        }
    }

    /**
     * @param {number} $clientId Specify web service client id, which is handling the server-client connection.
     * @param {string} $name Specify method name, which should be invoked.
     * @param {...any[]} [$args] Specify method arguments, which should be passed to the invoked method.
     * @returns {IWebServiceProtocol} Returns data suitable for server-client communication.
     */
    public getProtocolForInvokeMethod($clientId : number, $name : string, ...$args : any[]) : IWebServiceProtocol {
        this.protocolType("InvokeMethod");
        this.clientId = $clientId;
        this.memberName(this.serverClassName + "." + $name);
        let index : number;
        this.args = [];
        for (index = 0; index < $args.length; index++) {
            this.addArgument($args[index]);
        }
        return this.toProtocol();
    }

    /**
     * @param {number} $clientId Specify web service client id, which is handling the server-client connection.
     * @param {string} $name Specify property name, which should be invoked.
     * @param {any} $value Specify value, which should be passed to the invoked property.
     * @returns {IWebServiceProtocol} Returns data suitable for server-client communication.
     */
    public getProtocolForSetProperty($clientId : number, $name : string, $value : any) : IWebServiceProtocol {
        this.protocolType("setProperty");
        this.clientId = $clientId;
        this.memberName(this.serverClassName + "." + $name);
        this.args = [];
        this.addArgument($value);
        return this.toProtocol();
    }

    /**
     * @param {number} $clientId Specify web service client id, which is handling the server-client connection.
     * @param {string} $name Specify property name, which should be invoked.
     * @returns {IWebServiceProtocol} Returns data suitable for server-client communication.
     */
    public getProtocolForGetProperty($clientId : number, $name : string) : IWebServiceProtocol {
        this.protocolType("getProperty");
        this.clientId = $clientId;
        this.memberName(this.serverClassName + "." + $name);
        return this.toProtocol();
    }

    /**
     * @param {number} $clientId Specify web service client id, which is handling the server-client connection.
     * @param {string} $name Specify event name, which should be handled.
     * @returns {IWebServiceProtocol} Returns data suitable for server-client communication.
     */
    public getProtocolForAddEventHandler($clientId : number, $name : string) : IWebServiceProtocol {
        this.protocolType("AddEventHandler");
        this.clientId = $clientId;
        this.memberName(this.serverClassName + "." + $name);
        this.args = [];
        return this.toProtocol();
    }

    /**
     * @param {IWebServiceProtocol} $data Specify data from server-client communication.
     * @returns {any} Returns data expected as invoke return value parsed from server-client communication data.
     */
    public getReturnValue($data : IWebServiceProtocol) : any {
        let returnValue : any = null;
        if (StringUtils.StartsWith($data.type, "LiveContentWrapper.") && !ObjectValidator.IsEmptyOrNull($data.data)) {
            const wrapper : ILiveContentProtocol = <ILiveContentProtocol>$data.data;
            if (this.name === wrapper.name) {
                const value : string = ObjectDecoder.Base64(wrapper.returnValue);
                if (StringUtils.StartsWith(value, "{") || StringUtils.StartsWith(value, "[")) {
                    try {
                        returnValue = JSON.parse(value);
                    } catch (ex) {
                        returnValue = value;
                    }
                } else {
                    if (ObjectValidator.IsDouble(value)) {
                        returnValue = StringUtils.ToDouble(value);
                    } else if (ObjectValidator.IsInteger(value)) {
                        returnValue = StringUtils.ToInteger(value);
                    } else if (value === "true" || value === "false") {
                        returnValue = StringUtils.ToBoolean(value);
                    } else if (value === LiveContentArgumentType.RETURN_VOID || value === "null") {
                        returnValue = null;
                    } else {
                        returnValue = value;
                    }
                }
            }
        }
        return returnValue;
    }

    /**
     * @param {IWebServiceProtocol} $data Specify data from server-client communication.
     * @returns {any[]} Returns data expected as output arguments parsed from server-client communication data.
     */
    public getReturnArgs($data : IWebServiceProtocol) : any[] {
        let args : any[] = [];
        if (StringUtils.StartsWith($data.type, "LiveContentWrapper.") && !ObjectValidator.IsEmptyOrNull($data.data)) {
            const wrapper : ILiveContentProtocol = <ILiveContentProtocol>$data.data;
            if (this.name === wrapper.name) {
                const value : string = ObjectDecoder.Base64(wrapper.args);
                if (StringUtils.StartsWith(value, "[")) {
                    try {
                        args = JSON.parse(value);
                    } catch (ex) {
                        // return args as is, if parsing is not possible - args can be already in correct format
                    }
                }
            }
        }
        return args;
    }

    /**
     * @param {Function} $callback Specify callback function, which will be called on data received event.
     * @returns {void}
     */
    public Then($callback : (...$args : any[]) => void) : void {
        this.callback = $callback;
    }

    /**
     * @param {Function} $formatter Specify formatter function, which will be used for formatting of the received data.
     * @returns {ILiveContentErrorPromise} Returns promise interface suitable for asynchronous communication handling.
     */
    public DataFormatter($formatter : ($data : any) => any) : ILiveContentErrorPromise {
        this.dataFormatter = $formatter;
        return this;
    }

    /**
     * @param {Function} $callback Specify callback function, which will be called on error event.
     * @returns {ILiveContentPromise} Returns promise interface suitable for asynchronous communication handling.
     */
    public OnError($callback : ($error : ErrorEventArgs, ...$args : any[]) => void) : ILiveContentPromise {
        this.errorHandler = $callback;
        return this;
    }

    private protocolType($value? : string) : string {
        return this.type = Property.String(this.type, $value);
    }

    private memberName($value? : string) : string {
        return this.name = Property.String(this.name, $value);
    }

    private addArgument($value : any) : void {
        this.args.push($value);
    }

    private getCallback($data : IWebServiceProtocol) : void {
        const parameters : any[] = [];
        if (ObjectValidator.IsEmptyOrNull(this.dataFormatter)) {
            parameters.push(this.getReturnValue($data));
        } else {
            parameters.push(this.dataFormatter(this.getReturnValue($data)));
        }
        const args : any[] = this.getReturnArgs($data);
        let index : number;
        for (index = 0; index < args.length; index++) {
            parameters.push(args[index]);
        }
        this.callback.apply(this, parameters); // eslint-disable-line prefer-spread
    }

    private getProtocolForEventChecker($name : string, $subscriberId : number) : IWebServiceProtocol {
        this.protocolType("getEvent");
        this.memberName(this.serverClassName + "." + $name);
        this.args = [];
        this.addArgument($subscriberId);
        return this.toProtocol();
    }

    private getResponseHandler($client : IWebServiceClient, $data : IWebServiceProtocol) : void {
        if ($data.type === WebServiceClientEventType.ON_ERROR) {
            $data.data = ObjectDecoder.Base64(<string>$data.data);
            const exceptionData : IWebServiceException = <any>$data.data;
            let message : string | Error;
            if (ObjectValidator.IsString($data.data)) {
                message = $data.data;
            } else {
                message = ObjectDecoder.Base64(exceptionData.message);
                if (!ObjectValidator.IsEmptyOrNull(exceptionData.stack)) {
                    message = new Error(message);
                    (<Error>message).stack = ObjectDecoder.Base64(exceptionData.stack);
                }
            }
            const eventArgs : ErrorEventArgs = new ErrorEventArgs(message);
            eventArgs.Owner($client);
            if (!ObjectValidator.IsEmptyOrNull(this.errorHandler)) {
                let args : any[] = [eventArgs];
                if (!ObjectValidator.IsString($data.data) && !ObjectValidator.IsEmptyOrNull(exceptionData.args)) {
                    try {
                        exceptionData.args = JSON.parse(ObjectDecoder.Base64(exceptionData.args));
                        args = args.concat(exceptionData.args);
                    } catch (ex) {
                        LogIt.Warning("Unable to parse custom exception args. " + ex.stack);
                    }
                }
                this.errorHandler.apply(this, args); // eslint-disable-line prefer-spread
            } else {
                EventsManager.getInstanceSingleton().FireEvent("" + $client.getId(), WebServiceClientEventType.ON_ERROR, eventArgs);
            }
        } else {
            this.getCallback($data);
        }
    }

    private getEventHandler($data : IWebServiceProtocol, $clientId : number) : void {
        const parameters : any[] = [];
        const args : any[] = this.getReturnArgs($data);
        let index : number;
        for (index = 0; index < args.length; index++) {
            parameters.push(args[index]);
        }
        const asyncExecution : any = Loader.getInstance().getHttpResolver().getEvents().FireAsynchronousMethod;
        LiveContentWrapper.eventHandlers.getItem($clientId).getItem(this.memberName()).foreach(
            ($handler : (...$args : any[]) => void) : void => {
                asyncExecution(() : void => {
                    $handler.apply(this, parameters);
                }, false);
            }
        );
    }

    private toProtocol() : IWebServiceProtocol {
        return <IWebServiceProtocol>{
            clientId: this.clientId,
            data    : ObjectEncoder.Base64(JSON.stringify({
                args: ObjectEncoder.Base64(JSON.stringify(this.args)),
                name: this.name
            })),
            id      : null,
            origin  : null,
            status  : null,
            token   : AuthHttpResolver.getToken(),
            type    : "LiveContentWrapper." + this.type,
            version : LiveContentWrapper.currentLCPversion
        };
    }
}

export interface ILCPConfig {
    client : IWebServiceClient;
    className : string;
    name : string;
    args : any[];
    token? : string;
}

// generated-code-start
export const ILCPConfig = globalThis.RegisterInterface(["client", "className", "name", "args", "token"]);
// generated-code-end
