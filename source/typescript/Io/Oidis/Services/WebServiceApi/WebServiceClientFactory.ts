/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BuilderConnector } from "@io-oidis-commons/Io/Oidis/Commons/Connectors/BuilderConnector.js";
import { WebServiceClientEventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/WebServiceClientEventType.js";
import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { LogSeverity } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogSeverity.js";
import { Exception } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/Type/Exception.js";
import { HttpRequestParser } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/HttpRequestParser.js";
import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { IWebServiceRequestFormatterData } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IWebServiceClient.js";
import { PersistenceFactory } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { AjaxClient } from "@io-oidis-commons/Io/Oidis/Commons/WebServiceApi/Clients/AjaxClient.js";
import { IframeClient } from "@io-oidis-commons/Io/Oidis/Commons/WebServiceApi/Clients/IframeClient.js";
import { PostMessageClient } from "@io-oidis-commons/Io/Oidis/Commons/WebServiceApi/Clients/PostMessageClient.js";
import { WebSocketsClient } from "@io-oidis-commons/Io/Oidis/Commons/WebServiceApi/Clients/WebSocketsClient.js";
import { WebServiceClientFactory as Parent } from "@io-oidis-commons/Io/Oidis/Commons/WebServiceApi/WebServiceClientFactory.js";
import { WebServiceConfiguration } from "@io-oidis-commons/Io/Oidis/Commons/WebServiceApi/WebServiceConfiguration.js";
import { WindowManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/WindowManager.js";
import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { IWebServiceClient } from "../Interfaces/IWebServiceClient.js";
import { IWebServiceException, IWebServiceProtocol } from "../Interfaces/IWebServiceProtocol.js";
import { Loader } from "../Loader.js";
import { ForwardingClient } from "./Clients/ForwardingClient.js";

export class WebServiceClientFactory extends Parent {

    /**
     * @param {WebServiceClientType} $clientType Specify client type, which should be validated.
     * @returns {boolean} Returns true, if required client type is supported by runtime environment, othewise false.
     */
    public static IsSupported($clientType : WebServiceClientType) : boolean {
        if ($clientType === WebServiceClientType.REST_CONNECTOR) {
            return Parent.IsSupported(WebServiceClientType.AJAX) ||
                Parent.IsSupported(WebServiceClientType.IFRAME) ||
                Parent.IsSupported(WebServiceClientType.POST_MESSAGE);
        } else if (
            $clientType === WebServiceClientType.DESKTOP_CONNECTOR ||
            $clientType === WebServiceClientType.HUB_CONNECTOR ||
            $clientType === WebServiceClientType.FORWARD_CLIENT ||
            $clientType === WebServiceClientType.HOST_CLIENT ||
            $clientType === WebServiceClientType.BUILDER_CONNECTOR) {
            return Parent.IsSupported(WebServiceClientType.WEB_SOCKETS) ||
                Parent.IsSupported(WebServiceClientType.HTTP);
        } else {
            return Parent.IsSupported($clientType);
        }
    }

    /**
     * @param {number} $clientId Specify id of the desired service client.
     * @returns {IWebServiceClient} Returns instance of web service client based on $clientId,
     * if client has been found, otherwise null.
     */
    public static getClientById($clientId : number) : IWebServiceClient {
        return Parent.getClientById($clientId);
    }

    protected static createClientInstance($clientType : WebServiceClientType,
                                          $serverConfigurationSource? : string | WebServiceConfiguration) : IWebServiceClient {
        let configuration : WebServiceConfiguration;
        if (ObjectValidator.IsString($serverConfigurationSource) ||
            ObjectValidator.IsEmptyOrNull($serverConfigurationSource)) {
            configuration = new WebServiceConfiguration(<string>$serverConfigurationSource);
        } else {
            configuration = <WebServiceConfiguration>$serverConfigurationSource;
        }
        let client : IWebServiceClient = null;
        const request : HttpRequestParser = Loader.getInstance().getHttpManager().getRequest();

        if (WebServiceClientFactory.IsSupported($clientType)) {
            switch ($clientType) {
            case WebServiceClientType.BUILDER_CONNECTOR:
                client = WebServiceClientFactory.getClientById(BuilderConnector.Connect().getId());
                break;
            case WebServiceClientType.HOST_CLIENT:
                configuration = new WebServiceConfiguration(request.getHostUrl() + "/connector.config.jsonp");
                if (WebServiceClientFactory.IsSupported(WebServiceClientType.WEB_SOCKETS)) {
                    if (StringUtils.StartsWith(request.getHostUrl(), "https://")) {
                        configuration.ServerProtocol("wss");
                    } else {
                        configuration.ServerProtocol("ws");
                    }
                    client = new WebSocketsClient(configuration);
                } else {
                    if (StringUtils.StartsWith(request.getHostUrl(), "https://")) {
                        configuration.ServerProtocol("https");
                    } else {
                        configuration.ServerProtocol("http");
                    }
                    client = new AjaxClient(configuration);
                }
                break;
            case WebServiceClientType.REST_CONNECTOR:
            case WebServiceClientType.HUB_CONNECTOR:
            case WebServiceClientType.FORWARD_CLIENT:
            case WebServiceClientType.DESKTOP_CONNECTOR:
                if (ObjectValidator.IsEmptyOrNull($serverConfigurationSource)) {
                    if ($clientType === WebServiceClientType.REST_CONNECTOR ||
                        $clientType === WebServiceClientType.HUB_CONNECTOR ||
                        $clientType === WebServiceClientType.FORWARD_CLIENT) {
                        if (request.IsOnServer(true)) {
                            $serverConfigurationSource =
                                "https://localhost.oidis.io/connector.config.jsonp";
                        } else {
                            $serverConfigurationSource = "https://hub.oidis.io/connector.config.jsonp";
                        }
                        configuration = new WebServiceConfiguration($serverConfigurationSource);
                    } else {
                        if (request.IsOnServer(true)) {
                            $serverConfigurationSource = "connector.config.jsonp";
                        } else {
                            $serverConfigurationSource = "resource/libs/OidisConnector/connector.config.jsonp";
                        }
                    }
                }
                if (ObjectValidator.IsString($serverConfigurationSource)) {
                    configuration = new WebServiceConfiguration(<string>$serverConfigurationSource);
                    configuration.ServerAddress("127.0.0.1");
                    configuration.ServerPort(8888);
                } else {
                    configuration = <WebServiceConfiguration>$serverConfigurationSource;
                }

                if ($clientType !== WebServiceClientType.REST_CONNECTOR &&
                    WebServiceClientFactory.IsSupported(WebServiceClientType.WEB_SOCKETS)) {
                    if (StringUtils.StartsWith(request.getHostUrl(), "https://")) {
                        configuration.ServerProtocol("wss");
                    } else {
                        configuration.ServerProtocol("ws");
                    }
                    if ($clientType === WebServiceClientType.FORWARD_CLIENT) {
                        client = new ForwardingClient(configuration);
                    } else {
                        client = new WebSocketsClient(configuration);
                    }
                } else {
                    if (StringUtils.StartsWith(request.getHostUrl(), "https://")) {
                        configuration.ServerProtocol("https");
                    } else {
                        configuration.ServerProtocol("http");
                    }
                    if (request.IsSameOrigin(configuration.getServerUrl())) {
                        client = new AjaxClient(configuration);
                    } else if (WebServiceClientFactory.IsSupported(WebServiceClientType.POST_MESSAGE)) {
                        client = new PostMessageClient(configuration);
                    } else {
                        configuration.ResponseUrl(configuration.getServerUrl() + "connector.response.jsonp");
                        client = new IframeClient(configuration);
                    }
                }
                break;
            default :
                client = Parent.createClientInstance($clientType, $serverConfigurationSource);
            }
        }

        if (!ObjectValidator.IsEmptyOrNull(client)) {
            if (!client.IsTypeOf(WebSocketsClient)) {
                client.getEvents().OnStart(() : void => {
                    const closeUrl : string =
                        client.getServerUrl() + "CloseClient?id=" + ObjectEncoder.Base64(client.getId().toString());
                    const closeClient : any = ($url : string) : void => {
                        if (!StringUtils.ContainsIgnoreCase($url, "ws://", "wss://")) {
                            const closeRequest : HTMLScriptElement = <HTMLScriptElement>document.createElement("script");
                            closeRequest.src = $url + "&dummy=" + (new Date().getTime());
                            closeRequest.type = "text/javascript";
                            if (!ObjectValidator.IsEmptyOrNull(globalThis.nonce)) {
                                closeRequest.nonce = globalThis.nonce;
                            }
                            closeRequest.onload = () : void => {
                                if (!ObjectValidator.IsEmptyOrNull(closeRequest.parentNode)) {
                                    closeRequest.parentNode.removeChild(closeRequest);
                                }
                            };
                            document.body.appendChild(closeRequest);
                        }
                    };

                    const variableName : string = "pendingClose";
                    const persistence : IPersistenceHandler = PersistenceFactory.getPersistence("WebServiceClients");
                    if (persistence.Exists(variableName)) {
                        const pendingClose : string[] = persistence.Variable(variableName);
                        let index : number;
                        for (index = 0; index < pendingClose.length; index++) {
                            closeClient(pendingClose[index]);
                        }
                        persistence.Destroy(variableName);
                    }
                    WindowManager.getEvents().setBeforeRefresh(() : void => {
                        if (!ObjectValidator.IsEmptyOrNull(client) && client.CommunicationIsRunning()) {
                            let pendingClose : string[] = [];
                            if (persistence.Exists(variableName)) {
                                pendingClose = persistence.Variable(variableName);
                            }
                            if (pendingClose.indexOf(closeUrl) === -1) {
                                pendingClose.push(closeUrl);
                            }
                            persistence.Variable(variableName, pendingClose);
                        }
                    });
                    client.getEvents().OnClose(() : void => {
                        closeClient(closeUrl);
                    });
                });
            }
            client.setRequestFormatter(($data : IWebServiceRequestFormatterData) : void => {
                if (ObjectValidator.IsObject($data.value)) {
                    if (ObjectValidator.IsEmptyOrNull($data.value.id)) {
                        $data.value.id = StringUtils.getCrc(client.getUID());
                    }
                    $data.value.origin = request.getBaseUrl();
                    $data.value.status = HttpStatusType.CONTINUE;
                    $data.key = $data.value.id;
                }
            });
            client.setResponseFormatter(($data : any, $owner : IWebServiceClient,
                                         $onSuccess : ($value : any, $key? : number) => void,
                                         $onError : ($message : string | Error | Exception) => void) : void => {
                if (ObjectValidator.IsString($data)) {
                    $onSuccess($data);
                } else {
                    const protocolData : IWebServiceProtocol = <IWebServiceProtocol>$data;
                    let serverOrigin : string = $owner.getServerUrl();
                    if (StringUtils.Contains(serverOrigin, ":") && StringUtils.EndsWith(serverOrigin, "/")) {
                        serverOrigin = StringUtils.Substring(serverOrigin, 0, StringUtils.Length(serverOrigin) - 1);
                    }
                    this.validateOrigin(protocolData.origin, serverOrigin);
                    if (protocolData.status === HttpStatusType.SUCCESS) {
                        $onSuccess(protocolData, protocolData.id);
                    } else if (protocolData.status === HttpStatusType.ERROR) {
                        if (protocolData.type === "Server.Exception") {
                            const exceptionData : IWebServiceException = <IWebServiceException>protocolData.data;
                            const exception : Exception = new Exception(ObjectDecoder.Base64(exceptionData.message));
                            exception.Line(exceptionData.line);
                            exception.File(ObjectDecoder.Base64(exceptionData.file));
                            exception.Code(exceptionData.code);
                            exception.Stack(ObjectDecoder.Base64(exceptionData.stack));
                            $onError(exception);
                        } else if (protocolData.type === "Server.Timeout") {
                            Loader.getInstance().getHttpResolver().getEvents()
                                .FireEvent("" + $owner.getId(), WebServiceClientEventType.ON_TIMEOUT);
                        } else if (protocolData.type === "Request.Exception") {
                            protocolData.type = WebServiceClientEventType.ON_ERROR;
                            $onSuccess(protocolData, protocolData.id);
                        } else {
                            $onError("Unsupported response error type: " + protocolData.type);
                        }
                    } else if (protocolData.status === HttpStatusType.NOT_AUTHORIZED) {
                        if (Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed()) {
                            const postData : ArrayList<any> = new ArrayList<any>();
                            postData.Add(protocolData, "Protocol");
                            postData.Add($owner, "Client");
                            postData.Add(new HttpRequestParser(), "Referer");
                            Loader.getInstance().getHttpManager().ReloadTo("/ServerError/Http/NotAuthorized", postData, true);
                        } else {
                            // TODO(mkelnar) this is not optimal, however send over HttpServer will respond message back to hub
                            //  instead of translate to connector api call response (BE only), so connected directly to LCP processing.
                            //  This will be removed after LCP refactoring where handling of these situations will be clear and covered
                            //  directly by LCP code base.
                            //  Note: during LCP refactoring-> hub should return error message instead of called API parameters/args in
                            //  not authorized and any other errors!
                            protocolData.status = HttpStatusType.ERROR;
                            protocolData.type = WebServiceClientEventType.ON_ERROR;
                            protocolData.data = <IWebServiceException>{
                                message: "Method " + (<any>protocolData.data).name + " not authorized."
                            };
                            $onSuccess(protocolData, protocolData.id);
                        }
                    } else if (protocolData.status === HttpStatusType.NOT_FOUND) {
                        LogIt.Warning("Ignored unexpected response status 404. Continuing ...");
                    } else if (protocolData.status !== HttpStatusType.CONTINUE) {
                        $onError("Unsupported response status: " + protocolData.status);
                    }
                }
            });
        }
        return client;
    }

    protected static validateOrigin($clientOrigin : string, $serverOrigin : string) : void {
        if (!ObjectValidator.IsSet($clientOrigin) || (
            !StringUtils.Contains($clientOrigin, "127.0.0.1", "://localhost", $serverOrigin))) {
            LogIt.Info("Bad server origin: \"" + $clientOrigin + "\" from \"" + $serverOrigin + "\"", LogSeverity.LOW);
        }
    }
}
