/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { ProgressEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ProgressEventArgs.js";
import { TimeoutManager } from "@io-oidis-commons/Io/Oidis/Commons/Events/TimeoutManager.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import {
    FileSystemHandlerConnectorAsync,
    IFileSystemDownloadOptions,
    IFileSystemDownloadResponse, IShortcutOptions, ShortcutRunStyle
} from "../Connectors/FileSystemHandlerConnector.js";

export class AsyncFileSystemConnectorTest extends RuntimeTestRunner {
    private fileSystem : FileSystemHandlerConnectorAsync;
    private resourceData : string;

    constructor() {
        super();
        // this.setMethodFilter("testExists");
    }

    public async testExists() : Promise<void> {
        this.assertOk(await this.fileSystem.Exists(this.resourceData + "/testCaseA.cmd"));
    }

    public async testReadFile() : Promise<void> {
        const data : string = await this.fileSystem.Read(this.resourceData + "/testCaseA.cmd");
        this.assertOk(!ObjectValidator.IsEmptyOrNull(data));
        Echo.Printf("<b>File content:</b>");
        Echo.PrintCode(data);
    }

    public async testWriteFile() : Promise<void> {
        this.assertOk(await this.fileSystem.Write(this.resourceData + "/test.txt", "some test data"));
        await this.fileSystem.Delete(this.resourceData + "/test.txt");

        const data : string = await (new Promise<string>(($resolve : ($data : string) => void) : void => {
            let data : string = "some long long long long long long long long long long long long long long long long test data\r\n";
            const manager : TimeoutManager = new TimeoutManager();
            const prepareData : any = () : void => {
                if (StringUtils.Length(data) < 1024 * 1024 * 5) {
                    data += data;
                    manager.Add(prepareData);
                } else {
                    $resolve(data);
                }
            };
            manager.Add(prepareData);
            manager.Execute();
        }));
        this.assertOk(await this.fileSystem.Write(this.resourceData + "/largeTest.txt", data));
        await this.fileSystem.Delete(this.resourceData + "/largeTest.txt");
    }

    public async testRenameFile() : Promise<void> {
        this.assertOk(await this.fileSystem.Rename(this.resourceData + "/testCaseA.cmd", this.resourceData + "/renamed.cmd"));
        this.assertOk(await this.fileSystem.Rename(this.resourceData + "/renamed.cmd", this.resourceData + "/testCaseA.cmd"));
    }

    public async testCopyFile() : Promise<void> {
        this.assertOk(await this.fileSystem.Copy(this.resourceData + "/archTest1.zip", this.resourceData + "/copiedArch.zip"));
        await this.fileSystem.Delete(this.resourceData + "/copiedArch.zip");
    }

    public async testDeleteFile() : Promise<void> {
        await this.fileSystem.Copy(this.resourceData + "/archTest1.zip", this.resourceData + "/copiedArch.zip");
        this.assertOk(await this.fileSystem.Exists(this.resourceData + "/copiedArch.zip"));
        this.assertOk(await this.fileSystem.Delete(this.resourceData + "/copiedArch.zip"));
    }

    public async testGetTempPath() : Promise<void> {
        const data : string = await this.fileSystem.getTempPath();
        this.assertOk(!ObjectValidator.IsEmptyOrNull(data));
        Echo.Printf("<b>Temp folder path:</b>");
        Echo.PrintCode(data);
    }

    public async testCreateDirectory() : Promise<void> {
        this.assertOk(await this.fileSystem.CreateDirectory(this.resourceData + "/TestFolder"));
        await this.fileSystem.Delete(this.resourceData + "/TestFolder");
    }

    public async testDownload() : Promise<void> {
        let connectionId : number;
        this.addButton("Stop download", () : void => {
            this.fileSystem.AbortDownload(connectionId).then(($status : boolean) : void => {
                if ($status) {
                    Echo.Printf("Download has been aborted.");
                } else {
                    Echo.Printf("Download abort has been failed.");
                }
            });
        });
        Echo.Printf("complete: {0}", (await this.fileSystem
            .Download("https://gitlab.com/oidis/io-oidis-builder/raw/develop/README.md", {
                onChange: ($args : ProgressEventArgs) : void => {
                    Echo.Printf("change: {0}/{1}", $args.CurrentValue() + "", $args.RangeEnd() + "");
                },
                onStart : ($id : number) : void => {
                    connectionId = $id;
                    Echo.Printf("start");
                }
            })).tmpPath
        );

        const result : IFileSystemDownloadResponse = await this.fileSystem
            .Download(<IFileSystemDownloadOptions>{
                headers     : {
                    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0"
                },
                streamOutput: true,
                url         : "https://gitlab.com/oidis/io-oidis-builder/raw/develop/README.md"
            }, {
                onChange: ($args : ProgressEventArgs) : void => {
                    Echo.Printf("change: {0}/{1}", $args.CurrentValue() + "", $args.RangeEnd() + "");
                },
                onStart : () : void => {
                    Echo.Printf("start");
                }
            });
        Echo.Printf(result.headers);
        Echo.PrintCode(result.body);

        try {
            await this.fileSystem.Download(<IFileSystemDownloadOptions>{
                url: "https://bitbucket.org/unknown-for-test/io-oidis-builder/" +
                    "raw/f897ba2642a2a6512e8b2bbf1f0b44bfee75a474/README.md"
            });
            Echo.Printf("<b>Failed to call error handler for Download</b>");
        } catch (ex) {
            Echo.Printf("download error handler called with: {0}", ex.message);
        }
    }

    public async testUnpack() : Promise<void> {
        let path : string = await this.fileSystem.Unpack(this.resourceData + "/archTest1.zip");
        this.assertOk(!ObjectValidator.IsEmptyOrNull(path));
        if (!ObjectValidator.IsEmptyOrNull(path)) {
            Echo.Printf("<b>File unpacked to:</b>");
            Echo.Printf(path);
            await this.fileSystem.Delete(path);
        }

        path = await this.fileSystem.Unpack(this.resourceData + "/archTest2.tar.bz2");
        this.assertOk(!ObjectValidator.IsEmptyOrNull(path));
        if (!ObjectValidator.IsEmptyOrNull(path)) {
            Echo.Printf("<b>File unpacked to:</b>");
            Echo.Printf(path);
            await this.fileSystem.Delete(path);
        }
        try {
            /// TODO: use some corrupted archive file for more robust use-case
            await this.fileSystem.Unpack("*//*/*-+*$unknown");
            Echo.Printf("Failed to handle error for Unpack");
        } catch (ex) {
            Echo.Printf("Error handler called for Unpack with: {0}", ex.message);
        }
    }

    public async testPack() : Promise<void> {
        let path : string = await this.fileSystem.Pack(this.resourceData + "/archTest", {type: "zip"});
        this.assertOk(!ObjectValidator.IsEmptyOrNull(path));
        if (!ObjectValidator.IsEmptyOrNull(path)) {
            Echo.Printf("<b>File packed to:</b>");
            Echo.Printf(path);
            await this.fileSystem.Delete(path);
        }

        path = await this.fileSystem.Pack(this.resourceData + "/archTest", {type: "tar.bz2"});
        this.assertOk(!ObjectValidator.IsEmptyOrNull(path));
        if (!ObjectValidator.IsEmptyOrNull(path)) {
            Echo.Printf("<b>File packed to:</b>");
            Echo.Printf(path);
            await this.fileSystem.Delete(path);
        }

        try {
            await this.fileSystem.Pack(this.resourceData + "/unknown");
            Echo.Printf("Failed to handle error for Unpack");
        } catch (ex) {
            Echo.Printf("Error handler called for Unpack with: {0}", ex.message);
        }
    }

    public async testCopyDir() : Promise<void> {
        Echo.Println("Directory copy progress: <span id=\"testCopyDirBar\"></span>");
        this.assertOk(await this.fileSystem.Copy(this.resourceData, this.resourceData + "_Copy",
            ($args : ProgressEventArgs) : void => {
                ElementManager.setInnerHtml("testCopyDirBar", $args.CurrentValue() + "/" + $args.RangeEnd());
            }));
    }

    public async testDeleteDir() : Promise<void> {
        Echo.Println("Directory delete progress: <span id=\"testDeleteDirBar\"></span>");
        this.assertOk(await this.fileSystem
            .Delete(this.resourceData + "_Copy", ($args : ProgressEventArgs) : void => {
                ElementManager.setInnerHtml("testDeleteDirBar", $args.CurrentValue() + "/" + $args.RangeEnd());
            }));
    }

    public async __IgnoretestCreateShortcut() : Promise<void> {
        /// TODO: use correct parameters and limit it only for Windows
        this.assertOk(await this.fileSystem.CreateShortcut("source", "destination", <IShortcutOptions>{
            args      : ["arg1", "arg2"],
            desc      : "descritpion",
            hotkey    : 5,
            icon      : "path-to-icon",
            iconIndex : 3,
            runStyle  : ShortcutRunStyle.NORMAL,
            workingDir: "working directory"
        }));

        try {
            await this.fileSystem.CreateShortcut("", "", <IShortcutOptions>{
                args      : ["arg1", "arg2"],
                desc      : "descritpion",
                hotkey    : 5,
                icon      : "path-to-icon",
                iconIndex : 3,
                runStyle  : ShortcutRunStyle.NORMAL,
                workingDir: "working directory"
            });
            this.assertOk(false, "Failed to handle error for Create shortcut.");
        } catch (ex) {
            Echo.Printf("Error handler called for Create shortcut with: {0}", ex.message);
            this.assertOk(true);
        }
    }

    public async __IgnoretestGetLinkTargetPath() : Promise<void> {
        /// TODO: unsupported on backend
        const path : string = await this.fileSystem.getLinkTargetPath("D:/SystemTemp");
        this.assertOk(!ObjectValidator.IsEmptyOrNull(path));
        if (!ObjectValidator.IsEmptyOrNull(path)) {
            Echo.Println("real path: " + path);
        } else {
            Echo.Println("failed");
        }
    }

    public async __IgnoretestGetNetworkMap() : Promise<void> {
        /// TODO: unsupported on backend
        Echo.Printf(await this.fileSystem.getNetworkMap());
        Echo.Printf(await this.fileSystem.getNetworkMap(true));
    }

    public async __IgnoretestGetRootFolders() : Promise<void> {
        /// TODO: fatal error on backend
        Echo.Printf(await this.fileSystem.getPathMap());
    }

    public async testGetDirectoryContent() : Promise<void> {
        /// TODO: not working correctly...
        Echo.Printf(await this.fileSystem.getDirectoryContent("C:\\Windows"));
    }

    public async __IgnoretestGetPathMap() : Promise<void> {
        /// TODO: fatal error on backend
        Echo.Printf(await this.fileSystem.getPathMap("C:\\Program Files\\"));
    }

    public async testExpand() : Promise<void> {
        Echo.Printf(await this.fileSystem.Expand("C:/Windows/*"));
        Echo.Printf(await this.fileSystem.Expand(["C:/Program Files/*", "C:/Windows/*"]));
    }

    public async __IgnoreAgentTest() : Promise<void> {
        /// TODO: try automatic back-end registration to https://hub.oidis.io/connector.config.jsonp
        //        otherwise it requires locally running hub and can't be automated
        const connector : FileSystemHandlerConnectorAsync = new FileSystemHandlerConnectorAsync();
        connector.setAgent("test-connector-win", "http://localhost:8090/connector.config.jsonp");
        this.assertOk(await connector.Exists(this.getAbsoluteRoot()));
    }

    protected before() : void {
        this.fileSystem = new FileSystemHandlerConnectorAsync();
        this.fileSystem.getEvents().OnError(($args : ErrorEventArgs) : void => {
            Echo.Println($args.Message());
        });
        this.resourceData = this.getAbsoluteRoot() + "/test/resource/data/Io/Oidis/Services/RuntimeTests";
    }
}
/* dev:end */
