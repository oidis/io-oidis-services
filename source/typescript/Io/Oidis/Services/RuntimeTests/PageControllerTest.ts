/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralEventOwner } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/GeneralEventOwner.js";
import { ResizeableType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/ResizeableType.js";
import { MoveEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/MoveEventArgs.js";
import { ResizeBarEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/ResizeBarEventArgs.js";
import { GuiObjectManager } from "@io-oidis-gui/Io/Oidis/Gui/GuiObjectManager.js";
import { IDragBar } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IDragBar.js";
import { IResizeBar } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IResizeBar.js";
import { IButtonEvents } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/IButtonEvents.js";
import { IDragBarEvents } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/IDragBarEvents.js";
import { IResizeBarEvents } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/IResizeBarEvents.js";
import { IGuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommons.js";
import { IGuiElement } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { IButton } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/UserControls/IButton.js";
import { BasePanel } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanel.js";
import { BasePanelViewer } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { FormsObject } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/FormsObject.js";
import { GuiCommons } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { BasePageController } from "../HttpProcessor/Resolvers/BasePageController.js";

export class MockButton extends FormsObject implements IButton {
    private buttonType : string;

    constructor($buttonType? : any, $id? : string) {
        super($id);
        this.GuiType($buttonType);
    }

    public GuiType($buttonType? : any) : any {
        if (ObjectValidator.IsSet($buttonType)) {
            this.buttonType = $buttonType;
        }
        return this.buttonType;
    }

    public Text($value? : string) : string {
        return "";
    }

    public getEvents() : IButtonEvents {
        return <IButtonEvents>super.getEvents();
    }

    public IconName($value? : any) : any {
        return "";
    }

    public IsSelected($value? : boolean) : boolean {
        return false;
    }

    protected innerHtml() : IGuiElement {
        return this.addElement(this.Id() + "_Type").StyleClassName(this.buttonType);
    }

    protected innerCode() : IGuiElement {
        this.getEvents().Subscriber(this.Id() + "_Type");

        return super.innerCode();
    }

    protected cssContainerName() : string {
        return "Button";
    }
}

export class MockResizeBar extends GuiCommons implements IResizeBar {
    private resizeType : ResizeableType;

    public static setWidth($element : MockResizeBar, $value : number) : void {
        ElementManager.setWidth($element.Id() + "_Position", $value);
    }

    public static setHeight($element : MockResizeBar, $value : number) : void {
        ElementManager.setHeight($element.Id() + "_Position", $value);
    }

    constructor($resizeableType? : ResizeableType, $id? : string) {
        super($id);
        this.ResizeableType($resizeableType);
    }

    public ResizeableType($type? : ResizeableType) : ResizeableType {
        if (ObjectValidator.IsSet($type)) {
            this.resizeType = $type;
        }
        return this.resizeType;
    }

    public getEvents() : IResizeBarEvents {
        return <IResizeBarEvents>super.getEvents();
    }

    protected innerCode() : IGuiElement {
        this.getEvents().Subscriber(this.Id() + "_Position");

        this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_START,
            ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                this.getEvents().FireAsynchronousMethod(() : void => {
                    if ($manager.IsActive(MockResizeBar)) {
                        $args.StopAllPropagation();
                        const element : MockResizeBar = <MockResizeBar>$manager.getActive(MockResizeBar).getFirst();
                        const eventArgs : ResizeBarEventArgs = new ResizeBarEventArgs();
                        eventArgs.Owner(element);
                        eventArgs.ResizeableType(element.ResizeableType());
                        this.getEventsManager().FireEvent(element, EventType.ON_RESIZE_START, eventArgs, false);
                    }
                });
            });

        this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE,
            ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                this.getEvents().FireAsynchronousMethod(() : void => {
                    if ($manager.IsActive(MockResizeBar)) {
                        $args.StopAllPropagation();
                        const element : MockResizeBar = <MockResizeBar>$manager.getActive(MockResizeBar).getFirst();
                        switch (element.ResizeableType()) {
                        case ResizeableType.HORIZONTAL_AND_VERTICAL:
                            document.body.style.cursor = "nw-resize";
                            break;
                        case ResizeableType.HORIZONTAL:
                            document.body.style.cursor = "w-resize";
                            break;
                        case ResizeableType.VERTICAL:
                            document.body.style.cursor = "n-resize";
                            break;
                        default :
                            document.body.style.cursor = "default";
                            break;
                        }

                        let distanceX : number = 0;
                        let distanceY : number = 0;

                        if (element.ResizeableType() === ResizeableType.HORIZONTAL_AND_VERTICAL ||
                            element.ResizeableType() === ResizeableType.HORIZONTAL) {
                            distanceX = $args.getDistanceX();
                        }
                        if (element.ResizeableType() === ResizeableType.HORIZONTAL_AND_VERTICAL ||
                            element.ResizeableType() === ResizeableType.VERTICAL) {
                            distanceY = $args.getDistanceY();
                        }

                        const eventArgs : ResizeBarEventArgs = new ResizeBarEventArgs();
                        eventArgs.Owner(element);
                        eventArgs.ResizeableType(element.ResizeableType());
                        eventArgs.DistanceX(distanceX);
                        eventArgs.DistanceY(distanceY);
                        this.getEventsManager().FireEvent(element, EventType.ON_RESIZE_CHANGE, eventArgs, false);
                    }
                });
            });

        this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_COMPLETE,
            ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                this.getEvents().FireAsynchronousMethod(() : void => {
                    $args.StopAllPropagation();
                    const elements : ArrayList<IGuiCommons> = $manager.getActive(<IClassName>MockResizeBar);
                    elements.foreach(($element : MockResizeBar) : void => {
                        const eventArgs : ResizeBarEventArgs = new ResizeBarEventArgs();
                        eventArgs.Owner($element);
                        eventArgs.ResizeableType($element.ResizeableType());
                        this.getEventsManager().FireEvent($element, EventType.ON_RESIZE_COMPLETE, eventArgs, false);
                        this.getEventsManager().FireEvent($element, EventType.ON_RESIZE, eventArgs, false);
                        $manager.setActive($element, false);
                    });
                });
            });

        this.getEvents().setOnMouseDown(($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
            $manager.setActive(this, true);
        });

        this.getEvents().setOnMouseUp(() : void => {
            document.body.style.cursor = "default";
        });

        return super.innerCode();
    }

    protected innerHtml() : IGuiElement {
        return this.addElement().Add(this.addElement(this.Id() + "_Position").StyleClassName(this.resizeType));
    }

    protected cssContainerName() : string {
        return "ResizeBar";
    }
}

export class MockDragBar extends GuiCommons implements IDragBar {

    public getEvents() : IDragBarEvents {
        return <IDragBarEvents>super.getEvents();
    }

    protected innerCode() : IGuiElement {
        this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_START,
            ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                this.getEvents().FireAsynchronousMethod(() : void => {
                    if ($manager.IsActive(MockDragBar)) {
                        $args.StopAllPropagation();
                        const element : MockDragBar = <MockDragBar>$manager.getActive(MockDragBar).getFirst();
                        $args.Owner(element);
                        this.getEventsManager().FireEvent(element, EventType.ON_DRAG_START, $args, false);
                        document.body.style.cursor = "move";
                    }
                });
            });

        this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE,
            ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                this.getEvents().FireAsynchronousMethod(() : void => {
                    if ($manager.IsActive(MockDragBar)) {
                        $args.StopAllPropagation();
                        const element : MockDragBar = <MockDragBar>$manager.getActive(MockDragBar).getFirst();
                        $args.Owner(element);
                        this.getEventsManager().FireEvent(element, EventType.ON_DRAG_CHANGE, $args);
                    }
                });
            });

        this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_COMPLETE,
            ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                this.getEvents().FireAsynchronousMethod(() : void => {
                    $args.StopAllPropagation();
                    const elements : ArrayList<IGuiCommons> = $manager.getActive(<IClassName>MockDragBar);
                    elements.foreach(($element : MockDragBar) : void => {
                        $args.Owner($element);
                        this.getEventsManager().FireEvent($element, EventType.ON_DRAG_COMPLETE, $args, false);
                        $manager.setActive($element, false);
                    });
                });
            });

        this.getEvents().setOnMouseDown(($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
            $manager.setActive(this, true);
        });

        this.getEvents().setOnMouseUp(() : void => {
            document.body.style.cursor = "default";
        });

        return super.innerCode();
    }

    protected cssContainerName() : string {
        return "DragBar";
    }
}

export class MockPanel extends BasePanel {

    protected innerHtml() : IGuiElement {
        return this.addElement()
            .Add(this.addElement().Width(500).Height(500)
                .setAttribute("background-color", "silver")
                .setAttribute("position", "absolute")
                .Add("validation element with size: 500x500 px")
            )
            .Add(this.addElement().StyleClassName("Logo")
                .Add(this.addElement().StyleClassName("Oidis"))
            );
    }
}

export class MockViewer extends BasePanelViewer {
    constructor() {
        super();
        this.setInstance(new MockPanel());
    }
}

export class PageControllerTest extends BasePageController {

    constructor() {
        super();

        this.setModelClassName(MockViewer);

        this.minimizeButton.GuiType("AppMinimize");
        this.maximizeButton.GuiType("AppMaximize");
        this.closeButton.GuiType("AppClose");
        this.setResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);
    }

    protected getAppImageButtonClass() : any {
        return MockButton;
    }

    protected getAppResizeBarClass() : any {
        return MockResizeBar;
    }

    protected getAppDragBarClass() : any {
        return MockDragBar;
    }

    protected resolver() : void {
        Echo.Println("<style>" +
            ".AppDragBar {cursor: move;}" +
            ".AppButtons {top: 5px !important; right: 5px !important;}" +
            ".Button {float: left; margin-left: 3px;}" +
            ".Button .AppMinimize {border-radius: 10px; background-color: green; width: 20px; height: 20px;}" +
            ".Button .AppMaximize {border-radius: 10px; background-color: orange; width: 20px; height: 20px;}" +
            ".Button .AppClose {border-radius: 10px; background-color: red; width: 20px; height: 20px;}" +
            ".ResizeBar {float: right; left: 0; top: 1px; position: relative;}" +
            ".ResizeBar .X {cursor: w-resize;}" +
            ".ResizeBar .Y {cursor: n-resize;}" +
            ".ResizeBar .XY {cursor: nw-resize;}" +
            ".MockPanel {background-color: gainsboro;}" +
            "</style>");
        super.resolver();
    }
}
/* dev:end */
