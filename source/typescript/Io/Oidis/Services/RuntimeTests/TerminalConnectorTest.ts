/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { TerminalConnector } from "../Connectors/TerminalConnector.js";
import { IWebServiceClient } from "../Interfaces/IWebServiceClient.js";
import { WebServiceClientFactory } from "../WebServiceApi/WebServiceClientFactory.js";

export class TerminalConnectorTest extends RuntimeTestRunner {

    private connector : TerminalConnector;

    public before() : void {
        this.connector = new TerminalConnector();
        this.connector.getEvents().OnError(($args : ErrorEventArgs) : void => {
            Echo.Println($args.Message());
        });

        const client : IWebServiceClient = WebServiceClientFactory.getClientById(this.connector.getId());
        client.getServerPath(($path : string) : void => {
            Echo.Printf("<b>Server configuration:</b> uri - \"{0}\", server root - \"{1}\"", client.getServerUrl(), $path);
        });
    }

    public CmdSyncTest() : void {
        this.addButton("test execute (wui --version)", () : void => {
            this.connector
                .Execute("wui", ["--version"])
                .OnMessage(($data : string) : void => {
                    Echo.Print($data);
                })
                .Then(($code : number, $stdout? : string, $stderr? : string) : void => {
                    Echo.Printf("out: {0}</br>err: {1}", $stdout, $stderr);
                    if ($code === 0) {
                        Echo.Println("succeed");
                    } else {
                        Echo.Println("failed");
                    }
                });
        });

        this.addButton("test execute (cmd /c dir)", () : void => {
            this.connector
                .Execute("cmd", ["/c", "dir"], "../../")
                .OnMessage(($data : string) : void => {
                    Echo.Print($data);
                })
                .Then(($code : number, $stdout? : string, $stderr? : string) : void => {
                    Echo.Printf("out: {0}</br>err: {1}", $stdout, $stderr);
                    if ($code === 0) {
                        Echo.Println("succeed");
                    } else {
                        Echo.Println("failed");
                    }
                });
        });

        this.addButton("test execute (ping 127.0.0.1)", () : void => {
            this.connector
                .Execute("ping", ["127.0.0.1"])
                .OnMessage(($data : string) : void => {
                    Echo.Print($data);
                })
                .Then(($code : number) : void => {
                    if ($code === 0) {
                        Echo.Println("succeed");
                    } else {
                        Echo.Println("failed");
                    }
                });
        });

        this.addButton("test spawn (ping 127.0.0.1", () : void => {
            this.connector
                .Spawn("ping", ["127.0.0.1"])
                .OnMessage(($data : string) : void => {
                    Echo.Print($data);
                })
                .Then(($code : number) : void => {
                    if ($code === 0) {
                        Echo.Println("succeed");
                    } else {
                        Echo.Println("failed");
                    }
                });
        });

        this.addButton("get sync WUI Info - OnMessage", () : void => {
            this.connector
                .Spawn("wui", ["--version"])
                .OnMessage(($data : string) : void => {
                    if ($data === ".") {
                        Echo.Print($data);
                    } else {
                        Echo.Println($data);
                    }
                })
                .Then(($code : number) : void => {
                    if ($code === 0) {
                        this.connector
                            .Spawn("wui", ["--path"]);
                    }
                });
        });

        this.addButton("get sync WUI Info - stdout", () : void => {
            this.connector
                .Spawn("wui", ["--version"])
                .Then(($code : number, $stdout : string) : void => {
                    if ($code === 0) {
                        Echo.Printf($stdout);
                        this.connector
                            .Spawn("wui", ["--path"])
                            .Then(($code : number, $stdout : string) : void => {
                                if ($code === 0) {
                                    Echo.Printf($stdout);
                                }
                            });
                    }
                });
        });

        this.addButton("run sync testCase*.cmd", () : void => {
            this.connector
                .Execute("testCaseA.cmd", [],
                    this.getAbsoluteRoot() + "/test/resource/data/Io/Oidis/Services/RuntimeTests")
                .OnMessage(($data : string) : void => {
                    Echo.Println($data);
                })
                .Then(($code : number) : void => {
                    Echo.Print("testCaseA - ");
                    if ($code === 0) {
                        Echo.Println("succeed");
                    } else {
                        Echo.Println("failed");
                    }
                });

            this.connector
                .Execute("testCaseA.cmd", [],
                    {cwd: this.getAbsoluteRoot() + "/test/resource/data/Io/Oidis/Services/RuntimeTests"})
                .OnMessage(($data : string) : void => {
                    Echo.Println($data);
                })
                .Then(($code : number) : void => {
                    Echo.Print("testCaseA - ");
                    if ($code === 0) {
                        Echo.Println("succeed");
                    } else {
                        Echo.Println("failed");
                    }
                });

            this.connector
                .Execute("testCaseA.cmd", [], {
                    env: {
                        PATH: this.getAbsoluteRoot() + "/test/resource/data/Io/Oidis/Services/RuntimeTests;%PATH%"
                    }
                })
                .OnMessage(($data : string) : void => {
                    Echo.Println($data);
                })
                .Then(($code : number) : void => {
                    Echo.Print("testCaseA - ");
                    if ($code === 0) {
                        Echo.Println("succeed");
                    } else {
                        Echo.Println("failed");
                    }
                });
        });
    }

    public CmdAsyncTest() : void {
        this.addButton("get async WUI Info", () : void => {
            this.connector.Spawn("wui", ["--version"])
                .Then(($code : number) : void => {
                    Echo.Print("async: spawn wui --version");
                    if ($code === 0) {
                        Echo.Println("succeed");
                    } else {
                        Echo.Println("failed");
                    }
                });
            this.connector.Spawn("wui", ["--path"])
                .Then(($code : number) : void => {
                    Echo.Print("async: spawn wui --path");
                    if ($code === 0) {
                        Echo.Println("succeed");
                    } else {
                        Echo.Println("failed");
                    }
                });
        });

        this.addButton("test spawn async (ping 127.0.0.1 and ping 8.8.8.8)", () : void => {
            this.connector.Spawn("ping", ["127.0.0.1"])
                .OnMessage(($data : string) : void => {
                    Echo.Println($data);
                })
                .Then(($code : number) : void => {
                    Echo.Print("ping 127.0.0.1 - ");
                    if ($code === 0) {
                        Echo.Println("succeed");
                    } else {
                        Echo.Println("failed");
                    }
                });
            this.connector.Spawn("ping", ["8.8.8.8"])
                .OnMessage(($data : string) : void => {
                    Echo.Println($data);
                })
                .Then(($code : number) : void => {
                    Echo.Print("ping 8.8.8.8 - ");
                    if ($code === 0) {
                        Echo.Println("succeed");
                    } else {
                        Echo.Println("failed");
                    }
                });
        });

        this.addButton("test spawn async (test *.cmd)", () : void => {

            this.connector.Spawn("testCaseA.cmd", [], "./test/resource/data/Io/Oidis/Services/RuntimeTests")
                .OnMessage(($data : string) : void => {
                    Echo.Println($data);
                })
                .Then(($code : number) : void => {
                    Echo.Print("testCaseA - ");
                    if ($code === 0) {
                        Echo.Println("succeed");
                    } else {
                        Echo.Println("failed");
                    }
                });
            this.connector.Spawn("testCaseB.cmd", [], "./test/resource/data/Io/Oidis/Services/RuntimeTests")
                .OnMessage(($data : string) : void => {
                    Echo.Println($data);
                })
                .Then(($code : number) : void => {
                    Echo.Print("testCaseB - ");
                    if ($code === 0) {
                        Echo.Println("succeed");
                    } else {
                        Echo.Println("failed");
                    }
                });
        });
    }

    public OpenTest() : void {
        this.addButton("open executable", () : void => {
            this.connector.Open("notepad.exe");
        });
    }

    public ElevateTest() : void {
        this.addButton("elevate process", () : void => {
            this.connector.Elevate("AT");
        });
    }

    public AgentTest() : void {
        const connector : TerminalConnector = new TerminalConnector();
        connector.setAgent("test-connector-win", "https://localhost.oidis.io/connector.config.jsonp");

        this.addButton("Forward to agent", () : void => {
            connector
                .Spawn("wui", ["path"])
                .OnMessage(($data : string) : void => {
                    Echo.Printf($data);
                })
                .Then(($exitCode : number, $stdout : string, $stderr : string) : void => {
                    Echo.Printf($exitCode);
                });
        });
    }
}
/* dev:end */
