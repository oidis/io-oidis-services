/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { VirtualBoxConnector } from "../Connectors/VirtualBoxConnector.js";

export class VirtualBoxConnectorTest extends RuntimeTestRunner {

    private connector : VirtualBoxConnector;

    public before() : void {
        this.connector = new VirtualBoxConnector();
        this.connector.getEvents().OnError(($args : ErrorEventArgs) : void => {
            Echo.Println($args.Message());
        });
    }

    public testConnection() : void {
        let sessionId : string = "";
        this.addButton("Start machine", () : void => {
            this.connector
                .Start("Clean Win 7")
                .Then(($success : boolean, $sessionId : string) : void => {
                    if ($success) {
                        sessionId = $sessionId;
                        Echo.Printf("Hooray, virtual machine is running. Connected under: {0}", sessionId);
                    } else {
                        Echo.Printf("<b>Sorry, bad luck :-)</b>");
                    }
                });
        });

        this.addButton("Copy file", () : void => {
            this.connector
                .Copy(sessionId,
                    "C:/Windows/System32/notepad.exe",
                    "C:/Users/Guest/AppData/Local/Temp/notepad.exe")
                .Then(($success : boolean) : void => {
                    if ($success) {
                        Echo.Printf("File copy successful");
                    } else {
                        Echo.Printf("<b>File copy failed</b>");
                    }
                });
        });

        this.addButton("Execute file", () : void => {
            this.connector
                .Execute(sessionId, "C:/Windows/System32/notepad.exe")
                .Then(($success : boolean) : void => {
                    if ($success) {
                        Echo.Printf("File execute successful");
                    } else {
                        Echo.Printf("<b>File execute failed</b>");
                    }
                });
        });
    }
}
/* dev:end */
