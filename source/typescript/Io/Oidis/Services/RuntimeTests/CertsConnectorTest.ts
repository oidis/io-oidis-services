/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { IRuntimeTestPromise } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { CertsConnector } from "../Connectors/CertsConnector.js";

export class CertsConnectorTest extends RuntimeTestRunner {
    private connector : CertsConnector;

    public before() : void {
        this.connector = new CertsConnector();
        this.connector.getEvents().OnError(($args : ErrorEventArgs) : void => {
            Echo.Println($args.Message());
        });
    }

    public testGetCertsForLocalhost() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            this.connector
                .getCertsFor("localhost.oidis.io")
                .Then(($cert : string, $key : string) : void => {
                    this.assertEquals(ObjectValidator.IsEmptyOrNull($cert), false);
                    this.assertEquals(ObjectValidator.IsEmptyOrNull($key), false);
                    $done();
                });
        };
    }

    public testGetCertsFromBlackList() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            this.connector
                .getCertsFor("www.oidis.io")
                .Then(($cert : string, $key : string) : void => {
                    this.assertEquals(ObjectValidator.IsEmptyOrNull($cert), true);
                    this.assertEquals(ObjectValidator.IsEmptyOrNull($key), true);
                    $done();
                });
        };
    }
}
/* dev:end */
