/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { AgentsRegisterConnector, IAgentInfo } from "../Connectors/AgentsRegisterConnector.js";
import { IWebServiceClient } from "../Interfaces/IWebServiceClient.js";
import { Loader } from "../Loader.js";
import { WebServiceClientFactory } from "../WebServiceApi/WebServiceClientFactory.js";

export class AgentsRegisterConnectorTest extends RuntimeTestRunner {

    private connector : AgentsRegisterConnector;

    public before() : void {
        this.connector = new AgentsRegisterConnector();
        // this.connector = new AgentsRegisterConnector(true, "http://192.168.99.1:80/connector.config.jsonp");

        this.connector.getEvents().OnError(($args : ErrorEventArgs) : void => {
            Echo.Println($args.Message());
        });

        const client : IWebServiceClient = WebServiceClientFactory.getClientById(this.connector.getId());
        client.getServerPath(($path : string) : void => {
            Echo.Printf("<b>Hub configuration:</b> uri - \"{0}\", server root - \"{1}\"", client.getServerUrl(), $path);
        });
    }

    public AgentTest() : void {
        this.addButton("getAgentsList", () : void => {
            this.connector.getAgentsList()
                .Then(($list : IAgentInfo[]) : void => {
                    Echo.Printf(JSON.stringify($list, null, 2));
                });
        });

        this.addButton("RegisterAgent", () : void => {
            this.connector.RegisterAgent({name: "test-connector", platform: "unknown", domain: "none", version: "newest"})
                .OnMessage(($data : any) : void => {
                    Echo.Printf("Message data: " + JSON.stringify($data));
                })
                .Then(($success : boolean) : void => {
                    if ($success) {
                        Echo.Printf("Registration success");
                    } else {
                        Echo.Printf("Registration failed");
                    }
                });
        });

        this.addButton("RegisterAgentWithCredentials", () : void => {
            this.connector.RegisterAgent(
                {
                    credentials: {
                        authToken: "",
                        userName : "admin"
                    },
                    domain     : "none-with-credentials",
                    name       : "test-connector-with-credentials",
                    packageInfo: {
                        buildTime: new Date(Loader.getInstance().getEnvironmentArgs().getBuildTime()).getTime(),
                        name     : Loader.getInstance().getEnvironmentArgs().getProjectName(),
                        platform : Loader.getInstance().getEnvironmentArgs().getPlatform(),
                        release  : Loader.getInstance().getEnvironmentArgs().getReleaseName(),
                        version  : Loader.getInstance().getEnvironmentArgs().getProjectVersion()
                    },
                    platform   : "unknown",
                    version    : "newest"
                })
                .OnMessage(($data : any) : void => {
                    Echo.Printf("Message with credentials data: " + JSON.stringify($data));
                })
                .Then(($success : boolean) : void => {
                    if ($success) {
                        Echo.Printf("Registration with credentials succeed");
                    } else {
                        Echo.Printf("Registration with credentials failed");
                    }
                });
        });

        this.addButton("ForwardMessage", () : void => {
            this.connector.ForwardMessage(StringUtils.getSha1("none" + "unknown"), "some data")
                .Then(($status : boolean) : void => {
                    Echo.Printf("Forward message status: " + $status);
                });
        });

        this.addButton("ForwardMessageWithCredentials", () : void => {
            this.connector.ForwardMessage(StringUtils.getSha1("none-with-credentials" + "unknown"), "some data")
                .Then(($status : boolean) : void => {
                    Echo.Printf("Forward message with credentials status: " + $status);
                });
        });
    }
}
/* dev:end */
