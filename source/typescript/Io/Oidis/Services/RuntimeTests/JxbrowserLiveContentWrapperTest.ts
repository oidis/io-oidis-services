/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { IWebServiceClient } from "../Interfaces/IWebServiceClient.js";
import { LiveContentWrapper } from "../WebServiceApi/LiveContentWrapper.js";
import { WebServiceClientFactory } from "../WebServiceApi/WebServiceClientFactory.js";

export class JxbrowserLiveContentWrapperTest extends RuntimeTestRunner {

    private client : IWebServiceClient;
    private serverClassName : string = "Io.Oidis.Jcommons.TestClass";

    public setUp() : void {
        this.client = WebServiceClientFactory.getClient(WebServiceClientType.JXBROWSER_BRIDGE);
        this.client.setResponseFormatter(null);
    }

    public testMethodCalls() : void {
        this.addButton("string TestMethod(double, float)", () : void => {
            LiveContentWrapper.InvokeMethod(this.client, this.serverClassName, "TestMethod",
                1500.16498494, 2500.51561684986).Then(($response : string) => {
                Echo.Println("Returned : " + $response);
            });
        });

        this.addButton("float TestMethod(string, float)", () : void => {
            LiveContentWrapper.InvokeMethod(this.client, this.serverClassName, "TestMethod",
                "Test String", 45645.5156635434541684986).Then(($response : number) => {
                Echo.Println("Returned : " + $response);
            });
        });

        this.addButton("Non-existent method", () : void => {
            LiveContentWrapper.InvokeMethod(this.client, this.serverClassName, "TestMethodd",
                "Test String", 45645.5156635434541684986).Then(($response : number) => {
                Echo.Println("Returned : " + $response);
            });
        });
    }
}
/* dev:end */
