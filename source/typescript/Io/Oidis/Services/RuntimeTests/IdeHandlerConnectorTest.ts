/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { IdeHandlerConnector } from "../Connectors/IdeHandlerConnector.js";

export class IdeHandlerConnectorTest extends RuntimeTestRunner {

    private ideHandler : IdeHandlerConnector = new IdeHandlerConnector();

    public FileChooserTest() : void {
        this.addButton("Open file chooser", () : void => {
            this.ideHandler
                .OpenFileChooser("C:/OidisFramework/", ["*.txt"])
                .Then(($paths : string[]) : void => {
                    Echo.Println("Chosen files are : " + $paths);
                });
        });
    }

    public DialogTest() : void {
        this.addButton("Open info dialog", () : void => {
            this.ideHandler
                .OpenInfoDialog("Mock Title", "Mock Message")
                .Then(($success : boolean) : void => {
                    if ($success) {
                        Echo.Println("Info dialog opened");
                    } else {
                        Echo.Println("Info dialog opening failed");
                    }
                });
        });
    }

    public WorkspaceRootTest() : void {
        this.addButton("Get workspace root", () : void => {
            this.ideHandler
                .getWorkspaceRoot()
                .Then(($path : string) : void => {
                    Echo.Println("Workspace path : " + $path);
                });
        });
    }

    public AddEventListenerTest() : void {
        this.addButton("Add event listener", () : void => {
            this.ideHandler
                .AddEventListener("testEvent", ($eventData1 : string, $eventData2 : string) => {
                    Echo.Println($eventData1);
                    Echo.Println($eventData2);
                });
        });
    }
}
/* dev:end */
