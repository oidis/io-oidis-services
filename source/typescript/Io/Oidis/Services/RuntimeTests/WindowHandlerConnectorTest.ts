/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { KeyMap } from "@io-oidis-gui/Io/Oidis/Gui/Enums/KeyMap.js";
import { WindowCornerType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/WindowCornerType.js";
import { KeyEventArgs } from "@io-oidis-gui/Io/Oidis/Gui/Events/Args/KeyEventArgs.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { WindowManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/WindowManager.js";
import {
    IWindowHandlerCookie, IWindowHandlerFileDialogSettings, IWindowHandlerNotifyIcon,
    IWindowHandlerOpenOptions,
    IWindowHandlerScriptExecuteOptions,
    WindowHandlerConnector
} from "../Connectors/WindowHandlerConnector.js";
import { NotifyBalloonIconType } from "../Enums/NotifyBalloonIconType.js";
import { TaskBarProgressState } from "../Enums/TaskBarProgressState.js";
import { WindowStateType } from "../Enums/WindowStateType.js";
import { IWindowChangedEvent } from "../Interfaces/Events/IWindowChangedEvent.js";

export class WindowHandlerConnectorTest extends RuntimeTestRunner {

    private windowHandler : WindowHandlerConnector = new WindowHandlerConnector();

    public WindowStateTest() : void {
        this.addButton("Minimize", () : void => {
            this.windowHandler
                .Minimize()
                .Then(($success : boolean) : void => {
                    if ($success === true) {
                        Echo.Print("Minimized");
                    } else {
                        Echo.Print("Minimize failed");
                    }
                });
        });

        this.addButton("Maximize", () : void => {
            this.windowHandler
                .Maximize()
                .Then(($success : boolean) : void => {
                    if ($success === true) {
                        Echo.Print("Maximize");
                    } else {
                        Echo.Print("Maximize failed");
                    }
                });
        });

        this.addButton("Restore", () : void => {
            this.windowHandler
                .Restore()
                .Then(($success : boolean) : void => {
                    if ($success === true) {
                        Echo.Print("Restored");
                    } else {
                        Echo.Print("Restore failed");
                    }
                });
        });
    }

    public WindowLocationTest() : void {
        this.windowHandler.getEvents().OnWindowChanged(($changedEvent : IWindowChangedEvent) : void => {
            if ($changedEvent.state === WindowStateType.MAXIMIZED) {
                Echo.Println("Window = Maximized");
            } else if ($changedEvent.state === WindowStateType.MINIMIZED) {
                Echo.Println("Window = Minimized");
            } else {
                Echo.Println("Window = Normal");
            }
        });

        this.addButton("MoveTo [10; 10]", () : void => {
            this.windowHandler
                .MoveTo(10, 10)
                .Then(($success : boolean) : void => {
                    if ($success === true) {
                        Echo.Print("Moved to [10; 10]");
                    } else {
                        Echo.Print("Move failed");
                    }
                });
        });

        this.addButton("MoveTo [200 ;200]", () : void => {
            this.windowHandler
                .MoveTo(200, 200)
                .Then(($success : boolean) : void => {
                    if ($success === true) {
                        Echo.Print("Moved to [200; 200]");
                    } else {
                        Echo.Print("Move failed");
                    }
                });
        });

        this.addButton("Enable resize", () : void => {
            this.windowHandler
                .CanResize(true)
                .Then(($success : boolean) : void => {
                    if ($success === true) {
                        Echo.Print("Resize enabled");
                    } else {
                        Echo.Print("Resize disabled");
                    }
                });
        });

        this.addButton("Disable resize", () : void => {
            this.windowHandler
                .CanResize(false)
                .Then(($success : boolean) : void => {
                    if ($success === true) {
                        Echo.Print("Resize enabled");
                    } else {
                        Echo.Print("Resize disabled");
                    }
                });
        });

        this.addButton("Resize BOTTOM_RIGHT [20 ;20]", () : void => {
            this.windowHandler
                .Resize(WindowCornerType.BOTTOM_RIGHT, 20, 20)
                .Then(($success : boolean) : void => {
                    if ($success === true) {
                        Echo.Print("Resized BOTTOM_RIGHT [20; 20]");
                    } else {
                        Echo.Print("Resize failed");
                    }
                });
        });

        this.addButton("Resize BOTTOM_RIGHT [-20 ;-20]", () : void => {
            this.windowHandler
                .Resize(WindowCornerType.BOTTOM_RIGHT, -20, -20)
                .Then(($success : boolean) : void => {
                    if ($success === true) {
                        Echo.Print("Resized BOTTOM_RIGHT [-20; -20]");
                    } else {
                        Echo.Print("Resize failed");
                    }
                });
        });

        this.addButton("Get window state", () : void => {
            this.windowHandler
                .getState()
                .Then(($state : WindowStateType) : void => {
                    if ($state === WindowStateType.MAXIMIZED) {
                        Echo.Println("Window = Maximized");
                    } else if ($state === WindowStateType.MINIMIZED) {
                        Echo.Println("Window = Minimized");
                    } else {
                        Echo.Println("Window = Normal");
                    }
                });
        });
    }

    public WindowAdvancedTest() : void {
        let windowId : string = "";

        this.addButton("Open", () : void => {
            this.windowHandler
                .Open("https://www.nxp.com/webapp", <IWindowHandlerOpenOptions>{
                    hidden: false
                })
                .Then(($windowId : string, $url : string, $data : string) : void => {
                    windowId = $windowId;
                    let dataLen : number = -1;
                    if (ObjectValidator.IsSet($data) && ObjectValidator.IsString($data)) {
                        dataLen = StringUtils.Length($data);
                    }
                    Echo.Printf("Opened window: {0}/{1}:{2}", $windowId, $url, dataLen);
                });
        });

        Echo.Println("User name:</br><input id=\"testUserName\" type=\"text\">");
        Echo.Println("Password:</br><input id=\"testUserPass\" type=\"password\">");

        this.addButton("LogIn", () : void => {
            this.windowHandler
                .Open("https://www.nxp.com/webapp", <IWindowHandlerOpenOptions>{
                    hidden: false
                })
                .Then(($windowId : string, $url : string, $data : string) : void => {
                    windowId = $windowId;
                    Echo.Printf("Opened: {0}", $url);
                    if (StringUtils.Contains($data, "<title>NXP Sign In")) {
                        const name : string = (<HTMLInputElement>ElementManager.getElement("testUserName")).value;
                        const pass : string = (<HTMLInputElement>ElementManager.getElement("testUserPass")).value;
                        this.windowHandler.ScriptExecute($windowId, <IWindowHandlerScriptExecuteOptions>{
                            script: "" +
                                "document.getElementById(\"username\").value=\"" + name + "\";" +
                                "document.getElementById(\"password\").value=\"" + pass + "\";" +
                                "document.getElementsByName(\"loginbutton\")[0].click();"
                        })
                            .Then(($status : boolean) : void => {
                                if ($status === true) {
                                    Echo.Printf("Script succeed");
                                } else {
                                    Echo.Printf("Script failed");
                                }
                            });
                    } else if (StringUtils.Contains($data, "<title>My Account Home Page</title>")) {
                        this.windowHandler.Close($windowId);
                        this.windowHandler
                            .Open("http://www.nxp.com/products", <IWindowHandlerOpenOptions>{
                                hidden: false
                            })
                            .Then(($windowId : string) : void => {
                                this.windowHandler.getCookies($windowId).Then(($data : IWindowHandlerCookie[]) : void => {
                                    let casId : string = "";
                                    $data.forEach(($cookie : IWindowHandlerCookie) : void => {
                                        if ($cookie.name === "JSESSIONID" && $cookie.path === "/webapp") {
                                            casId = $cookie.value;
                                        }
                                    });
                                    Echo.Printf("cas: {0}", casId);
                                    this.windowHandler.Close($windowId);
                                });
                            });
                    }
                });
        });

        this.addButton("getCookies", () : void => {
            this.windowHandler.getCookies().Then(($data : IWindowHandlerCookie[]) : void => {
                Echo.Printf($data);
            });
        });

        this.addButton("ExecuteScript", () : void => {
            this.windowHandler
                .ScriptExecute(windowId, <IWindowHandlerScriptExecuteOptions>{
                    script() : void {
                        (<any>window).cefQuery({request: "script has been executed"});
                    }
                })
                .OnStart(() : void => {
                    Echo.Printf("Script started.");
                })
                .OnMessage(($data : string) : void => {
                    Echo.Printf("Script output data: {0}", $data);
                })
                .Then(($status : boolean) : void => {
                    if ($status === true) {
                        Echo.Print("Script succeed");
                    } else {
                        Echo.Print("Script failed");
                    }
                });
        });

        this.addButton("Close", () : void => {
            this.windowHandler
                .Close(windowId)
                .Then(($success : boolean) => {
                    if ($success === true) {
                        Echo.Println("Closed");
                    } else {
                        Echo.Println("Close failed");
                    }
                });
        });

        this.addButton("DebugConsole", () : void => {
            this.windowHandler
                .ShowDebugConsole()
                .Then(($success : boolean) => {
                    if ($success === true) {
                        Echo.Println("Debug console opened.");
                    } else {
                        Echo.Println("Failed to open debug console.");
                    }
                });
        });
    }

    public NotifyIconTest() : void {
        this.windowHandler.getEvents().OnNotifyIconBalloonClick(() : void => {
            Echo.Println("OnNotifyIconBalloonClick called");
            this.windowHandler.Show();
        });
        this.windowHandler.getEvents().OnNotifyIconBalloonClick(() : void => {
            Echo.Println("OnNotifyIconBalloonClick called");
            this.windowHandler.Show();
        });
        this.windowHandler.getEvents().OnNotifyIconDoubleClick(() : void => {
            Echo.Println("OnNotifyIconDbClick called");
        });
        this.windowHandler.getEvents().OnNotifyIconContextItemSelected(($name : string) : void => {
            Echo.Printf("OnNotifyIconContextItemSelected called with \"{0}\"", $name);
            if ($name === "exit") {
                Echo.Println("closing");
                this.windowHandler.Close();
            } else if ($name === "hide") {
                this.windowHandler.Hide();
            } else if ($name === "open") {
                this.windowHandler.Show();
            }
        });
        this.windowHandler.getEvents().OnNotifyIconClick(() : void => {
            Echo.Println("OnNotifyIconClick called");
        });

        this.addButton("CreateNotifyIcon", () : void => {
            this.windowHandler
                .CreateNotifyIcon(<IWindowHandlerNotifyIcon>{
                    contextMenu: {
                        items: [
                            {
                                label   : "Exit",
                                name    : "exit",
                                position: 1
                            },
                            {
                                label   : "Open",
                                name    : "open",
                                position: 0
                            },
                            {
                                label   : "Hide",
                                name    : "hide",
                                position: 2
                            }
                        ]
                    },
                    tip        : "Test application"
                })
                .OnMessage(($request : string) : void => {
                    Echo.Println("Notify message: " + $request);
                })
                .Then(($success : boolean) : void => {
                    Echo.Println("Notify created");
                });
        });

        this.addButton("ModifyNotifyIcon", () : void => {
            this.windowHandler.ModifyNotifyIcon(<IWindowHandlerNotifyIcon>{
                balloon    : {
                    message: "Custom data. Custom data. Custom data. Custom data. Custom data.",
                    title  : "Custom title",
                    type   : NotifyBalloonIconType.ERROR
                },
                contextMenu: {
                    items: [
                        {
                            label   : "Exit",
                            name    : "exit",
                            position: 0
                        },
                        {
                            label   : "Open",
                            name    : "open",
                            position: 1
                        },
                        {
                            label   : "Hide",
                            name    : "hide",
                            position: 2
                        }
                    ]
                },
                tip        : "Modified application"
            });
        });

        this.addButton("DestroyNotifyIcon", () : void => {
            this.windowHandler.DestroyNotifyIcon();
        });
    }

    public ProgressTest() : void {
        this.addButton("ProgressState", () : void => {
            this.windowHandler.setTaskBarProgressState(TaskBarProgressState.ERROR);
        });

        this.addButton("ProgressValue", () : void => {
            this.windowHandler.setTaskBarProgressValue(70, 100);
        });
    }

    public FileDialogTest() : void {
        this.addButton("ShowFileDialog - openOnly", () : void => {
            const fileDialogSettings : IWindowHandlerFileDialogSettings = <IWindowHandlerFileDialogSettings>{
                filter          : ["Archive|.zip"],
                initialDirectory: this.getAbsoluteRoot(),
                openOnly        : true,
                title           : "Open existing file"
            };
            this.windowHandler.ShowFileDialog(fileDialogSettings)
                .Then(($success : boolean, $path : string) : void => {
                    if ($success) {
                        Echo.Println("File \"" + $path + "\" has been selected.");
                    } else {
                        Echo.Println("File dialog selection failed.");
                    }
                });
        });
        this.addButton("ShowFileDialog - open/create", () : void => {
            const fileDialogSettings : IWindowHandlerFileDialogSettings = <IWindowHandlerFileDialogSettings>{
                filter  : ["Text|.txt"],
                openOnly: false,
                title   : "Open/Create file"
            };
            this.windowHandler.ShowFileDialog(fileDialogSettings)
                .Then(($success : boolean, $path : string) : void => {
                    if ($success) {
                        Echo.Println("File \"" + $path + "\" has been selected.");
                    } else {
                        Echo.Println("File dialog selection failed.");
                    }
                });
        });
        this.addButton("ShowFileDialog - openMultiple", () : void => {
            const fileDialogSettings : IWindowHandlerFileDialogSettings = <IWindowHandlerFileDialogSettings>{
                filter     : ["text/*"],
                multiSelect: true,
                openOnly   : true,
                title      : "Open multiple files"
            };
            this.windowHandler.ShowFileDialog(fileDialogSettings)
                .Then(($success : boolean, $path : string[]) : void => {
                    if ($success) {
                        Echo.Println("Files \"" + JSON.stringify($path) + "\" has been selected.");
                    } else {
                        Echo.Println("File dialog selection failed.");
                    }
                });
        });
        this.addButton("ShowFileDialog - folder", () : void => {
            const fileDialogSettings : IWindowHandlerFileDialogSettings = <IWindowHandlerFileDialogSettings>{
                folderOnly: true,
                title     : "Open folder"
            };
            this.windowHandler.ShowFileDialog(fileDialogSettings)
                .Then(($success : boolean, $path : string) : void => {
                    if ($success) {
                        Echo.Println("File \"" + $path + "\" has been selected.");
                    } else {
                        Echo.Println("File dialog selection failed.");
                    }
                });
        });
        this.addButton("ShowFileDialog - create default", () : void => {
            const fileDialogSettings : IWindowHandlerFileDialogSettings = <IWindowHandlerFileDialogSettings>{
                filter          : ["Text|.txt", "PNGs|.png", "BMPs|.bmp"],
                filterIndex     : 2,
                initialDirectory: this.getAbsoluteRoot(),
                openOnly        : false,
                path            : "someNewFile",
                title           : "Create file"
            };
            this.windowHandler.ShowFileDialog(fileDialogSettings)
                .Then(($success : boolean, $path : string) : void => {
                    if ($success) {
                        Echo.Println("File \"" + $path + "\" has been selected.");
                    } else {
                        Echo.Println("File dialog selection failed.");
                    }
                });
        });
        this.addButton("ShowFileDialog - open, absolute path", () : void => {
            const fileDialogSettings : IWindowHandlerFileDialogSettings = <IWindowHandlerFileDialogSettings>{
                filter  : ["Text|.txt", "Html|.html;.htm"],
                openOnly: false,
                path    : this.getAbsoluteRoot() + "\\index.html",
                title   : "Create file"
            };
            this.windowHandler.ShowFileDialog(fileDialogSettings)
                .Then(($success : boolean, $path : string) : void => {
                    if ($success) {
                        Echo.Println("File \"" + $path + "\" has been selected.");
                    } else {
                        Echo.Println("File dialog selection failed.");
                    }
                });
        });
    }

    protected before() : void {
        WindowManager.getEvents().setOnKeyDown(($eventArgs : KeyEventArgs) : void => {
            if ($eventArgs.getKeyCode() === KeyMap.F5) {
                this.getHttpManager().Refresh();
            }
        });
    }
}
/* dev:end */
