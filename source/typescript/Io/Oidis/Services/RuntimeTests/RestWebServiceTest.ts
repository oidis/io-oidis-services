/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { WebServiceConfiguration } from "@io-oidis-commons/Io/Oidis/Commons/WebServiceApi/WebServiceConfiguration.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { IWebServiceClient } from "../Interfaces/IWebServiceClient.js";
import { IWebServiceProtocol } from "../Interfaces/IWebServiceProtocol.js";
import { LiveContentWrapper } from "../WebServiceApi/LiveContentWrapper.js";
import { WebServiceClientFactory } from "../WebServiceApi/WebServiceClientFactory.js";

export class RestWebServiceTest extends RuntimeTestRunner {

    public testRESTConnection() : void {
        const serviceConfigPath : string = "resource/libs/OidisConnector/connector.config.jsonp";
        const service : IWebServiceClient =
            WebServiceClientFactory.getClient(WebServiceClientType.REST_CONNECTOR, serviceConfigPath);

        const serviceConfig : WebServiceConfiguration = new WebServiceConfiguration();
        serviceConfig.ServerAddress("localhost");

        // serviceConfig.ServerAddress("localhost.oidis.io");

        // let service : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.REST_CONNECTOR, serviceConfig);

        service.getEvents().OnStart(() : void => {
            Echo.Printf("Connection has been started.");
        });
        service.getEvents().OnClose(() : void => {
            Echo.Printf("Connection has been closed.");
        });
        service.getEvents().OnTimeout(() : void => {
            Echo.Printf("Connection timeout has been reached.");
        });
        service.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            ExceptionsManager.Throw($eventArgs.Owner().getClassName(), $eventArgs.Exception());
        });

        let sendCounter : number = 0;
        this.addButton("Send data to REST service", () : void => {
            let receiveCounter : number = 0;
            for (let index : number = 0; index < 5; index++) {
                const wrapper : LiveContentWrapper = new LiveContentWrapper("Io.Oidis.Connector.Commons.LogIt");
                service.Send(wrapper.getProtocolForInvokeMethod(
                        service.getId(), "Info", "Data send from test code. Current session send count: " + sendCounter),
                    ($dataObject : IWebServiceProtocol) : void => {
                        receiveCounter++;
                        Echo.Printf("<b>server acknowledge[" + receiveCounter + "] from " + $dataObject.origin + "</b>");
                    });
                sendCounter++;
            }
        });

        this.addButton("Close connection", () : void => {
            service.StopCommunication();
        });
    }
}
/* dev:end */
