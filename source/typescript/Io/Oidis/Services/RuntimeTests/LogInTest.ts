/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { ElementEventsManager } from "@io-oidis-gui/Io/Oidis/Gui/Events/ElementEventsManager.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";
import { StaticPageContentManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/StaticPageContentManager.js";
import { AuthManagerConnector } from "../Connectors/AuthManagerConnector.js";
import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { AuthHttpResolver } from "../ErrorPages/AuthHttpResolver.js";
import { IWebServiceClient } from "../Interfaces/IWebServiceClient.js";
import { LiveContentWrapper } from "../WebServiceApi/LiveContentWrapper.js";
import { WebServiceClientFactory } from "../WebServiceApi/WebServiceClientFactory.js";

class LogInPage extends AuthHttpResolver {
    private service : AuthManagerConnector;

    public constructor() {
        super();
        this.service = new AuthManagerConnector(false, "https://localhost.oidis.io/connector.config.jsonp");
        this.setExpirationTime("10 sec");
    }

    protected resolver() : void {
        const loginFormId : string = "LogInForm";
        if (!ElementManager.Exists(loginFormId)) {
            const loginNameId : string = "LoginUserNameId";
            const loginPassId : string = "LoginPasswordId";
            const loginButtonId : string = "LoginLogInButtonId";

            StaticPageContentManager.BodyAppend(
                "<div id=\"" + loginFormId + "\">" +
                "<h2>Log in</h2>" +
                "Username<br><input type=\"text\" id=\"" + loginNameId + "\"><br>" +
                "Password<br><input type=\"password\" id=\"" + loginPassId + "\"><br>" +
                "<div style=\"border: 1px solid red; cursor: pointer; " +
                "width: 250px; overflow-x: hidden; overflow-y: hidden; " +
                "text-align: center; font-size: 16px; font-family: Consolas; color: red;\" " +
                "id=\"" + loginButtonId + "\">Log in</div>" +
                "</div>");

            const eventsLogin : ElementEventsManager = new ElementEventsManager(loginButtonId);
            eventsLogin.setOnClick(async () : Promise<void> => {
                try {
                    const $token : string = await this.service.LogIn(
                        (<HTMLInputElement>ElementManager.getElement(loginNameId)).value,
                        (<HTMLInputElement>ElementManager.getElement(loginPassId)).value);
                    if (!ObjectValidator.IsEmptyOrNull($token)) {
                        ElementManager.Hide(loginFormId);
                        Echo.Printf("You have successfully logged in with token: {0}", $token);
                        this.reinvokeRequest($token);
                    } else {
                        Echo.Printf("Invalid user, please try again.");
                    }
                } catch (ex) {
                    Echo.Printf(ex.message);
                }
            });

            this.getEventsManager().FireAsynchronousMethod(() : void => {
                eventsLogin.Subscribe();
            });
        } else {
            ElementManager.Show(loginFormId);
        }
    }
}

export class LogInTest extends RuntimeTestRunner {
    public testInvokeSecuredFunction() : void {
        const loginFormId : string = "LogInForm";

        const client : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.HUB_CONNECTOR,
            "https://localhost.oidis.io/connector.config.jsonp");

        this.addButton("Invoke secure function", () : void => {
            LiveContentWrapper.InvokeMethod(client,
                "Io.Oidis.Hub.RuntimeTests.UserManagerTestHandler", "SecuredMethod")
                .Then(($success : boolean) : void => {
                    ElementManager.Hide(loginFormId);
                    if ($success) {
                        Echo.Printf("Secured function successfully invoked.");
                    } else {
                        Echo.Printf("Secured function invocation failed.");
                    }
                });
        });
    }

    public testInvalidateToken() : void {
        this.addButton("Invalidate token", () : void => {
            AuthHttpResolver.LogOut();
            Echo.Printf("User has been logged out.");
        });
    }

    public __IgnoretestLoginFacebook() : void {
        // example url for logging into fb/getting token by redirecting
        //
        // https://www.facebook.com/dialog/oauth/
        // ?client_id=201158640315306&redirect_uri=http://localhost.oidis.io/io-oidis-services/
    }

    public __IgnoretestLoginGoogle() : void {
        // example url for logging into google/getting token by redirecting
        //
        // https://accounts.google.com/o/oauth2/
        // v2/auth?scope=email%20profile&redirect_uri=http://localhost.oidis.io/io-oidis-services
        // &response_type=code&client_id=176530116802-vfhiqjrgt5f5th6tokqutgp3ke3dpafn.apps.googleusercontent.com
    }

    protected before() : void {
        this.overrideResolver("/ServerError/Http/NotAuthorized", LogInPage);
    }
}
/* dev:end */
