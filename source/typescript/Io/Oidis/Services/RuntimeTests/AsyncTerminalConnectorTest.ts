/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { IRuntimeTestPromise } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { IExecuteResult, TerminalConnectorAsync } from "../Connectors/TerminalConnector.js";
import { IWebServiceClient } from "../Interfaces/IWebServiceClient.js";
import { WebServiceClientFactory } from "../WebServiceApi/WebServiceClientFactory.js";

export class AsyncTerminalConnectorTest extends RuntimeTestRunner {
    private connector : TerminalConnectorAsync;

    public async CmdSyncTest() : Promise<void> {
        this.timeoutLimit(-1);
        let output : IExecuteResult = await this.connector
            .Execute("wui", ["--version"], ($data : string) : void => {
                Echo.Print($data);
            });
        Echo.Printf("out: {0}</br>err: {1}", output.stdout, output.stderr);
        this.assertOk(output.exitCode === 0, "Execution status (wui)");

        output = await this.connector
            .Execute("cmd", ["/c", "dir"], "../../", ($data : string) : void => {
                Echo.Print($data);
            });
        Echo.Printf("out: {0}</br>err: {1}", output.stdout, output.stderr);
        this.assertOk(output.exitCode === 0, "Execution status (cmd)");

        output = await this.connector
            .Execute("ping", ["127.0.0.1"], ($data : string) : void => {
                Echo.Print($data);
            });
        this.assertOk(output.exitCode === 0, "Execution status (ping)");

        output = await this.connector
            .Spawn("ping", ["127.0.0.1"], ($data : string) : void => {
                Echo.Print($data);
            });
        this.assertOk(output.exitCode === 0, "Execution status (ping spawn)");

        output = await this.connector
            .Spawn("wui", ["--version"], ($data : string) : void => {
                if ($data === ".") {
                    Echo.Print($data);
                } else {
                    Echo.Println($data);
                }
            });
        if (output.exitCode === 0) {
            await this.connector.Spawn("wui", ["--path"]);
        }

        output = await this.connector.Spawn("wui", ["--version"]);
        if (output.exitCode === 0) {
            Echo.Printf(output.stdout);
            output = await this.connector.Spawn("wui", ["--path"]);
            if (output.exitCode === 0) {
                Echo.Printf(output.stdout);
            }
        }

        this.addButton("run sync testCase*.cmd", () : void => {
            this.connector
                .Execute("testCaseA.cmd", [],
                    this.getAbsoluteRoot() + "/test/resource/data/Io/Oidis/Services/RuntimeTests",
                    ($data : string) : void => {
                        Echo.Print($data);
                    })
                .then(($result : IExecuteResult) : void => {
                    Echo.Println("test call #1 - " + ($result.exitCode === 0 ? "succeed" : "failed"));
                });

            this.connector
                .Execute("testCaseA.cmd", [],
                    {cwd: this.getAbsoluteRoot() + "/test/resource/data/Io/Oidis/Services/RuntimeTests"},
                    ($data : string) : void => {
                        Echo.Print($data);
                    })
                .then(($result : IExecuteResult) : void => {
                    Echo.Println("test call #2 - " + ($result.exitCode === 0 ? "succeed" : "failed"));
                });

            this.connector
                .Execute("testCaseA.cmd", [], {
                    env: {
                        PATH: this.getAbsoluteRoot() + "/test/resource/data/Io/Oidis/Services/RuntimeTests;%PATH%"
                    }
                }, ($data : string) : void => {
                    Echo.Print($data);
                })
                .then(($result : IExecuteResult) : void => {
                    Echo.Println("test call #3 - " + ($result.exitCode === 0 ? "succeed" : "failed"));
                });
        });
    }

    public CmdAsyncTest() : void {
        this.addButton("get async WUI Info", () : void => {
            this.connector.Spawn("wui", ["--version"])
                .then(($result : IExecuteResult) : void => {
                    Echo.Println("async: spawn wui --version - " + ($result.exitCode === 0 ? "succeed" : "failed"));
                });
            this.connector.Spawn("wui", ["--path"])
                .then(($result : IExecuteResult) : void => {
                    Echo.Println("async: spawn wui --path - " + ($result.exitCode === 0 ? "succeed" : "failed"));
                });
        });

        this.addButton("test spawn async (ping 127.0.0.1 and ping 8.8.8.8)", () : void => {
            this.connector
                .Spawn("ping", ["127.0.0.1"], ($data : string) : void => {
                    Echo.Println($data);
                })
                .then(($result : IExecuteResult) : void => {
                    Echo.Println("ping 127.0.0.1 - " + ($result.exitCode === 0 ? "succeed" : "failed"));
                });
            this.connector
                .Spawn("ping", ["8.8.8.8"], ($data : string) : void => {
                    Echo.Println($data);
                })
                .then(($result : IExecuteResult) : void => {
                    Echo.Println("ping 8.8.8.8 - " + ($result.exitCode === 0 ? "succeed" : "failed"));
                });
        });

        this.addButton("test spawn async (test *.cmd)", () : void => {
            this.connector
                .Spawn("testCaseA.cmd", [], "./test/resource/data/Io/Oidis/Services/RuntimeTests",
                    ($data : string) : void => {
                        Echo.Println($data);
                    })
                .then(($result : IExecuteResult) : void => {
                    Echo.Println("test call #1 - " + ($result.exitCode === 0 ? "succeed" : "failed"));
                });
            this.connector
                .Spawn("testCaseB.cmd", [], "./test/resource/data/Io/Oidis/Services/RuntimeTests",
                    ($data : string) : void => {
                        Echo.Println($data);
                    })
                .then(($result : IExecuteResult) : void => {
                    Echo.Println("test call #2 - " + ($result.exitCode === 0 ? "succeed" : "failed"));
                });
        });
    }

    public OpenTest() : void {
        this.addButton("open executable", async () : Promise<void> => {
            await this.connector.Open("notepad.exe");
        });
    }

    public ElevateTest() : void {
        this.addButton("elevate process", async () : Promise<void> => {
            await this.connector.Elevate("AT");
        });
    }

    public __IgnoreAgentTest() : void {
        const connector : TerminalConnectorAsync = new TerminalConnectorAsync();
        connector.setAgent("test-connector-win", "https://localhost.wuiframework.com/connector.config.jsonp");

        this.addButton("Forward to agent", async () : Promise<void> => {
            this.assertOk((await connector.Spawn("wui", ["path"], ($data : string) : void => {
                Echo.Printf($data);
            })).exitCode === 0);
        });
    }

    protected before() : IRuntimeTestPromise {
        this.connector = new TerminalConnectorAsync();
        this.connector.getEvents().OnError(($args : ErrorEventArgs) : void => {
            Echo.Println($args.Message());
        });

        const client : IWebServiceClient = WebServiceClientFactory.getClientById(this.connector.getId());
        return ($done : any) : void => {
            client.getServerPath(($path : string) : void => {
                Echo.Printf("<b>Server configuration:</b> uri - \"{0}\", server root - \"{1}\"", client.getServerUrl(), $path);
                $done();
            });
        };
    }
}
/* dev:end */
