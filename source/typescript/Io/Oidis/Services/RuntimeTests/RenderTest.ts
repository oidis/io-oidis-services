/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { ElementManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/ElementManager.js";

export class RenderTest extends RuntimeTestRunner {

    public CSSTest() : void {
        Echo.Println("" +
            "<div id=\"CssTestTarget\" style=\"width: 200px; height: 50px; border: 1px solid blue;\">" +
            "Test something on me..." +
            "</div>");
        this.addButton("Blur Filter", () : void => {
            if (this.getRequest().IsJre()) {
                // under Chrome version 61 a prefix -webkit- is REQUIRED. (we use Chrome 51.x)
                ElementManager.setCssProperty("CssTestTarget", "-webkit-filter", "blur(4px)");
            } else {
                ElementManager.setCssProperty("CssTestTarget", "filter", "blur(4px)");
            }
        });
    }
}
/* dev:end */
