/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { IRuntimeTestPromise } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { WindowManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/WindowManager.js";
import { FFIProxyConnector, IFFIArgument } from "../Connectors/FFIProxyConnector.js";
import { FileSystemHandlerConnector } from "../Connectors/FileSystemHandlerConnector.js";
import { TerminalConnector } from "../Connectors/TerminalConnector.js";
import { LiveContentArgumentType } from "../Enums/LiveContentArgumentType.js";
import { WebServiceClientFactory } from "../WebServiceApi/WebServiceClientFactory.js";

export class FFIProxyConnectorTest extends RuntimeTestRunner {
    private service : FFIProxyConnector = new FFIProxyConnector();
    private targetRoot : string;
    private testResourceRoot : string;
    private libraryPath : string;
    private generator : string;

    constructor() {
        super();
        // this.setMethodFilter("testVarious");
    }

    public testLoadLibraryFromPath() : void {
        this.addButton("Load Library from absolute path", () : void => {
            this.service
                .Load(this.libraryPath)
                .Then(($returnValue : boolean, $libPath : string) : void => {
                    if ($returnValue) {
                        Echo.Printf("Library loaded from: {0}", $libPath);
                    } else {
                        Echo.Printf("Failed to load the library");
                    }
                });
        });
    }

    public testReturn() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            const invokes : any[] = [];
            let index : number = 0;
            const getNextInvoke : any = () : void => {
                if (index < invokes.length) {
                    Echo.Println("invoke " + (index + 1) + "/" + invokes.length);
                    invokes[index]();
                    index++;
                } else {
                    $done();
                }
            };

            invokes.push(() : void => {
                // #1
                this.service
                    .InvokeMethod("ReturnInt", LiveContentArgumentType.RETURN_INT)
                    .Then(($returnValue : number) : void => {
                        this.assertEquals($returnValue, 23);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #2
                this.service
                    .InvokeMethod("ReturnBool", LiveContentArgumentType.RETURN_BOOLEAN)
                    .Then(($returnValue : boolean) : void => {
                        this.assertEquals($returnValue, true);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #3
                this.service
                    .InvokeMethod("ReturnFloat", LiveContentArgumentType.RETURN_FLOAT)
                    .Then(($returnValue : number) : void => {
                        this.assertEquals(Convert.ToFixed($returnValue, 12), 16.299999237061);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #4
                this.service
                    .InvokeMethod("ReturnDouble", LiveContentArgumentType.RETURN_DOUBLE)
                    .Then(($returnValue : number) : void => {
                        this.assertEquals(Convert.ToFixed($returnValue, 12), 33.44);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #5
                this.service
                    .InvokeMethod("ReturnString", LiveContentArgumentType.RETURN_STRING)
                    .Then(($returnValue : string) : void => {
                        this.assertEquals($returnValue, "returned string");
                        getNextInvoke();
                    });
            });
            this.service
                .Load(this.libraryPath)
                .Then(($returnValue : boolean, $libPath : string) : void => {
                    if ($returnValue) {
                        Echo.Printf("Library loaded from: {0}", $libPath);
                        getNextInvoke();
                    } else {
                        Echo.Printf("Failed to load the library");
                        $done();
                    }
                });
        };
    }

    public testInputArgs() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            const invokes : any[] = [];
            let index : number = 0;
            const getNextInvoke : any = () : void => {
                if (index < invokes.length) {
                    Echo.Println("invoke " + (index + 1) + "/" + invokes.length);
                    invokes[index]();
                    index++;
                } else {
                    $done();
                }
            };

            invokes.push(() : void => {
                // #1
                this.service
                    .InvokeMethod("CallWithEmpty", LiveContentArgumentType.RETURN_BOOLEAN)
                    .Then(($returnValue : boolean) : void => {
                        this.assertEquals($returnValue, true);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #2
                this.service
                    .InvokeMethod("CallWithInt", LiveContentArgumentType.RETURN_BOOLEAN, 123)
                    .Then(($returnValue : boolean) : void => {
                        this.assertEquals($returnValue, true);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #3
                this.service
                    .InvokeMethod("CallWithBool", LiveContentArgumentType.RETURN_BOOLEAN, true)
                    .Then(($returnValue : boolean) : void => {
                        this.assertEquals($returnValue, true);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #4
                this.service
                    .InvokeMethod("CallWithFloat", LiveContentArgumentType.RETURN_BOOLEAN, <IFFIArgument>{
                        type : LiveContentArgumentType.IN_FLOAT,
                        value: 3.14
                    })
                    .Then(($returnValue : boolean) : void => {
                        this.assertEquals($returnValue, true);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #5
                this.service
                    .InvokeMethod("CallWithDouble", LiveContentArgumentType.RETURN_BOOLEAN, 6.28)
                    .Then(($returnValue : boolean) : void => {
                        this.assertEquals($returnValue, true);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #6
                this.service
                    .InvokeMethod("CallWithString", LiveContentArgumentType.RETURN_BOOLEAN, <IFFIArgument>{
                        type : LiveContentArgumentType.IN_STRING,
                        value: "hello world"
                    })
                    .Then(($returnValue : boolean) : void => {
                        this.assertEquals($returnValue, true);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #7
                this.service
                    .InvokeMethod("CallWithString", LiveContentArgumentType.RETURN_BOOLEAN, "hello world")
                    .Then(($returnValue : boolean) : void => {
                        this.assertEquals($returnValue, true);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #8
                this.service
                    .InvokeMethod("CallWithStruct", LiveContentArgumentType.RETURN_BOOLEAN, <IFFIArgument>{
                        type : LiveContentArgumentType.IN_STRUCT,
                        value: {
                            fields  : {
                                a: 234,
                                b: true,
                                c: "struct string"
                            },
                            typeName: "TestStruct"
                        }
                    })
                    .Then(($returnValue : boolean) : void => {
                        this.assertEquals($returnValue, true);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #9
                this.service
                    .InvokeMethod("CallWithIntArray", LiveContentArgumentType.RETURN_BOOLEAN, <IFFIArgument>{
                        type : LiveContentArgumentType.IN_ARRAY,
                        value: {
                            items   : [301, 303, 305, 407, 409, 502, 504, 806, 708, 900],
                            typeName: LiveContentArgumentType.IN_INT
                        }
                    }, 10)
                    .Then(($returnValue : boolean) : void => {
                        this.assertEquals($returnValue, true);
                        this.service
                            .InvokeMethod("CallWithIntArray", LiveContentArgumentType.RETURN_BOOLEAN, <IFFIArgument>{
                                type : LiveContentArgumentType.IN_ARRAY,
                                value: {
                                    items   : [301, 303, 305, 407, 409, 502, 504, 806, 708, 900],
                                    typeName: LiveContentArgumentType.IN_INT
                                }
                            }, 10)
                            .Then(($returnValue : boolean) : void => {
                                this.assertEquals($returnValue, true);
                                getNextInvoke();
                            });
                    });
            });
            invokes.push(() : void => {
                // #10
                this.service
                    .InvokeMethod("CallWithBuffer", LiveContentArgumentType.RETURN_BOOLEAN,
                        <IFFIArgument>{
                            type : LiveContentArgumentType.IN_BUFFER,
                            value: [0, 1, 2, 3, 4, 5, 6, 7]
                        },
                        8)
                    .Then(($returnValue : boolean) : void => {
                        this.assertEquals($returnValue, true);
                        getNextInvoke();
                    });
            });
            this.service
                .Load(this.libraryPath)
                .Then(($returnValue : boolean, $libPath : string) : void => {
                    if ($returnValue) {
                        Echo.Printf("Library loaded from: {0}", $libPath);
                        getNextInvoke();
                    } else {
                        Echo.Printf("Failed to load the library");
                        $done();
                    }
                });
        };
    }

    public testOutputArgs() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            const invokes : any[] = [];
            let index : number = 0;
            const getNextInvoke : any = () : void => {
                if (index < invokes.length) {
                    Echo.Println("invoke " + (index + 1) + "/" + invokes.length);
                    invokes[index]();
                    index++;
                } else {
                    $done();
                }
            };

            invokes.push(() : void => {
                // #1
                this.service
                    .InvokeMethod("output_args_int", LiveContentArgumentType.RETURN_VOID, <IFFIArgument>{
                        type : LiveContentArgumentType.OUT_INT,
                        value: 111
                    })
                    .Then(($returnValue : any, $output : number) : void => {
                        this.assertEquals($returnValue, null);
                        this.assertEquals($output, 15);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #2
                this.service
                    .InvokeMethod("output_args_char", LiveContentArgumentType.RETURN_VOID, <IFFIArgument>{
                        type : LiveContentArgumentType.OUT_STRING,
                        value: "hello world"
                    })
                    .Then(($returnValue : any, $output : string) : void => {
                        this.assertEquals($returnValue, null);
                        this.assertEquals($output, "some text");
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #3
                this.service
                    .InvokeMethod("output_args_char", LiveContentArgumentType.RETURN_VOID, <IFFIArgument>{
                        type : LiveContentArgumentType.OUT_STRING,
                        value: 10
                    })
                    .Then(($returnValue : any, $output : string) : void => {
                        this.assertEquals($returnValue, null);
                        this.assertEquals($output, "some text");
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #4
                this.service
                    .InvokeMethod("output_args_float", LiveContentArgumentType.RETURN_VOID, <IFFIArgument>{
                        type : LiveContentArgumentType.OUT_FLOAT,
                        value: 1.1
                    })
                    .Then(($returnValue : any, $data : number) : void => {
                        this.assertEquals($returnValue, null);
                        this.assertEquals(Convert.ToFixed($data, 12), 12.119999885559);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #5
                this.service
                    .InvokeMethod("output_args_double", LiveContentArgumentType.RETURN_VOID, <IFFIArgument>{
                        type : LiveContentArgumentType.OUT_DOUBLE,
                        value: 6.66
                    })
                    .Then(($returnValue : any, $data : number) : void => {
                        this.assertEquals($returnValue, null);
                        this.assertEquals(Convert.ToFixed($data, 12), 56.119998931885);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #6
                this.service
                    .InvokeMethod("output_args_bool", LiveContentArgumentType.RETURN_VOID, <IFFIArgument>{
                        type : LiveContentArgumentType.OUT_BOOLEAN,
                        value: true
                    })
                    .Then(($returnValue : any, $data : boolean) : void => {
                        this.assertEquals($returnValue, null);
                        this.assertEquals($data, false);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #7
                this.service
                    .InvokeMethod("output_args_buffer", LiveContentArgumentType.RETURN_VOID,
                        <IFFIArgument>{
                            type : LiveContentArgumentType.OUT_BUFFER,
                            value: 20
                        })
                    .Then(($returnValue : any, $data : any) : void => {
                        this.assertEquals($returnValue, null);
                        this.assertEquals($data.type, "Buffer");
                        this.assertDeepEqual($data.data, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]);
                        getNextInvoke();
                    });
            });
            this.service
                .Load(this.libraryPath)
                .Then(($returnValue : boolean, $libPath : string) : void => {
                    if ($returnValue) {
                        Echo.Printf("Library loaded from: {0}", $libPath);
                        getNextInvoke();
                    } else {
                        Echo.Printf("Failed to load the library");
                        $done();
                    }
                });
        };
    }

    public testVarious() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            const invokes : any[] = [];
            let index : number = 0;
            const getNextInvoke : any = () : void => {
                if (index < invokes.length) {
                    Echo.Println("invoke " + (index + 1) + "/" + invokes.length);
                    invokes[index]();
                    index++;
                } else {
                    $done();
                }
            };

            invokes.push(() : void => {
                // #1
                this.service
                    .InvokeMethod("int_func_int_bool", LiveContentArgumentType.RETURN_INT, 43, true)
                    .Then(($returnValue : number) : void => {
                        this.assertEquals($returnValue, 29);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #2
                this.service
                    .InvokeMethod("int_func_bool_int", LiveContentArgumentType.RETURN_INT, true, 44)
                    .Then(($returnValue : number) : void => {
                        this.assertEquals($returnValue, 23);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #3
                this.service
                    .InvokeMethod("add_input_ints", LiveContentArgumentType.RETURN_INT, 11, 21)
                    .Then(($returnValue : number) : void => {
                        this.assertEquals($returnValue, 32);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #4
                this.service
                    .InvokeMethod("output_args_int_bool_float", LiveContentArgumentType.RETURN_INT,
                        <IFFIArgument>{
                            type : LiveContentArgumentType.OUT_INT,
                            value: 11
                        },
                        <IFFIArgument>{
                            type : LiveContentArgumentType.OUT_BOOLEAN,
                            value: true
                        },
                        <IFFIArgument>{
                            type : LiveContentArgumentType.OUT_FLOAT,
                            value: 36.98
                        })
                    .Then(($returnValue : number, $intOut : number, $boolOut : boolean, $floatOut : number) : void => {
                        this.assertEquals($returnValue, 3);
                        this.assertEquals($intOut, 12);
                        this.assertEquals($boolOut, false);
                        this.assertEquals(Convert.ToFixed($floatOut, 12), 3.140000104904);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #5
                this.service
                    .InvokeMethod("input_int_double_output_bool_int_input_string", LiveContentArgumentType.RETURN_INT,
                        78,
                        <IFFIArgument>{
                            value: 79.5
                        },
                        <IFFIArgument>{
                            type : LiveContentArgumentType.OUT_BOOLEAN,
                            value: true
                        },
                        <IFFIArgument>{
                            type : LiveContentArgumentType.OUT_INT,
                            value: 17
                        },
                        <IFFIArgument>{
                            type : LiveContentArgumentType.IN_STRING,
                            value: "last one"
                        })
                    .Then(($returnValue : number, $boolOut : boolean, $intOut : number) : void => {
                        this.assertEquals($returnValue, 8);
                        this.assertEquals($boolOut, false);
                        this.assertEquals($intOut, 432);
                        getNextInvoke();
                    });
            });
            invokes.push(() : void => {
                // #5
                this.service
                    .InvokeMethod("append_string", LiveContentArgumentType.RETURN_INT,
                        "hello ", "world",
                        <IFFIArgument>{type: LiveContentArgumentType.OUT_STRING, value: 20}
                    )
                    .Then(($returnValue : number, $output : string) : void => {
                        this.assertEquals($returnValue, 11);
                        this.assertEquals($output, "hello world");
                        getNextInvoke();
                    });
            });
            this.service
                .Load(this.libraryPath)
                .Then(($returnValue : boolean, $libPath : string) : void => {
                    if ($returnValue) {
                        Echo.Printf("Library loaded from: {0}", $libPath);
                        getNextInvoke();
                    } else {
                        Echo.Printf("Failed to load the library");
                        $done();
                    }
                });
        };
    }

    protected testBenchmark() : IRuntimeTestPromise {
        const iterations : number = 1000;
        const delay : number = 0; // ms
        // const iterations : number = 3000;
        // const delay : number = 50; // ms

        this.timeoutLimit(100000);
        return ($done : () => void) : void => {
            let index : number = 0;
            const makeCall : any = ($callback : () => void) : void => {
                if (index % 100 === 0) {
                    Echo.Println(".");
                    WindowManager.ScrollToBottom();
                } else {
                    Echo.Print(".");
                }
                this.service
                    .InvokeMethod("CallWithEmpty", LiveContentArgumentType.RETURN_VOID)
                    .Then(() : void => {
                        index++;
                        if (index > iterations) {
                            $callback();
                        } else if (delay > 0) {
                            setTimeout(() : void => {
                                makeCall($callback);
                            }, delay);
                        } else {
                            makeCall($callback);
                        }
                    });
            };
            this.service
                .Load(this.libraryPath)
                .Then(($returnValue : boolean, $libPath : string) : void => {
                    if ($returnValue) {
                        Echo.Printf("Library loaded from: {0}", $libPath);
                        const start : number = new Date().getTime();
                        makeCall(() : void => {
                            const end : number = Convert.TimeToSeconds(new Date().getTime() - start);
                            Echo.Printf("Invoked {0} iterations in {1}s with average {2}ms",
                                iterations + "", end + "", (end / iterations * 1000) + "");
                            $done();
                        });
                    } else {
                        Echo.Printf("Failed to load the library");
                        $done();
                    }
                });
        };
    }

    protected before() : IRuntimeTestPromise {
        this.timeoutLimit(100000);
        return ($done : () => void) : void => {
            const buildLib : any = () : void => {
                const terminal : TerminalConnector = new TerminalConnector(true);
                const fileSystem : FileSystemHandlerConnector = new FileSystemHandlerConnector(true);

                this.testResourceRoot = this.targetRoot + "/test/resource/data/Io/Oidis/Services/RuntimeTests/FFIProxyLib";

                fileSystem.getTempPath()
                    .Then(($tmpPath : string) : void => {
                        if (!StringUtils.StartsWith($tmpPath, "/")) {
                            Echo.Print("Identified as windows platform...");
                            this.generator = "MinGW Makefiles";
                            this.libraryPath = "libFFIProxyLib.dll";
                        } else {
                            Echo.Print("Identified as posix platform...");
                            this.generator = "Unix Makefiles";
                            this.libraryPath = "libFFIProxyLib.so";
                        }

                        this.libraryPath = this.testResourceRoot + "/build/" + this.libraryPath;

                        fileSystem
                            .Exists(this.libraryPath)
                            .Then(($status : boolean) : void => {
                                if ($status) {
                                    Echo.Printf("Library already exists, build skipped.");
                                    $done();
                                } else {
                                    fileSystem
                                        .CreateDirectory(this.testResourceRoot + "/build_cache")
                                        .Then(($status : boolean) : void => {
                                            if ($status) {
                                                terminal
                                                    .Spawn(
                                                        "wui",
                                                        ["--", "cmake", "-G", "\"" + this.generator + "\"", ".."],
                                                        this.testResourceRoot + "/build_cache")
                                                    .Then(($exitCode : number, $stdOut : string, $stdErr : string) : void => {
                                                        if ($exitCode !== 0) {
                                                            Echo.Printf("Build failed:\n" + $stdOut + $stdErr);
                                                        } else {
                                                            terminal
                                                                .Spawn(
                                                                    "wui",
                                                                    ["--", "cmake", "--build", "."],
                                                                    this.testResourceRoot + "/build_cache")
                                                                .Then(($exitCode : number, $stdOut : string,
                                                                       $stdErr : string) : void => {
                                                                        if ($exitCode === 0) {
                                                                            $done();
                                                                        } else {
                                                                            Echo.Printf("Can not build library:\n" + $stdOut + $stdErr);
                                                                        }
                                                                    }
                                                                );
                                                        }
                                                    });
                                            } else {
                                                Echo.Printf("Failed to prepare build cache for test resource.");
                                            }
                                        });
                                }
                            });

                    });
            };

            if (this.getRequest().IsConnector()) {
                WebServiceClientFactory.getClientById(this.service.getId()).getServerPath(($path : string) : void => {
                    this.targetRoot = StringUtils.Replace($path, "\\", "/");
                    buildLib();
                });
            } else {
                this.targetRoot = StringUtils.Remove(this.getRequest().getHostUrl(), "file:///", "file://", "/index.html");
                buildLib();
            }
        };
    }
}
/* dev:end */
