/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { EnvironmentArgs } from "@io-oidis-commons/Io/Oidis/Commons/EnvironmentArgs.js";
import { BaseAdapter } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Adapters/BaseAdapter.js";
import { ILoggerTrace, Logger } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Logger.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { ImageTransform } from "@io-oidis-gui/Io/Oidis/Gui/ImageProcessor/ImageTransform.js";
import { IReportProtocol, ReportServiceConnector } from "../Connectors/ReportServiceConnector.js";

export class ReportServiceTest extends RuntimeTestRunner {
    private service : ReportServiceConnector;

    public testLogItService() : void {
        let sendCounter : number = 0;
        this.addButton("Transfer log message", () : void => {
            this.service
                .LogIt("Data send from test code. Current session send count: {0}", sendCounter)
                .Then(() : void => {
                    Echo.Printf("Data has been sent.");
                    sendCounter++;
                });
        });
    }

    public testSendMailService() : void {
        this.addButton("Send test mail", () : void => {
            this.service
                .SendMail("kuba@oidis.io", "Test of send mail service",
                    "<html><body>" +
                    "<h2 style='font-style:italic; color:brown;'>HTML text</h2>" +
                    "<a href='http://www.oidis.io' target='_blank'>Link to Oidis</a>" +
                    "</body></html>",
                    [
                        "./test/resource/data/testFile1.txt",
                        "io-oidis-hub/test/resource/data/testFile2.txt"
                    ],
                    "plain text, utf-8 test: +++ ;ěščřžýáíé=°\"ů")
                .Then(($success : boolean) : void => {
                    if ($success) {
                        Echo.Printf("Email has been sent successfully.");
                    } else {
                        Echo.Printf("Email send has failed.");
                    }
                });
        });
    }

    public testCrashReportService() : void {
        const args : EnvironmentArgs = this.getEnvironmentArgs();
        const appLogger : Logger = new Logger();
        const logsStore : MockLogAdapter = new MockLogAdapter();
        appLogger.AddAdapter(logsStore);
        LogIt.Init(appLogger);
        LogIt.setLevel(LogLevel.ALL);
        LogIt.Info("Initialization of ReportServiceTest Unit test");
        const printScreen : HTMLImageElement = document.createElement("img");
        printScreen.src = "test/resource/data/Io/Oidis/Services/PrintScreen.png";
        this.addButton("Create report", () : void => {
            Echo.Println("Sending report .");
            const timeout : any = setInterval(() : void => {
                Echo.Print(".");
            }, 250);
            LogIt.Info("Preparation for initialization of report service");
            this.service.getEvents().OnStart(() : void => {
                LogIt.Debug("Report services has been initialized");
            });
            const EOL : string = StringUtils.NewLine(false);
            const data : IReportProtocol = {
                appId      : StringUtils.getSha1(args.getAppName() + args.getProjectVersion()),
                appName    : args.getAppName(),
                appVersion : args.getProjectVersion(),
                log        : logsStore.getLogs(),
                printScreen: ImageTransform.getStream(
                    ImageTransform.Resize(ImageTransform.ToCanvas(printScreen), 500, 500, false), true),
                timeStamp  : new Date().getTime(),
                trace      : "" +
                    "ExceptionsManagerTest.prototype.testSetEvent@file:///io-oidis-commons/build/target/resource/javascript/" +
                    "io-oidis-commons-1-1-0-beta.min.js:10553:71 " + EOL +
                    "RuntimeTestRunner.prototype.resolver@file:///io-oidis-commons/build/target/resource/javascript/" +
                    "io-oidis-commons-1-1-0-beta.min.js:7649:37 " + EOL +
                    "BaseHttpResolver.prototype.Process@file:///io-oidis-commons/build/target/resource/javascript/" +
                    "io-oidis-commons-1-1-0-beta.min.js:7481:37 " + EOL +
                    "HttpResolver.prototype.ResolveRequest@file:///io-oidis-commons/build/target/resource/javascript/" +
                    "io-oidis-commons-1-1-0-beta.min.js:11380:37 " + EOL +
                    "EventsManager.prototype.FireEvent/:<@file:///io-oidis-commons/build/target/resource/javascript/" +
                    "io-oidis-commons-1-1-0-beta.min.js:5352:53"
            };
            this.service
                .CreateReport(data)
                .Then(($success : boolean) : void => {
                    clearInterval(timeout);
                    if ($success) {
                        Echo.Printf("Report has been sent successfully.");
                    } else {
                        Echo.Printf("Report send has failed.");
                    }
                });
        });
    }

    public testTimeout() : void {
        const service : ReportServiceConnector = new ReportServiceConnector(false,
            "test/resource/data/Io/Oidis/Services/RuntimeTests/timeout.connector.config.jsonp");
        this.addButton("Test connection", () : void => {
            service.LogIt("Data send from test code.");
        });
    }

    protected before() : void {
        this.service = new ReportServiceConnector(false,
            // "https://hub.dev.oidis.io/connector.config.jsonp"
            "https://localhost.oidis.io/connector.config.jsonp"
        );
    }
}

class MockLogAdapter extends BaseAdapter {
    private readonly buffer : ILoggerTrace[];

    constructor() {
        super();
        this.buffer = [];
    }

    public getLogs() : string {
        let output : string = "";
        this.buffer.forEach(($trace : ILoggerTrace) : void => {
            output += $trace.message + <string>this.NewLineType();
        });
        return output;
    }

    protected print($trace : ILoggerTrace) {
        this.buffer.push($trace);
    }
}
/* dev:end */
