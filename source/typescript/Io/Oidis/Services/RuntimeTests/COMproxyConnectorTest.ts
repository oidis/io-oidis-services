/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { COMproxyConnector } from "../Connectors/COMproxyConnector.js";
import { LiveContentArgumentType } from "../Enums/LiveContentArgumentType.js";

export class COMproxyConnectorTest extends RuntimeTestRunner {
    private instanceId : number;

    public testCreateInstanceFromCLSID() : void {
        const service : COMproxyConnector = new COMproxyConnector();
        this.addButton("Create FreeMASTER instance", () : void => {
            service
                .CreateInstanceFromCLSID("{48A185F1-FFDB-11D3-80E3-00C04F176153}")
                .Then(($instanceId : number) : void => {
                    this.instanceId = $instanceId;
                    Echo.Printf("Instance id: " + $instanceId);
                });
        });
    }

    public testCreateInstanceFromCLSIDas() : void {
        const service : COMproxyConnector = new COMproxyConnector();
        this.addButton("Create named FreeMASTER instance", () : void => {
            service
                .CreateInstanceFromCLSIDas("{48A185F1-FFDB-11D3-80E3-00C04F176153}", "FM")
                .Then(($instanceId : number) : void => {
                    this.instanceId = $instanceId;
                    Echo.Printf("Instance id: " + $instanceId);
                });
        });
    }

    public testCreateInstanceFromProgID() : void {
        const service : COMproxyConnector = new COMproxyConnector();
        this.addButton("Create MCB.PCM instance", () : void => {
            service
                .CreateInstanceFromProgID("MCB.PCM")
                .Then(($instanceId : number) : void => {
                    this.instanceId = $instanceId;
                    Echo.Printf("Instance id: " + $instanceId);
                });
        });
    }

    public testCreateInstanceFromProgIDas() : void {
        const service : COMproxyConnector = new COMproxyConnector();
        this.addButton("Create named MCB.PCM instance", () : void => {
            service
                .CreateInstanceFromProgIDas("MCB.PCM", "FM")
                .Then(($instanceId : number) : void => {
                    this.instanceId = $instanceId;
                    Echo.Printf("Instance id: " + $instanceId);
                });
        });
    }

    public testGetFreeMASTERversion() : void {
        const service : COMproxyConnector = new COMproxyConnector();
        this.addButton("Get FreeMASTER version", () : void => {
            service
                .InvokeMethod(this.instanceId, "GetAppVersion", LiveContentArgumentType.OUT_STRING)
                .Then(($versionNumber : number, $versionString : string) : void => {
                    Echo.Printf("FreeMASTER version: " + $versionString + "[" + $versionNumber + "]");
                });
        });
    }

    public testInstanceExists() : void {
        const service : COMproxyConnector = new COMproxyConnector();
        this.addButton("Named instance exists", () : void => {
            service
                .Exists("FM")
                .Then(($status : boolean) : void => {
                    if ($status) {
                        Echo.Printf("instance exists");
                    } else {
                        Echo.Printf("instance does not exists");
                    }
                });
        });
    }

    public testDestroyInstance() : void {
        const service : COMproxyConnector = new COMproxyConnector();
        this.addButton("Destroy current instance", () : void => {
            service.Destroy(this.instanceId);
        });
    }

    public testTestEvent() : void {
        const service : COMproxyConnector = new COMproxyConnector();

        let registered : boolean = false;
        this.addButton("Test event", () : void => {
            if (!registered) {
                service.AddEventHandler(this.instanceId, "OnCommPortStateChanged", 0x1002, (...$args : any[]) : void => {
                    Echo.Printf("OnCommPortStateChanged args: {0}", $args);
                });
                service.AddEventHandler(this.instanceId, "OnTestEv", 0x1001, (...$args : any[]) : void => {
                    Echo.Printf("OnTestEv args: {0}", $args);
                });
                registered = true;
            }
            const callback : any = ($returnValue : any, $instanceID : number, $eventName : string) : void => {
                Echo.Printf("event fired: " + $instanceID + ", " + $eventName);
            };
            service.TestEvent(this.instanceId, 0x1002).Then(callback);
            service.TestEvent(this.instanceId, 0x1001).Then(callback);
        });
    }

    public testTearDown() : void {
        const service : COMproxyConnector = new COMproxyConnector();
        this.addButton("Tear down", () : void => {
            service
                .TearDown()
                .Then(($status : boolean) : void => {
                    if ($status) {
                        Echo.Printf("session destroyed");
                    } else {
                        Echo.Printf("failed to destroy session");
                    }
                });
        });
    }
}
/* dev:end */
