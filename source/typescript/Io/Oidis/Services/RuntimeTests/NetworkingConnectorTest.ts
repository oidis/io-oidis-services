/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { NetworkingConnector } from "../Connectors/NetworkingConnector.js";

export class NetworkingConnectorTest extends RuntimeTestRunner {
    private networkingConnector : NetworkingConnector;

    public before() : void {
        this.networkingConnector = new NetworkingConnector();
        this.networkingConnector.getEvents().OnError(($args : ErrorEventArgs) : void => {
            Echo.Println($args.Message());
        });
    }

    public testSingleGetFreePort() : void {
        this.addButton("Find free port", () : void => {
            this.networkingConnector
                .getFreePort()
                .Then(($port : number) : void => {
                    Echo.Printf("<b>Free port found: </b>" + $port);
                });
        });
    }

    public testRepeatGetFreePort() : void {
        this.addButton("repeat getFreePort request - async", () : void => {
            let lastPort : number = -1;

            const getNextPort : any = ($index : number) : void => {
                if ($index < 20) {
                    this.networkingConnector
                        .getFreePort()
                        .Then(($port : number) : void => {
                            if (lastPort === $port) {
                                Echo.Println("<b style=\"color: red;\">Ports are identical<b/>[" + $index + "]");
                            } else {
                                Echo.Println("<span style=\"color: green;\">OK [" + $index + "]</span>");
                            }
                            lastPort = $port;
                            getNextPort($index + 1);
                        });
                }
            };
            getNextPort(0);
        });

        this.addButton("repeat getFreePort request - sync", () : void => {
            let lastPort : number = -1;

            for (let $index : number = 0; $index < 20; $index++) {
                this.networkingConnector
                    .getFreePort()
                    .Then(($port : number) : void => {
                        if (lastPort === $port) {
                            Echo.Println("<b style=\"color: red;\">Ports are identical<b/>[" + $index + "]");
                        } else {
                            Echo.Println("<span style=\"color: green;\">OK [" + $index + "]</span>");
                        }
                        lastPort = $port;
                    });
            }
        });
    }
}
/* dev:end */
