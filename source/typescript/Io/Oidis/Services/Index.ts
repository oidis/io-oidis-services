/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IndexPage } from "@io-oidis-gui/Io/Oidis/Gui/Pages/IndexPage.js";
import { AsyncBasePageController } from "./HttpProcessor/Resolvers/AsyncBasePageController.js";
import { AgentsRegisterConnectorTest } from "./RuntimeTests/AgentsRegisterConnectorTest.js";
import { AsyncFileSystemConnectorTest } from "./RuntimeTests/AsyncFileSystemConnectorTest.js";
import { AsyncTerminalConnectorTest } from "./RuntimeTests/AsyncTerminalConnectorTest.js";
import { CertsConnectorTest } from "./RuntimeTests/CertsConnectorTest.js";
import { COMproxyConnectorTest } from "./RuntimeTests/COMproxyConnectorTest.js";
import { FFIProxyConnectorTest } from "./RuntimeTests/FFIProxyConnectorTest.js";
import { FileSystemConnectorTest } from "./RuntimeTests/FileSystemConnectorTest.js";
import { IdeHandlerConnectorTest } from "./RuntimeTests/IdeHandlerConnectorTest.js";
import { InstallationProcessTest } from "./RuntimeTests/InstallationProcessTest.js";
import { JxbrowserLiveContentWrapperTest } from "./RuntimeTests/JxbrowserLiveContentWrapperTest.js";
import { LogInTest } from "./RuntimeTests/LogInTest.js";
import { NetworkingConnectorTest } from "./RuntimeTests/NetworkingConnectorTest.js";
import { PageControllerTest } from "./RuntimeTests/PageControllerTest.js";
import { RenderTest } from "./RuntimeTests/RenderTest.js";
import { ReportServiceTest } from "./RuntimeTests/ReportServiceTest.js";
import { RestWebServiceTest } from "./RuntimeTests/RestWebServiceTest.js";
import { TerminalConnectorTest } from "./RuntimeTests/TerminalConnectorTest.js";
import { VirtualBoxConnectorTest } from "./RuntimeTests/VirtualBoxConnectorTest.js";
import { WebSocketsServiceTest } from "./RuntimeTests/WebSocketsServiceTest.js";
import { WindowHandlerConnectorTest } from "./RuntimeTests/WindowHandlerConnectorTest.js";

/**
 * Index request resolver class provides handling of web index page.
 */
export class Index extends AsyncBasePageController {

    constructor() {
        super();
        this.setInstanceOwner(new IndexPage());
    }

    protected async onSuccess($instance : IndexPage) : Promise<void> {
        $instance.headerTitle.Content("Oidis Framework Services Library");
        $instance.headerInfo.Content("Oidis Framework library focused on services and " +
            "business logic for applications build on Oidis Framework");

        let content : string = "";
        /* dev:start */
        content += `
            <h3>Runtime tests</h3>
            <a href="${RestWebServiceTest.CallbackLink()}">REST web services test</a>
            <a href="${WebSocketsServiceTest.CallbackLink()}">WebSockets services test</a>
            <a href="${ReportServiceTest.CallbackLink()}">Report service test</a>
            <a href="${FileSystemConnectorTest.CallbackLink()}">FileSystem connector test</a>
            <a href="${AsyncFileSystemConnectorTest.CallbackLink()}">FileSystem connector test ASYNC</a>
            <a href="${COMproxyConnectorTest.CallbackLink()}">COM Proxy test</a>
            <a href="${FFIProxyConnectorTest.CallbackLink()}">FFI Proxy test</a>
            <a href="${TerminalConnectorTest.CallbackLink()}">Terminal connector test</a>
            <a href="${AsyncTerminalConnectorTest.CallbackLink()}">Terminal connector test ASYNC</a>
            <a href="${AgentsRegisterConnectorTest.CallbackLink()}">AgentsRegister connector test</a>
            <a href="${NetworkingConnectorTest.CallbackLink()}">Terminal connector test ASYNC</a>
            <a href="${LogInTest.CallbackLink()}">LogIn test</a>
            <a href="${InstallationProcessTest.CallbackLink()}">Installation process test</a>
            <a href="${WindowHandlerConnectorTest.CallbackLink()}">WindowHandler connector test</a>
            <a href="${JxbrowserLiveContentWrapperTest.CallbackLink()}">Jxbrowser LiveContentWrapper Test</a>
            <a href="${IdeHandlerConnectorTest.CallbackLink()}">Jxbrowser IdeHandlerConnector Test</a>
            <a href="${VirtualBoxConnectorTest.CallbackLink()}">VirtualBox connector Test</a>
            <a href="?debug=JRESimulator#/${PageControllerTest.ClassNameWithoutNamespace()}">PageController Test</a>
            <a href="${RenderTest.CallbackLink()}">Render Test</a>
            <a href="${CertsConnectorTest.CallbackLink()}">Certs connector Test</a>
            `;
        /* dev:end */

        $instance.pageIndex.Content(content);
        $instance.footerInfo.Content("" +
            "version: " + this.getEnvironmentArgs().getProjectVersion() +
            ", build: " + this.getEnvironmentArgs().getBuildTime() +
            ", " + this.getEnvironmentArgs().getProjectConfig().target.copyright);
    }
}
