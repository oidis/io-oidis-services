/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IEventsHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IEventsHandler.js";
import { InstallationProgressEventArgs } from "../../Events/Args/InstallationProgressEventArgs.js";

export interface IInstallationProcessEventsHandler extends IEventsHandler {
    ($eventArgs : InstallationProgressEventArgs, ...$args : any[]) : void;
}

// generated-code-start
export const IInstallationProcessEventsHandler = globalThis.RegisterInterface([], <any>IEventsHandler);
// generated-code-end
