/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IBaseObject.js";
import { IEventsHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IEventsHandler.js";
import { IErrorEventsHandler } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/IErrorEventsHandler.js";
import { IInstallationProcessEventsHandler } from "./IInstallationProcessEventsHandler.js";

export interface IInstallationRecipeEvents extends IBaseObject {
    setOnStart($handler : IEventsHandler) : void;

    setOnChange($handler : IInstallationProcessEventsHandler) : void;

    setOnComplete($handler : IInstallationProcessEventsHandler) : void;

    setOnError($handler : IErrorEventsHandler) : void;
}

// generated-code-start
/* eslint-disable */
export const IInstallationRecipeEvents = globalThis.RegisterInterface(["setOnStart", "setOnChange", "setOnComplete", "setOnError"], <any>IBaseObject);
/* eslint-enable */
// generated-code-end
