/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IWebServiceClientEvents } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Events/IWebServiceClientEvents.js";
import { IWindowChangedEvent } from "./IWindowChangedEvent.js";

export interface IWindowHandlerConnectorEvents extends IWebServiceClientEvents {
    OnNotifyIconClick($handler : () => void) : void;

    OnNotifyIconDoubleClick($handler : () => void) : void;

    OnNotifyIconBalloonClick($handler : () => void) : void;

    OnNotifyIconContextItemSelected($handler : ($name : string) => void) : void;

    OnWindowChanged($handler : (changedArgs : IWindowChangedEvent) => void) : void;
}

// generated-code-start
/* eslint-disable */
export const IWindowHandlerConnectorEvents = globalThis.RegisterInterface(["OnNotifyIconClick", "OnNotifyIconDoubleClick", "OnNotifyIconBalloonClick", "OnNotifyIconContextItemSelected", "OnWindowChanged"], <any>IWebServiceClientEvents);
/* eslint-enable */
// generated-code-end
