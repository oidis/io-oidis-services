/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { WindowStateType } from "../../Enums/WindowStateType.js";

export interface IWindowChangedEvent {
    x : number;
    y : number;
    width : number;
    height : number;
    state : WindowStateType;
}

// generated-code-start
export const IWindowChangedEvent = globalThis.RegisterInterface(["x", "y", "width", "height", "state"]);
// generated-code-end
