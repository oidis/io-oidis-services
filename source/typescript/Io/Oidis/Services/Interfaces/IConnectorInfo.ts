/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export interface IConnectorInfo {
    owner : string;
    namespace : string;
    requester : string;
    created : number;
}

// generated-code-start
export const IConnectorInfo = globalThis.RegisterInterface(["owner", "namespace", "requester", "created"]);
// generated-code-end
