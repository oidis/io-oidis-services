/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";

export interface ILiveContentErrorPromise {

    OnError($callback : ($error : ErrorEventArgs, ...$args : any[]) => void) : ILiveContentPromise;

    Then($callback : (...$args : any[]) => void) : void;
}

export interface ILiveContentPromise {
    Then($callback : (...$args : any[]) => void) : void;
}

// generated-code-start
export const ILiveContentErrorPromise = globalThis.RegisterInterface(["OnError", "Then"]);
export const ILiveContentPromise = globalThis.RegisterInterface(["Then"]);
// generated-code-end
