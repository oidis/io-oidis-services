/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/**
 * Imports are focused on reuse of another configuration files
 */
export interface IImports {
    /**
     * Specify variable name, which will be accessible in interfaces block
     */
    variable : string;

    /**
     * Specify jsonp configuration source path from, which should be variable loaded
     */
    source : string;
}

/**
 * Localization is suitable for extending of existing configuration to its language mutations
 */
export interface ILocalization {
    /**
     * Specify jsonp configuration source path from, which should be loaded English localization
     */
    en? : string;

    /**
     * Specify jsonp configuration source path from, which should be loaded Czech localization
     */
    cz? : string;
    cs? : string;

    /**
     * Specify jsonp configuration source path from, which should be loaded Chinese localization
     */
    cn? : string;
}

/**
 * IBaseConfiguration is minimal interface expected at any type of configuration
 */
export interface IBaseConfiguration {
    /**
     * Specify version, which will be used as unique identifier for current version of the configuration file
     */
    version? : string;

    /**
     * Specify jsonp configuration source path, which should be extended by the current configuration
     */
    extendsConfig? : string;

    /**
     * Specify interface type, which should be used for identification of DAO parser
     * (currently available IBaseConfiguration, IBasePageConfiguration)
     */
    $interface? : string;

    /**
     * Specify json object as array of variables,
     * which should be imported as instances of configuration files
     */
    imports? : IImports[];

    /**
     * Specify language mutation settings for current configuration file
     */
    localization? : ILocalization;

    /**
     * Specify specify true to disable resources cache
     */
    noCache? : boolean;

    /**
     * Get instance of parent configuration, if current configuration is extending another one
     */
    $super : IBaseConfiguration;

    /**
     * @param {string} $messages Specify messages, which should be printed to Echo container in rendered GUI
     * Supported is string formatting by {0}, {1}, ...
     * @param {...any} $attributes Specify attributes for string formatting
     * @returns {void}
     */
    Echo : ($message : string, ...$attributes : any[]) => void;

    /**
     * @param {string} $messages Specify message, which should be printed as debug information.
     * Supported is string formatting by {0}, {1}, ...
     * @param {...any} $attributes Specify attributes for string formatting
     * @returns {void}
     */
    Log : ($message : string, ...$attributes : any[]) => void;

    /**
     * @param {string} $messages Specify message, which should be printed/notified as warning.
     * Supported is string formatting by {0}, {1}, ...
     * @param {...any} $attributes Specify attributes for string formatting
     * @returns {void}
     */
    Warning : ($message : string, ...$attributes : any[]) => void;

    /**
     * @param {string} $messages Specify message, which should be printed/notified as an error.
     * Supported is string formatting by {0}, {1}, ...
     * @param {...any} $attributes Specify attributes for string formatting
     * @returns {void}
     */
    Error : ($message : string, ...$attributes : any[]) => void;
}

// generated-code-start
/* eslint-disable */
export const IImports = globalThis.RegisterInterface(["variable", "source"]);
export const ILocalization = globalThis.RegisterInterface(["en", "cz", "cs", "cn"]);
export const IBaseConfiguration = globalThis.RegisterInterface(["version", "extendsConfig", "$interface", "imports", "localization", "noCache", "$super", "Echo", "Log", "Warning", "Error"]);
/* eslint-enable */
// generated-code-end
