/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/**
 * IInstallationUtils is interface, which provides useful functions required by Installation process.
 */
export interface IInstallationUtils {
    /**
     * @param {string} $program Program name, which should be treated by standard version call.
     * @param {IInstallationUtilValuePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    getVersion : ($program : string, $done : IInstallationUtilValuePromise) => void;

    /**
     * @param {string} $program Program name, which should be treated by standard version call.
     * @param {string} $versionPrefix Specify version prefix, which should be trimmed.
     * @param {string} $pattern Specify version patter suitable for validation.
     * @param {IInstallationUtilAcknowledgePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    VersionIsIncluded : ($program : string, $versionPrefix : string, $pattern : string,
                         $done : IInstallationUtilAcknowledgePromise) => void;

    /**
     * @param {string} $key Register key, which should be searched.
     * @param {string} $name Register value name, which should be searched.
     * @param {IInstallationUtilValuePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    getRegisterValue : ($key : string, $name : string, $done : IInstallationUtilValuePromise) => void;

    /**
     * @param {string} $key Register key, which should be added.
     * @param {string} $name Register value name, which should be added.
     * @param {string|number} $value Value, which should be stored.
     * @param {IInstallationUtilStatusPromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    setRegisterValue : ($key : string, $name : string, $value : string | number, $done : IInstallationUtilStatusPromise) => void;

    /**
     * @param {string[][]} $pairs Register key/name/value paris, which should be added.
     * @param {IInstallationUtilAcknowledgePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    setRegisterValues : ($pairs : string[][], $done : IInstallationUtilAcknowledgePromise) => void;

    /**
     * @param {string} $key Register key, which should be validated.
     * @param {string} $name Register name, which should be validated.
     * @param {IInstallationUtilAcknowledgePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    RegisterValueExists : ($key : string, $name : string, $done : IInstallationUtilAcknowledgePromise) => void;

    /**
     * @param {string[][]} $pairs Register key/name paris, which should be validated.
     * @param {IInstallationUtilAcknowledgePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    AnyOfRegisterValueExists : ($pairs : string[][], $done : IInstallationUtilAcknowledgePromise) => void;

    /**
     * @param {string} $key Register key, which should be removed.
     * @param {string} $name Register value name, which should be removed.
     * @param {IInstallationUtilStatusPromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    RemoveRegisterKey : ($key : string, $name : string, $done : IInstallationUtilStatusPromise) => void;

    /**
     * @param {string} $path Specify path to the executable, which should be executed as child process.
     * @param {IInstallationUtilAcknowledgePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    RunExecutable : ($path : string, $done : IInstallationUtilAcknowledgePromise) => void;

    /**
     * @param {string} $cmd Specify command, which should be elevate as child process.
     * @param {string[]} $args Specify command line arguments for child process.
     * @param {IInstallationUtilStatusPromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    Elevate : ($cmd : string, $args : string[], $done : IInstallationUtilStatusPromise) => void;

    /**
     * Returns true, if current platform is 64 bit, otherwise false
     * @param {IInstallationUtilStatusPromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    Is64Bit : ($done : IInstallationUtilStatusPromise) => void;

    /**
     * @param {string} $path Specify path, which should be validated.
     * @param {IInstallationUtilStatusPromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    SystemPathExists : ($path : string, $done : IInstallationUtilStatusPromise) => void;

    /**
     * @param {string} $path Specify path, which should be added to system PATH.
     * @param {IInstallationUtilStatusPromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    AddToSystemPath : ($forAll : boolean, $path : string, $done : IInstallationUtilStatusPromise) => void;

    /**
     * @param {string} $path Specify path, which should be removed from system PATH.
     * @param {IInstallationUtilStatusPromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    RemoveSystemPath : ($path : string, $done : IInstallationUtilStatusPromise) => void;

    /**
     * Returns drive label where is OS hosted.
     * @param {IInstallationUtilStatusOrValuePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    getSystemDrive : ($done : IInstallationUtilStatusOrValuePromise) => void;

    /**
     * Returns directory where is Windows OS installed.
     * @param {IInstallationUtilStatusOrValuePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    getWinDir : ($done : IInstallationUtilStatusOrValuePromise) => void;

    /**
     * Returns path to Program files based on current OS platform.
     * @param {IInstallationUtilStatusOrValuePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    getProgramFilesPath : ($done : IInstallationUtilStatusOrValuePromise) => void;

    /**
     * Returns path to Desktop for all users or just current user.
     * @param {boolean} $forAll Specify, if path should be for all users.
     * @param {IInstallationUtilStatusOrValuePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    getDesktopPath : ($forAll : boolean, $done : IInstallationUtilStatusOrValuePromise) => void;

    /**
     * Returns path to Start menu for all users or just current user.
     * @param {boolean} $forAll Specify, if path should be for all users.
     * @param {IInstallationUtilStatusOrValuePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    getStartMenuPath : ($forAll : boolean, $done : IInstallationUtilStatusOrValuePromise) => void;

    /**
     * Returns path to local app data folder for current user.
     * @param {IInstallationUtilValuePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    getLocalAppDataPath : ($done : IInstallationUtilValuePromise) => void;

    /**
     * Returns path to Git executable. Note that default relative path "git.exe" expects globally installed git with
     * path to executable registered in environment PATH.
     * @param {IInstallationUtilValuePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    getGitPath : ($done : IInstallationUtilValuePromise) => void;

    /**
     * Returns path to MinGW installation folder
     * @param {IInstallationUtilValuePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    getMinGWPath : ($done : IInstallationUtilValuePromise) => void;

    /**
     * Returns path to MSYS2 installation folder
     * @param {IInstallationUtilValuePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    getMsysPath : ($done : IInstallationUtilValuePromise) => void;

    /**
     * Returns path to CMake installation folder
     * @param {IInstallationUtilValuePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    getCmakePath : ($done : IInstallationUtilValuePromise) => void;

    /**
     * @param {string} $source Specify repository source path.
     * @param {string} $destination Specify path where should be repository cloned.
     * @param {IInstallationUtilValuePromise} $onMessage Specify, handler for messages received from terminal.
     * @param {IInstallationUtilStatusOrValuePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    Clone : ($source : string, $destination : string, $onMessage : IInstallationUtilValuePromise,
             $done : IInstallationUtilStatusOrValuePromise) => void;

    /**
     * @param {boolean} $forAll Specify, if application should be registered for all users.
     * @param {string} $name Specify application name.
     * @param {string} $version Specify application version.
     * @param {string} $path Specify path to the application executable.
     * @param {IInstallationUtilAcknowledgePromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    RegisterApp : ($forAll : boolean, $name : string, $version : string, $path : string,
                   $done : IInstallationUtilAcknowledgePromise) => void;

    /**
     * @param {string} $path Specify path to installer or MSI Register Key.
     * @param {IInstallationUtilStatusPromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    RunSilentUninstall : ($path : string, $done : IInstallationUtilStatusPromise) => void;

    /**
     * @param {string} $command Specify command, which should be executed in Unix shell environment.
     * @param {string[]} $args Specify command args, which should be passed to the executed process.
     * @param {IInstallationUtilTerminalPromise} $done Method callback for async handling with desired interface.
     * @returns {void}
     */
    ShellExecute : ($command : string, $args : string[], $callback : IInstallationUtilTerminalPromise,
                    $useMsysSystem? : boolean) => void;
}

export type IInstallationUtilValuePromise = ($value : string) => void;

export type IInstallationUtilAcknowledgePromise = ($status : boolean) => void;

export type IInstallationUtilStatusPromise = ($status : boolean, $error? : string) => void;

export type IInstallationUtilStatusOrValuePromise = ($value : string | boolean, $error? : string) => void;

export type IInstallationUtilTerminalPromise = ($exitCode : number, $stdout? : string, $stderr? : string) => void;

// generated-code-start
/* eslint-disable */
export const IInstallationUtils = globalThis.RegisterInterface(["getVersion", "VersionIsIncluded", "getRegisterValue", "setRegisterValue", "setRegisterValues", "RegisterValueExists", "AnyOfRegisterValueExists", "RemoveRegisterKey", "RunExecutable", "Elevate", "Is64Bit", "SystemPathExists", "AddToSystemPath", "RemoveSystemPath", "getSystemDrive", "getWinDir", "getProgramFilesPath", "getDesktopPath", "getStartMenuPath", "getLocalAppDataPath", "getGitPath", "getMinGWPath", "getMsysPath", "getCmakePath", "Clone", "RegisterApp", "RunSilentUninstall", "ShellExecute"]);
/* eslint-enable */
// generated-code-end
