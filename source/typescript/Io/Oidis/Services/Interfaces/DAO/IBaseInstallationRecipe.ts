/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { FileSystemHandlerConnector, IFileSystemDownloadOptions } from "../../Connectors/FileSystemHandlerConnector.js";
import { TerminalConnector } from "../../Connectors/TerminalConnector.js";
import { InstallationProgressCode } from "../../Enums/InstallationProgressCode.js";
import { IBaseConfiguration } from "./IBaseConfiguration.js";
import { IInstallationUtils } from "./IInstallationUtils.js";

/**
 * IInstallationHandler is interface required by package functional API.
 */
export interface IInstallationHandler {
    (...$args : any[]) : void;

    ($status : boolean, $message? : string) : void;
}

/**
 * IInstallationPackageSource is interface required by package source if source should be diverged based on platform type or
 * package source requires specification of additional HTTP request options.
 */
export interface IInstallationPackageSource {
    /**
     * Specify global HTTP request options for diverged data sources.
     */
    options? : IFileSystemDownloadOptions;
    /**
     * Specify data source, which is specific for 32 bit platforms
     * as string or with additional HTTP request options.
     */
    x86 : string | IFileSystemDownloadOptions;
    /**
     * Specify data source, which is specific for 64 bit platforms
     * as string or with additional HTTP request options.
     */
    x64 : string | IFileSystemDownloadOptions;
}

/**
 * IInstallationPackage is interface, which provides detailed instruction for handling of Installation process.
 */
export interface IInstallationPackage {
    /**
     * Readonly property, which reflex package name defined directly by its name in packages list.
     */
    name? : string;
    /**
     * Package description, which should be used by installation process.
     */
    description? : string;
    /**
     * Package info, which should be used by installation summary.
     */
    info? : string;
    /**
     * Estimated package size after installation.
     */
    size? : number;
    /**
     * Required package version, which should be validated.
     */
    version? : string;
    /**
     * Package source, which will be used for package execution.
     */
    source : string | IFileSystemDownloadOptions | IInstallationPackageSource;
    /**
     * Condition, which should be used as decision maker for installation of the package.
     */
    installCondition? : IInstallationHandler;
    /**
     * Function, which is able to select correct package source suitable for execution based
     * on current OS version and installation conditions. This function should be overridden only in case of that standard
     * implementation is not flexible enough.
     */
    selectSource? : IInstallationHandler;
    /**
     * Function, which is able to serve and prepare package source for execution.
     * This function should be overridden only in case of that standard implementation is not flexible enough.
     */
    getSource? : IInstallationHandler;
    /**
     * Function, which should be used for installation process.
     */
    install? : IInstallationHandler;
    /**
     * Function, which should be used for uninstallation process.
     */
    uninstall? : IInstallationHandler;
    /**
     * Function, which should be used for repair process.
     */
    repair? : IInstallationHandler;
    /**
     * Function, which should be used for validation, if package is installed correctly.
     */
    validate? : IInstallationHandler;
    /**
     * Specify, if package should be force processed.
     */
    force? : boolean;
    /**
     * Specify, if package installation is optional.
     */
    optional? : boolean;
}

export interface IInstallationRecipeNotifications {
    package_is_installed_correctly : string;
    package_is_missing_or_damaged__0__ : string;
    package_is_already_installed : string;
    step__0__has_been_skipped : string;
    unsupported_callback_status__0__detected_for__1__at_step__2__ : string;
    undefined_installation_protocol__0__ : string;
    undefined_installation_chain__0__ : string;
    unsupported_definition_of_package_source_for__0__ : string;
    package_source__0__for__1__does_not_exist : string;
    unable_to_clone_package_source_for__0__ : string;
    required_GIT__0__has_not_been_found : string;
    get_source__0__ : string;
    unable_to_delete_downloaded_package : string;
    unable_to_move_package_source_to_tmp_destination : string;
    unable_to_create_installation_folder : string;
    undefined_package_source_for__0__ : string;
}

export interface IInstallationEnvironment extends IBaseConfiguration {
    /**
     * Automatically generated variable, which reflex currently processed package.
     */
    block? : IInstallationPackage;
    /**
     * List of all available packages for installation chain in JSON format.
     */
    packages : IInstallationPackage[];
    /**
     * List of all packages, which should be processed by current installation.
     */
    getChain : () => string[];
    /**
     * Quick access to instance of Terminal connector, which can be required by installation functions.
     */
    terminal? : TerminalConnector;
    /**
     * Quick access to instance of File System connector,
     * which can be required by installation functions.
     */
    fileSystem? : FileSystemHandlerConnector;
    /**
     * Instance of Installation utils, which will be imported based on current platform.
     */
    utils? : IInstallationUtils;
    /**
     * List of notifications used by installation logic.
     */
    notifications : IInstallationRecipeNotifications;
    /**
     * @param {boolean} $status Specify on change status.
     * @param {string} $description Specify message, which describes current change.
     * @param {number} [$currentValue] Specify current change index.
     * @param {number} [$endValue] Specify expected changes count.
     * @param {InstallationProgressCode} [$code] Specify change code.
     * @returns {void}
     */
    onChange? : ($status : boolean, $description : string, $currentValue? : number, $endValue? : number,
                 $code? : InstallationProgressCode) => void;
}

// generated-code-start
/* eslint-disable */
export const IInstallationHandler = globalThis.RegisterInterface([]);
export const IInstallationPackageSource = globalThis.RegisterInterface(["options", "x86", "x64"]);
export const IInstallationPackage = globalThis.RegisterInterface(["name", "description", "info", "size", "version", "source", "installCondition", "selectSource", "getSource", "install", "uninstall", "repair", "validate", "force", "optional"]);
export const IInstallationRecipeNotifications = globalThis.RegisterInterface(["package_is_installed_correctly", "package_is_missing_or_damaged__0__", "package_is_already_installed", "step__0__has_been_skipped", "unsupported_callback_status__0__detected_for__1__at_step__2__", "undefined_installation_protocol__0__", "undefined_installation_chain__0__", "unsupported_definition_of_package_source_for__0__", "package_source__0__for__1__does_not_exist", "unable_to_clone_package_source_for__0__", "required_GIT__0__has_not_been_found", "get_source__0__", "unable_to_delete_downloaded_package", "unable_to_move_package_source_to_tmp_destination", "unable_to_create_installation_folder", "undefined_package_source_for__0__"]);
export const IInstallationEnvironment = globalThis.RegisterInterface(["block", "packages", "getChain", "terminal", "fileSystem", "utils", "notifications", "onChange"], <any>IBaseConfiguration);
/* eslint-enable */
// generated-code-end
