/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseConfiguration } from "./IBaseConfiguration.js";

/**
 * IBasePageLocalization is minimal interface expected at any type of configuration focused on Page localization
 */
export interface IBasePageLocalization extends IBaseConfiguration {
    /**
     * Specify file path to the GUI static cache, which should be used for page fast load
     */
    cacheFile? : string;

    /**
     * Specify page title value
     */
    title? : string;

    /**
     * Specify text value for page loading label
     */
    loading? : string;

    /**
     * Specify text value for page loading progress label
     */
    loadingProgress? : string;
}

// generated-code-start
/* eslint-disable */
export const IBasePageLocalization = globalThis.RegisterInterface(["cacheFile", "title", "loading", "loadingProgress"], <any>IBaseConfiguration);
/* eslint-enable */
// generated-code-end
