/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export interface IWebServiceException {
    message : string;
    file : string;
    line : number;
    col : number;
    code : number;
    stack : string;
    args? : any;
}

export interface ILiveContentProtocol {
    name : string;
    args : string;
    returnValue : any;
}

export interface IWebServiceProtocol {
    id? : number;
    clientId? : number;
    type? : string;
    status? : number;
    origin? : string;
    token? : string;
    data : string | IWebServiceException | ILiveContentProtocol | IWebServiceProtocol[];
    language? : string;
    version? : string;
}

// generated-code-start
/* eslint-disable */
export const IWebServiceException = globalThis.RegisterInterface(["message", "file", "line", "col", "code", "stack", "args"]);
export const ILiveContentProtocol = globalThis.RegisterInterface(["name", "args", "returnValue"]);
export const IWebServiceProtocol = globalThis.RegisterInterface(["id", "clientId", "type", "status", "origin", "token", "data", "language", "version"]);
/* eslint-enable */
// generated-code-end
