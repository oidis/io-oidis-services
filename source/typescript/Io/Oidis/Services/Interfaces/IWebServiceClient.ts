/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import {
    IWebServiceClient as Parent,
    IWebServiceResponseHandler
} from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IWebServiceClient.js";
import { IWebServiceProtocol } from "./IWebServiceProtocol.js";

export interface IWebServiceClient extends Parent {

    /**
     * @param {IWebServiceProtocol} $data Specify request command.
     * @param {IWebServiceResponseHandler} [$handler] Specify response handler.
     * @returns {void}
     */
    Send($data : IWebServiceProtocol, $handler? : IWebServiceResponseHandler) : void;
}

// generated-code-start
export const IWebServiceClient = globalThis.RegisterInterface(["Send"], <any>Parent);
// generated-code-end
