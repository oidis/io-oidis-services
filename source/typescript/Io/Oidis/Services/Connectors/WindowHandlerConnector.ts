/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { IWebServiceClientEvents } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Events/IWebServiceClientEvents.js";
import { IWebServiceResponseHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IWebServiceClient.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { WindowCornerType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/WindowCornerType.js";
import { NotifyBalloonIconType } from "../Enums/NotifyBalloonIconType.js";
import { TaskBarProgressState } from "../Enums/TaskBarProgressState.js";
import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { WindowStateType } from "../Enums/WindowStateType.js";
import { IWindowHandlerConnectorEvents } from "../Interfaces/Events/IWindowHandlerConnectorEvents.js";
import { BaseConnector, IConnectorOptions } from "../Primitives/BaseConnector.js";

/**
 * WindowHandlerConnector class provides wrapping of window manipulation provided by Oidis Connector instance.
 */
export class WindowHandlerConnector extends BaseConnector {
    private readonly subscribers : ArrayList<ArrayList<any>>;
    private readonly pid : number;
    private readonly title : string;

    /**
     * @param {number} $pid Specify process ID, which should be handled
     * @param {string} [$title] Specify Window title in desired process
     */
    constructor($pid? : number, $title? : string) {
        super(<IConnectorOptions>{reconnect: true, suppressInit: true});

        this.subscribers = new ArrayList<ArrayList<any>>();
        if (ObjectValidator.IsSet($pid)) {
            this.pid = Property.Integer(this.pid, $pid);
            this.title = Property.String(this.title, $title);
        }
        this.init();
    }

    /**
     * @returns {IWindowHandlerConnectorEvents} Returns events interface connected with connector instance.
     */
    public getEvents() : IWindowHandlerConnectorEvents {
        const superEvents : IWebServiceClientEvents = this.getClient().getEvents();
        this.extendClientEvents(superEvents, "OnNotifyIconClick");
        this.extendClientEvents(superEvents, "OnNotifyIconDoubleClick", "OnNotifyIconDbClick");
        this.extendClientEvents(superEvents, "OnNotifyIconBalloonClick");
        this.extendClientEvents(superEvents, "OnNotifyIconContextItemSelected");
        this.extendClientEvents(superEvents, "OnWindowChanged");
        return <IWindowHandlerConnectorEvents>superEvents;
    }

    /**
     * @param {string} $windowId Specify window identifier.
     * @returns {IWindowHandlerStatusPromise} Returns promise interface for handling of window close event.
     */
    public Close($windowId? : string) : IWindowHandlerStatusPromise {
        return this.invoke("Close", $windowId, this.pid, this.title);
    }

    /**
     * @param {string} $url Specify url which should be opened in new window.
     * @param {IWindowHandlerOpenOptions} [$options] Specify options, which should be passed to the executed command.
     * @returns {IWindowHandlerOpenPromise} Returns promise interface for handling of open event.
     */
    public Open($url : string, $options? : IWindowHandlerOpenOptions) : IWindowHandlerOpenPromise {
        const callbacks : any = {
            then($windowId : string, $url? : string, $data? : string) : void {
                // declare default callback
            }
        };
        this.invoke("Open", $url, $options)
            .Then(($result : IWindowHandlerResultProtocol | boolean) : void => {
                if (ObjectValidator.IsSet((<IWindowHandlerResultProtocol>$result).type)) {
                    const result : IWindowHandlerResultProtocol = <IWindowHandlerResultProtocol>$result;
                    if (result.type === EventType.ON_COMPLETE) {
                        callbacks.then(result.windowId, result.url, result.data);
                    }
                }
            });
        return <IWindowHandlerOpenPromise>{
            Then: ($callback : ($windowId : string, $url? : string, $data? : string) => void) : void => {
                callbacks.then = <any>$callback;
            }
        };
    }

    /**
     * @param {string} $windowId Specify window identifier.
     * @returns {IWindowHandlerCookiesPromise} Returns promise interface for handling of get cookies event.
     */
    public getCookies($windowId? : string) : IWindowHandlerCookiesPromise {
        return this.invoke("getCookies", $windowId);
    }

    /**
     * @param {string} $windowId Specify window identifier.
     * @param {IWindowHandlerScriptExecuteOptions} $options Specify options, which should be passed to the executed command.
     * @returns {IWindowHandlerChangePromise} Returns promise interface for handling of script execute event.
     */
    public ScriptExecute($windowId : string, $options : IWindowHandlerScriptExecuteOptions) : IWindowHandlerChangePromise {
        const callbacks : any = {
            onComplete($status : boolean) : any {
                // declare default callback
            },
            onMessage($message : string) : any {
                // declare default callback
            },
            onStart() : any {
                // declare default callback
            }
        };
        if (ObjectValidator.IsFunction($options.script)) {
            $options.script = "(" + Convert.FunctionToString($options.script) + ")();";
        }
        this.invoke("ScriptExecute", $windowId, $options)
            .Then(($result : IWindowHandlerResultProtocol | boolean) : void => {
                if (ObjectValidator.IsSet((<IWindowHandlerResultProtocol>$result).type)) {
                    const result : IWindowHandlerResultProtocol = <IWindowHandlerResultProtocol>$result;
                    if (result.type === EventType.ON_START) {
                        callbacks.onStart();
                    } else if (result.type === EventType.ON_CHANGE) {
                        callbacks.onMessage(result.data);
                    } else if (result.type === EventType.ON_COMPLETE) {
                        callbacks.onComplete(true);
                    }
                }
            });
        const promise : IWindowHandlerChangePromise = <IWindowHandlerChangePromise>{
            OnMessage: ($callback : ($message : string) => void) : IWindowHandlerChangePromise => {
                callbacks.onMessage = $callback;
                return promise;
            },
            OnStart  : ($callback : () => void) : IWindowHandlerChangePromise => {
                callbacks.onStart = $callback;
                return promise;
            },
            Then     : ($callback : ($status : boolean) => void) : void => {
                callbacks.onComplete = $callback;
            }
        };

        return promise;
    }

    /**
     * @returns {IWindowHandlerStatusPromise} Returns promise interface for handling of window minimize event.
     */
    public Minimize() : IWindowHandlerStatusPromise {
        return this.invoke("Minimize", this.pid, this.title);
    }

    /**
     * @returns {IWindowHandlerStatusPromise} Returns promise interface for handling of window maximize event.
     */
    public Maximize() : IWindowHandlerStatusPromise {
        return this.invoke("Maximize", this.pid, this.title);
    }

    /**
     * Restore window state to normal state
     * @returns {IWindowHandlerStatusPromise} Returns promise interface for handling of window restore event.
     */
    public Restore() : IWindowHandlerStatusPromise {
        return this.invoke("Restore", this.pid, this.title);
    }

    /**
     * @param {number} $x Specify position from top
     * @param {number} $y Specify position from left
     * @returns {IWindowHandlerStatusPromise} Returns promise interface for handling of window move event.
     */
    public MoveTo($x : number, $y : number) : IWindowHandlerStatusPromise {
        return this.invoke("MoveTo", $x, $y, this.pid, this.title);
    }

    /**
     * @param {WindowCornerType} $cornerType Specify corner, which should be binded with new size coordinates
     * @param {number} $dx Specify position from top
     * @param {number} $dy Specify position from left
     * @returns {IWindowHandlerStatusPromise} Returns promise interface for handling of window move event.
     */
    public Resize($cornerType : WindowCornerType, $dx : number, $dy : number) : IWindowHandlerStatusPromise {
        return this.invoke("Resize", $cornerType, $dx, $dy, this.pid, this.title);
    }

    /**
     * @param {boolean} $status Specify, if window can be resized or not.
     * @returns {IWindowHandlerStatusPromise} Returns promise interface for handling of window resize handling event.
     */
    public CanResize($status : boolean) : IWindowHandlerStatusPromise {
        return this.invoke("CanResize", $status);
    }

    /**
     * @param {string} [$windowId] Specify window identifier.
     * @returns {IWindowHandlerStatusPromise} Returns promise interface for handling of window existence event.
     */
    public Exists($windowId? : string) : IWindowHandlerStatusPromise {
        return this.invoke("Exists", $windowId, this.pid, this.title);
    }

    /**
     * @param {string} [$windowId] Specify window identifier.
     * @returns {IWindowHandlerStatusPromise} Returns promise interface for handling of window show event.
     */
    public Show($windowId? : string) : IWindowHandlerStatusPromise {
        return this.invoke("Show", $windowId);
    }

    /**
     * @param {string} [$windowId] Specify window identifier.
     * @returns {IWindowHandlerStatusPromise} Returns promise interface for handling of window hide event.
     */
    public Hide($windowId? : string) : IWindowHandlerStatusPromise {
        return this.invoke("Hide", $windowId);
    }

    /**
     * @param {string} [$windowId] Specify window identifier.
     * @returns {IWindowHandlerStatusPromise} Returns promise interface for handling of window state event.
     */
    public getState($windowId? : string) : IWindowHandlerStatePromise {
        return this.invoke("getState", $windowId);
    }

    /**
     * Creates notify icon in the system tray. Method fails if notify icon exists for current root window.
     * @param {IWindowHandlerNotifyIcon} $notifyIcon Specify notify icon options.
     * @returns {ILiveContentFormatterPromise} Returns promise interface for handling of window state event.
     */
    public CreateNotifyIcon($notifyIcon : IWindowHandlerNotifyIcon) : IWindowHandlerNotifyPromise {
        const callbacks : any = {
            onComplete($status : boolean) : any {
                // declare default callback
            },
            onMessage($message : string) : any {
                // declare default callback
            }
        };
        this.invoke("CreateNotifyIcon", $notifyIcon)
            .Then(($result : IWindowHandlerNotifyProtocol | boolean) : void => {
                if (ObjectValidator.IsSet((<IWindowHandlerNotifyProtocol>$result).type)) {
                    const result : IWindowHandlerNotifyProtocol = <IWindowHandlerNotifyProtocol>$result;
                    if (result.type === EventType.ON_MESSAGE) {
                        callbacks.onMessage(result.request);
                    }
                }
            });
        const promise : IWindowHandlerNotifyPromise = <IWindowHandlerNotifyPromise>{
            OnMessage: ($callback : ($message : string) => void) : IWindowHandlerNotifyPromise => {
                callbacks.onMessage = $callback;
                return promise;
            },
            Then     : ($callback : ($status : boolean) => void) : void => {
                callbacks.onComplete = $callback;
            }
        };

        return promise;
    }

    /**
     * Modify notify icon previously created in the system tray. If notify icon is not exists than will be created with current options.
     * @param {IWindowHandlerNotifyIcon} $notifyIcon Specify notify icno options.
     * @returns {ILiveContentFormatterPromise} Returns promise interface for handling of window state event.
     */
    public ModifyNotifyIcon($notifyIcon : IWindowHandlerNotifyIcon) : IWindowHandlerStatusPromise {
        return this.invoke("ModifyNotifyIcon", $notifyIcon);
    }

    /**
     * Destroys existing notify icon.
     * @returns {ILiveContentFormatterPromise} Returns promise interface for handling of window state event.
     */
    public DestroyNotifyIcon() : IWindowHandlerStatusPromise {
        return this.invoke("DestroyNotifyIcon");
    }

    /**
     * Sets state of TaskBar progress attached with current root window.
     * @param {TaskBarProgressState} $state Specify new progress state.
     * @returns {ILiveContentFormatterPromise} Returns promise interface for handling of window state event.
     */
    public setTaskBarProgressState($state : TaskBarProgressState) : IWindowHandlerStatusPromise {
        return this.invoke("setTaskBarProgressState", $state);
    }

    /**
     * Sets progress value of TaskBar attached with current root window.
     * @param {number} $completed Specify completed progress (must be less or equal to $total).
     * @param {number} $total Specify maximal limit of progress.
     * @returns {ILiveContentFormatterPromise} Returns promise interface for handling of window state event.
     */
    public setTaskBarProgressValue($completed : number, $total : number) : IWindowHandlerStatusPromise {
        return this.invoke("setTaskBarProgressValue", $completed, $total);
    }

    /**
     * @param {string} $name Specify event name value.
     * @param {IWebServiceResponseHandler} $handler Specify handler, which should be used for handling of event data.
     * @returns {void}
     */
    public AddEventListener($name : string, $handler : IWebServiceResponseHandler) : void {
        this.getClient().Send(this.getProtocol("AddEventListener", {
                name: $name
            }),
            ($data : any) : void => {
                if ($data.type === "AddEventListener") {
                    if (ObjectDecoder.Base64($data.data) === "true") {
                        if (!this.subscribers.KeyExists($name)) {
                            this.subscribers.Add(new ArrayList<any>(), $name);
                        }
                        this.subscribers.getItem($name).Add($handler, $data.id);
                    }
                } else if ($data.type === "FireEvent") {
                    const data : any = JSON.parse(ObjectDecoder.Base64($data.data));
                    if (this.subscribers.KeyExists(data.name)) {
                        const handlers : ArrayList<any> = this.subscribers.getItem(data.name);
                        handlers.foreach(($handler : any, $id : number) : void => {
                            if ($id === $data.id) {
                                $handler.apply(this, JSON.parse(ObjectDecoder.Base64(data.args)));
                            }
                        });
                    }
                }
            });
    }

    /**
     * @param {string} $name Specify event name value, which should be removed from events pool.
     * @returns {void}
     */
    public RemoveEventListener($name : string) : void {
        if (this.subscribers.KeyExists($name)) {
            this.subscribers.RemoveAt(this.subscribers.getKeys().indexOf($name));
        }
    }

    /**
     * Sets progress value of TaskBar attached with current root window.
     * @param {IWindowHandlerFileDialogSettings} $settings Specify file dialog options.
     * @returns {IWindowHandlerFileDialogPromise} Returns promise interface for handling of file dialog events.
     */
    public ShowFileDialog($settings : IWindowHandlerFileDialogSettings) : IWindowHandlerFileDialogPromise {
        const callbacks : any = {
            then($status : boolean, $path : string | string[]) : void {
                // declare default callback
            }
        };
        this.invoke("ShowFileDialog", $settings)
            .Then(($result : boolean, $path : string[]) : void => {
                if (ObjectValidator.IsArray($path) && $path.length === 1) {
                    callbacks.then($result, $path[0]);
                } else {
                    callbacks.then($result, $path);
                }
            });

        return <IWindowHandlerFileDialogPromise>{
            Then: ($callback : ($status : boolean, $path : string | string[]) => void) : void => {
                callbacks.then = <any>$callback;
            }
        };
    }

    /**
     * Show HTML debug window from CEF instance.
     * @returns {IWindowHandlerStatusPromise} Returns promise interface for handling of debug console promise.
     */
    public ShowDebugConsole() : IWindowHandlerStatusPromise {
        return this.invoke("ShowDebugConsole");
    }

    protected getClientType() : WebServiceClientType {
        if (!ObjectValidator.IsEmptyOrNull(this.pid) || !ObjectValidator.IsEmptyOrNull(this.title)) {
            return WebServiceClientType.DESKTOP_CONNECTOR;
        }
        return WebServiceClientType.CEF_QUERY;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.CEF_QUERY] = "Io.Oidis.ChromiumRE.Connectors.WindowHandler";
        namespaces[WebServiceClientType.DESKTOP_CONNECTOR] = "Io.Oidis.Connector.Connectors.WindowHandler";
        return namespaces;
    }

    private getProtocol($type : string, $data : any) : any {
        return {
            data: ObjectEncoder.Base64(JSON.stringify($data)),
            type: $type
        };
    }

    private extendClientEvents($container : IWebServiceClientEvents, $eventType : string, $interfaceName? : string) : void {
        if (!ObjectValidator.IsSet($interfaceName)) {
            $interfaceName = $eventType;
        }
        $container[$eventType] = ($handler : IWebServiceResponseHandler) : void => {
            this.AddEventListener($interfaceName, $handler);
        };
    }
}

export interface IWindowHandlerOpenOptions {
    hidden : boolean;
}

export interface IWindowHandlerScriptExecuteOptions {
    script : string | (() => void);
}

export interface IWindowHandlerResultProtocol {
    type : EventType;
    data : string;
    url : string;
    windowId : string;
}

export interface IWindowHandlerNotifyProtocol {
    type : EventType;
    request : string;
}

export interface IWindowHandlerOpenPromise {
    Then($callback : ($windowId : string, $url? : string, $data? : string) => void) : void;
}

export interface IWindowHandlerChangePromise {
    OnStart($callback : () => void) : IWindowHandlerChangePromise;

    OnMessage($callback : ($message : string) => void) : IWindowHandlerChangePromise;

    Then($callback : ($status : boolean) => void) : void;
}

export interface IWindowHandlerStatusPromise {
    Then($callback : ($success : boolean) => void) : void;
}

export interface IWindowHandlerStatePromise {
    Then($callback : ($state : WindowStateType) => void) : void;
}

export interface IWindowHandlerCookiesPromise {
    Then($callback : ($data : IWindowHandlerCookie[]) => void) : void;
}

export interface IWindowHandlerNotifyPromise {
    OnMessage($callback : ($message : string) => void) : IWindowHandlerNotifyPromise;

    Then($callback : ($success : boolean) => void) : void;
}

export interface IWindowHandlerFileDialogPromise {
    Then($callback : ($status : boolean, $path : string | string[]) => void) : void;
}

export interface IWindowHandlerFileDialogSettings {
    title : string;
    filter? : string[];
    filterIndex? : number;
    path? : string;
    openOnly? : boolean;
    folderOnly? : boolean;
    multiSelect? : boolean;
    initialDirectory? : string;
    singleton? : boolean;
    timeout? : number;
}

export interface IWindowHandlerSaveFileDialogSettings {
    title : string;
    filter? : string[];
    filterIndex? : number;
    path? : string;
    initialDirectory? : string;
    extension? : string;
    autoExtension? : boolean;
    restore? : boolean;
    override? : boolean;
    singleton? : boolean;
    timeout? : number;
}

export interface IWindowHandlerCookie {
    path : string;
    name : string;
    value : string;
}

export interface IWindowHandlerNotifyIcon {
    tip : string;
    icon? : string;
    balloon? : IWindowHandlerNotifyBalloon;
    contextMenu? : IWindowHandlerNotifyIconContextMenu;
}

export interface IWindowHandlerNotifyBalloon {
    title : string;
    message? : string;
    type? : NotifyBalloonIconType;
    userIcon? : string;
    noSound? : boolean;
}

export interface IWindowHandlerNotifyIconContextMenu {
    items? : IWindowHandlerNotifyIconContextMenuItem[];
}

export interface IWindowHandlerNotifyIconContextMenuItem {
    name : string;
    label : string;
    position? : number;
}

// generated-code-start
/* eslint-disable */
export const IWindowHandlerOpenOptions = globalThis.RegisterInterface(["hidden"]);
export const IWindowHandlerScriptExecuteOptions = globalThis.RegisterInterface(["script"]);
export const IWindowHandlerResultProtocol = globalThis.RegisterInterface(["type", "data", "url", "windowId"]);
export const IWindowHandlerNotifyProtocol = globalThis.RegisterInterface(["type", "request"]);
export const IWindowHandlerOpenPromise = globalThis.RegisterInterface(["Then"]);
export const IWindowHandlerChangePromise = globalThis.RegisterInterface(["OnStart", "OnMessage", "Then"]);
export const IWindowHandlerStatusPromise = globalThis.RegisterInterface(["Then"]);
export const IWindowHandlerStatePromise = globalThis.RegisterInterface(["Then"]);
export const IWindowHandlerCookiesPromise = globalThis.RegisterInterface(["Then"]);
export const IWindowHandlerNotifyPromise = globalThis.RegisterInterface(["OnMessage", "Then"]);
export const IWindowHandlerFileDialogPromise = globalThis.RegisterInterface(["Then"]);
export const IWindowHandlerFileDialogSettings = globalThis.RegisterInterface(["title", "filter", "filterIndex", "path", "openOnly", "folderOnly", "multiSelect", "initialDirectory", "singleton", "timeout"]);
export const IWindowHandlerSaveFileDialogSettings = globalThis.RegisterInterface(["title", "filter", "filterIndex", "path", "initialDirectory", "extension", "autoExtension", "restore", "override", "singleton", "timeout"]);
export const IWindowHandlerCookie = globalThis.RegisterInterface(["path", "name", "value"]);
export const IWindowHandlerNotifyIcon = globalThis.RegisterInterface(["tip", "icon", "balloon", "contextMenu"]);
export const IWindowHandlerNotifyBalloon = globalThis.RegisterInterface(["title", "message", "type", "userIcon", "noSound"]);
export const IWindowHandlerNotifyIconContextMenu = globalThis.RegisterInterface(["items"]);
export const IWindowHandlerNotifyIconContextMenuItem = globalThis.RegisterInterface(["name", "label", "position"]);
/* eslint-enable */
// generated-code-end
