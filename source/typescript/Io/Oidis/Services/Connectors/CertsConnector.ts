/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";

/**
 * CertsConnector class provides wrapping of certificates handler provided by Oidis Hub instance.
 */
export class CertsConnector extends BaseConnector {

    /**
     * @param {string} $domain Specify domain name for which should be required certificate.
     * @returns {ICertsConnectorPromise} Returns promise interface for handling of certificates.
     */
    public getCertsFor($domain : string) : ICertsConnectorPromise {
        return this.invoke("getCertsFor", $domain);
    }

    protected getClientType() : WebServiceClientType {
        return WebServiceClientType.HUB_CONNECTOR;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.HUB_CONNECTOR] = "Io.Oidis.Hub.Connectors.CertsHandler";
        return namespaces;
    }
}

export interface ICertsConnectorPromise {
    Then($callback : ($cert : string, $key : string) => void) : void;
}

// generated-code-start
export const ICertsConnectorPromise = globalThis.RegisterInterface(["Then"]);
// generated-code-end
