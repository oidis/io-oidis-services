/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";
import { IAcknowledgePromise } from "./ReportServiceConnector.js";

export class UpdatesManagerConnector extends BaseConnector {

    /**
     * @param {string} $appName Specify application name, which should be validated.
     * @param {string} $releaseName Specify release name, which should be validated.
     * @param {string} $platform Specify platform type, which should be validated.
     * @param {string} $version Specify current application version.
     * @param {number} $buildTime Specify current application build time.
     * @param {boolean} [$fixedVersion=false] Specify if validation should be limited on specified version.
     * @returns {IAcknowledgePromise} Returns promise interface for handling of update exists event.
     */
    public UpdateExists($appName : string, $releaseName : string, $platform : string, $version : string, $buildTime : number,
                        $fixedVersion : boolean = false) : IAcknowledgePromise {
        return this.invoke("UpdateExists", $appName, $releaseName, $platform, $version, $buildTime, $fixedVersion);
    }

    protected getClientType() : WebServiceClientType {
        return WebServiceClientType.HUB_CONNECTOR;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.HUB_CONNECTOR] = "Io.Oidis.Hub.Utils.SelfupdatesManager";
        return namespaces;
    }
}
