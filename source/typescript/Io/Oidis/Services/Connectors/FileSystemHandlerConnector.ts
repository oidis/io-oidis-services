/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { HttpMethodType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpMethodType.js";
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { ProgressEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ProgressEventArgs.js";
import { IFileSystemItemProtocol } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IFileSystemItemProtocol.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { IWebServiceProtocol } from "../Interfaces/IWebServiceProtocol.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";
import { LiveContentWrapper } from "../WebServiceApi/LiveContentWrapper.js";

abstract class BaseFileSystemHandlerConnector extends BaseConnector {

    public getTempPath() : any {
        return this.invoke("getTempPath")
            .DataFormatter(($data : string) : string => {
                return StringUtils.Replace(StringUtils.Replace(StringUtils.Remove($data, "\""), "\\", "/"), "//", "/");
            });
    }

    public getProjectBasePath() : any {
        return this.invoke("getProjectBasePath");
    }

    public getAppDataPath() : any {
        return this.invoke("getAppDataPath");
    }

    public getLocalAppDataPath() : any {
        return this.invoke("getLocalAppDataPath");
    }

    public getHostLocation() : any {
        return {
            Then: ($callBack : ($path : string) => void) : void => {
                this.getClient().getServerPath($callBack);
            }
        };
    }

    public getPathMap($path? : string) : any {
        return this.invoke("getPathMap", $path)
            .DataFormatter(($data : IFileSystemItemProtocol[]) : IFileSystemItemProtocol[] => {
                if (!ObjectValidator.IsArray($data)) {
                    return [];
                }
                return $data;
            });
    }

    public getNetworkMap($additionalInfo : boolean = false) : any {
        return this.invoke("getNetworkMap", $additionalInfo)
            .DataFormatter(($data : IFileSystemItemProtocol[]) : IFileSystemItemProtocol[] => {
                if (!ObjectValidator.IsArray($data)) {
                    return [];
                }
                return $data;
            });
    }

    public getDirectoryContent($path : string) : any {
        return this.invoke("getDirectoryContent", $path)
            .DataFormatter(($data : IFileSystemItemProtocol[]) : IFileSystemItemProtocol[] => {
                if (!ObjectValidator.IsArray($data)) {
                    return [];
                }
                return $data;
            });
    }

    public Expand($pattern : string | string[]) : any {
        return this.invoke("Expand", $pattern);
    }

    public Exists($path : string) : any {
        return this.invoke("Exists", $path);
    }

    public Write($path : string, $data : string, $append : boolean = false, $stream : boolean = false) : any {
        return this.invoke(!$stream ? "Write" : "WriteStream", $path, $data, $append);
    }

    public Read($path : string, $stream : boolean = false) : any {
        if ($stream) {
            return this.invoke("ReadStream", $path);
        } else {
            return this.invoke("Read", $path)
                .DataFormatter(($data : string) : string => {
                    if (StringUtils.StartsWith($data, "\"") && StringUtils.EndsWith($data, "\"")) {
                        $data = StringUtils.Replace($data, "\\r", "\r");
                        $data = StringUtils.Replace($data, "\\n", "\n");
                        $data = StringUtils.Replace($data, "\r\n", "\n");
                        $data = StringUtils.Replace($data, "\\\"", "\"");
                        return StringUtils.Substring($data, 1, StringUtils.Length($data) - 2);
                    }
                    return $data;
                });
        }
    }

    public Delete($path : string) : any {
        const callbacks : any = {
            onChange($args : ProgressEventArgs) : any {
                // declare default callback
            },
            onComplete($success : boolean) : any {
                // declare default callback
            }
        };
        this.invoke("Delete", $path)
            .Then(($result : IFileSystemResultProtocol) : void => {
                if (ObjectValidator.IsSet($result.type)) {
                    if ($result.type === EventType.ON_CHANGE) {
                        if (ObjectValidator.IsSet($result.data)) {
                            $result = <IFileSystemResultProtocol>$result.data;
                        }
                        const args : ProgressEventArgs = new ProgressEventArgs();
                        args.RangeStart($result.rangeStart);
                        args.RangeEnd($result.rangeEnd);
                        args.CurrentValue($result.currentValue);
                        callbacks.onChange(args);
                    } else if ($result.type === EventType.ON_COMPLETE) {
                        callbacks.onComplete($result.data);
                    }
                } else if (ObjectValidator.IsBoolean($result)) {
                    callbacks.onComplete($result);
                }
            });
        const promise : IFileSystemChangePromise = {
            OnChange: ($callback : ($args : ProgressEventArgs) => void) : IFileSystemChangePromise => {
                callbacks.onChange = $callback;
                return promise;
            },
            Then    : ($callback : ($success : boolean) => void) : void => {
                callbacks.onComplete = $callback;
            }
        };
        return promise;
    }

    public CreateDirectory($path : string) : any {
        return this.invoke("CreateDirectory", $path);
    }

    public Rename($oldPath : string, $newPath : string) : any {
        return this.invoke("Rename", $oldPath, $newPath);
    }

    public Copy($sourcePath : string, $destinationPath : string) : any {
        const callbacks : any = {
            onChange($args : ProgressEventArgs) : any {
                // declare default callback
            },
            onComplete($success : boolean) : any {
                // declare default callback
            }
        };
        this.invoke("Copy", $sourcePath, $destinationPath)
            .Then(($result : IFileSystemResultProtocol) : void => {
                if (ObjectValidator.IsSet($result.type)) {
                    if ($result.type === EventType.ON_CHANGE) {
                        if (ObjectValidator.IsSet($result.data)) {
                            $result = <IFileSystemResultProtocol>$result.data;
                        }
                        const args : ProgressEventArgs = new ProgressEventArgs();
                        args.RangeStart($result.rangeStart);
                        args.RangeEnd($result.rangeEnd);
                        args.CurrentValue($result.currentValue);
                        callbacks.onChange(args);
                    } else if ($result.type === EventType.ON_COMPLETE) {
                        callbacks.onComplete($result.data);
                    }
                } else if (ObjectValidator.IsBoolean($result)) {
                    callbacks.onComplete($result);
                }
            });
        const promise : IFileSystemChangePromise = {
            OnChange: ($callback : ($args : ProgressEventArgs) => void) : IFileSystemChangePromise => {
                callbacks.onChange = $callback;
                return promise;
            },
            Then    : ($callback : ($success : boolean) => void) : void => {
                callbacks.onComplete = $callback;
            }
        };
        return promise;
    }

    public Download($sourcePathOrOptions : any) : any {
        const callbacks : any = {
            connectionId: -1,
            onChange($args : ProgressEventArgs) : any {
                // declare default callback
            },
            onComplete($tmpPathOrHeaders : any, $bodyOrHeaders : any) : any {
                // declare default callback
            },
            onError: null,
            onStart($connectionId? : number) : any {
                // declare default callback
            }
        };
        const methodCall : LiveContentWrapper = new LiveContentWrapper(this.getServerNamespace());
        this.getClient().Send(
            methodCall.getProtocolForInvokeMethod.apply(methodCall, [this.getId(), "Download", $sourcePathOrOptions]),
            ($data : IWebServiceProtocol) : void => {
                callbacks.connectionId = $data.id;
                (<any>methodCall).getResponseHandler(methodCall, $data);
            });
        methodCall
            .OnError(($error : ErrorEventArgs, ...$args : any[]) : void => {
                this.errorHandler(callbacks.onError, $error, $args);
            })
            .Then(($result : IFileSystemResultProtocol) : void => {
                if (!ObjectValidator.IsEmptyOrNull($result) && ObjectValidator.IsSet($result.type)) {
                    if ($result.type === EventType.ON_START) {
                        callbacks.onStart(callbacks.connectionId);
                    } else if ($result.type === EventType.ON_CHANGE) {
                        if (ObjectValidator.IsSet($result.data)) {
                            $result = <IFileSystemResultProtocol>$result.data;
                        }
                        const args : ProgressEventArgs = new ProgressEventArgs();
                        args.RangeStart($result.rangeStart);
                        args.RangeEnd($result.rangeEnd);
                        args.CurrentValue($result.currentValue);
                        callbacks.onChange(args);
                    } else if ($result.type === EventType.ON_COMPLETE) {
                        $result.headers = {};
                        if (ObjectValidator.IsSet($result.data)) {
                            $result.headers = <any>$result.data[0];
                            if (ObjectValidator.IsObject($sourcePathOrOptions) &&
                                (<IFileSystemDownloadOptions>$sourcePathOrOptions).streamOutput) {
                                $result.body = <string>$result.data[1];
                            } else {
                                $result.path = <string>$result.data[1];
                            }
                        }
                        const headers : ArrayList<string> = new ArrayList<string>();
                        let parameter : any;
                        for (parameter in $result.headers) {
                            if ($result.headers.hasOwnProperty(parameter)) {
                                headers.Add($result.headers[parameter], parameter);
                            }
                        }
                        if (ObjectValidator.IsSet($result.path)) {
                            callbacks.onComplete($result.path, headers);
                        } else {
                            callbacks.onComplete(headers, $result.body);
                        }
                    }
                }
            });

        const promise : IFileSystemDownloadPromise = {
            OnChange  : ($callback : ($args : ProgressEventArgs) => void) : IFileSystemDownloadPromise => {
                callbacks.onChange = $callback;
                return promise;
            },
            OnComplete: ($callback : ($tmpPathOrHeaders : any, $bodyOrHeaders : any) => void) : IFileSystemDownloadPromise => {
                callbacks.onComplete = $callback;
                return promise;
            },
            OnError   : ($callback : ($error : ErrorEventArgs, ...$args : any[]) => void) : IFileSystemDownloadPromise => {
                callbacks.onError = $callback;
                return promise;
            },
            OnStart   : ($callback : ($connectionId? : number) => void) : IFileSystemDownloadPromise => {
                callbacks.onStart = $callback;
                return promise;
            },
            Then      : ($callback : ($tmpPathOrHeaders : any, $body? : string) => void) : void => {
                callbacks.onComplete = $callback;
            }
        };
        return promise;
    }

    public AbortDownload($id : number) : any {
        return this.invoke("AbortDownload", $id);
    }

    public Unpack($path : string, $options? : IArchiveOptions) : any {
        const callbacks : any = {
            onError: null,
            then($data : string) : any {
                // declare default callback
            }
        };
        this.invoke("Unpack", $path, $options)
            .OnError(($error : ErrorEventArgs, ...$args : any[]) : void => {
                this.errorHandler(callbacks.onError, $error, $args);
            })
            .Then(($result : IFileSystemResultProtocol) : void => {
                if (ObjectValidator.IsSet($result.type) && $result.type === EventType.ON_COMPLETE) {
                    if (ObjectValidator.IsSet($result.path)) {
                        callbacks.then($result.path);
                    } else {
                        callbacks.then(<string>$result.data);
                    }
                }
            });
        const promise : IFileSystemFileErrorPromise = {
            OnError: ($callback : ($error : ErrorEventArgs, ...$args : any[]) => void) : IFileSystemFilePromise => {
                callbacks.onError = $callback;
                return promise;
            },
            Then   : ($callback : ($data : string) => void) : void => {
                callbacks.then = $callback;
            }
        };
        return promise;
    }

    public CreateShortcut($source : string, $destination : string, $options? : IShortcutOptions) : any {
        const callbacks : any = {
            onError: null,
            then($status : boolean) : any {
                // declare default callback
            }
        };
        this.invoke("CreateShortcut", $source, $destination, $options)
            .OnError(($error : ErrorEventArgs, ...$args : any[]) : void => {
                this.errorHandler(callbacks.onError, $error, $args);
            })
            .Then(($status : boolean) : void => {
                callbacks.then($status);
            });

        const promise : IFileSystemStatusErrorPromise = {
            OnError: ($callback : ($error : ErrorEventArgs, ...$args : any[]) => void) : IFileSystemStatusPromise => {
                callbacks.onError = $callback;
                return promise;
            },
            Then   : ($callback : ($status : boolean) => void) : void => {
                callbacks.then = $callback;
            }
        };
        return promise;
    }

    public getLinkTargetPath($path : string) : any {
        return this.invoke("getLinkTargetPath", $path);
    }

    public IsEmpty($path : string) : any {
        return this.invoke("IsEmpty", $path);
    }

    public Pack($path : string, $options? : IArchiveOptions) : any {
        const callbacks : any = {
            onError: null,
            then($data : string) : any {
                // declare default callback
            }
        };
        this.invoke("Pack", $path, $options)
            .OnError(($error : ErrorEventArgs, ...$args : any[]) : void => {
                this.errorHandler(callbacks.onError, $error, $args);
            })
            .Then(($data : string) : void => {
                callbacks.then($data);
            });
        const promise : IFileSystemFileErrorPromise = {
            OnError: ($callback : ($error : ErrorEventArgs, ...$args : any[]) => void) : IFileSystemFilePromise => {
                callbacks.onError = $callback;
                return promise;
            },
            Then   : ($callback : ($data : string) => void) : void => {
                callbacks.then = $callback;
            }
        };
        return promise;
    }

    public IsFile($path : string) : any {
        return this.invoke("IsFile", $path);
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.DESKTOP_CONNECTOR] = "Io.Oidis.Connector.Connectors.FileSystemHandler";
        namespaces[WebServiceClientType.BUILDER_CONNECTOR] = "Io.Oidis.Builder.Connectors.FileSystemHandler";
        namespaces[WebServiceClientType.FORWARD_CLIENT] = "Io.Oidis.Connector.Connectors.FileSystemHandler";
        namespaces[WebServiceClientType.HOST_CLIENT] = "Io.Oidis.Connector.Connectors.FileSystemHandler";
        return namespaces;
    }
}

/**
 * FileSystemHandlerConnector class provides wrapping of local file system services provided by Oidis Connector instance.
 */
export class FileSystemHandlerConnector extends BaseFileSystemHandlerConnector {

    /**
     * @returns {IFileSystemFilePromise} Returns promise interface for handling of data receive event.
     */
    public getTempPath() : IFileSystemFilePromise {
        return super.getTempPath();
    }

    /**
     * @returns {IFileSystemFilePromise} Returns promise interface for handling of data receive event.
     */
    public getProjectBasePath() : IFileSystemFilePromise {
        return super.getProjectBasePath();
    }

    /**
     * @returns {IFileSystemFilePromise} Returns promise interface for handling of data receive event.
     */
    public getAppDataPath() : IFileSystemFilePromise {
        return super.getAppDataPath();
    }

    /**
     * @returns {IFileSystemFilePromise} Returns promise interface for handling of data receive event.
     */
    public getLocalAppDataPath() : IFileSystemFilePromise {
        return super.getLocalAppDataPath();
    }

    /**
     * @returns {IFileSystemFilePromise} Returns promise interface for handling of data receive event.
     */
    public getHostLocation() : IFileSystemFilePromise {
        return super.getHostLocation();
    }

    /**
     * @param {string} [$path] Specify path, which should be looked up.
     * @returns {IFileSystemMapPromise} Returns promise interface for handling of data receive event.
     */
    public getPathMap($path? : string) : IFileSystemMapPromise {
        return super.getPathMap($path);
    }

    /**
     * @param {string} [$additionalInfo=false] Specify, if additional network information should be processed e.g. ARP with DNS lookup.
     * @returns {IFileSystemMapPromise} Returns promise interface for handling of data receive event.
     */
    public getNetworkMap($additionalInfo : boolean = false) : IFileSystemMapPromise {
        return super.getNetworkMap($additionalInfo);
    }

    /**
     * @param {string} $path Specify path, which should be looked up.
     * @returns {IFileSystemMapPromise} Returns promise interface for handling of data receive event.
     */
    public getDirectoryContent($path : string) : IFileSystemMapPromise {
        return super.getDirectoryContent($path);
    }

    /**
     * @param {string|string[]} $pattern Specify pattern for search.
     * @returns {IFileSystemExpandPromise} Returns a unique array of all file or directory paths that match the given $pattern.
     */
    public Expand($pattern : string | string[]) : IFileSystemExpandPromise {
        return super.Expand($pattern);
    }

    /**
     * @param {string} $path Specify path, which should be validated.
     * @returns {IFileSystemStatusPromise} Returns promise interface for handling of existence event.
     */
    public Exists($path : string) : IFileSystemStatusPromise {
        return super.Exists($path);
    }

    /**
     * @param {string} $path Specify file path, which should be used as data destination.
     * @param {string} $data Specify data, which should saved.
     * @param {boolean} [$append=false] Specify, if data should be appended to existing data in the file.
     * @param {boolean} [$stream=false] Specify, if data should be consumed as base64 stream.
     * @returns {IFileSystemStatusPromise} Returns promise interface for handling of write event.
     */
    public Write($path : string, $data : string, $append : boolean = false, $stream : boolean = false) : IFileSystemStatusPromise {
        return super.Write($path, $data, $append, $stream);
    }

    /**
     * @param {string} $path Specify file path, which should be read.
     * @param {boolean} [$stream=false] Specify, if output should be in base64 format.
     * @returns {IFileSystemFilePromise} Returns promise interface for handling of data receive event.
     */
    public Read($path : string, $stream : boolean = false) : IFileSystemFilePromise {
        return super.Read($path, $stream);
    }

    /**
     * @param {string} $path Specify path, which should be deleted.
     * @returns {IFileSystemChangePromise} Returns promise interface for handling delete event.
     */
    public Delete($path : string) : IFileSystemChangePromise {
        return super.Delete($path);
    }

    /**
     * @param {string} $path Specify path, which should be created.
     * @returns {IFileSystemStatusPromise} Returns promise interface for handling directory create event.
     */
    public CreateDirectory($path : string) : IFileSystemStatusPromise {
        return super.CreateDirectory($path);
    }

    /**
     * @param {string} $oldPath Specify path, which should be renamed.
     * @param {string} $newPath Specify new path value.
     * @returns {IFileSystemStatusPromise} Returns promise interface for handling rename event.
     */
    public Rename($oldPath : string, $newPath : string) : IFileSystemStatusPromise {
        return super.Rename($oldPath, $newPath);
    }

    /**
     * @param {string} $sourcePath Specify path, which should be used as data source.
     * @param {string} $destinationPath Specify path, where should be data stored.
     * @returns {IFileSystemChangePromise} Returns promise interface for handling copy event.
     */
    public Copy($sourcePath : string, $destinationPath : string) : IFileSystemChangePromise {
        return super.Copy($sourcePath, $destinationPath);
    }

    /**
     * @param {string | IFileSystemDownloadOptions} $source Specify path or HTTP request options,
     * which should be used for download of data source.
     * @returns {IFileSystemStatusPromise} Returns promise interface for handling download event.
     */
    public Download($source : string | IFileSystemDownloadOptions) : IFileSystemDownloadPromise {
        return super.Download($source);
    }

    /**
     * @param {number} $id Specify connection id, which should be closed.
     * @returns {IFileSystemStatusPromise} Returns promise interface for handling download abort event.
     */
    public AbortDownload($id : number) : IFileSystemStatusPromise {
        return super.AbortDownload($id);
    }

    /**
     * @param {string} $path Specify path, which should be unpacked.
     * @param {IArchiveOptions} [$options] Specify additional options for unpack process.
     * @returns {IFileSystemFilePromise} Returns promise interface for handling unpack event.
     */
    public Unpack($path : string, $options? : IArchiveOptions) : IFileSystemFileErrorPromise {
        return super.Unpack($path, $options);
    }

    /**
     * @param {string} $source Specify path, which should be used as shortcut target.
     * @param {string} $destination Specify path, where should be shortcut created.
     * @param {IShortcutOptions} [$options] Specify shortcut details.
     * @returns {IFileSystemStatusPromise} Returns promise interface for handling shortcut event.
     */
    public CreateShortcut($source : string, $destination : string, $options? : IShortcutOptions) : IFileSystemStatusErrorPromise {
        return super.CreateShortcut($source, $destination, $options);
    }

    /**
     * @param {string} $path Specify link path, which should be resolved to target path.
     * @returns {IFileSystemStatusPromise} Returns promise interface for handling of write event.
     */
    public getLinkTargetPath($path : string) : IFileSystemFilePromise {
        return super.getLinkTargetPath($path);
    }

    /**
     * Validate, if Directory or File is empty
     * @param {string} $path Specify path, which should be validated.
     * @returns {IFileSystemStatusPromise} Returns promise interface for handling this event.
     */
    public IsEmpty($path : string) : IFileSystemStatusPromise {
        return super.IsEmpty($path);
    }

    /**
     * @param {string} $path Specify path, which should be packed.
     * @param {IArchiveOptions} [$options] Specify additional options for pack process.
     * @returns {IFileSystemFilePromise} Returns promise interface for handling pack event.
     */
    public Pack($path : string, $options? : IArchiveOptions) : IFileSystemFileErrorPromise {
        return super.Pack($path, $options);
    }

    /**
     * @param {string} $path Specify path, which should be validated.
     * @returns {IFileSystemStatusPromise} Returns promise interface for handling this event.
     */
    public IsFile($path : string) : IFileSystemStatusPromise {
        return super.IsFile($path);
    }
}

/**
 * FileSystemHandlerConnectorAsync class provides same wrapping as FileSystemHandlerConnector but with native promise handling.
 */
export class FileSystemHandlerConnectorAsync extends BaseFileSystemHandlerConnector {

    /**
     * @returns {Promise<string>} Returns promise interface for handling of data receive event.
     */
    public getTempPath() : Promise<string> {
        return this.asyncInvoke(super.getTempPath);
    }

    /**
     * @returns {Promise<string>} Returns promise interface for handling of data receive event.
     */
    public getProjectBasePath() : Promise<string> {
        return this.asyncInvoke(super.getProjectBasePath);
    }

    /**
     * @returns {Promise<string>} Returns promise interface for handling of data receive event.
     */
    public getAppDataPath() : Promise<string> {
        return this.asyncInvoke(super.getAppDataPath);
    }

    /**
     * @returns {Promise<string>} Returns promise interface for handling of data receive event.
     */
    public getLocalAppDataPath() : Promise<string> {
        return this.asyncInvoke(super.getLocalAppDataPath);
    }

    /**
     * @returns {Promise<string>} Returns promise interface for handling of data receive event.
     */
    public getHostLocation() : Promise<string> {
        return this.asyncInvoke(super.getHostLocation);
    }

    /**
     * @param {string} [$path] Specify path, which should be looked up.
     * @returns {Promise<IFileSystemItemProtocol[]>} Returns promise interface for handling of data receive event.
     */
    public getPathMap($path? : string) : Promise<IFileSystemItemProtocol[]> {
        return this.asyncInvoke(super.getPathMap, $path);
    }

    /**
     * @param {string} [$additionalInfo=false] Specify, if additional network information should be processed e.g. ARP with DNS lookup.
     * @returns {Promise<IFileSystemItemProtocol[]>} Returns promise interface for handling of data receive event.
     */
    public getNetworkMap($additionalInfo : boolean = false) : Promise<IFileSystemItemProtocol[]> {
        return this.asyncInvoke(super.getNetworkMap, $additionalInfo);
    }

    /**
     * @param {string} $path Specify path, which should be looked up.
     * @returns {Promise<IFileSystemItemProtocol[]>} Returns promise interface for handling of data receive event.
     */
    public getDirectoryContent($path : string) : Promise<IFileSystemItemProtocol[]> {
        return this.asyncInvoke(super.getDirectoryContent, $path);
    }

    /**
     * @param {string|string[]} $pattern Specify pattern for search.
     * @returns {Promise<string[]>} Returns a unique array of all file or directory paths that match the given $pattern.
     */
    public Expand($pattern : string | string[]) : Promise<string[]> {
        return this.asyncInvoke(super.Expand, $pattern);
    }

    /**
     * @param {string} $path Specify path, which should be validated.
     * @returns {Promise<boolean>} Returns promise interface for handling of existence event.
     */
    public Exists($path : string) : Promise<boolean> {
        return this.asyncInvoke(super.Exists, $path);
    }

    /**
     * @param {string} $path Specify file path, which should be used as data destination.
     * @param {string} $data Specify data, which should saved.
     * @param {boolean} [$append=false] Specify, if data should be appended to existing data in the file.
     * @param {boolean} [$stream=false] Specify, if data should be consumed as base64 stream.
     * @returns {Promise<boolean>} Returns promise interface for handling of write event.
     */
    public Write($path : string, $data : string, $append : boolean = false, $stream : boolean = false) : Promise<boolean> {
        return this.asyncInvoke(super.Write, $path, $data, $append, $stream);
    }

    /**
     * @param {string} $path Specify file path, which should be read.
     * @param {boolean} [$stream=false] Specify, if output should be in base64 format.
     * @returns {Promise<string>} Returns promise interface for handling of data receive event.
     */
    public Read($path : string, $stream : boolean = false) : Promise<string> {
        return this.asyncInvoke(super.Read, $path, $stream);
    }

    /**
     * @param {string} $path Specify path, which should be deleted.
     * @param {($args : ProgressEventArgs) => void} [$onChange] Specify callback used for progress processing.
     * @returns {Promise<boolean>} Returns promise interface for handling delete event.
     */
    public Delete($path : string, $onChange? : ($args : ProgressEventArgs) => void) : Promise<boolean> {
        return new Promise<boolean>(($resolve, $reject) : void => {
            try {
                const promise : any = super.Delete($path);
                if (!ObjectValidator.IsEmptyOrNull($onChange)) {
                    promise.OnChange($onChange);
                }
                if (!ObjectValidator.IsEmptyOrNull(promise.OnError)) {
                    promise.OnError(($error : ErrorEventArgs) : void => {
                        $reject(new Error($error.Message()));
                    });
                }
                promise.Then($resolve);
            } catch (ex) {
                $reject(ex);
            }
        });
    }

    /**
     * @param {string} $path Specify path, which should be created.
     * @returns {Promise<boolean>} Returns promise interface for handling directory create event.
     */
    public CreateDirectory($path : string) : Promise<boolean> {
        return this.asyncInvoke(super.CreateDirectory, $path);
    }

    /**
     * @param {string} $oldPath Specify path, which should be renamed.
     * @param {string} $newPath Specify new path value.
     * @returns {Promise<boolean>} Returns promise interface for handling rename event.
     */
    public Rename($oldPath : string, $newPath : string) : Promise<boolean> {
        return this.asyncInvoke(super.Rename, $oldPath, $newPath);
    }

    /**
     * @param {string} $sourcePath Specify path, which should be used as data source.
     * @param {string} $destinationPath Specify path, where should be data stored.
     * @param {($args : ProgressEventArgs) => void} [$onChange] Specify callback used for progress processing.
     * @returns {Promise<boolean>} Returns promise interface for handling copy event.
     */
    public Copy($sourcePath : string, $destinationPath : string, $onChange? : ($args : ProgressEventArgs) => void) : Promise<boolean> {
        return new Promise<boolean>(($resolve, $reject) : void => {
            try {
                const promise : any = super.Copy($sourcePath, $destinationPath);
                if (!ObjectValidator.IsEmptyOrNull($onChange)) {
                    promise.OnChange($onChange);
                }
                if (!ObjectValidator.IsEmptyOrNull(promise.OnError)) {
                    promise.OnError(($error : ErrorEventArgs) : void => {
                        $reject(new Error($error.Message()));
                    });
                }
                promise.Then($resolve);
            } catch (ex) {
                $reject(ex);
            }
        });
    }

    /**
     * @param {string | IFileSystemDownloadOptions} $source Specify path or HTTP request options,
     * which should be used for download of data source.
     * @param {IFileSystemDownloadHandlers} [$handlers] Specify callbacks used for progress processing.
     * @returns {Promise<IFileSystemDownloadResponse>} Returns promise interface for handling download event.
     */
    public Download($source : string | IFileSystemDownloadOptions,
                    $handlers? : IFileSystemDownloadHandlers) : Promise<IFileSystemDownloadResponse> {
        return new Promise<IFileSystemDownloadResponse>(($resolve, $reject) : void => {
            try {
                const promise : any = super.Download($source);
                if (!ObjectValidator.IsEmptyOrNull($handlers)) {
                    if (!ObjectValidator.IsEmptyOrNull($handlers.onStart)) {
                        promise.OnStart($handlers.onStart);
                    }
                    if (!ObjectValidator.IsEmptyOrNull($handlers.onChange)) {
                        promise.OnChange($handlers.onChange);
                    }
                }
                if (!ObjectValidator.IsEmptyOrNull(promise.OnError)) {
                    promise.OnError(($error : ErrorEventArgs) : void => {
                        $reject(new Error($error.Message()));
                    });
                }
                promise.Then(($pathOrHeaders : any, $body : string) : void => {
                    $resolve(<IFileSystemDownloadResponse>{
                        body   : $body,
                        headers: $pathOrHeaders,
                        tmpPath: $pathOrHeaders
                    });
                });
            } catch (ex) {
                $reject(ex);
            }
        });
    }

    /**
     * @param {number} $id Specify connection id, which should be closed.
     * @returns {Promise<boolean>} Returns promise interface for handling download abort event.
     */
    public AbortDownload($id : number) : Promise<boolean> {
        return this.asyncInvoke(super.AbortDownload, $id);
    }

    /**
     * @param {string} $path Specify path, which should be unpacked.
     * @param {IArchiveOptions} [$options] Specify additional options for unpack process.
     * @returns {Promise<string>} Returns promise interface for handling unpack event.
     */
    public Unpack($path : string, $options? : IArchiveOptions) : Promise<string> {
        return this.asyncInvoke(super.Unpack, $path, $options);
    }

    /**
     * @param {string} $source Specify path, which should be used as shortcut target.
     * @param {string} $destination Specify path, where should be shortcut created.
     * @param {IShortcutOptions} [$options] Specify shortcut details.
     * @returns {Promise<boolean>} Returns promise interface for handling shortcut event.
     */
    public CreateShortcut($source : string, $destination : string, $options? : IShortcutOptions) : Promise<boolean> {
        return this.asyncInvoke(super.CreateShortcut, $source, $destination, $options);
    }

    /**
     * @param {string} $path Specify link path, which should be resolved to target path.
     * @returns {Promise<string>} Returns promise interface for handling of write event.
     */
    public getLinkTargetPath($path : string) : Promise<string> {
        return this.asyncInvoke(super.getLinkTargetPath, $path);
    }

    /**
     * Validate, if Directory or File is empty
     * @param {string} $path Specify path, which should be validated.
     * @returns {Promise<boolean>} Returns promise interface for handling this event.
     */
    public IsEmpty($path : string) : Promise<boolean> {
        return this.asyncInvoke(super.IsEmpty, $path);
    }

    /**
     * @param {string} $path Specify path, which should be packed.
     * @param {IArchiveOptions} [$options] Specify additional options for pack process.
     * @returns {Promise<string>} Returns promise interface for handling pack event.
     */
    public Pack($path : string, $options? : IArchiveOptions) : Promise<string> {
        return this.asyncInvoke(super.Pack, $path, $options);
    }

    /**
     * @param {string} $path Specify path, which should be validated.
     * @returns {Promise<boolean>} Returns promise interface for handling this event.
     */
    public IsFile($path : string) : Promise<boolean> {
        return this.asyncInvoke(super.IsFile, $path);
    }
}

export interface IFileSystemMapPromise {
    Then($callback : ($map : IFileSystemItemProtocol[]) => void) : void;
}

export interface IFileSystemStatusErrorPromise {
    OnError($callback : ($error : ErrorEventArgs, ...$args : any[]) => void) : IFileSystemStatusPromise;

    Then($callback : ($success : boolean) => void) : void;
}

export interface IFileSystemStatusPromise {
    Then($callback : ($success : boolean) => void) : void;
}

export interface IFileSystemFileErrorPromise {
    OnError($callback : ($error : ErrorEventArgs, ...$args : any[]) => void) : IFileSystemFilePromise;

    Then($callback : ($data : string) => void) : void;
}

export interface IFileSystemFilePromise {
    Then($callback : ($data : string) => void) : void;
}

export interface IFileSystemChangePromise {
    OnChange($callback : ($args : ProgressEventArgs) => void) : IFileSystemChangePromise;

    Then($callback : ($success : boolean) => void) : void;
}

export interface IFileSystemDownloadPromise {
    OnStart($callback : ($connectionId? : number) => void) : IFileSystemDownloadPromise;

    OnChange($callback : ($args : ProgressEventArgs) => void) : IFileSystemDownloadPromise;

    OnComplete($callback : ($tmpPath : string, $headers : ArrayList<string>) => void) : IFileSystemDownloadPromise;

    OnComplete($callback : ($headers : ArrayList<string>, $body : string) => void) : IFileSystemDownloadPromise;

    OnError($callback : ($error : ErrorEventArgs, ...$args : any[]) => void) : IFileSystemDownloadPromise;

    Then($callback : ($tmpPath : string) => void) : void;

    Then($callback : ($headers : ArrayList<string>, $body : string) => void) : void;
}

export interface IFileSystemExpandPromise {
    Then($callback : ($paths : string[]) => void) : void;
}

export interface IFileSystemResultProtocol {
    type : EventType;
    rangeStart : number;
    rangeEnd : number;
    currentValue : number;
    path : string;
    headers : any;
    body : string;
    data? : string | IFileSystemResultProtocol;
}

export interface IFileSystemDownloadOptions {
    method? : HttpMethodType;
    url : string | any;
    headers? : any;
    body? : string;
    streamOutput? : boolean;
    proxy? : boolean;
    streamReturnsBuffer? : boolean;
    verbose? : boolean;
    strictSSL? : boolean;
    proxyConfig? : IProxyConfig;
}

export interface IProxyConfig {
    httpProxy? : string;
    httpsProxy? : string;
    noProxy? : string;
}

export interface IFileSystemDownloadHandlers {
    onStart? : ($connectionId? : number) => void;
    onChange? : ($args : ProgressEventArgs) => void;
}

export interface IFileSystemDownloadResponse {
    tmpPath? : string;
    headers? : ArrayList<string>;
    body? : string;
}

export enum ShortcutRunStyle {
    NORMAL = 1,
    MAX = 3,
    MIN = 7
}

export interface IShortcutOptions {
    args? : string[];
    workingDir? : string;
    runStyle? : ShortcutRunStyle;
    icon? : string;
    iconIndex? : number;
    hotkey? : number;
    desc? : string;
}

export interface IArchiveOptions {
    cwd? : string;
    output? : string;
    type? : string;
    autoStrip? : boolean;
    override? : boolean;
}

// generated-code-start
/* eslint-disable */
export const IFileSystemMapPromise = globalThis.RegisterInterface(["Then"]);
export const IFileSystemStatusErrorPromise = globalThis.RegisterInterface(["OnError", "Then"]);
export const IFileSystemStatusPromise = globalThis.RegisterInterface(["Then"]);
export const IFileSystemFileErrorPromise = globalThis.RegisterInterface(["OnError", "Then"]);
export const IFileSystemFilePromise = globalThis.RegisterInterface(["Then"]);
export const IFileSystemChangePromise = globalThis.RegisterInterface(["OnChange", "Then"]);
export const IFileSystemDownloadPromise = globalThis.RegisterInterface(["OnStart", "OnChange", "OnComplete", "OnError", "Then"]);
export const IFileSystemExpandPromise = globalThis.RegisterInterface(["Then"]);
export const IFileSystemResultProtocol = globalThis.RegisterInterface(["type", "rangeStart", "rangeEnd", "currentValue", "path", "headers", "body", "data"]);
export const IFileSystemDownloadOptions = globalThis.RegisterInterface(["method", "url", "headers", "body", "streamOutput", "proxy", "streamReturnsBuffer", "verbose", "strictSSL", "proxyConfig"]);
export const IProxyConfig = globalThis.RegisterInterface(["httpProxy", "httpsProxy", "noProxy"]);
export const IFileSystemDownloadHandlers = globalThis.RegisterInterface(["onStart", "onChange"]);
export const IFileSystemDownloadResponse = globalThis.RegisterInterface(["tmpPath", "headers", "body"]);
export const IShortcutOptions = globalThis.RegisterInterface(["args", "workingDir", "runStyle", "icon", "iconIndex", "hotkey", "desc"]);
export const IArchiveOptions = globalThis.RegisterInterface(["cwd", "output", "type", "autoStrip", "override"]);
/* eslint-enable */
// generated-code-end
