/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { WebServiceConfiguration } from "@io-oidis-commons/Io/Oidis/Commons/WebServiceApi/WebServiceConfiguration.js";
import { EventType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/Events/EventType.js";
import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { IWebServiceProtocol } from "../Interfaces/IWebServiceProtocol.js";
import { BaseConnector, IConnectorOptions } from "../Primitives/BaseConnector.js";
import { LiveContentWrapper } from "../WebServiceApi/LiveContentWrapper.js";
import { IAcknowledgePromise } from "./ReportServiceConnector.js";

abstract class BaseAgentsRegisterConnector extends BaseConnector {

    constructor($reconnect : boolean | number = true, $serverConfigurationSource? : string | WebServiceConfiguration) {
        super($reconnect, $serverConfigurationSource, WebServiceClientType.HUB_CONNECTOR);
    }

    public RegisterAgent($capabilities : IAgentCapabilities) : any {
        const callbacks : any = {
            onComplete($success : boolean) : any {
                // declare default callback
            },
            onError: null,
            onMessage($data : any) : any {
                // declare default callback
            }
        };
        LiveContentWrapper.InvokeMethod(this.getClient(), this.getServerNamespace(),
            "RegisterAgent", $capabilities)
            .OnError(($error : ErrorEventArgs, ...$args : any[]) : void => {
                this.errorHandler(callbacks.onError, $error, $args);
            })
            .Then(($result : any) : void => {
                if (ObjectValidator.IsSet($result.type)) {
                    if ($result.type === EventType.ON_CHANGE) {
                        callbacks.onMessage($result.data);
                    }
                } else if (ObjectValidator.IsBoolean($result)) {
                    callbacks.onComplete($result);
                }
            });
        const promise : IAgentsRegisterPromise = {
            OnError  : ($callback : ($args : ErrorEventArgs) => void) : IAcknowledgePromise => {
                callbacks.onError = $callback;
                return promise;
            },
            OnMessage: ($callback : ($data : any) => void) : IAcknowledgePromise => {
                callbacks.onMessage = $callback;
                return promise;
            },
            Then     : ($callback : ($success : boolean) => void) : void => {
                callbacks.onComplete = $callback;
            }
        };
        return promise;
    }

    public ForwardMessage($agentId : string, $data : any) : any {
        return this.invoke("ForwardMessage", $agentId, $data);
    }

    public getAgentsList() : any {
        return this.invoke("getAgentsList");
    }

    public StartCommunication() : void {
        this.getClient().StartCommunication();
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.HUB_CONNECTOR] = "Io.Oidis.Hub.Primitives.AgentsRegister";
        return namespaces;
    }

    protected resolveDefaultOptions($options : IConnectorOptions) : IConnectorOptions {
        const options : IConnectorOptions = super.resolveDefaultOptions($options);
        if (ObjectValidator.IsEmptyOrNull(options.serverConfigurationSource)) {
            options.serverConfigurationSource = "https://hub.oidis.io/connector.config.jsonp";
        }
        return options;
    }
}

/**
 * AgentsRegisterConnector class provides wrapping of Oidis Hub register of connected agents.
 */
export class AgentsRegisterConnector extends BaseAgentsRegisterConnector {
    public RegisterAgent($capabilities : IAgentCapabilities) : IAgentsRegisterPromise {
        return super.RegisterAgent($capabilities);
    }

    public ForwardMessage($agentId : string, $data : any) : IAcknowledgePromise {
        return super.ForwardMessage($agentId, $data);
    }

    public getAgentsList() : IAgentsListPromise {
        return super.getAgentsList();
    }
}

/**
 * AgentsRegisterConnector class provides wrapping of Oidis Hub register of connected agents.
 */
export class AgentsRegisterConnectorAsync extends BaseAgentsRegisterConnector {
    public RegisterAgent($capabilities : IAgentCapabilities, $handlers? : IRegisterAgentHandlers) : Promise<boolean> {
        return new Promise<boolean>(($resolve, $reject) : void => {
            try {
                const promise : any = super.RegisterAgent($capabilities);
                if (!ObjectValidator.IsEmptyOrNull($handlers)) {
                    if (!ObjectValidator.IsEmptyOrNull($handlers.onMessage)) {
                        promise.OnMessage($handlers.onMessage);
                    }
                }
                if (!ObjectValidator.IsEmptyOrNull(promise.OnError)) {
                    promise.OnError(($error : ErrorEventArgs) : void => {
                        $reject(new Error($error.Message()));
                    });
                }
                promise.Then(($success : boolean) : void => {
                    $resolve($success);
                });
            } catch (ex) {
                $reject(ex);
            }
        });
    }

    public ForwardMessage($agentId : string, $data : any) : Promise<boolean> {
        return this.asyncInvoke(super.ForwardMessage, $agentId, $data);
    }

    public getAgentsList() : Promise<IAgentInfo[]> {
        return this.asyncInvoke(super.getAgentsList);
    }

    public getAgentsMetadata($agentId) : Promise<IAgentMetadata> {
        return this.asyncInvoke("getAgentsMetadata", $agentId);
    }

    public setAgentStatus($agentId : string, $status : IAgentStatus) : Promise<void> {
        return this.asyncInvoke("setAgentStatus", $agentId, $status);
    }

    public getAgentHandshakeToken($agentId : string) : Promise<string> {
        return this.asyncInvoke("getAgentHandshakeToken", $agentId);
    }
}

export interface IAgentCapabilities {
    name : string;
    platform : string;
    domain : string;
    version : string;
    poolName? : string;
    tag? : string;
    credentials? : ICredentials;
    packageInfo? : IPackageInfo;
    withProtectedAPI? : boolean;
    metadata? : IAgentMetadata;
}

export interface ICredentials {
    userName : string;
    authToken : string;
    groups? : string[];
}

export interface IPackageInfo {
    name : string;
    release : string;
    platform : string;
    version : string;
    buildTime : number;
}

export interface IAgentInfo extends IAgentCapabilities {
    startTime : Date;
    id : string;
    status? : IAgentStatus;
}

export interface IAgentMetadata {
    configuration : any;
    environment : any;
}

export enum IAgentStatus {
    Idle,
    Running,
    Stopped
}

export interface IAgentsRegisterPromise extends IAcknowledgePromise {
    OnMessage($callback : ($data : any) => void) : IAcknowledgePromise;
}

export interface IRegisterAgentHandlers {
    onMessage? : ($data : any) => void;
}

export interface IAgentsListPromise {
    Then($callback : ($list : IAgentInfo[]) => void) : void;
}

export interface IRequestForwardingProtocol {
    protocol : IWebServiceProtocol;
    taskId : string;
}

// generated-code-start
/* eslint-disable */
export const IAgentCapabilities = globalThis.RegisterInterface(["name", "platform", "domain", "version", "poolName", "tag", "credentials", "packageInfo", "withProtectedAPI", "metadata"]);
export const ICredentials = globalThis.RegisterInterface(["userName", "authToken", "groups"]);
export const IPackageInfo = globalThis.RegisterInterface(["name", "release", "platform", "version", "buildTime"]);
export const IAgentInfo = globalThis.RegisterInterface(["startTime", "id", "status"], <any>IAgentCapabilities);
export const IAgentMetadata = globalThis.RegisterInterface(["configuration", "environment"]);
export const IAgentsRegisterPromise = globalThis.RegisterInterface(["OnMessage"], <any>IAcknowledgePromise);
export const IRegisterAgentHandlers = globalThis.RegisterInterface(["onMessage"]);
export const IAgentsListPromise = globalThis.RegisterInterface(["Then"]);
export const IRequestForwardingProtocol = globalThis.RegisterInterface(["protocol", "taskId"]);
/* eslint-enable */
// generated-code-end
