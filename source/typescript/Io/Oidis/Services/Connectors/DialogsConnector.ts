/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";
import { IAcknowledgePromise } from "./ReportServiceConnector.js";
import { IWindowHandlerFileDialogSettings, IWindowHandlerSaveFileDialogSettings } from "./WindowHandlerConnector.js";

abstract class BaseDialogsConnector extends BaseConnector {

    public ShowFileDialog($settings : IWindowHandlerFileDialogSettings) : any {
        return this.invoke("ShowFileDialog", $settings);
    }

    public ShowSaveFileDialog($settings : IWindowHandlerSaveFileDialogSettings) : any {
        return this.invoke("ShowSaveFileDialog", $settings);
    }

    public ShowFileExplorer($path : string) : any {
        return this.invoke("ShowFileExplorer", $path);
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.DESKTOP_CONNECTOR] = "Io.Oidis.Connector.Connectors.Dialogs";
        namespaces[WebServiceClientType.BUILDER_CONNECTOR] = "Io.Oidis.Builder.Connectors.Dialogs";
        return namespaces;
    }
}

/**
 * DialogsConnector class provides wrapping for invoke of native dialogs.
 */
export class DialogsConnector extends BaseDialogsConnector {

    /**
     * @param {IWindowHandlerFileDialogSettings} $settings Specify file dialog options.
     * @returns {IDialogConnectorPromise} Returns promise interface for handling of directory browser dialog.
     */
    public ShowFileDialog($settings : IWindowHandlerFileDialogSettings) : IDialogConnectorPromise {
        return super.ShowFileDialog($settings);
    }

    /**
     * @param {IWindowHandlerSaveFileDialogSettings} $settings Specify save file dialog options.
     * @returns {IDialogConnectorPromise} Returns promise interface for handling of save file dialog.
     */
    public ShowSaveFileDialog($settings : IWindowHandlerSaveFileDialogSettings) : IDialogConnectorPromise {
        return super.ShowSaveFileDialog($settings);
    }

    /**
     * @param {string} $path Specify path which should be opened in OS file explorer
     * @returns {IAcknowledgePromise} Returns promise interface for handling of native file explorer window.
     */
    public ShowFileExplorer($path : string) : IAcknowledgePromise {
        return super.ShowFileExplorer($path);
    }
}

/**
 * DialogsConnector class provides same wrapping as DialogsConnector but with native promise handling.
 */
export class DialogsConnectorAsync extends BaseDialogsConnector {

    /**
     * @param {IWindowHandlerFileDialogSettings} $settings Specify file dialog options.
     * @returns {Promise<IDialogConnectorResult>} Returns promise interface for handling of directory browser dialog.
     */
    public async ShowFileDialog($settings : IWindowHandlerFileDialogSettings) : Promise<IDialogConnectorResult> {
        const output : any[] = await this.asyncInvoke(super.ShowFileDialog, $settings);
        return {
            path  : output[1],
            status: output[0]
        };
    }

    /**
     * @param {IWindowHandlerSaveFileDialogSettings} $settings Specify save file dialog options.
     * @returns {Promise<IDialogConnectorResult>} Returns promise interface for handling of save file dialog.
     */
    public async ShowSaveFileDialog($settings : IWindowHandlerSaveFileDialogSettings) : Promise<IDialogConnectorResult> {
        const output : any[] = await this.asyncInvoke(super.ShowSaveFileDialog, $settings);
        return {
            path  : output[1],
            status: output[0]
        };
    }

    /**
     * @param {string} $path Specify path which should be opened in OS file explorer
     * @returns {Promise<boolean>} Returns promise interface for handling of native file explorer window.
     */
    public ShowFileExplorer($path : string) : Promise<boolean> {
        return this.asyncInvoke(super.ShowFileExplorer, $path);
    }
}

export interface IDialogConnectorPromise {
    Then($callback : ($status : boolean, $path : string) => void) : void;
}

export interface IDialogConnectorResult {
    status : boolean;
    path : string;
}

// generated-code-start
export const IDialogConnectorPromise = globalThis.RegisterInterface(["Then"]);
export const IDialogConnectorResult = globalThis.RegisterInterface(["status", "path"]);
// generated-code-end
