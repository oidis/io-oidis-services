/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";
import { LiveContentWrapper } from "../WebServiceApi/LiveContentWrapper.js";

/**
 * FileSystemHandlerConnector class provides wrapping of local file system services provided by Oidis Connector instance.
 */
export class COMproxyConnector extends BaseConnector {

    /**
     * @param {string} $clsid Specify CLSID of COM object, which should be created.
     * @returns {ICOMproxyInstancePromise} Returns promise interface for handling of creation event.
     */
    public CreateInstanceFromCLSID($clsid : string) : ICOMproxyInstancePromise {
        return this.invoke("CreateInstanceFromCLSID", $clsid);
    }

    /**
     * @param {string} $clsid Specify CLSID of COM object, which should be created.
     * @param {string} $instanceName Specify registration name for created instance.
     * @returns {ICOMproxyInstancePromise} Returns promise interface for handling of creation event.
     */
    public CreateInstanceFromCLSIDas($clsid : string, $instanceName : string) : ICOMproxyInstancePromise {
        return this.invoke("CreateInstanceFromCLSIDas", $clsid, $instanceName);
    }

    /**
     * @param {string} $progid Specify PROGID of COM object, which should be created.
     * @returns {ICOMproxyInstancePromise} Returns promise interface for handling of creation event.
     */
    public CreateInstanceFromProgID($progid : string) : ICOMproxyInstancePromise {
        return this.invoke("CreateInstanceFromProgID", $progid);
    }

    /**
     * @param {string} $progid Specify PROGID of COM object, which should be created.
     * @param {string} $instanceName Specify registration name for created instance.
     * @returns {ICOMproxyInstancePromise} Returns promise interface for handling of creation event.
     */
    public CreateInstanceFromProgIDas($progid : string, $instanceName : string) : ICOMproxyInstancePromise {
        return this.invoke("CreateInstanceFromProgIDas", $progid, $instanceName);
    }

    /**
     * @param {string} $instanceName Specify registration name, which should be validated.
     * @returns {ICOMproxyBooleanPromise} Returns promise interface for handling of exists event.
     */
    public Exists($instanceName : string) : ICOMproxyBooleanPromise {
        return this.invoke("Exists", $instanceName);
    }

    /**
     * @param {string} $instanceName Specify registration name, which should be looked up.
     * @returns {ICOMproxyInstancePromise} Returns promise interface for handling of instance receive event.
     */
    public getInstanceId($instanceName : string) : ICOMproxyInstancePromise {
        return this.invoke("getInstanceId", $instanceName);
    }

    /**
     * @param {number} $instanceId Specify instance id, which should be destroyed.
     * @returns {ICOMproxyVoidPromise} Returns promise interface for handling of destroy event.
     */
    public Destroy($instanceId : number) : ICOMproxyVoidPromise {
        return this.invoke("Destroy", $instanceId);
    }

    public TestEvent($instanceId : number, $eventID : number) : ICOMproxyVoidPromise {
        return this.invoke("TestEvent", $instanceId, $eventID);
    }

    /**
     * Frees all allocated instances, releases memory and Un-initializes to Co interface.
     * @returns {ILiveContentFormatterPromise} Returns promise interface for handling of tearDown event.
     */
    public TearDown() : ICOMproxyBooleanPromise {
        return this.invoke("TearDown");
    }

    /**
     * @param {number} $instanceId Specify instance id, which should be handled.
     * @param {string} $name Specify method name, which should be invoked.
     * @param {...any[]} [$args] Specify method arguments, which should be passed to the invoked method.
     * @returns {ICOMproxyMethodPromise} Returns promise interface for handling of invoke method event.
     */
    public InvokeMethod($instanceId : number, $name : string, ...$args : any[]) : ICOMproxyMethodPromise {
        // eslint-disable-next-line prefer-spread
        return this.invoke.apply(this, (<any[]>["InvokeMethod", $instanceId, $name]).concat($args));
    }

    /**
     * @param {number} $instanceId Specify instance id, which should be handled.
     * @param {string} $name Specify property name, which should be invoked.
     * @returns {ICOMproxyPropertyPromise} Returns promise interface for handling of invoke property event.
     */
    public getProperty($instanceId : number, $name : string) : ICOMproxyPropertyPromise {
        return this.invoke("getProperty", $instanceId, $name);
    }

    /**
     * @param {number} $instanceId Specify instance id, which should be handled.
     * @param {string} $name Specify property name, which should be set.
     * @param {any} $value Specify value, which should be set to the property.
     * @returns {ICOMproxyVoidPromise} Returns promise interface for handling of property set event.
     */
    public setProperty($instanceId : number, $name : string, $value : any) : ICOMproxyVoidPromise {
        return this.invoke("setProperty", $instanceId, $name, $value);
    }

    /**
     * @param {number} $instanceId Specify instance id, which should be handled.
     * @param {string} $name Specify event name, which should be handled.
     * @param {number} $eventId Specify event id, which reflects COM interface.
     * @param {Function} $handler Specify event handler callback, which should be asigned.
     * @returns {void}
     */
    public AddEventHandler($instanceId : number, $name : string, $eventId : number, $handler : (...$args : any[]) => void) : void {
        LiveContentWrapper.AddEventHandler(this.getClient(), this.getServerNamespaces(), "OnCOMevent",
            ($eventInstanceId : number, $eventName : string, ...$args : any[]) : void => {
                if ($eventInstanceId === $instanceId && $eventName === $name) {
                    $handler.apply(this, $args[0]);
                }
            });
        this.invoke("AddEventHandler", $instanceId, $name, $eventId);
    }

    protected getClientType() : WebServiceClientType {
        return WebServiceClientType.DESKTOP_CONNECTOR;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.DESKTOP_CONNECTOR] = "Io.Oidis.Connector.Connectors.COMproxy";
        return namespaces;
    }
}

export interface ICOMproxyVoidPromise {
    Then($callback : () => void) : void;
}

export interface ICOMproxyBooleanPromise {
    Then($callback : ($value : boolean) => void) : void;
}

export interface ICOMproxyInstancePromise {
    Then($callback : ($instanceId : number) => void) : void;
}

export interface ICOMproxyMethodPromise {
    Then($callback : ($returnValue : any, ...$args : any[]) => void) : void;
}

export interface ICOMproxyPropertyPromise {
    Then($callback : ($returnValue : any) => void) : void;
}

// generated-code-start
export const ICOMproxyVoidPromise = globalThis.RegisterInterface(["Then"]);
export const ICOMproxyBooleanPromise = globalThis.RegisterInterface(["Then"]);
export const ICOMproxyInstancePromise = globalThis.RegisterInterface(["Then"]);
export const ICOMproxyMethodPromise = globalThis.RegisterInterface(["Then"]);
export const ICOMproxyPropertyPromise = globalThis.RegisterInterface(["Then"]);
// generated-code-end
