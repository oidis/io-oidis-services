/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { ILiveContentErrorPromise } from "../Interfaces/ILiveContentPromise.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";
import { LiveContentWrapper } from "../WebServiceApi/LiveContentWrapper.js";

/**
 * ReportServiceConnector class provides wrapping of Reporting services provided by Oidis Connector servers.
 */
export class ReportServiceConnector extends BaseConnector {

    /**
     * @param {string} $message Specify message, which should be logged.
     * @param {...any[]} [$args] Specify args, for message in formatter type.
     * @returns {ILogItPromise} Returns promise interface for handling of log event.
     */
    public LogIt($message : string, ...$args : any[]) : ILogItPromise {
        return this.invoke("LogIt", ObjectEncoder.Base64(StringUtils.Format($message, $args), true));
    }

    /**
     * @param {string|string[]} $to Specify email receiver as string splitted by ',' or in array format.
     * @param {string} $subject Specify subject text.
     * @param {string} $body Specify email body in pain-text/html format or as path to email template file.
     * @param {string[]} [$attachments] Specify email attachments.
     * @param {string} [$plainText] Specify alternate text for clients without HTML support.
     * @param {string|string[]} [$copyTo] Specify email copy receiver as string splitted by ',' or in array format.
     * @param {string} [$from] Specify email sender.
     * @returns {IAcknowledgePromise} Returns promise interface for handling of send mail event.
     */
    public SendMail($to : string | string[], $subject : string, $body : string,
                    $attachments : string[] = null, $plainText : string = "", $copyTo : string | string[] = "",
                    $from : string = "") : IAcknowledgePromise {
        return LiveContentWrapper.InvokeMethod(this.getClient(),
            "Io.Oidis.Hub.Utils.SendMail", "Send",
            $to,
            $subject,
            ObjectEncoder.Base64($body, true),
            $attachments,
            ObjectEncoder.Base64($plainText),
            $copyTo,
            $from);
    }

    /**
     * @param {IReportProtocol} $data Specify data, which should be reported.
     * @returns {ICreateReportPromise} Returns promise interface for handling of create report event.
     */
    public CreateReport($data : IReportProtocol) : ICreateReportPromise {
        $data.appName = ObjectEncoder.Base64($data.appName, true);
        $data.appVersion = ObjectEncoder.Base64($data.appVersion, true);
        $data.trace = ObjectEncoder.Base64($data.trace, true);
        $data.log = ObjectEncoder.Base64($data.log, true);
        return this.invoke("CreateReport", $data, true);
    }

    protected getClientType() : WebServiceClientType {
        return WebServiceClientType.HUB_CONNECTOR;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.HUB_CONNECTOR] = "Io.Oidis.Hub.Utils.ReportAPI";
        return namespaces;
    }
}

export interface ILogItPromise extends ILiveContentErrorPromise {
    Then($callback : () => void) : void;
}

export interface IAcknowledgePromise extends ILiveContentErrorPromise {
    Then($callback : ($success : boolean) => void) : void;
}

export interface  IReportProtocol {
    appName : string;
    appVersion : string;
    appId : string;
    trace : string;
    timeStamp : number;
    printScreen : string;
    log : string;
}

export interface ICreateReportPromise extends ILiveContentErrorPromise {
    Then($callback : ($success : boolean) => void) : void;
}

// generated-code-start
export const ILogItPromise = globalThis.RegisterInterface(["Then"], <any>ILiveContentErrorPromise);
export const IAcknowledgePromise = globalThis.RegisterInterface(["Then"], <any>ILiveContentErrorPromise);
export const IReportProtocol = globalThis.RegisterInterface(["appName", "appVersion", "appId", "trace", "timeStamp", "printScreen", "log"]);
export const ICreateReportPromise = globalThis.RegisterInterface(["Then"], <any>ILiveContentErrorPromise);
// generated-code-end
