/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { EnvironmentState } from "../Enums/EnvironmentState.js";
import { EnvironmentType } from "../Enums/EnvironmentType.js";
import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { ILiveContentErrorPromise } from "../Interfaces/ILiveContentPromise.js";
import { Loader } from "../Loader.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";
import { LiveContentWrapper } from "../WebServiceApi/LiveContentWrapper.js";

export class EnvironmentConnector extends BaseConnector {

    public static getInstanceSingleton() : EnvironmentConnector {
        const instance : EnvironmentConnector =
            new this(true, Loader.getInstance().getHttpManager().CreateLink("/connector.config.jsonp"));
        this.getInstanceSingleton = () : EnvironmentConnector => {
            return instance;
        };
        return instance;
    }

    public getEnvType() : Promise<EnvironmentType> {
        return this.asyncInvoke("getEnvType");
    }

    public async InitPageEnabled() : Promise<boolean> {
        const status : boolean = await this.asyncInvoke("InitPageEnabled");
        if (status) {
            this.InitPageEnabled = async () : Promise<boolean> => {
                return status;
            };
        }
        return status;
    }

    public async IsEnvironmentLoaded() : Promise<boolean> {
        const status : boolean = await this.asyncInvoke("IsEnvironmentLoaded");
        if (status) {
            this.IsEnvironmentLoaded = async () : Promise<boolean> => {
                return status;
            };
        }
        return status;
    }

    public AddLoadingListener() : ILoadingProcessPromise {
        const callbacks : any = {
            onChange($state : EnvironmentState) : any {
                // declare default callback
            },
            onError: null,
            then() : any {
                // declare default callback
            }
        };
        LiveContentWrapper.InvokeMethod(this.getClient(), this.getServerNamespace(), "AddLoadingListener")
            .OnError(($error : ErrorEventArgs, ...$args : any[]) : void => {
                this.errorHandler(callbacks.onError, $error, $args);
            })
            .Then(($result : any) : void => {
                if (!ObjectValidator.IsEmptyOrNull($result)) {
                    if (ObjectValidator.IsObject($result) && ObjectValidator.IsSet($result.type)) {
                        if ($result.type === EventType.ON_CHANGE || $result.type === EventType.ON_START) {
                            callbacks.onChange($result.data);
                        } else {
                            callbacks.then();
                        }
                    } else {
                        LogIt.Debug("Incorrect result received: {0}", $result);
                        this.errorHandler(callbacks.onError, new ErrorEventArgs("Unrecognized state type received"), []);
                    }
                }
            });
        const promise : ILoadingProcessPromise = {
            OnChange: ($callback : ($data : EnvironmentState) => void) : ILoadingProcessPromise => {
                callbacks.onChange = $callback;
                return promise;
            },
            OnError : ($callback : ($args : ErrorEventArgs) => void) : ILoadingProcessPromise => {
                callbacks.onError = $callback;
                return promise;
            },
            Then    : ($callback : () => void) : void => {
                callbacks.then = $callback;
            }
        };
        return promise;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.HUB_CONNECTOR] = "Io.Oidis.Hub.Utils.EnvironmentHelper";
        return namespaces;
    }
}

export interface ILoadingProcessPromise extends ILiveContentErrorPromise {
    OnChange($callback : ($status : EnvironmentState) => void) : ILoadingProcessPromise;

    Then($callback : () => void) : void;
}

// generated-code-start
export const ILoadingProcessPromise = globalThis.RegisterInterface(["OnChange", "Then"], <any>ILiveContentErrorPromise);
// generated-code-end
