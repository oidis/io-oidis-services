/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";
import { IAcknowledgePromise } from "./ReportServiceConnector.js";

abstract class BaseTerminalConnector extends BaseConnector {

    public Execute($command : string, $args? : string[], $cwdOrOptions? : string | ITerminalOptions) : any {
        return this.getProcessProtocol("Execute", $command,
            ObjectValidator.IsEmptyOrNull($args) ? [] : $args, $cwdOrOptions);
    }

    public Spawn($command : string, $args? : string[], $cwdOrOptions? : string | ITerminalOptions) : any {
        return this.getProcessProtocol("Spawn", $command,
            ObjectValidator.IsEmptyOrNull($args) ? [] : $args, $cwdOrOptions);
    }

    public Open($path : string) : any {
        return this.invoke("Open", $path)
            .DataFormatter(this.getDataFormatter);
    }

    public Elevate($command : string, $args? : string[], $cwdOrOptions? : string | ITerminalOptions) : any {
        return this.getProcessProtocol("Elevate", $command, ObjectValidator.IsEmptyOrNull($args) ? [] : $args, $cwdOrOptions);
    }

    public Kill($pathOrName : string) : any {
        return this.invoke("Kill", $pathOrName);
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.DESKTOP_CONNECTOR] = "Io.Oidis.Connector.Connectors.Terminal";
        namespaces[WebServiceClientType.BUILDER_CONNECTOR] = "Io.Oidis.Builder.Connectors.Terminal";
        namespaces[WebServiceClientType.FORWARD_CLIENT] = "Io.Oidis.Connector.Connectors.Terminal";
        namespaces[WebServiceClientType.HOST_CLIENT] = "Io.Oidis.Connector.Connectors.Terminal";
        return namespaces;
    }

    private getDataFormatter($data : any) : any {
        if ($data === "null") {
            return null;
        } else {
            if (!ObjectValidator.IsEmptyOrNull($data) && ObjectValidator.IsSet($data.type)) {
                return $data.data;
            } else {
                return $data;
            }
        }
    }

    private getProcessProtocol($methodName : string, $command : string, $args? : string[],
                               $cwdOrOptions? : string | ITerminalOptions) : IProcessPromise {
        const callbacks : any = {
            OnExit($exitCode : number, $stdout? : string, $stderr? : string) : void {
                // declare default callback
            },
            OnMessage($data : string) : void {
                // declare default callback
            }
        };
        let options : ITerminalOptions = {cwd: ""};
        if (!ObjectValidator.IsEmptyOrNull($cwdOrOptions)) {
            if (ObjectValidator.IsString($cwdOrOptions)) {
                options = {cwd: <string>$cwdOrOptions};
            } else {
                options = <ITerminalOptions>$cwdOrOptions;
            }
        }

        this.invoke($methodName, $command, $args, options)
            .DataFormatter(this.getDataFormatter)
            .Then(($returnValue : number | string, $stdout? : string, $stderr? : string) : void => {
                if (ObjectValidator.IsArray($returnValue)) {
                    $stdout = $returnValue[1][0];
                    $stderr = $returnValue[1][1];
                    $returnValue = $returnValue[0];
                }
                if ($returnValue === null || ObjectValidator.IsInteger($returnValue)) {
                    callbacks.OnExit(<number>$returnValue, $stdout, $stderr);
                } else if (!ObjectValidator.IsEmptyOrNull($returnValue)) {
                    callbacks.OnMessage(<string>$returnValue);
                }
            });
        const promise : IProcessPromise = <IProcessPromise>{
            OnMessage($callback : ($data : string) => void) : IProcessPromise {
                callbacks.OnMessage = $callback;
                return promise;
            },
            Then($callback : ($exitCode : number, $stdout? : string, $stderr? : string) => void) : void {
                callbacks.OnExit = $callback;
            }
        };
        return promise;
    }
}

/**
 * TerminalConnector class provides wrapping of local file system services provided by Oidis Connector instance.
 */
export class TerminalConnector extends BaseTerminalConnector {

    /**
     * Create new child process and wait for process end
     * @param {string} $command Specify command, which should be executed.
     * @param {string[]} [$args] Specify command args, which should be passed to the executed process.
     * @param {string} [$cwdOrOptions] Specify current working directory for process execution or compact terminal options.
     * @returns {IProcessPromise} Returns promise API connected with response on process exit.
     */
    public Execute($command : string, $args? : string[], $cwdOrOptions? : string | ITerminalOptions) : IProcessPromise {
        return super.Execute($command, $args, $cwdOrOptions);
    }

    /**
     * Create new child process and provide async stdout/stderr by OnMessage event
     * @param {string} $command Specify command, which should be executed.
     * @param {string[]} [$args] Specify command args, which should be passed to the executed process.
     * @param {string} [$cwdOrOptions] Specify current working directory for process execution or compact terminal options.
     * @returns {IProcessPromise} Returns promise API connected with response on process exit.
     */
    public Spawn($command : string, $args? : string[], $cwdOrOptions? : string | ITerminalOptions) : IProcessPromise {
        return super.Spawn($command, $args, $cwdOrOptions);
    }

    /**
     * @param {string} $path Specify path, which should be opened.
     * @returns {IOpenPromise} Returns promise API connected with terminal response.
     */
    public Open($path : string) : IOpenPromise {
        return super.Open($path);
    }

    /**
     * Create new elevated child process and wait for process end
     * @param {string} $command Specify command, which should be executed.
     * @param {string[]} [$args] Specify command args, which should be passed to the executed process.
     * @param {string} [$cwdOrOptions] Specify current working directory for process execution or compact terminal options.
     * @returns {IProcessPromise} Returns promise API connected with response on process exit.
     */
    public Elevate($command : string, $args? : string[], $cwdOrOptions? : string | ITerminalOptions) : IProcessPromise {
        return super.Elevate($command, $args, $cwdOrOptions);
    }

    /**
     * Kill process on specified path or by name
     * @param {string} $pathOrName Specify path where should be process located or process name.
     * @returns {IAcknowledgePromise} Returns promise API connected with response on process kill.
     */
    public Kill($pathOrName : string) : IAcknowledgePromise {
        return super.Kill($pathOrName);
    }
}

/**
 * TerminalConnectorAsync class provides same wrapping as TerminalConnector but with native promise handling.
 */
export class TerminalConnectorAsync extends BaseTerminalConnector {

    /**
     * Create new child process and wait for process end
     * @param {string} $command Specify command, which should be executed.
     * @param {Function} [$onChange] Specify handler for std data processing.
     * @returns {Promise<IExecuteResult>} Returns promise API connected with response on process exit.
     */
    public Execute($command : string, $onChange? : ($data : string) => void) : Promise<IExecuteResult>;
    /**
     * Create new child process and wait for process end
     * @param {string} $command Specify command, which should be executed.
     * @param {string[]} $args Specify command args, which should be passed to the executed process.
     * @param {Function} [$onChange] Specify handler for std data processing.
     * @returns {Promise<IExecuteResult>} Returns promise API connected with response on process exit.
     */
    public Execute($command : string, $args : string[], $onChange? : ($data : string) => void) : Promise<IExecuteResult>;
    /**
     * Create new child process and wait for process end
     * @param {string} $command Specify command, which should be executed.
     * @param {string[]} $args Specify command args, which should be passed to the executed process.
     * @param {string} $cwdOrOptions Specify current working directory for process execution or compact terminal options.
     * @param {Function} [$onChange] Specify handler for std data processing.
     * @returns {Promise<IExecuteResult>} Returns promise API connected with response on process exit.
     */
    public Execute($command : string, $args : string[], $cwdOrOptions : string | ITerminalOptions,
                   $onChange? : ($data : string) => void) : Promise<IExecuteResult>;

    public Execute(...$args : any[]) : Promise<IExecuteResult> {
        return this.asyncProcess(super.Execute, $args);
    }

    /**
     * Create new child process and provide async stdout/stderr by OnMessage event
     * @param {string} $command Specify command, which should be executed.
     * @param {Function} [$onChange] Specify handler for std data processing.
     * @returns {Promise<IExecuteResult>} Returns promise API connected with response on process exit.
     */
    public Spawn($command : string, $onChange? : ($data : string) => void) : Promise<IExecuteResult>;
    /**
     * Create new child process and provide async stdout/stderr by OnMessage event
     * @param {string} $command Specify command, which should be executed.
     * @param {string[]} $args Specify command args, which should be passed to the executed process.
     * @param {Function} [$onChange] Specify handler for std data processing.
     * @returns {Promise<IExecuteResult>} Returns promise API connected with response on process exit.
     */
    public Spawn($command : string, $args : string[], $onChange? : ($data : string) => void) : Promise<IExecuteResult>;
    /**
     * Create new child process and provide async stdout/stderr by OnMessage event
     * @param {string} $command Specify command, which should be executed.
     * @param {string[]} $args Specify command args, which should be passed to the executed process.
     * @param {string} $cwdOrOptions Specify current working directory for process execution or compact terminal options.
     * @param {Function} [$onChange] Specify handler for std data processing.
     * @returns {Promise<IExecuteResult>} Returns promise API connected with response on process exit.
     */
    public Spawn($command : string, $args : string[], $cwdOrOptions : string | ITerminalOptions,
                 $onChange? : ($data : string) => void) : Promise<IExecuteResult>;

    public Spawn(...$args : any[]) : Promise<IExecuteResult> {
        return this.asyncProcess(super.Spawn, $args);
    }

    /**
     * @param {string} $path Specify path, which should be opened.
     * @returns {IOpenPromise} Returns promise API connected with terminal response.
     */
    public Open($path : string) : Promise<boolean> {
        return this.asyncInvoke(super.Open, $path);
    }

    /**
     * Create new elevated child process and wait for process end
     * @param {string} $command Specify command, which should be executed.
     * @param {Function} [$onChange] Specify handler for std data processing.
     * @returns {Promise<IExecuteResult>} Returns promise API connected with response on process exit.
     */
    public Elevate($command : string, $onChange? : ($data : string) => void) : Promise<IExecuteResult>;
    /**
     * Create new elevated child process and wait for process end
     * @param {string} $command Specify command, which should be executed.
     * @param {string[]} $args Specify command args, which should be passed to the executed process.
     * @param {Function} [$onChange] Specify handler for std data processing.
     * @returns {Promise<IExecuteResult>} Returns promise API connected with response on process exit.
     */
    public Elevate($command : string, $args : string[], $onChange? : ($data : string) => void) : Promise<IExecuteResult>;
    /**
     * Create new elevated child process and wait for process end
     * @param {string} $command Specify command, which should be executed.
     * @param {string[]} $args Specify command args, which should be passed to the executed process.
     * @param {string} $cwdOrOptions Specify current working directory for process execution or compact terminal options.
     * @param {Function} [$onChange] Specify handler for std data processing.
     * @returns {Promise<IExecuteResult>} Returns promise API connected with response on process exit.
     */
    public Elevate($command : string, $args : string[], $cwdOrOptions : string | ITerminalOptions,
                   $onChange? : ($data : string) => void) : Promise<IExecuteResult>;

    public Elevate(...$args : any[]) : Promise<IExecuteResult> {
        return this.asyncProcess(super.Elevate, $args);
    }

    /**
     * Kill process on specified path or by name
     * @param {string} $pathOrName Specify path where should be process located or process name.
     * @returns {Promise<boolean>} Returns promise API connected with response on process kill.
     */
    public Kill($pathOrName : string) : Promise<boolean> {
        return this.asyncInvoke(super.Kill, $pathOrName);
    }

    private asyncProcess($method : any, $args : any[]) : Promise<IExecuteResult> {
        return new Promise<IExecuteResult>(($resolve, $reject) : void => {
            try {
                const options : IChildProcessOptions = {
                    command: $args[0]
                };
                if ($args.length === 2) {
                    if (ObjectValidator.IsArray($args[1])) {
                        options.args = $args[1];
                    } else {
                        options.onChange = $args[1];
                    }
                } else if ($args.length === 3) {
                    options.args = $args[1];
                    if (ObjectValidator.IsString($args[2]) || ObjectValidator.IsObject($args[2]) ||
                        ObjectValidator.IsEmptyOrNull($args[2])) {
                        options.cwdOrOptions = $args[2];
                    } else {
                        options.onChange = $args[2];
                    }
                } else if ($args.length > 3) {
                    options.args = $args[1];
                    options.cwdOrOptions = $args[2];
                    options.onChange = $args[3];
                }
                const promise : any = $method.apply(this, [options.command, options.args, options.cwdOrOptions]);
                if (!ObjectValidator.IsEmptyOrNull(options.onChange)) {
                    promise.OnMessage(options.onChange);
                }
                if (!ObjectValidator.IsEmptyOrNull(promise.OnError)) {
                    promise.OnError(($error : ErrorEventArgs) : void => {
                        $reject(new Error($error.Message()));
                    });
                }
                promise.Then(($exitCode : number, $stdout? : string, $stderr? : string) : void => {
                    $resolve({
                        exitCode: $exitCode,
                        std     : [$stdout, $stderr],
                        stderr  : $stderr,
                        stdout  : $stdout
                    });
                });
            } catch (ex) {
                $reject(ex);
            }
        });
    }
}

export interface IAdvancedTerminalOptions {
    colored? : boolean;
    noTerminalLog? : boolean;
    noExitOnStderr? : boolean;
    useStdIn? : boolean;
}

export interface ITerminalOptions {
    cwd? : string;
    env? : any;
    shell? : boolean;
    verbose? : boolean;
    detached? : boolean;
    advanced? : IAdvancedTerminalOptions;
}

export interface IProcessPromise {
    OnMessage($callback : ($data : string) => void) : IProcessPromise;

    Then($callback : ($exitCode : number, $stdout? : string, $stderr? : string) => void) : void;
}

export interface IOpenPromise {
    Then($callback : ($status : boolean) => void) : void;
}

export interface IExecuteResult {
    exitCode : number;
    stdout? : string;
    stderr? : string;
    std : string[];
}

interface IChildProcessOptions {
    command : string;
    args? : string[];
    cwdOrOptions? : string | ITerminalOptions;
    onChange? : ($data : string) => void;
}

// generated-code-start
export const IAdvancedTerminalOptions = globalThis.RegisterInterface(["colored", "noTerminalLog", "noExitOnStderr", "useStdIn"]);
export const ITerminalOptions = globalThis.RegisterInterface(["cwd", "env", "shell", "verbose", "detached", "advanced"]);
export const IProcessPromise = globalThis.RegisterInterface(["OnMessage", "Then"]);
export const IOpenPromise = globalThis.RegisterInterface(["Then"]);
export const IExecuteResult = globalThis.RegisterInterface(["exitCode", "stdout", "stderr", "std"]);
// generated-code-end
