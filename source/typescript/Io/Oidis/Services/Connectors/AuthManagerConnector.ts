/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { PersistenceType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/PersistenceType.js";
import { CookiesHandler } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/Handlers/CookiesHandler.js";
import { StorageHandler } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/Handlers/StorageHandler.js";
import { PersistenceFactory } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { AuthHttpResolver } from "../ErrorPages/AuthHttpResolver.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";
import { IAuthorizedMethods } from "../Structures/IAuthorizedMethods.js";

export class AuthManagerConnector extends BaseConnector {

    public CurrentToken($value? : string) : string {
        if (ObjectValidator.IsSet($value)) {
            PersistenceFactory.getPersistence(PersistenceType.CLIENT_IP, AuthHttpResolver.ClassName())
                .Variable("token", $value);
            if (StorageHandler.IsSupported()) {
                localStorage.setItem(AuthHttpResolver.getTokenName(), $value);
            } else if (this.httpManager.getRequest().IsCookieEnabled()) {
                CookiesHandler.setCookie(AuthHttpResolver.getTokenName(), $value);
            }
        }
        return AuthHttpResolver.getToken();
    }

    public LogIn($userNameOrEmail : string, $password : string, $noExpire : boolean = false) : Promise<string> {
        return this.asyncInvoke("LogIn", $userNameOrEmail, StringUtils.getSha1($password), $noExpire);
    }

    public LogOut($token : string = null) : Promise<boolean> {
        if (ObjectValidator.IsEmptyOrNull($token)) {
            $token = this.CurrentToken();
        }
        this.CurrentToken("");
        return this.asyncInvoke("LogOut", $token);
    }

    public IsAuthenticated($token : string = null) : Promise<boolean> {
        return this.asyncInvoke("IsAuthenticated", $token);
    }

    public IsAuthorizedFor($method : string, $token : string = null) : Promise<boolean> {
        return this.asyncInvoke("IsAuthorizedFor", $method, $token);
    }

    public Register($options : IUserRegisterOptions) : Promise<boolean> {
        $options.password = StringUtils.getSha1($options.password);
        return this.asyncInvoke("Register", $options);
    }

    public async ActivateUser($token : string) : Promise<boolean> {
        return this.asyncInvoke("ActivateUser", $token);
    }

    public DeactivateUser($id : string, $value : boolean = true) : Promise<boolean> {
        return this.asyncInvoke("DeactivateUser", $id, $value);
    }

    public ResetPassword($email : string) : Promise<boolean> {
        return this.asyncInvoke("ResetPassword", $email);
    }

    public ResetPasswordConfirm($token : string, $newPassword : string) : Promise<boolean> {
        return this.asyncInvoke("ResetPasswordConfirm", $token, StringUtils.getSha1($newPassword));
    }

    public ChangePassword($password : string, $userId : string = null) : Promise<string> {
        return this.asyncInvoke("ChangePassword", StringUtils.getSha1($password), $userId);
    }

    public RemoveUser($userId : string | string[]) : Promise<boolean> {
        return this.asyncInvoke("RemoveUser", $userId);
    }

    public DeleteUser($userId : string) : Promise<boolean> {
        return this.asyncInvoke("DeleteUser", $userId);
    }

    public RestoreUser($userId : string) : Promise<boolean> {
        return this.asyncInvoke("RestoreUser", $userId);
    }

    public getAuthorizedMethods($token : string = null) : Promise<string[]> {
        return this.asyncInvoke("getAuthorizedMethods", $token);
    }

    public setAuthorizedMethods($group : string, $authMethods : string[], $append : boolean = false) : Promise<boolean> {
        return this.asyncInvoke("setAuthorizedMethods", $group, $authMethods, $append);
    }

    public getAuthorizedMethodsFor($userId : string) : Promise<string[]> {
        return this.asyncInvoke("getAuthorizedMethodsFor", $userId);
    }

    public AddGroup($groupName : string, $userId : string = null) : Promise<boolean> {
        return this.asyncInvoke("AddGroup", $groupName, $userId);
    }

    public getGroups($userId : string = null) : Promise<string[]> {
        return this.asyncInvoke("getGroups", $userId);
    }

    public RemoveGroup($groupName : string, $userId : string = null) : Promise<boolean> {
        return this.asyncInvoke("RemoveGroup", $groupName, $userId);
    }

    public RestoreGroup($id : string) : Promise<boolean> {
        return this.asyncInvoke("RestoreGroup", $id);
    }

    public GenerateAuthToken($name : string = "", $userId : string = null, $noExpire : boolean = false) : Promise<IAuthToken> {
        return this.asyncInvoke("GenerateAuthToken", $name, $userId, $noExpire);
    }

    public RevokeAuthToken($authToken : string, $userId : string = null) : Promise<boolean> {
        return this.asyncInvoke("RevokeAuthToken", $authToken, $userId);
    }

    public UpdateAuthToken($authToken : string, $options : IAuthTokenOptions, $userId : string = null) : Promise<boolean> {
        return this.asyncInvoke("UpdateAuthToken", $authToken, $options, $userId);
    }

    public async getAuthToken($name : string, $userId : string = null) : Promise<string> {
        return this.asyncInvoke("getAuthToken", $name, $userId);
    }

    public IsAuthTokenValid($authToken : string, $userId : string = null) : Promise<boolean> {
        return this.asyncInvoke("IsAuthTokenValid", $authToken, $userId);
    }

    public IsPasswordValid($token : string, $password : string) : Promise<boolean> {
        return this.asyncInvoke("IsPasswordValid", $token, StringUtils.getSha1($password));
    }

    public RegisterApp($options : IAppRegisterOptions) : Promise<boolean> {
        return this.asyncInvoke("RegisterApp", $options);
    }

    public async getGroupMethods($group : string) : Promise<string[]> {
        return this.asyncInvoke("getGroupMethods", $group);
    }

    public getAppMethods($options : IAppProfileOptions) : Promise<IAuthorizedMethods[]> {
        return this.asyncInvoke("getAppMethods", $options);
    }

    public async FindUsers($idOrUserNameFilter : string | any, $options : IAuthFindUsersOptions = null) : Promise<IModelListResult> {
        return this.asyncInvoke("FindUsers", $idOrUserNameFilter, $options);
    }

    public async CreateGroup($name : string, $methods : string[] = [], $force : boolean = false) : Promise<string> {
        return this.asyncInvoke("CreateGroup", $name, $methods, $force);
    }

    public async FindGroups($idOrNameFilter : string, $options : IAuthFindGroupsOptions = null) : Promise<IModelListResult> {
        return this.asyncInvoke("FindGroups", $idOrNameFilter, $options);
    }

    public async DeleteGroup($idOrName : string) : Promise<boolean> {
        return this.asyncInvoke("DeleteGroup", $idOrName);
    }

    public async getUserId($tokenOrName : string) : Promise<string> {
        return this.asyncInvoke("getUserId", $tokenOrName);
    }

    public async getUserProfile($withData : boolean = false, $userIdOrToken : string = null) : Promise<any> {
        return this.asyncInvoke("getUserProfile", $withData, $userIdOrToken);
    }

    public async setUserProfile($options : IUserProfileOptions, $userId : string = null) : Promise<boolean> {
        return this.asyncInvoke("setUserProfile", $options, $userId);
    }

    public async CheckEmailDuplicity() : Promise<any> {
        return this.asyncInvoke("CheckEmailDuplicity");
    }

    public RegisterOrganization($options : IOrganizationProfileOptions, $owner? : IUserRegisterOptions) : Promise<string> {
        if (!ObjectValidator.IsEmptyOrNull($owner)) {
            $owner.password = StringUtils.getSha1($owner.password);
        }
        return this.asyncInvoke("RegisterOrganization", $options, $owner);
    }

    public setOrganizationProfile($options : IOrganizationProfileOptions, $organizationId : string) : Promise<boolean> {
        return this.asyncInvoke("setOrganizationProfile", $options, $organizationId);
    }

    public getOrganizationProfile($idOrOwnerId : string, $withData : boolean = false) : Promise<any> {
        return this.asyncInvoke("getOrganizationProfile", $idOrOwnerId, $withData);
    }

    public getOrganizationChart($organizationId : string, $filter? : IModelListFilter) : Promise<IModelListResult> {
        return this.asyncInvoke("getOrganizationChart", $organizationId);
    }

    public AddUserToOrganization($userId : string, $organizationId : string) : Promise<boolean> {
        return this.asyncInvoke("AddUserToOrganization", $userId, $organizationId);
    }

    public RemoveUserFromOrganization($userId : string, $organizationId : string) : Promise<boolean> {
        return this.asyncInvoke("RemoveUserFromOrganization", $userId, $organizationId);
    }

    public FindOrganizations($filter : string | any, $options : IAuthFindOptions = null) : Promise<IModelListResult> {
        return this.asyncInvoke("FindOrganizations", $filter, $options);
    }

    public DeactivateOrganization($id : string, $value : boolean = true) : Promise<boolean> {
        return this.asyncInvoke("DeactivateOrganization", $id, $value);
    }

    public DeleteOrganization($id : string) : Promise<boolean> {
        return this.asyncInvoke("DeleteOrganization", $id);
    }

    public RestoreOrganization($id : string) : Promise<boolean> {
        return this.asyncInvoke("RestoreOrganization", $id);
    }

    protected getClientType() : WebServiceClientType {
        return WebServiceClientType.HUB_CONNECTOR;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.HUB_CONNECTOR] = "Io.Oidis.Hub.Utils.AuthManager";
        return namespaces;
    }
}

export interface IAuthToken {
    name : string;
    value : string;
    id : string;
    expire : number;
}

export interface IAuthTokenOptions {
    name? : string;
    expireTime? : number;
}

export interface IUserProfileOptions {
    name? : string;
    firstname? : string;
    lastname? : string;
    email? : string;
    phone? : string;
    image? : string;
    altEmails? : string[];
    throwExceptions? : boolean;
    activated? : boolean;
    twoFA? : string;
    whatsNewSeen? : string;
}

export interface IUserRegisterOptions extends IUserProfileOptions {
    password : string;
    withActivation? : boolean;
    withInvitation? : boolean;
}

export interface IAppProfileOptions {
    appName : string;
    releaseName : string;
    platform : string;
    version : string;
    buildTime : string | number;
}

export interface IAppRegisterOptions extends IAppProfileOptions {
    authMethods : IAuthorizedMethods[];
}

export interface IModelListFilter {
    offset? : number;
    limit? : number;
}

export interface IModelListResult<T = any> {
    data : T[];
    size : number;
    offset : number;
    limit : number;
}

export interface IAuthFindOptions {
    offset? : number;
    limit? : number;
    onlyActive? : boolean;
    withDeleted? : boolean;
}

export interface IAuthFindUsersOptions extends IAuthFindOptions {
    onlyWithOwnPermissions? : boolean;
}

export interface IAuthFindGroupsOptions extends IAuthFindOptions {
    withHidden? : boolean;
    withReadonly? : boolean;
    withEmptyPermissions? : boolean;
    onlyWithAsterix? : boolean;
    onlySystem? : boolean;
}

export interface IOrganizationProfileOptions {
    name? : string;
    image? : string;
    activated? : boolean;
}

// generated-code-start
/* eslint-disable */
export const IAuthToken = globalThis.RegisterInterface(["name", "value", "id", "expire"]);
export const IAuthTokenOptions = globalThis.RegisterInterface(["name", "expireTime"]);
export const IUserProfileOptions = globalThis.RegisterInterface(["name", "firstname", "lastname", "email", "phone", "image", "altEmails", "throwExceptions", "activated", "twoFA", "whatsNewSeen"]);
export const IUserRegisterOptions = globalThis.RegisterInterface(["password", "withActivation", "withInvitation"], <any>IUserProfileOptions);
export const IAppProfileOptions = globalThis.RegisterInterface(["appName", "releaseName", "platform", "version", "buildTime"]);
export const IAppRegisterOptions = globalThis.RegisterInterface(["authMethods"], <any>IAppProfileOptions);
export const IModelListFilter = globalThis.RegisterInterface(["offset", "limit"]);
export const IModelListResult = globalThis.RegisterInterface(["data", "size", "offset", "limit"]);
export const IAuthFindOptions = globalThis.RegisterInterface(["offset", "limit", "onlyActive", "withDeleted"]);
export const IAuthFindUsersOptions = globalThis.RegisterInterface(["onlyWithOwnPermissions"], <any>IAuthFindOptions);
export const IAuthFindGroupsOptions = globalThis.RegisterInterface(["withHidden", "withReadonly", "withEmptyPermissions", "onlyWithAsterix", "onlySystem"], <any>IAuthFindOptions);
export const IOrganizationProfileOptions = globalThis.RegisterInterface(["name", "image", "activated"]);
/* eslint-enable */
// generated-code-end
