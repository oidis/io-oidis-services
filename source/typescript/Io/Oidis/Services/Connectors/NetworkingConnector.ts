/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";

/**
 * NetworkingConnector class provides wrapping of networking services provided by Oidis Connector instance.
 */
export class NetworkingConnector extends BaseConnector {

    /**
     * @returns {INetworkingConnectorPortPromise} Returns promise interface for handling of free port.
     */
    public getFreePort() : INetworkingConnectorPortPromise {
        return this.invoke("getFreePort");
    }

    public AddPortListener($port : number) : INetworkingConnectorListenPromise {
        const callbacks : any = {
            onConnect($clientId : string) : any {
                // declare default callback
            },
            onMessage($data : string) : any {
                // declare default callback
            },
            onComplete() : any {
                // declare default callback
            }
        };
        this.invoke("AddPortListener", $port)
            .Then(($result : any) : void => {
                if (ObjectValidator.IsSet($result.type)) {
                    if ($result.type === EventType.ON_START) {
                        callbacks.onConnect($result.data);
                    } else if ($result.type === EventType.ON_CHANGE) {
                        callbacks.onMessage($result.data);
                    } else if ($result.type === EventType.ON_COMPLETE) {
                        callbacks.onComplete();
                    }
                }
            });
        const promise : INetworkingConnectorListenPromise = {
            OnConnect: ($callback : ($clientId : string) => void) : INetworkingConnectorListenPromise => {
                callbacks.onConnect = $callback;
                return promise;
            },
            OnMessage: ($callback : ($data : string) => void) : INetworkingAcknowledgePromise => {
                callbacks.onMessage = $callback;
                return promise;
            },
            Then     : ($callback : () => void) : void => {
                callbacks.onComplete = $callback;
            }
        };
        return promise;
    }

    public RemovePortListener($port : number, $clientId : string) : INetworkingAcknowledgePromise {
        return this.invoke("RemovePortListener", $clientId, $port);
    }

    public WriteToPort($data : string, $port : number) : INetworkingAcknowledgePromise {
        return this.invoke("WriteToPort", $data, $port);
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.DESKTOP_CONNECTOR] = "Io.Oidis.Connector.Connectors.Network";
        namespaces[WebServiceClientType.BUILDER_CONNECTOR] = "Io.Oidis.Builder.Connectors.Network";
        namespaces[WebServiceClientType.FORWARD_CLIENT] = "Io.Oidis.Connector.Connectors.Network";
        return namespaces;
    }
}

export interface INetworkingConnectorPortPromise {
    Then($callback : ($port : number) => void) : void;
}

export interface INetworkingAcknowledgePromise {
    Then($callback : () => void) : void;
}

export interface INetworkingConnectorListenPromise extends INetworkingAcknowledgePromise {
    OnConnect($callback : ($clientId : string) => void) : INetworkingConnectorListenPromise;

    OnMessage($callback : ($data : string) => void) : INetworkingAcknowledgePromise;
}

// generated-code-start
/* eslint-disable */
export const INetworkingConnectorPortPromise = globalThis.RegisterInterface(["Then"]);
export const INetworkingAcknowledgePromise = globalThis.RegisterInterface(["Then"]);
export const INetworkingConnectorListenPromise = globalThis.RegisterInterface(["OnConnect", "OnMessage"], <any>INetworkingAcknowledgePromise);
/* eslint-enable */
// generated-code-end
