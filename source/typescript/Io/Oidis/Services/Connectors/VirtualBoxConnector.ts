/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";

/**
 * VirtualBoxConnector class provides wrapping of Virtualbox control services.
 */
export class VirtualBoxConnector extends BaseConnector {

    constructor() {
        super(true);
    }

    /**
     * @param {string} $machineNameOrId Specify name or id of existing VM instance at VirtualBox
     * @param {string} [$user="Guest"] Specify user name, which should be used for login into the VM.
     * @param {string} [$pass=""] Specify password, which should be used for login into the VM.
     * @returns {IVirtualBoxStartPromise} Returns promise interface for handling this event.
     */
    public Start($machineNameOrId : string, $user : string = "Guest", $pass : string = "") : IVirtualBoxStartPromise {
        return this.invoke("Start", $machineNameOrId, $user, $pass);
    }

    /**
     * @param {string} $sessionId Specify session id, which should be invoked on VM.
     * @param {string} $sourcePath Specify source file, which should be copied to VM.
     * @param {string} $destinationPath Specify destination path, where should be file copied to VM.
     * @returns {IVirtualBoxStatusPromise} Returns promise interface for handling this event.
     */
    public Copy($sessionId : string, $sourcePath : string, $destinationPath : string) : IVirtualBoxStatusPromise {
        return this.invoke("Copy", $sessionId, $sourcePath, $destinationPath);
    }

    /**
     * @param {string} $sessionId Specify session id, which should be invoked on VM.
     * @param {string} $filePath Specify path to file, which should be executed on VM.
     * @returns {IVirtualBoxStatusPromise} Returns promise interface for handling this event.
     */
    public Execute($sessionId : string, $filePath : string) : IVirtualBoxStatusPromise {
        return this.invoke("Execute", $sessionId, $filePath);
    }

    protected getClientType() : WebServiceClientType {
        return WebServiceClientType.BUILDER_CONNECTOR;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.BUILDER_CONNECTOR] = "Io.Oidis.Builder.Connectors.VirtualBoxHandler";
        return namespaces;
    }
}

export interface IVirtualBoxStartPromise {
    Then($callback : ($success : boolean, $sessionId : string) => void) : void;
}

export interface IVirtualBoxStatusPromise {
    Then($callback : ($success : boolean) => void) : void;
}

// generated-code-start
export const IVirtualBoxStartPromise = globalThis.RegisterInterface(["Then"]);
export const IVirtualBoxStatusPromise = globalThis.RegisterInterface(["Then"]);
// generated-code-end
