/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LiveContentArgumentType } from "../Enums/LiveContentArgumentType.js";
import { WebServiceClientType } from "../Enums/WebServiceClientType.js";
import { BaseConnector } from "../Primitives/BaseConnector.js";

/**
 * FFIProxyConnector class provides wrapping of FFI proxy services provided by Oidis Connector instance.
 */
export class FFIProxyConnector extends BaseConnector {

    /**
     * @param {string|IFFIOptions} $fileOrOptions Specify name of the library file.
     * @returns {IFFIProxyMethodPromise} Returns promise interface for handling of creation event.
     */
    public Load($fileOrOptions : string | IFFIOptions) : IFFIProxyMethodPromise {
        return this.invoke("Load", $fileOrOptions);
    }

    /**
     * @param {string} $name Specify method name, which should be invoked.
     * @param {LiveContentArgumentType} $returnType Specify return type for method, which should be invoked.
     * @param {...any[]} [$args] Specify method arguments, which should be passed to the invoked method.
     * @returns {IFFIProxyMethodPromise} Returns promise interface for handling of invoke method event.
     */
    public InvokeMethod($name : string, $returnType : LiveContentArgumentType, ...$args : any[]) : IFFIProxyMethodPromise {
        // eslint-disable-next-line prefer-spread
        return this.invoke.apply(this, ["InvokeMethod", $name, $returnType].concat($args));
    }

    /**
     * @returns {IFFIProxyMethodPromise} Returns promise interface for handling of release event.
     */
    public Release() : IFFIProxyMethodPromise {
        return this.invoke.apply(this, ["Release"]);
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.DESKTOP_CONNECTOR] = "Io.Oidis.Connector.Connectors.FFIProxy";
        namespaces[WebServiceClientType.BUILDER_CONNECTOR] = "Io.Oidis.Builder.Connectors.FFIProxy";
        namespaces[WebServiceClientType.FORWARD_CLIENT] = "Io.Oidis.Connector.Connectors.FFIProxy";
        return namespaces;
    }
}

export interface IFFIProxyMethodPromise {
    Then($callback : ($returnValue : any, ...$args : any[]) => void) : void;
}

export interface IFFIArgumentArray {
    type : LiveContentArgumentType;
    items : any[];
}

export interface IFFIArgumentStruct {
    typeName : string;
    fields : any;
}

export interface IFFIArgumentCallback {
    typeName : string;
    callback : any;
    returnType? : LiveContentArgumentType;
    args? : LiveContentArgumentType[] | IFFIArgument[];
}

export interface IFFIArgument {
    type? : LiveContentArgumentType;
    value : IFFIArgumentArray | IFFIArgumentStruct | IFFIArgumentCallback | any;
}

export interface IFFIOptions {
    libraryPath : string;
    env? : any;
    cwd? : string;
}

// generated-code-start
export const IFFIProxyMethodPromise = globalThis.RegisterInterface(["Then"]);
export const IFFIArgumentArray = globalThis.RegisterInterface(["type", "items"]);
export const IFFIArgumentStruct = globalThis.RegisterInterface(["typeName", "fields"]);
export const IFFIArgumentCallback = globalThis.RegisterInterface(["typeName", "callback", "returnType", "args"]);
export const IFFIArgument = globalThis.RegisterInterface(["type", "value"]);
export const IFFIOptions = globalThis.RegisterInterface(["libraryPath", "env", "cwd"]);
// generated-code-end
