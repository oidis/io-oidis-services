/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { WebServiceClientType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/WebServiceClientType.js";

export abstract class NotifyBalloonIconType extends WebServiceClientType {
    public static readonly NONE : string = "none";
    public static readonly INFO : string = "info";
    public static readonly WARNING : string = "warning";
    public static readonly ERROR : string = "error";
    public static readonly USER : string = "user";
}
