/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";

export abstract class InstallationProtocolType extends BaseEnum {
    public static readonly VALIDATE : string = "validate";
    public static readonly INSTALL_STATUS : string = "installCondition";
    public static readonly INSTALL : string = "install";
    public static readonly UNINSTALL : string = "uninstall";
    public static readonly REPAIR : string = "repair";
}
