/*! ******************************************************************************************************** *
 *
 * Copyright 2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";

export abstract class EnvironmentType extends BaseEnum {
    public static readonly NONE : string = "none";
    public static readonly DEV : string = "dev";
    public static readonly EAP : string = "eap";
    public static readonly PROD : string = "prod";
    public static readonly MAINTENANCE : string = "maintenance";
    public static readonly LOCALHOST : string = "localhost";
}
