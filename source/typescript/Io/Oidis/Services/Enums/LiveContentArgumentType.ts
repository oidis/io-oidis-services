/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";

export abstract class LiveContentArgumentType extends BaseEnum {
    public static readonly CALLBACK : string = "__[CALLBACK]__";

    public static readonly IN_STRING : string = "__[IN:STRING]__";
    public static readonly IN_INT : string = "__[IN:INT]__";
    public static readonly IN_DOUBLE : string = "__[IN:DOUBLE]__";
    public static readonly IN_FLOAT : string = "__[IN:FLOAT]__";
    public static readonly IN_BOOLEAN : string = "__[IN:BOOLEAN]__";
    public static readonly IN_OBJECT : string = "__[IN:OBJECT]__";
    public static readonly IN_STRUCT : string = "__[IN:STRUCT]__";
    public static readonly IN_ARRAY : string = "__[IN:ARRAY]__";
    public static readonly IN_BUFFER : string = "__[IN:BUFFER]__";

    public static readonly OUT_STRING : string = "__[OUT:STRING]__";
    public static readonly OUT_INT : string = "__[OUT:INT]__";
    public static readonly OUT_DOUBLE : string = "__[OUT:DOUBLE]__";
    public static readonly OUT_FLOAT : string = "__[OUT:FLOAT]__";
    public static readonly OUT_BOOLEAN : string = "__[OUT:BOOLEAN]__";
    public static readonly OUT_OBJECT : string = "__[OUT:OBJECT]__";
    public static readonly OUT_ARRAY : string = "__[OUT:ARRAY]__";
    public static readonly OUT_BUFFER : string = "__[OUT:BUFFER]__";

    public static readonly RETURN_VOID : string = "__[RETURN:VOID]__";
    public static readonly RETURN_STRING : string = "__[RETURN:STRING]__";
    public static readonly RETURN_INT : string = "__[RETURN:INT]__";
    public static readonly RETURN_DOUBLE : string = "__[RETURN:DOUBLE]__";
    public static readonly RETURN_FLOAT : string = "__[RETURN:FLOAT]__";
    public static readonly RETURN_BOOLEAN : string = "__[RETURN:BOOLEAN]__";
}
