/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";

export abstract class EnvironmentState extends BaseEnum {
    public static readonly IDLE : string = "idle";
    public static readonly STARTING : string = "starting";
    public static readonly LOADED : string = "loaded";
    public static readonly DB_CONNECTING : string = "dbConnecting";
    public static readonly DB_CONNECTION_ERROR : string = "dbConnectionError";
    public static readonly DB_CONNECTED : string = "dbConnected";
    public static readonly DB_NOT_CONNECTED : string = "dbNotConnected";
    public static readonly DB_PREPARED : string = "dbPrepared";
    public static readonly AGENTS_INIT : string = "agentsInit";
    public static readonly ADMIN_INIT : string = "adminInit";
    public static readonly ADMIN_ENABLED : string = "adminEnabled";
}
