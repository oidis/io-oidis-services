/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { WebServiceClientType as Parent } from "@io-oidis-commons/Io/Oidis/Commons/Enums/WebServiceClientType.js";

export abstract class WebServiceClientType extends Parent {
    public static readonly BUILDER_CONNECTOR : string = "BuilderConnector";
    public static readonly DESKTOP_CONNECTOR : string = "DesktopConnector";
    public static readonly REST_CONNECTOR : string = "RestConnector";
    public static readonly HUB_CONNECTOR : string = "HubConnector";
    public static readonly FORWARD_CLIENT : string = "ForwardClient";
    public static readonly HOST_CLIENT : string = "HostClient";
}
