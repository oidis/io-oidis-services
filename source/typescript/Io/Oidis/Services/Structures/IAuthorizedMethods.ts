/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export interface IAuthorizedMethods {
    className : string;
    namespaceName : string;
    methods : string[];
}

// generated-code-start
export const IAuthorizedMethods = globalThis.RegisterInterface(["className", "namespaceName", "methods"]);
// generated-code-end
