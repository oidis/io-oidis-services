/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpMethodType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpMethodType.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";

export class FileFetch extends BaseObject {

    constructor() {
        super();
    }

    /// TODO: issue on Safari can be connected with Nginx config due to need of TLS 1.2
    public async Open($options : IFileFetchOptions) : Promise<void> {
        $options = JsonUtils.Extend(<IFileFetchOptions>{
            method    : HttpMethodType.GET,
            saveAsFile: true
        }, $options);

        if (ObjectValidator.IsEmptyOrNull($options.content)) {
            const requestOptions : any = {
                method: $options.method + ""
            };
            if (!ObjectValidator.IsEmptyOrNull($options.bearerToken)) {
                requestOptions.headers = new Headers({
                    authorization: "Bearer " + $options.bearerToken
                });
            }
            const response = await fetch($options.url, requestOptions);

            if (!response.ok) {
                const msg : string = StringUtils.Format("File fetch response failed: [{0}] {1}", response.status, response.statusText);
                LogIt.Warning(msg);
                throw new Error(msg);
            }

            $options.fileName = this.constructDT();
            const disposition : string = response.headers.get("content-disposition");
            if (!ObjectValidator.IsEmptyOrNull(disposition)) {
                const filenameKey : string = "filename=";
                if (StringUtils.Contains(disposition, "attachment;") &&
                    StringUtils.Contains(disposition, filenameKey)) {
                    const parts : string[] = StringUtils.Split(disposition, ";");
                    parts.forEach(($part : string) : void => {
                        $part = $part.trim();
                        if (StringUtils.Contains($part, filenameKey)) {
                            $part = StringUtils.Remove($part, filenameKey);
                            $part = StringUtils.Remove($part, "\"");
                            $options.fileName = ObjectDecoder.Url($part.trim());
                        }
                    });
                }
            }
            LogIt.Debug("Fetch file will start download with: {0}", $options.fileName);

            this.openBlob(await response.blob(), $options.saveAsFile ? $options.fileName : "");
        } else {
            this.openBlob($options.content, $options.saveAsFile ? $options.fileName : "");
        }
    }

    /// TODO: blob open can be blocked by popup blockers
    //        > this may be avoid if it is not invoked from timeout which can be done by current events handling
    private openBlob($data : Blob, $name? : string) : void {
        const URL = (<any>window).URL || (<any>window).webkitURL;
        const data = URL.createObjectURL($data);
        if (!ObjectValidator.IsEmptyOrNull($name)) {
            const a = document.createElement("a");
            a.href = data;
            a.download = $name;
            document.body.appendChild(a);
            a.click();
            setTimeout(() : void => {
                document.body.removeChild(a);
                URL.revokeObjectURL(data);
            }, 100);
        } else {
            window.open(data);
        }
    }

    private constructDT() : string {
        const date : Date = new Date();
        const pad : any = ($value : number, $chars : number) : string => {
            let retVal : string = $value.toString();
            if (retVal.length < $chars) {
                retVal = "0".repeat($chars - retVal.length) + retVal;
            }
            return retVal;
        };
        return "" +
            pad(date.getFullYear(), 4) +
            pad(date.getMonth() + 1, 2) +
            pad(date.getDate(), 2) +
            pad(date.getHours(), 2) +
            pad(date.getMinutes(), 2) +
            pad(date.getSeconds(), 2);
    }
}

export interface IFileFetchOptions {
    url? : string;
    content? : Blob;
    saveAsFile? : boolean;
    bearerToken? : string;
    method? : HttpMethodType;
    fileName? : string;
}

// generated-code-start
export const IFileFetchOptions = globalThis.RegisterInterface(["url", "content", "saveAsFile", "bearerToken", "method", "fileName"]);
// generated-code-end
