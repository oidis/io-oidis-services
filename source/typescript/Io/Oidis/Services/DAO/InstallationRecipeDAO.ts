/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { ProgressEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ProgressEventArgs.js";
import { IEventsHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IEventsHandler.js";
import { IEventsManager } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IEventsManager.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IErrorEventsHandler } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Events/IErrorEventsHandler.js";
import { FileSystemHandlerConnector, IFileSystemDownloadOptions } from "../Connectors/FileSystemHandlerConnector.js";
import { TerminalConnector } from "../Connectors/TerminalConnector.js";
import { InstallationProgressCode } from "../Enums/InstallationProgressCode.js";
import { InstallationProtocolType } from "../Enums/InstallationProtocolType.js";
import { InstallationProgressEventArgs } from "../Events/Args/InstallationProgressEventArgs.js";
import {
    IInstallationEnvironment, IInstallationHandler,
    IInstallationPackage, IInstallationPackageSource,
    IInstallationRecipeNotifications
} from "../Interfaces/DAO/IBaseInstallationRecipe.js";
import { IInstallationProcessEventsHandler } from "../Interfaces/Events/IInstallationProcessEventsHandler.js";
import { IInstallationRecipeEvents } from "../Interfaces/Events/IInstallationRecipeEvents.js";
import { Loader } from "../Loader.js";
import { BaseDAO } from "./BaseDAO.js";

/**
 * InstallationRecipeDAO class provides logic for creation and management of Installation protocol.
 */
export class InstallationRecipeDAO extends BaseDAO {
    public static defaultConfigurationPath : string =
        "resource/data/Io/Oidis/Services/Configuration/BaseInstallationRecipe.jsonp";
    private enabled : boolean;
    private abortIds : number[];
    private readonly protocolsChain : InstallationProtocolType[];
    private events : IEventsManager;
    private readonly fileSystem : FileSystemHandlerConnector;
    private readonly terminal : TerminalConnector;

    protected static getDaoInterfaceClassName($interfaceName : string) : any {
        switch ($interfaceName) {
        case "IBaseInstallationRecipe":
            return InstallationRecipeDAO;
        default:
            break;
        }
        return BaseDAO;
    }

    constructor() {
        super();

        this.enabled = true;
        this.abortIds = [];
        this.protocolsChain = [];
        this.events = Loader.getInstance().getHttpResolver().getEvents();
        this.fileSystem = this.getFileSystem();
        this.terminal = this.getTerminal();
    }

    /**
     * @returns {IInstallationRecipeEvents} Returns events connected with the installation process.
     */
    public getEvents() : IInstallationRecipeEvents {
        return <IInstallationRecipeEvents>{
            setOnChange  : ($handler : IInstallationProcessEventsHandler) : void => {
                this.events.setEvent(this.getClassName(), EventType.ON_CHANGE, $handler);
            },
            setOnComplete: ($handler : IInstallationProcessEventsHandler) : void => {
                this.events.setEvent(this.getClassName(), EventType.ON_COMPLETE, $handler);
            },
            setOnError   : ($handler : IErrorEventsHandler) : void => {
                this.events.setEvent(this.getClassName(), EventType.ON_ERROR, $handler);
            },
            setOnStart   : ($handler : IEventsHandler) : void => {
                this.events.setEvent(this.getClassName(), EventType.ON_START, $handler);
            }
        };
    }

    public getStaticConfiguration() : IInstallationEnvironment {
        return <IInstallationEnvironment>super.getStaticConfiguration();
    }

    public ConfigurationLibrary($value? : any) : IInstallationEnvironment {
        const library : IInstallationEnvironment = super.ConfigurationLibrary($value);
        library.terminal = this.terminal;
        library.fileSystem = this.fileSystem;
        library.onChange = ($status : boolean, $description : string, $currentValue? : number, $endValue? : number,
                            $code? : InstallationProgressCode) : void => {
            this.fireOnChangeEvent($status, $description, $currentValue, $endValue, $code);
        };
        return library;
    }

    /**
     * @returns {string[]} Returns array of items, which should be processed by installation logic.
     */
    public getChain() : string[] {
        if (!ObjectValidator.IsSet(this.getStaticConfiguration().getChain)) {
            this.getStaticConfiguration().getChain = () : string[] => {
                return [];
            };
        }
        return this.getStaticConfiguration().getChain();
    }

    /**
     * @param {string} $name Specify package, which should be looked up.
     * @returns {IInstallationPackage} Returns detailed information about installation item, which should be prepared for execution.
     */
    public getPackage($name : string) : IInstallationPackage {
        const instance : IInstallationEnvironment = this.getConfigurationInstance();
        const notifications : IInstallationRecipeNotifications = this.getStaticConfiguration().notifications;
        if (instance.packages.hasOwnProperty($name)) {
            const recipe : IInstallationPackage = instance.packages[$name];
            const $this : any = this; // eslint-disable-line @typescript-eslint/no-this-alias
            const override : any = function ($property : any, $callback : IInstallationHandler) : void {
                try {
                    if (ObjectValidator.IsSet($property)) {
                        $property.apply(instance, [$callback]);
                    } else {
                        $callback(true);
                    }
                } catch (ex) {
                    $this.fireOnErrorEvent(ex.stack);
                }
            };
            if (!ObjectValidator.IsSet(recipe.selectSource)) {
                (<any>recipe).selectSource = ($callback : IInstallationHandler) : void => {
                    if (ObjectValidator.IsObject(recipe.source)) {
                        const source : IInstallationPackageSource = <IInstallationPackageSource>instance.block.source;
                        instance.utils.Is64Bit(($value : boolean) : void => {
                            if (ObjectValidator.IsSet(source.options)) {
                                instance.block.source = source.options;
                                if (ObjectValidator.IsString(source.x86) && ObjectValidator.IsString(source.x64)) {
                                    (<IFileSystemDownloadOptions>instance.block.source).url = <string>($value ? source.x64 : source.x86);
                                    $callback(true);
                                } else {
                                    if (ObjectValidator.IsObject(source.x86) && ObjectValidator.IsObject(source.x64)) {
                                        const margeOptions : any =
                                            ($source : IFileSystemDownloadOptions) : IFileSystemDownloadOptions => {
                                                let parameter : any;
                                                for (parameter in <IFileSystemDownloadOptions>instance.block.source) {
                                                    if (!$source.hasOwnProperty(parameter)) {
                                                        $source[parameter] = instance.block.source[parameter];
                                                    }
                                                }
                                                return $source;
                                            };
                                        instance.block.source = $value ? margeOptions(source.x64) : margeOptions(source.x86);
                                        $callback(true);
                                    } else {
                                        $callback(false,
                                            StringUtils.Format(notifications.unsupported_definition_of_package_source_for__0__,
                                                $name));
                                    }
                                }
                            } else {
                                if (ObjectValidator.IsString(source.x86) && ObjectValidator.IsString(source.x64) ||
                                    ObjectValidator.IsObject(source.x86) && ObjectValidator.IsObject(source.x64)) {
                                    instance.block.source = $value ? source.x64 : source.x86;
                                }
                                $callback(true);
                            }
                        });
                    } else {
                        $callback(true);
                    }
                };
            }
            if (!ObjectValidator.IsSet(recipe.getSource)) {
                (<any>recipe).getSource = ($callback : IInstallationHandler) : void => {
                    let source : string;
                    if (ObjectValidator.IsObject(instance.block.source)) {
                        source = StringUtils.Replace((<IFileSystemDownloadOptions>instance.block.source).url, "\\", "/");
                        (<IFileSystemDownloadOptions>instance.block.source).streamOutput = false;
                    } else {
                        source = StringUtils.Replace(<string>instance.block.source, "\\", "/");
                    }
                    if (!ObjectValidator.IsEmptyOrNull(source)) {
                        if (!StringUtils.StartsWith(source, "http://") &&
                            !StringUtils.StartsWith(source, "https://") &&
                            !StringUtils.StartsWith(source, "ftp://")) {
                            if (StringUtils.StartsWith(source, "file://")) {
                                source = StringUtils.Remove(source, "file://");
                            } else if (!StringUtils.StartsWith(source, "/")) {
                                let rootPath : string = Loader.getInstance().getHttpManager().getRequest().getHostUrl();
                                if (StringUtils.Contains(rootPath, ".html")) {
                                    rootPath = StringUtils.Substring(rootPath, 0, StringUtils.IndexOf(rootPath, "/", false));
                                }
                                source = rootPath + "/" + source;
                            }
                            this.fileSystem.Exists(source)
                                .Then(($status : boolean) : void => {
                                    if ($status) {
                                        instance.block.source = source;
                                        $callback(true);
                                    } else {
                                        $callback(false, StringUtils.Format(notifications.package_source__0__for__1__does_not_exist,
                                            source, $name));
                                    }
                                });
                        } else {
                            let sourceName : string = source;
                            let isRepository : boolean = false;
                            if (StringUtils.StartsWith(source, "git+") || StringUtils.EndsWith(source, ".git")) {
                                source = StringUtils.Remove(source, "git+");
                                sourceName = StringUtils.Substring(source,
                                    StringUtils.IndexOf(sourceName, "/", false) + 1, StringUtils.IndexOf(source, ".git", false));
                                isRepository = true;
                            } else {
                                if (StringUtils.Contains(source, "/")) {
                                    if (StringUtils.Contains(source, "?")) {
                                        sourceName = StringUtils.Substring(source, 0, StringUtils.IndexOf(source, "?", false));
                                    }
                                    sourceName = StringUtils.Substring(sourceName, StringUtils.IndexOf(sourceName, "/", false) + 1);
                                }
                                if (!StringUtils.Contains(sourceName, ".")) {
                                    sourceName = instance.block.name + instance.block.version + ".exe";
                                }
                            }
                            const moveSource : any = ($destination : string) : void => {
                                this.fileSystem.Exists($destination).Then(($status : boolean) : void => {
                                    if ($status) {
                                        instance.block.source = $destination;
                                        $callback(true);
                                    } else {
                                        if (isRepository) {
                                            instance.utils.getGitPath(($gitPath : string) : void => {
                                                instance.utils.getVersion($gitPath, ($value : string | boolean,
                                                                                     $error? : string) : void => {
                                                    if (ObjectValidator.IsString($value)) {
                                                        if (!StringUtils.VersionIsLower(
                                                            StringUtils.Remove(<string>$value, "git version ", " "), "2.1.x")) {
                                                            this.fireOnChangeEvent(
                                                                true,
                                                                StringUtils.Format(notifications.get_source__0__, source),
                                                                null, null,
                                                                InstallationProgressCode.DOWNLOAD_START);
                                                            let lastLength : number = 0;
                                                            instance.utils.Clone(source, $destination,
                                                                ($data : string) : void => {
                                                                    if (StringUtils.PatternMatched("*: *% (*/*)*", $data)) {
                                                                        $data = StringUtils.Substring(
                                                                            $data,
                                                                            StringUtils.IndexOf($data, "(", false) + 1,
                                                                            StringUtils.IndexOf($data, ")", false));
                                                                        const progress : string[] = StringUtils.Split($data, "/");
                                                                        const length : number = StringUtils.ToInteger(progress[0]);
                                                                        if (ObjectValidator.IsEmptyOrNull(instance.block.size)) {
                                                                            instance.block.size = StringUtils.ToInteger(progress[1]);
                                                                        }
                                                                        if (lastLength > length) {
                                                                            this.fireOnChangeEvent(
                                                                                true,
                                                                                source,
                                                                                null, null,
                                                                                InstallationProgressCode.EXTRACT_START);
                                                                        }
                                                                        lastLength = length;
                                                                        this.fireOnChangeEvent(
                                                                            true,
                                                                            StringUtils.Remove($data, "\n"),
                                                                            length / StringUtils.ToInteger(progress[1]) *
                                                                            instance.block.size,
                                                                            instance.block.size,
                                                                            InstallationProgressCode.DOWNLOAD_CHANGE);
                                                                    } else {
                                                                        this.fireOnChangeEvent(
                                                                            true,
                                                                            StringUtils.Remove($data, "\n"),
                                                                            null, null,
                                                                            InstallationProgressCode.CLONE);
                                                                    }
                                                                },
                                                                ($status : boolean) : void => {
                                                                    if ($status) {
                                                                        instance.block.source = $destination;
                                                                        $callback(true);
                                                                    } else {
                                                                        $callback(false, StringUtils.Format(
                                                                            notifications.unable_to_clone_package_source_for__0__,
                                                                            $name));
                                                                    }
                                                                });
                                                        } else {
                                                            $callback(false, StringUtils.Format(
                                                                notifications.required_GIT__0__has_not_been_found, ">= 2.8.x"));
                                                        }
                                                    } else {
                                                        $callback(false, $error);
                                                    }
                                                });
                                            });
                                        } else {
                                            this.fileSystem
                                                .Download(<string>instance.block.source)
                                                .OnStart(($id : number) : void => {
                                                    this.abortIds.push($id);
                                                    this.fireOnChangeEvent(
                                                        true,
                                                        StringUtils.Format(notifications.get_source__0__, source),
                                                        null, null,
                                                        InstallationProgressCode.DOWNLOAD_START);
                                                })
                                                .OnChange(($eventArgs : ProgressEventArgs) : void => {
                                                    if (this.enabled) {
                                                        this.fireOnChangeEvent(
                                                            true,
                                                            sourceName,
                                                            $eventArgs.CurrentValue(), $eventArgs.RangeEnd(),
                                                            InstallationProgressCode.DOWNLOAD_CHANGE);
                                                    }
                                                })
                                                .Then(($tmpPath : string) : void => {
                                                    this.fileSystem.Copy($tmpPath, $destination)
                                                        .Then(($status : boolean) : void => {
                                                            if ($status) {
                                                                this.fileSystem.Delete($tmpPath)
                                                                    .Then(($status : boolean) : void => {
                                                                        if ($status) {
                                                                            instance.block.source = $destination;
                                                                            $callback(true);
                                                                        } else {
                                                                            $callback(false,
                                                                                notifications.unable_to_delete_downloaded_package);
                                                                        }
                                                                    });
                                                            } else {
                                                                $callback(false,
                                                                    notifications.unable_to_move_package_source_to_tmp_destination);
                                                            }
                                                        });
                                                });
                                        }
                                    }
                                });
                            };

                            const prepareTempPath : ($done : ($path : string) => void) => void =
                                ($done : ($path : string) => void) : void => {
                                    this.fileSystem.getTempPath().Then(($path : string) : void => {
                                        $done($path + "/" + Loader.getInstance().getEnvironmentArgs().getProjectName() + "-" +
                                            StringUtils.Replace(
                                                Loader.getInstance().getEnvironmentArgs().getProjectVersion(), ".", "-") +
                                            "/resource/data/");
                                    });
                                };

                            if (isRepository) {
                                prepareTempPath(($path : string) : void => {
                                    this.fileSystem.CreateDirectory(<string>$path)
                                        .Then(($status : boolean) : void => {
                                            if ($status) {
                                                moveSource($path + "/" + StringUtils.Remove(sourceName, ".git"));
                                            } else {
                                                $callback(false, notifications.unable_to_create_installation_folder);
                                            }
                                        });
                                });
                            } else {
                                prepareTempPath(($path : string) : void => {
                                    moveSource($path + "/" + ObjectDecoder.Url(sourceName));
                                });
                            }
                        }
                    } else {
                        $callback(false, notifications.undefined_package_source_for__0__);
                    }
                };
            }
            if (!ObjectValidator.IsSet(recipe.validate)) {
                if (!ObjectValidator.IsSet(recipe.installCondition)) {
                    (<any>recipe).validate = ($callback : IInstallationHandler) : void => {
                        $callback(true);
                    };
                } else {
                    (<any>recipe).validate = function ($callback : IInstallationHandler) : void {
                        this.block.installCondition(($status : boolean, $message? : string) : void => {
                            $callback(!$status, $message);
                        });
                    };
                }
            }
            if (!ObjectValidator.IsSet(recipe.install)) {
                (<any>recipe).install = function ($callback : IInstallationHandler) : void {
                    this.utils.RunExecutable(<string>this.block.source, $callback);
                };
            }
            if (!ObjectValidator.IsSet(recipe.repair)) {
                (<any>recipe).repair = function ($callback : IInstallationHandler) : void {
                    this.block.install($callback);
                };
            }
            if (ObjectValidator.IsEmptyOrNull(recipe.info)) {
                recipe.info = "";
            }
            if (!ObjectValidator.IsSet(recipe.force)) {
                recipe.force = false;
            }
            if (!ObjectValidator.IsSet(recipe.optional)) {
                recipe.optional = true;
            }

            instance.block = <IInstallationPackage>{
                name       : $name,
                description: ObjectValidator.IsSet(recipe.description) ? recipe.description : $name,
                info       : recipe.info,
                version    : ObjectValidator.IsSet(recipe.version) ? recipe.version : "",
                source     : recipe.source,
                force      : recipe.force,
                optional   : recipe.optional,
                selectSource($callback : IInstallationHandler) : void {
                    override(recipe.selectSource, $callback);
                },
                getSource($callback : IInstallationHandler) : void {
                    override(recipe.getSource, $callback);
                },
                installCondition($callback : IInstallationHandler) : void {
                    override(recipe.installCondition, $callback);
                },
                install($callback : IInstallationHandler) : void {
                    override(recipe.install, $callback);
                },
                uninstall($callback : IInstallationHandler) : void {
                    if (ObjectValidator.IsSet(recipe.uninstall)) {
                        recipe.uninstall.apply(instance, [$callback]);
                    } else {
                        $callback(false);
                    }
                },
                repair($callback : IInstallationHandler) : void {
                    override(recipe.repair, $callback);
                },
                validate($callback : IInstallationHandler) : void {
                    override(recipe.validate, $callback);
                }
            };

            if (!ObjectValidator.IsEmptyOrNull(recipe.size)) {
                instance.block.size = recipe.size;
            }

            return instance.block;
        }
        return null;
    }

    /**
     * @returns {void}
     */
    public Abort() : void {
        this.enabled = false;
        this.protocolsChain.shift();
        this.abortIds.forEach(($id : number) : void => {
            this.fileSystem.AbortDownload($id);
        });
        this.abortIds = [];
    }

    /**
     * Execute installation chain defined by Installation protocol with desired installation profile.
     * @param {InstallationProtocolType} $protocolType Specify protocol, which should be executed.
     * @returns {void}
     */
    public RunInstallationChain($protocolType : InstallationProtocolType) : void {
        const notifications : IInstallationRecipeNotifications = this.getStaticConfiguration().notifications;
        let startChain : any; // eslint-disable-line prefer-const
        const nextPackage : any = ($index : number, $protocolType : InstallationProtocolType) : void => {
            const chain : string[] = this.getChain();
            const installProtocol : string[] = this.getProtocolChain($protocolType);
            if ($index < chain.length) {
                const configuration : IInstallationPackage = this.getPackage(chain[$index]);
                if (!ObjectValidator.IsEmptyOrNull(configuration)) {
                    const nextStep : any = ($stepIndex : number) : void => {
                        if (this.enabled) {
                            if ($stepIndex < installProtocol.length) {
                                if (ObjectValidator.IsSet(configuration[installProtocol[$stepIndex]])) {
                                    this.fireOnChangeEvent(
                                        true,
                                        installProtocol[$stepIndex],
                                        $stepIndex + 1, installProtocol.length,
                                        InstallationProgressCode.STEP_START);

                                    configuration[installProtocol[$stepIndex]](($status : boolean, $message : string) : void => {
                                        if ($stepIndex === 0) {
                                            this.fireOnChangeEvent(
                                                true,
                                                !ObjectValidator.IsEmptyOrNull(configuration.version) ?
                                                    configuration.description + " (" + configuration.version + ")" :
                                                    configuration.description,
                                                $index + 1, chain.length,
                                                InstallationProgressCode.CHAIN_START);
                                        }
                                        if (ObjectValidator.IsBoolean($status)) {
                                            if ($protocolType === InstallationProtocolType.VALIDATE ||
                                                $protocolType === InstallationProtocolType.INSTALL_STATUS) {
                                                if ($status && $protocolType === InstallationProtocolType.VALIDATE ||
                                                    !$status && $protocolType === InstallationProtocolType.INSTALL_STATUS) {
                                                    this.fireOnChangeEvent(
                                                        true,
                                                        notifications.package_is_installed_correctly,
                                                        $index + 1, chain.length,
                                                        InstallationProgressCode.VALIDATED);
                                                } else {
                                                    this.fireOnChangeEvent(
                                                        false,
                                                        StringUtils.Format(notifications.package_is_missing_or_damaged__0__,
                                                            !ObjectValidator.IsEmptyOrNull($message) ? $message : " "),
                                                        $index + 1, chain.length,
                                                        InstallationProgressCode.VALIDATED);
                                                }
                                                nextStep($stepIndex + 1);
                                            } else {
                                                if (configuration.force || $status ||
                                                    (!$status && $protocolType === InstallationProtocolType.UNINSTALL)) {
                                                    nextStep($stepIndex + 1);
                                                } else {
                                                    if (!ObjectValidator.IsEmptyOrNull($message)) {
                                                        this.fireOnChangeEvent(
                                                            false,
                                                            $message,
                                                            null, null,
                                                            InstallationProgressCode.CHAIN_CHANGE);
                                                    } else if ($stepIndex === 0) {
                                                        if ($protocolType === InstallationProtocolType.UNINSTALL) {
                                                            this.fireOnChangeEvent(
                                                                true,
                                                                StringUtils.Format(notifications.step__0__has_been_skipped,
                                                                    installProtocol[$stepIndex + 1]),
                                                                null, null,
                                                                InstallationProgressCode.SKIPPED);
                                                        } else {
                                                            if ($protocolType === InstallationProtocolType.REPAIR) {
                                                                this.fireOnChangeEvent(
                                                                    true,
                                                                    notifications.package_is_installed_correctly,
                                                                    null, null,
                                                                    InstallationProgressCode.CHAIN_END);
                                                            } else {
                                                                this.fireOnChangeEvent(
                                                                    true,
                                                                    notifications.package_is_already_installed,
                                                                    null, null,
                                                                    InstallationProgressCode.SKIPPED);
                                                            }
                                                        }
                                                    } else {
                                                        this.fireOnChangeEvent(
                                                            true,
                                                            StringUtils.Format(notifications.step__0__has_been_skipped,
                                                                installProtocol[$stepIndex]),
                                                            null, null,
                                                            InstallationProgressCode.SKIPPED);
                                                    }
                                                    nextPackage($index + 1, $protocolType);
                                                }
                                            }
                                        } else {
                                            this.fireOnErrorEvent(StringUtils.Format(
                                                notifications.unsupported_callback_status__0__detected_for__1__at_step__2__,
                                                $status, installProtocol[$stepIndex], $index));
                                        }
                                    });
                                } else {
                                    this.fireOnErrorEvent(StringUtils.Format(
                                        notifications.undefined_installation_protocol__0__, installProtocol[$stepIndex]));
                                    nextStep($stepIndex + 1);
                                }
                            } else {
                                nextPackage($index + 1, $protocolType);
                            }
                        } else {
                            startChain();
                        }
                    };
                    nextStep(0);
                } else {
                    this.fireOnErrorEvent(StringUtils.Format(notifications.undefined_installation_chain__0__, chain[$index]));
                }
            } else {
                const eventArgs : InstallationProgressEventArgs = new InstallationProgressEventArgs();
                eventArgs.Owner(this);
                if ($protocolType === InstallationProtocolType.VALIDATE) {
                    eventArgs.ProgressCode(InstallationProgressCode.VALIDATED);
                }
                this.events.FireEvent(this.getClassName(), EventType.ON_COMPLETE, eventArgs);
            }
        };

        startChain = () : void => {
            const eventArgs : EventArgs = new EventArgs();
            eventArgs.Owner(this);
            this.events.FireEvent(this.getClassName(), EventType.ON_START, eventArgs);
            this.enabled = true;
            if (this.protocolsChain.length > 0) {
                nextPackage(0, this.protocolsChain[0]);
            }
        };

        this.events.setEvent(this.getClassName(), EventType.ON_COMPLETE, () : void => {
            this.protocolsChain.shift();
            startChain();
        });

        this.protocolsChain.push($protocolType);
        if (this.protocolsChain.length === 1) {
            startChain();
        }
    }

    protected getProtocolChain($protocolType : InstallationProtocolType) : string[] {
        switch ($protocolType) {
        case InstallationProtocolType.VALIDATE:
            return [
                InstallationProtocolType.VALIDATE
            ];
        case InstallationProtocolType.INSTALL_STATUS:
            return [
                InstallationProtocolType.INSTALL_STATUS
            ];
        case InstallationProtocolType.INSTALL:
            return [
                InstallationProtocolType.INSTALL_STATUS,
                "selectSource",
                "getSource",
                InstallationProtocolType.INSTALL,
                InstallationProtocolType.VALIDATE
            ];
        case InstallationProtocolType.UNINSTALL:
            return [
                InstallationProtocolType.INSTALL_STATUS,
                InstallationProtocolType.UNINSTALL
            ];
        case InstallationProtocolType.REPAIR:
            return [
                InstallationProtocolType.INSTALL_STATUS,
                "selectSource",
                "getSource",
                InstallationProtocolType.REPAIR,
                InstallationProtocolType.VALIDATE
            ];
        default:
            return [];
        }
    }

    protected getFileSystem() : any {
        const instance : FileSystemHandlerConnector = new FileSystemHandlerConnector(true);
        instance.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            this.fireOnErrorEvent($eventArgs.Message());
        });
        return instance;
    }

    protected getTerminal() : any {
        const instance : TerminalConnector = new TerminalConnector();
        instance.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            this.fireOnErrorEvent($eventArgs.Message());
        });
        return instance;
    }

    private fireOnChangeEvent($status : boolean, $description : string, $currentValue? : number, $endValue? : number,
                              $code? : InstallationProgressCode) : void {
        if (this.enabled) {
            const eventArgs : InstallationProgressEventArgs = new InstallationProgressEventArgs($description);
            eventArgs.Owner(this);
            eventArgs.ProgressCode($code);
            eventArgs.Status($status);
            if (!ObjectValidator.IsEmptyOrNull($currentValue)) {
                eventArgs.CurrentValue($currentValue);
                eventArgs.RangeEnd($endValue);
            } else {
                eventArgs.CurrentValue(-1);
            }
            this.events.FireEvent(this.getClassName(), EventType.ON_CHANGE, eventArgs);
        }
    }

    private fireOnErrorEvent($message : string) : void {
        const eventArgs : ErrorEventArgs = new ErrorEventArgs($message);
        eventArgs.Owner(this);
        this.events.FireEvent(this.getClassName(), EventType.ON_ERROR, eventArgs);
    }
}
