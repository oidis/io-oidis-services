/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonpFileReader } from "@io-oidis-commons/Io/Oidis/Commons/IOApi/Handlers/JsonpFileReader.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";

export class Resources extends JsonpFileReader {

    /**
     * @param {object} $parent Specify data object, which should be extended.
     * @param {object} $child Specify data object, which should be used for parent override and extend.
     * @returns {object} Returns extended data object.
     */
    public static Extend($parent : any, $child : any) : any {
        return JsonUtils.Extend($parent, $child);
    }

    /**
     * @param {object} $source Specify data object, which should be deeply cloned.
     * @returns {object} Returns deep clone of the source data object.
     */
    public static DeepClone($source : any) : any {
        return JsonUtils.DeepClone($source);
    }
}
