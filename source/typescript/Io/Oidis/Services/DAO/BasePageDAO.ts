/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { BasePanelViewerArgs } from "@io-oidis-gui/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";
import { IBasePageLocalization } from "../Interfaces/DAO/IBasePageLocalization.js";
import { BaseDAO } from "./BaseDAO.js";

/**
 * BasePageDAO class provides extension of BaseDAO focused on controllers for pages.
 */
export class BasePageDAO extends BaseDAO {
    public static defaultConfigurationPath : string =
        "resource/data/Io/Oidis/Services/Localization/BasePageLocalization.jsonp";

    protected modelArgs : BasePanelViewerArgs;

    protected static getDaoInterfaceClassName($interfaceName : string) : any {
        switch ($interfaceName) {
        case "IBasePageConfiguration":
            return BasePageDAO;
        default:
            break;
        }
        return BaseDAO;
    }

    /**
     * @returns {IBasePageLocalization} Returns data, which are representing page configuration.
     */
    public getPageConfiguration() : IBasePageLocalization {
        return <IBasePageLocalization>super.getStaticConfiguration();
    }

    /**
     * @returns {string} Returns file path, which should be used for page loading from its cache.
     */
    public getCacheFilePath() : string {
        return this.getPageConfiguration().cacheFile;
    }

    /**
     * @returns {string} Returns value, which should be used as page title.
     */
    public getPageTitle() : string {
        return this.getPageConfiguration().title;
    }

    /**
     * @returns {string} Returns value, which should be used as page loader text.
     */
    public getPageLoadingText() : string {
        return this.getPageConfiguration().loading;
    }

    /**
     * @returns {string} Returns value, which should be used as page loader progress text.
     */
    public getPageLoadingProgressText() : string {
        return this.getPageConfiguration().loadingProgress;
    }

    /**
     * @returns {BasePageViewerArgs} Returns arguments, which can be used as page viewer arguments.
     */
    public getModelArgs() : BasePanelViewerArgs {
        if (!ObjectValidator.IsSet(this.modelArgs)) {
            this.modelArgs = new BasePanelViewerArgs();
        }
        return this.modelArgs;
    }
}
