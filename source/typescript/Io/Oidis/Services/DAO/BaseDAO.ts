/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LanguageType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LanguageType.js";
import { SyntaxConstants } from "@io-oidis-commons/Io/Oidis/Commons/Enums/SyntaxConstants.js";
import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IBaseConfiguration } from "../Interfaces/DAO/IBaseConfiguration.js";
import { Resources } from "./Resources.js";

/**
 * BaseDAO class provides object oriented data parsed from resources stored at external files in JSON format
 * and basic data handling focused on controllers for business logic.
 */
export class BaseDAO extends BaseObject {
    public static defaultConfigurationPath : string =
        "resource/data/Io/Oidis/Services/Configuration/BaseConfiguration.jsonp";
    private static staticConfiguration : ArrayList<ArrayList<IBaseConfiguration>>;
    private static loadedResources : ArrayList<any>;

    private language : string;
    private configurationPath : string;
    private parent : BaseDAO;
    private imports : ArrayList<BaseDAO>;
    private configLibrary : any;
    private configInstance : any;
    private errorHandler : ($eventArgs? : ErrorEvent, $filePath? : string) => void;

    public static UnloadResource($filePath : string) : void {
        BaseDAO.loadedResources.RemoveAt(BaseDAO.loadedResources.IndexOf(BaseDAO.loadedResources.getItem($filePath)));
    }

    protected static getDaoInterfaceClassName($interfaceName : string) : any {
        return BaseDAO;
    }

    private static passGlobalVariables($processor : any, $variables : object) : void {
        let propertyName : string;
        for (propertyName in $variables) {
            if (ObjectValidator.IsSet($processor[propertyName])) {
                $processor[propertyName] = $variables[propertyName];
                if (ObjectValidator.IsSet($processor.$super)) {
                    $processor.$super[propertyName] = $variables[propertyName];
                }
            } else {
                ExceptionsManager.Throw("Configuration Processor",
                    "Unable to pass global variables - identifier \"" + propertyName + "\" has not been detected.");
            }
        }
    }

    constructor() {
        super();
        this.setConfigurationPath(Reflection.getInstance().getClass(this.getClassName()).defaultConfigurationPath);
        this.setDefaultLanguage(LanguageType.EN);
        if (!ObjectValidator.IsSet(BaseDAO.staticConfiguration)) {
            BaseDAO.staticConfiguration = new ArrayList<ArrayList<IBaseConfiguration>>();
        }
        if (!ObjectValidator.IsSet(BaseDAO.loadedResources)) {
            BaseDAO.loadedResources = new ArrayList<any>();
        }
        this.parent = null;
        this.imports = new ArrayList<BaseDAO>();
    }

    /**
     * @returns {BaseDAO} Returns DAO from, which has been current DAO extended.
     */
    public getParentDAO() : BaseDAO {
        return this.parent;
    }

    /**
     * @param {string} $variableName Specify import variable name, which should be looked up
     * @returns {BaseDAO} Returns imported DAO in case of that variable name exist, otherwise null.
     */
    public getImportedDAO($variableName : string) : BaseDAO {
        if (this.imports.KeyExists($variableName)) {
            return this.imports.getItem($variableName);
        }
        return null;
    }

    /**
     * @returns {string} Returns data source name for current DAO instance
     */
    public getDaoDataSource() : string {
        return this.configurationPath;
    }

    /**
     * @param {Language} $language Specify language for, which should be data loaded.
     * @param {Function} $asyncHandler Specify asynchronous handler which should be executed when DAO is loaded.
     * @param {boolean} [$force=false] Force load DAO data all its dependencies.
     * @returns {void}
     */
    public Load($language : LanguageType, $asyncHandler : () => void, $force : boolean = false) : void {
        if ($force) {
            const propertyName : string = "configInstance";
            delete this[propertyName];
        }
        const requiredLanguage : string = $language.toString();
        const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
        const resourcesClass : any = this.getResourcesHandler();
        if (!BaseDAO.staticConfiguration.KeyExists(this.configurationPath) || $force) {
            BaseDAO.staticConfiguration.Add(new ArrayList<IBaseConfiguration>(), this.configurationPath);
        }
        const thisConfiguration : ArrayList<IBaseConfiguration> = BaseDAO.staticConfiguration.getItem(this.configurationPath);
        const fireLoaded : any = () : void => {
            const loadImports : any = () : void => {
                const imports : any = this.getStaticConfiguration().imports;
                if (!ObjectValidator.IsEmptyOrNull(imports)) {
                    const importsVariables : string[] = [];
                    let index : number = 0;
                    let varName : string;
                    for (varName in imports) {
                        if (!ObjectValidator.IsEmptyOrNull(varName) && !ObjectValidator.IsEmptyOrNull(imports[varName])) {
                            importsVariables[index] = varName;
                            index++;
                        }
                    }
                    if (index > 0) {
                        index = 0;
                        const loadImport : any = () : void => {
                            const importDAO : BaseDAO = new (thisClass.getDaoInterfaceClassName(null))();
                            importDAO.setErrorHandler(this.errorHandler);
                            importDAO.setConfigurationPath(imports[importsVariables[index]]);
                            importDAO.Load($language, () : void => {
                                const interfaceDaoClass : any =
                                    thisClass.getDaoInterfaceClassName(importDAO.getConfigurationInterface());
                                let interfaceDAO : BaseDAO = importDAO;
                                if (!importDAO.IsTypeOf(interfaceDaoClass)) {
                                    interfaceDAO = new interfaceDaoClass();
                                    let propertyName : string;
                                    for (propertyName in importDAO) {
                                        if (interfaceDAO.hasOwnProperty(propertyName)) {
                                            interfaceDAO[propertyName] = importDAO[propertyName];
                                        }
                                    }
                                }

                                this.imports.Add(interfaceDAO, importsVariables[index]);
                                index++;
                                if (index < importsVariables.length) {
                                    loadImport();
                                } else {
                                    $asyncHandler();
                                }
                            }, $force);
                        };
                        loadImport();
                    } else {
                        $asyncHandler();
                    }
                } else {
                    $asyncHandler();
                }
            };
            const extendsConfig : string = this.getStaticConfiguration().extendsConfig;
            if (!ObjectValidator.IsEmptyOrNull(extendsConfig)) {
                const parent : BaseDAO = new (thisClass.getDaoInterfaceClassName(null))();
                parent.setErrorHandler(this.errorHandler);
                parent.setConfigurationPath(extendsConfig);
                parent.Load($language, () : void => {
                    const interfaceDaoClass : any = thisClass.getDaoInterfaceClassName(parent.getConfigurationInterface());
                    let interfaceDAO : BaseDAO = parent;
                    if (!parent.IsTypeOf(interfaceDaoClass)) {
                        interfaceDAO = new interfaceDaoClass();
                        let propertyName : string;
                        for (propertyName in parent) {
                            if (interfaceDAO.hasOwnProperty(propertyName)) {
                                interfaceDAO[propertyName] = parent[propertyName];
                            }
                        }
                    }
                    this.parent = interfaceDAO;
                    loadImports();
                }, $force);
            } else {
                loadImports();
            }
        };
        if (!thisConfiguration.KeyExists(requiredLanguage) ||
            (!ObjectValidator.IsEmptyOrNull(thisConfiguration.getItem(requiredLanguage)) &&
                thisConfiguration.getItem(requiredLanguage).hasOwnProperty("noCache"))) {
            const loadedResources : string[] = [];
            let localization : IBaseConfiguration;
            const loadParents : any = ($filePath : string, $localizationLoad : boolean, $onLoadHandler : () => void) : void => {
                if (!ObjectValidator.IsEmptyOrNull($filePath) && loadedResources.indexOf($filePath) === -1) {
                    const processData : any = ($data : any, $filePath : string) : void => {
                        loadedResources.push($filePath);
                        if (!ObjectValidator.IsEmptyOrNull($data)) {
                            const parentPath : string = (<IBaseConfiguration>$data).extendsConfig;
                            if (!$localizationLoad) {
                                thisConfiguration.Add(
                                    resourcesClass.Extend($data, thisConfiguration.getItem(this.language)), this.language);
                            } else {
                                localization = resourcesClass.Extend($data, localization);
                            }
                            loadParents(parentPath, $localizationLoad, $onLoadHandler);
                        } else {
                            $onLoadHandler();
                        }
                    };
                    const loadResource : any = ($path : string) : void => {
                        resourcesClass.Load($path,
                            ($data : any, $filePath? : string) : void => {
                                if (!ObjectValidator.IsEmptyOrNull($data)) {
                                    BaseDAO.loadedResources.Add(resourcesClass.DeepClone($data), $filePath);
                                }
                                processData($data, $filePath);
                            },
                            this.errorHandler);
                    };
                    if (BaseDAO.loadedResources.KeyExists($filePath) && !$force) {
                        const item : any = BaseDAO.loadedResources.getItem($filePath);
                        if (item.hasOwnProperty("noCache") && item.noCache) {
                            loadResource($filePath);
                        } else {
                            processData(resourcesClass.DeepClone(item), $filePath);
                        }
                    } else {
                        loadResource($filePath);
                    }
                } else {
                    $onLoadHandler();
                }
            };
            const onLoadHandler : () => void = () : void => {
                if (ObjectValidator.IsSet(localization)) {
                    thisConfiguration.Add(
                        resourcesClass.Extend(
                            resourcesClass.DeepClone(thisConfiguration.getItem(this.language)), localization), requiredLanguage);
                    this.language = requiredLanguage;
                }
                fireLoaded();
            };
            const loadLocalization : any = () : void => {
                if (!ObjectValidator.IsEmptyOrNull(requiredLanguage) && this.language !== requiredLanguage &&
                    ObjectValidator.IsSet(thisConfiguration.getItem(this.language).localization) &&
                    thisConfiguration.getItem(this.language).localization.hasOwnProperty(StringUtils.ToLowerCase(requiredLanguage))) {
                    const processData : any = ($data : any, $filePath : string) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($data) && loadedResources.indexOf($filePath) === -1) {
                            loadedResources.push($filePath);
                            localization = $data;
                            loadParents((<IBaseConfiguration>$data).extendsConfig, true, onLoadHandler);
                        } else {
                            onLoadHandler();
                        }
                    };
                    const loadResource : any = ($path : string) : void => {
                        resourcesClass.Load($path,
                            ($data : any, $filePath? : string) : void => {
                                if (!ObjectValidator.IsEmptyOrNull($data)) {
                                    BaseDAO.loadedResources.Add(resourcesClass.DeepClone($data), $filePath);
                                }
                                processData($data, $filePath);
                            },
                            onLoadHandler);
                    };
                    const localizationFilePath : string =
                        thisConfiguration.getItem(this.language).localization[StringUtils.ToLowerCase(requiredLanguage)];

                    if (BaseDAO.loadedResources.KeyExists(localizationFilePath) && !$force) {
                        const item : any = BaseDAO.loadedResources.getItem(localizationFilePath);
                        if (item.hasOwnProperty("noCache") && item.noCache) {
                            processData(resourcesClass.DeepClone(item), localizationFilePath);
                        }
                    } else {
                        loadResource(localizationFilePath);
                    }
                } else {
                    fireLoaded();
                }
            };
            if (!thisConfiguration.KeyExists(this.language) ||
                (!ObjectValidator.IsEmptyOrNull(thisConfiguration.getItem(requiredLanguage)) &&
                    thisConfiguration.getItem(requiredLanguage).hasOwnProperty("noCache"))) {
                const processData : any = ($data : any, $filePath : string) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($data) && loadedResources.indexOf($filePath) === -1) {
                        loadedResources.push($filePath);
                        thisConfiguration.Add($data, this.language);
                        loadParents((<IBaseConfiguration>$data).extendsConfig, false, loadLocalization);
                    }
                };
                const loadResource : any = ($path : string) : void => {
                    resourcesClass.Load($path,
                        ($data : any, $filePath? : string) : void => {
                            if (!ObjectValidator.IsEmptyOrNull($data)) {
                                BaseDAO.loadedResources.Add(resourcesClass.DeepClone($data), $filePath);
                            }
                            processData($data, $filePath);
                        },
                        this.errorHandler);
                };
                if (BaseDAO.loadedResources.KeyExists(this.configurationPath) && !$force) {
                    const item : any = BaseDAO.loadedResources.getItem(this.configurationPath);
                    if (item.hasOwnProperty("noCache") && item.noCache) {
                        loadResource(this.configurationPath);
                    } else {
                        processData(resourcesClass.DeepClone(item), this.configurationPath);
                    }
                } else {
                    loadResource(this.configurationPath);
                }
            } else {
                loadLocalization();
            }
        } else {
            if (!ObjectValidator.IsEmptyOrNull(requiredLanguage) && this.language !== requiredLanguage) {
                this.language = requiredLanguage;
            }
            fireLoaded();
        }
    }

    /**
     * @returns {IBaseConfiguration} Returns data, which are representing DAO static configuration.
     */
    public getStaticConfiguration() : IBaseConfiguration {
        if (!BaseDAO.staticConfiguration.KeyExists(this.configurationPath)) {
            BaseDAO.staticConfiguration.Add(new ArrayList<IBaseConfiguration>(), this.configurationPath);
        }
        return BaseDAO.staticConfiguration.getItem(this.configurationPath).getItem(this.language);
    }

    /**
     * @returns {object} Returns list of global variables, which have been parsed from page configuration.
     */
    public getGlobalVariables() : object {
        return {};
    }

    /**
     * @param {object} $variables Specify variables, which should be passed to page configuration to the global variables identifiers.
     * @returns {void}
     */
    public setGlobalVariables($variables : object) : void {
        if (!ObjectValidator.IsSet(this.configInstance)) {
            this.getConfigurationInstance();
        }
        BaseDAO.passGlobalVariables(this.configInstance, $variables);
    }

    /**
     * @returns {string} Returns value, which represents current version of the configuration.
     */
    public getConfigurationInterface() : string {
        return ObjectValidator.IsSet(this.getStaticConfiguration().$interface) ?
            this.getStaticConfiguration().$interface : (<any>this.getStaticConfiguration()).interface;
    }

    /**
     * @returns {string} Returns value, which represents current version of the configuration.
     */
    public getConfigurationVersion() : string {
        return this.getStaticConfiguration().version;
    }

    /**
     * @param {string} $value Specify file path, which should be used as DAO data source.
     * @returns {void}
     */
    public setConfigurationPath($value : string) : void {
        this.configurationPath = Property.String(this.configurationPath, $value);
    }

    /**
     * @param {object} [$value] Specify library object, which should be appended to the configuration instance.
     * @returns {object} Returns library object, which should be appended to the configuration instance.
     */
    public ConfigurationLibrary($value? : any) : any {
        if (ObjectValidator.IsSet($value)) {
            this.configLibrary = $value;
        }
        if (!ObjectValidator.IsSet(this.configLibrary)) {
            const library : any = {};

            library.Echo = (...$attributes : any[]) : void => {
                Echo.Printf.apply(Echo, $attributes); // eslint-disable-line prefer-spread
            };
            library.Log = (...$attributes : any[]) : void => {
                LogIt.Debug.apply(LogIt, $attributes); // eslint-disable-line prefer-spread
            };
            library.Error = (...$attributes : any[]) : void => {
                LogIt.Error(StringUtils.Format.apply(String, $attributes));
            };
            library.Warning = (...$attributes : any[]) : void => {
                LogIt.Warning(StringUtils.Format.apply(String, $attributes));
            };
            this.configLibrary = library;
        }
        return this.configLibrary;
    }

    /**
     * @returns {object} Returns page configuration as class instance.
     */
    public getConfigurationInstance() : any {
        if (!ObjectValidator.IsSet(this.configInstance)) {
            const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
            const getConfigClass : any = ($dao : BaseDAO, $extend : any) : any => {
                const config : IBaseConfiguration = $dao.getStaticConfiguration();
                let dao : any = $dao;
                const interfaceDaoClass : any = thisClass.getDaoInterfaceClassName(
                    ObjectValidator.IsSet(config.$interface) ? config.$interface : (<any>config).interface);
                let propertyName : string;
                if (!dao.IsTypeOf(interfaceDaoClass)) {
                    dao = new interfaceDaoClass();
                    for (propertyName in $dao) {
                        if (dao.hasOwnProperty(propertyName)) {
                            dao[propertyName] = $dao[propertyName];
                        }
                    }
                    $dao = dao;
                }

                const processor : any = () : void => {
                    // create class instance with empty body
                };

                for (propertyName in $extend) {
                    if (!processor.prototype.hasOwnProperty(propertyName)) {
                        processor.prototype[propertyName] = $extend[propertyName];
                    }
                }

                const superDao : BaseDAO = $dao.parent;
                if (!ObjectValidator.IsEmptyOrNull(superDao)) {
                    const superClass : any = getConfigClass(superDao, $extend);
                    processor.prototype.$super = new superClass();
                    processor.prototype._super = processor.prototype.$super;
                }

                $dao.imports.foreach(
                    ($importDao : BaseDAO, $variableName? : string) : void => {
                        if (!processor.prototype.hasOwnProperty($variableName)) {
                            const importClass : any = getConfigClass($importDao, $extend);
                            processor.prototype[$variableName] = new importClass();
                        } else {
                            ExceptionsManager.Throw("Configuration Processor",
                                "Import identifier \"" + $variableName + "\" already in use.");
                        }
                    });

                for (propertyName in config) {
                    if (propertyName !== SyntaxConstants.CONSTRUCTOR) {
                        if (!processor.prototype.hasOwnProperty(propertyName)) {
                            processor.prototype[propertyName] = config[propertyName];
                        } else {
                            ExceptionsManager.Throw("Configuration Processor",
                                "Reserved identifier \"" + propertyName + "\" has been detected.");
                        }
                    }
                }
                BaseDAO.passGlobalVariables(processor.prototype, $dao.getGlobalVariables());

                return processor;
            };

            const configClass : any = getConfigClass(this, this.ConfigurationLibrary());
            this.configInstance = new configClass();
        }
        BaseDAO.passGlobalVariables(this.configInstance, this.getGlobalVariables());

        return this.configInstance;
    }

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        let source : string = "/" + this.getDaoDataSource();
        if ($htmlTag) {
            source = "<b>" + source + "</b>";
        }
        return source + StringUtils.NewLine($htmlTag) +
            $prefix + JSON.stringify(this.getStaticConfiguration());
    }

    public setErrorHandler($handler : ($eventArgs? : ErrorEvent, $filePath? : string) => void) : void {
        this.errorHandler = $handler;
    }

    protected getResourcesHandler() : any {
        return Resources;
    }

    protected setDefaultLanguage($value : LanguageType) : void {
        if (LanguageType.Contains($value)) {
            this.language = $value.toString();
        }
    }
}
