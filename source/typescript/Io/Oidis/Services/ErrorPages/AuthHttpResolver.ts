/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { PersistenceType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/PersistenceType.js";
import { BaseErrorPage } from "@io-oidis-gui/Io/Oidis/Gui/ErrorPages/BaseErrorPage.js";
import { PersistenceFactory } from "@io-oidis-gui/Io/Oidis/Gui/PersistenceFactory.js";
import { IWebServiceClient } from "../Interfaces/IWebServiceClient.js";
import { IWebServiceProtocol } from "../Interfaces/IWebServiceProtocol.js";

export class AuthHttpResolver extends BaseErrorPage {
    private tokenExpiration : string;

    public static LogOut() : void {
        PersistenceFactory.getPersistence(PersistenceType.CLIENT_IP, AuthHttpResolver.ClassName()).Destroy("token");
    }

    public static getToken() : string {
        const token : string = PersistenceFactory.getPersistence(PersistenceType.CLIENT_IP, AuthHttpResolver.ClassName())
            .Variable("token");
        return ObjectValidator.IsEmptyOrNull(token) ? "" : token;
    }

    public static getTokenName() : string {
        return AuthHttpResolver.ClassName() + ".CurrentToken";
    }

    protected getFaviconSource() : string {
        return "resource/graphics/Io/Oidis/Gui/ForbiddenIcon.ico";
    }

    protected getPageTitle() : string {
        return "HTTP 401";
    }

    protected getMessageHeader() : string {
        return "HTTP status 401";
    }

    protected getMessageBody() : string {
        return "Not authorized.";
    }

    protected setExpirationTime($value : string) : void {
        this.tokenExpiration = Property.String(this.tokenExpiration, $value);
    }

    protected reinvokeRequest($token : string) : void {
        if (this.RequestArgs().POST().KeyExists("Protocol")) {
            const protocol : IWebServiceProtocol = this.RequestArgs().POST().getItem("Protocol");
            const client : IWebServiceClient = this.RequestArgs().POST().getItem("Client");
            if (!ObjectValidator.IsEmptyOrNull(client) && !ObjectValidator.IsEmptyOrNull(protocol)) {
                protocol.origin = this.getRequest().getBaseUrl();
                protocol.status = HttpStatusType.CONTINUE;
                protocol.token = $token;
                if (ObjectValidator.IsObject(protocol.data)) {
                    protocol.data = ObjectEncoder.Base64(JSON.stringify(protocol.data));
                } else {
                    protocol.data = ObjectEncoder.Base64(ObjectDecoder.Base64(ObjectDecoder.Base64(<string>protocol.data)));
                }

                const persistence : IPersistenceHandler =
                    PersistenceFactory.getPersistence(PersistenceType.CLIENT_IP, this.getClassName());
                persistence.ExpireTime(this.tokenExpiration);
                persistence.Variable("token", $token);
                client.Send(<any>JSON.stringify(protocol));
            }
        }
    }
}
