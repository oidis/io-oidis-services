/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseErrorPage } from "@io-oidis-gui/Io/Oidis/Gui/ErrorPages/BaseErrorPage.js";
import { IWebServiceClient } from "../Interfaces/IWebServiceClient.js";

export class ConnectionLostPage extends BaseErrorPage {

    protected getPageTitle() : string {
        return "Connection Lost";
    }

    protected getMessageHeader() : string {
        return "Service is unreachable";
    }

    protected getMessageBody() : string {
        return "Service request done by: " + this.RequestArgs().POST().getItem("Owner") + ", " +
            "to: " + this.RequestArgs().POST().getItem("Client").getServerUrl();
    }

    protected getOwner() : string {
        return this.RequestArgs().POST().getItem("Owner");
    }

    protected restart() : void {
        const client : IWebServiceClient = this.RequestArgs().POST().getItem("Client");
        client.StopCommunication();
        client.StartCommunication();
    }
}
