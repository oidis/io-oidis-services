/* ********************************************************************************************************* *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
var ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
var StringUtils = Io.Oidis.Commons.Utils.StringUtils;

Io.Oidis.Services.DAO.Resources.Data({
    version: "2019.0.1",
    $interface: "IBaseInstallationRecipe",
    imports: {
        utils: "<? @var project.target.hubLocation ?>/Configuration/com-wui-framework-services/InstallationUtils"
    },
    block: {/*object, which reflects currently running installation recipe*/},
    notifications: {
        package_is_installed_correctly: "Package is installed correctly.",
        package_is_missing_or_damaged__0__: "Package is missing or damaged. {0}",
        package_is_already_installed: "Package is already installed.",
        step__0__has_been_skipped: "Step \"{0}\" has been skipped.",
        unsupported_callback_status__0__detected_for__1__at_step__2__: "Unsupported callback status \"{0}\" detected for \"{1}\" at step {2}",
        undefined_installation_protocol__0__: "Undefined installation protocol \"{0}\"",
        undefined_installation_chain__0__: "Undefined installation chain \"{0}\"",
        unsupported_definition_of_package_source_for__0__: "Unsupported definition of package source for \"{0}\".",
        package_source__0__for__1__does_not_exist: "Package source \"{0}\" for \"{1}\" does not exist.",
        unable_to_clone_package_source_for__0__: "Unable to clone package source for \"{0}\".",
        required_GIT__0__has_not_been_found: "Required GIT {0} has not been found.",
        get_source__0__: "Get source: {0}",
        unable_to_delete_downloaded_package: "Unable to delete downloaded package.",
        unable_to_move_package_source_to_tmp_destination: "Unable to move package source to tmp destination.",
        unable_to_create_installation_folder: "Unable to create installation folder.",
        undefined_package_source_for__0__: "Undefined package source for \"{0}\""
    },
    packages: {
        Netfx4Full: {
            description: "Microsoft .NET Framework",
            source: "https://download.microsoft.com/download/1/4/A/14A6C422-0D3C-4811-A31F-5EF91A83C368/NDP46-KB3045560-Web.exe",
            installCondition: function ($resultIs) {
                this.utils.RegisterValueExists("HKLM/SOFTWARE/Microsoft/Net Framework Setup/NDP/v4/Full", "Version", function ($value) {
                    $resultIs(!$value);
                });
            }
        },
        Java: {
            description: "Java",
            version: "1.8.0",
            source: "http://javadl.oracle.com/webapps/download/AutoDL?BundleId=216431",
            installCondition: function ($resultIs) {
                var $this = this;
                this.terminal.Execute("java", ["-version"]).Then(function ($exitCode, $stdOut, $stdErr) {
                    if (!ObjectValidator.IsEmptyOrNull($exitCode)) {
                        if ($exitCode === 0) {
                            $resultIs(StringUtils.VersionIsLower(
                                StringUtils.Split(StringUtils.Remove($stdErr, "\r\n", "java version ", "\""), "Java(TM) ")[0],
                                $this.block.version));
                        } else {
                            $resultIs(true);
                        }
                    }
                });
            }
        },
        Python: {
            description: "Python",
            info: "Python is a programming language that lets you work quickly and integrate systems more effectively",
            version: "2.7.13",
            size: 54.6 * 1024 * 1024,
            source: "https://www.python.org/ftp/python/2.7.13/python-2.7.13.msi",
            installCondition: function ($resultIs) {
                this.utils.VersionIsIncluded("python", "Python ", this.block.version, $resultIs);
            }
        },
        Ruby: {
            description: "Ruby",
            version: "2.3.1",
            source: {
                x86: "http://dl.bintray.com/oneclick/rubyinstaller/rubyinstaller-2.3.1.exe",
                x64: "http://dl.bintray.com/oneclick/rubyinstaller/rubyinstaller-2.3.1-x64.exe"
            },
            installCondition: function ($resultIs) {
                var $this = this;
                this.utils.getVersion("ruby", function ($value) {
                    $resultIs(
                        ObjectValidator.IsEmptyOrNull($value) ||
                        StringUtils.VersionIsLower(StringUtils.Split($value, " ")[1], $this.block.version));
                });
            }
        },
        VisualStudio2015: {
            description: "Microsoft Visual Studio 2015 Express",
            info: "Integrated development environment (IDE) from Microsoft",
            size: 5.56 * 1024 * 1024 * 1024,
            source: "https://download.microsoft.com/download/e/9/9/e99b1d42-6416-4f74-b905-9fecd799a2d7/wdexpress_full.exe",
            installCondition: function ($resultIs) {
                this.utils.AnyOfRegisterValueExists([
                    ["HKLM/SOFTWARE/Microsoft/DevDiv/vs/Servicing/14.0", "SP"],
                    ["HKLM/SOFTWARE/Wow6432Node/Microsoft/DevDiv/vs/Servicing/14.0", "SP"]
                ], function ($value) {
                    $resultIs(!$value);
                });
            }
        },
        VisualCppBuildTools: {
            description: "Microsoft Visual C++ Build Tools",
            size: 8 * 1024 * 1024 * 1024,
            source: "http://download.microsoft.com/download/5/f/7/5f7acaeb-8363-451f-9425-68a90f98b238/visualcppbuildtools_full.exe",
            installCondition: function ($resultIs) {
                this.utils.AnyOfRegisterValueExists([
                    ["HKLM/SOFTWARE/Microsoft/DevDiv/vs/Servicing/12.0", "SP"],
                    ["HKLM/SOFTWARE/Wow6432Node/Microsoft/DevDiv/vs/Servicing/12.0", "SP"],
                    ["HKLM/SOFTWARE/Microsoft/DevDiv/vs/Servicing/14.0", "SP"],
                    ["HKLM/SOFTWARE/Wow6432Node/Microsoft/DevDiv/vs/Servicing/14.0", "SP"],
                    ["HKLM/SOFTWARE/Microsoft/DevDiv/VisualCppBuildTools", ""],
                    ["HKLM/SOFTWARE/Wow6432Node/Microsoft/DevDiv/VisualCppBuildTools", ""]
                ], function ($value) {
                    $resultIs(!$value);
                });
            }
        },
        VCRedist2005: {
            description: "Microsoft Visual C++ 2008 Redistributable",
            source: {
                x86: "https://download.microsoft.com/download/e/1/c/e1c773de-73ba-494a-a5ba-f24906ecf088/vcredist_x86.exe",
                x64: "https://download.microsoft.com/download/d/4/1/d41aca8a-faa5-49a7-a5f2-ea0aa4587da0/vcredist_x64.exe"
            },
            installCondition: function ($resultIs) {
                this.utils.AnyOfRegisterValueExists([
                    ["HKLM/SOFTWARE/Microsoft/DevDiv/vc/Servicing/8.0", "SP"],
                    ["HKLM/SOFTWARE/Wow6432Node/Microsoft/DevDiv/vc/Servicing/8.0", "SP"]
                ], function ($value) {
                    $resultIs(!$value);
                });
            }
        },
        VCRedist2008: {
            description: "Microsoft Visual C++ 2008 Redistributable",
            source: {
                x86: "https://download.microsoft.com/download/d/d/9/dd9a82d0-52ef-40db-8dab-795376989c03/vcredist_x86.exe",
                x64: "https://download.microsoft.com/download/2/d/6/2d61c766-107b-409d-8fba-c39e61ca08e8/vcredist_x64.exe"
            },
            installCondition: function ($resultIs) {
                this.utils.AnyOfRegisterValueExists([
                    ["HKLM/SOFTWARE/Microsoft/DevDiv/vc/Servicing/9.0", "SP"],
                    ["HKLM/SOFTWARE/Wow6432Node/Microsoft/DevDiv/vc/Servicing/9.0", "SP"]
                ], function ($value) {
                    $resultIs(!$value);
                });
            }
        },
        VCRedist2012: {
            description: "Microsoft Visual C++ 2012 Redistributable",
            source: {
                x86: "https://download.microsoft.com/download/1/6/B/16B06F60-3B20-4FF2-B699-5E9B7962F9AE/VSU_4/vcredist_x86.exe",
                x64: "https://download.microsoft.com/download/1/6/B/16B06F60-3B20-4FF2-B699-5E9B7962F9AE/VSU_4/vcredist_x64.exe"
            },
            installCondition: function ($resultIs) {
                this.utils.AnyOfRegisterValueExists([
                    ["HKLM/SOFTWARE/Microsoft/DevDiv/vc/Servicing/11.0", "SP"],
                    ["HKLM/SOFTWARE/Wow6432Node/Microsoft/DevDiv/vc/Servicing/11.0", "SP"]
                ], function ($value) {
                    $resultIs(!$value);
                });
            }
        },
        VCRedist2015: {
            description: "Microsoft Visual C++ 2015 Redistributable",
            source: {
                x86: "https://download.microsoft.com/download/9/3/F/93FCF1E7-E6A4-478B-96E7-D4B285925B00/vc_redist.x86.exe",
                x64: "https://download.microsoft.com/download/9/3/F/93FCF1E7-E6A4-478B-96E7-D4B285925B00/vc_redist.x64.exe"
            },
            installCondition: function ($resultIs) {
                this.utils.AnyOfRegisterValueExists([
                    ["HKLM/SOFTWARE/Microsoft/DevDiv/vc/Servicing/14.0", "SP"],
                    ["HKLM/SOFTWARE/Wow6432Node/Microsoft/DevDiv/vc/Servicing/14.0", "SP"]
                ], function ($value) {
                    $resultIs(!$value);
                });
            }
        },
        Git: {
            description: "Git",
            info: "Free and open source distributed version control system.",
            version: "2.12.2",
            size: 212 * 1024 * 1024,
            source: {
                x86: "https://github.com/git-for-windows/git/releases/download/v2.12.2.windows.1/Git-2.12.2-32-bit.exe",
                x64: "https://github.com/git-for-windows/git/releases/download/v2.12.2.windows.1/Git-2.12.2-64-bit.exe"
            },
            installCondition: function ($resultIs) {
                this.utils.VersionIsIncluded("git", "git version ", this.block.version, $resultIs);
            }
        },
        PortableGit: {
            description: "Git (Portable distribution)",
            info: "Free and open source distributed version control system.",
            version: "2.14.1",
            size: 242 * 1024 * 1024,
            source: {
                x86: "https://github.com/git-for-windows/git/releases/download/v2.14.1.windows.1/PortableGit-2.14.1-32-bit.7z.exe",
                x64: "https://github.com/git-for-windows/git/releases/download/v2.14.1.windows.1/PortableGit-2.14.1-64-bit.7z.exe"
            },
            installCondition: function ($resultIs) {
                var $this = this;
                this.utils.getGitPath(function ($value) {
                    $this.fileSystem.Exists($value).Then(function ($success) {
                        if ($success) {
                            $this.utils.VersionIsIncluded($value, "git version ", $this.block.version, $resultIs);
                        } else {
                            $resultIs(true);
                        }
                    });
                });
            },
            install: function ($resultIs) {
                var $this = this;
                this.utils.getGitPath(function ($value) {
                    $this.fileSystem.Exists($value).Then(function ($success) {
                        if (!$success) {
                            $this.terminal.Execute($this.block.source, ["-y", "-gm2"])
                                .Then(function ($exitCode, $stdout, $stderr) {
                                    if ($exitCode === 0) {
                                        var destination = StringUtils.Remove($value, "/bin/git.exe", "/cmd/git.exe");
                                        var path = StringUtils.Substring($this.block.source, 0,
                                            StringUtils.IndexOf($this.block.source, "/", false)) + "/PortableGit";
                                        path = StringUtils.Replace(path, "//", "/");

                                        var move = function () {
                                            $this.fileSystem.Copy(path, destination).Then(function ($copied) {
                                                if ($copied) {
                                                    $this.fileSystem.Delete(path).Then(function () {
                                                        $this.fileSystem.Exists(destination).Then(function ($status) {
                                                            if ($status) {
                                                                $resultIs(true);
                                                            } else {
                                                                $resultIs(false);
                                                            }
                                                        });
                                                    });
                                                } else {
                                                    $resultIs(false);
                                                }
                                            });
                                        };

                                        $this.fileSystem.Exists(destination).Then(function ($status) {
                                            if (!$status) {
                                                $this.fileSystem.CreateDirectory(destination).Then(function ($status) {
                                                    if ($status) {
                                                        move();
                                                    } else {
                                                        $resultIs(false);
                                                    }
                                                });
                                            } else {
                                                move();
                                            }
                                        });
                                    } else if (!ObjectValidator.IsEmptyOrNull($exitCode)) {
                                        $resultIs(false, $stderr);
                                    }
                                });
                        } else {
                            $resultIs(false, "Error occurred while installing portable git.");
                        }
                    });
                });
            }
        },
        Cmake: {
            description: "Cmake",
            info: "CMake is an open-source, cross-platform family of tools designed to build, test and package software.",
            version: "3.8.0",
            size: 74.1 * 1024 * 1024,
            source: {
                x86: "https://cmake.org/files/v3.8/cmake-3.8.0-win32-x86.msi",
                x64: "https://cmake.org/files/v3.8/cmake-3.8.0-win64-x64.msi"
            },
            installCondition: function ($resultIs) {
                this.utils.VersionIsIncluded("cmake", "cmake version  ", this.block.version, $resultIs);
            }
        },
        PortableCMake: {
            description: "CMake (Portable distribution)",
            info: "CMake is an open-source, cross-platform family of tools designed to build, test and package software.",
            version: "3.8.2",
            size: 67.4 * 1024 * 1024,
            source: {
                x64: "https://cmake.org/files/v3.8/cmake-3.8.2-win64-x64.zip",
                x86: "https://cmake.org/files/v3.8/cmake-3.8.2-win32-x86.zip"
            },
            installCondition: function ($resultIs) {
                var $this = this;
                this.utils.getCmakePath(function ($cmakePath) {
                    $this.fileSystem.Exists($cmakePath + "/bin/cmake.exe")
                        .Then(function ($success) {
                            if ($success) {
                                $resultIs(false);
                            } else {
                                $resultIs(true);
                            }
                        });
                });
            },
            install: function ($resultIs) {
                var $this = this;
                this.fileSystem.Exists($this.block.source)
                    .Then(function ($success) {
                        if ($success) {
                            $this.utils.getCmakePath(function ($cmakePath) {
                                $this.fileSystem.CreateDirectory($cmakePath).Then(function ($success) {
                                    if ($success) {
                                        $this.onChange(true, "Unpacking downloaded archive ...");
                                        $this.fileSystem.Unpack($this.block.source).Then(function ($path) {
                                            $this.fileSystem.Exists($path).Then(function ($success) {
                                                if ($success) {
                                                    $this.onChange(true, "Preparing installation folder ...");
                                                    $this.fileSystem.Copy($path, $cmakePath).Then(function ($success) {
                                                        $this.fileSystem.Delete($path).Then(function () {
                                                            $resultIs($success);
                                                        });
                                                    });
                                                } else {
                                                    $resultIs(false);
                                                }
                                            });
                                        });
                                    } else {
                                        $resultIs(false);
                                    }
                                });
                            });
                        } else {
                            $resultIs(false);
                        }
                    });
            },
            uninstall: function ($resultIs) {
                var $this = this;
                this.utils.getCmakePath(function ($cmakePath) {
                    $this.fileSystem.Delete($cmakePath)
                        .Then(function ($success) {
                            $resultIs($success);
                        });
                });
            }
        },
        Mingw: {
            description: "MinGW",
            info: "Minimalist GNU for Windows - a minimalist development environment for native Microsoft Windows applications.",
            source: "https://10gbps-io.dl.sourceforge.net/project/mingw/Installer/mingw-get/mingw-get-0.6.2-beta-20131004-1/mingw-get-0.6.2-mingw32-beta-20131004-1-bin.tar.xz",
            requiredPackages: ["gcc", "g++", "gdb", "mingw32-make"],
            size: 310 * 1024 * 1024,
            installCondition: function ($resultIs) {
                var $this = this;
                this.utils.getMinGWPath(function ($mingwPath) {
                    $this.fileSystem.Exists($mingwPath + "/bin")
                        .Then(function ($success) {
                            if ($success) {
                                var statusAcc = 0;
                                var find = function ($index, $done) {
                                    if ($index < $this.packages.Mingw.requiredPackages.length) {
                                        $this.terminal.Execute($mingwPath + "/bin/" + $this.packages.Mingw.requiredPackages[$index] + ".exe", ["-v"])
                                            .Then(function ($exitCode) {
                                                if ($exitCode === 0) {
                                                    statusAcc++;
                                                }
                                                find($index + 1, $done);
                                            });
                                    } else {
                                        $done(statusAcc !== $this.packages.Mingw.requiredPackages.length);
                                    }
                                };

                                find(0, function ($status) {
                                    $resultIs($status);
                                });
                            } else {
                                $resultIs(true);
                            }
                        });
                });
            },
            install: function ($resultIs) {
                var $this = this;
                $this.fileSystem.Exists($this.block.source)
                    .Then(function ($success) {
                        if ($success) {
                            $this.utils.getMinGWPath(function ($mingwPath) {
                                $this.fileSystem.CreateDirectory($mingwPath)
                                    .Then(function ($success) {
                                        if ($success) {
                                            $this.fileSystem.Unpack($this.block.source, {output: $mingwPath})
                                                .Then(function ($path) {
                                                    $this.fileSystem.Exists($path)
                                                        .Then(function ($success) {
                                                            if ($success) {
                                                                $this.terminal.Spawn("mingw-get.exe", ["update"], $mingwPath + "/bin")
                                                                    .Then(function ($exitCode) {
                                                                        if ($exitCode === 0) {
                                                                            $this.terminal.Spawn("mingw-get.exe", ["install"].concat($this.packages.Mingw.requiredPackages), $mingwPath + "/bin")
                                                                                .Then(function ($exitCode, $stdout) {
                                                                                    if ($exitCode === 0) {
                                                                                        $resultIs(true);
                                                                                    } else {
                                                                                        $resultIs(false, "MinGW packages installation failed.");
                                                                                    }
                                                                                });
                                                                        } else {
                                                                            $resultIs(false, "MinGW packages list update failed.");
                                                                        }
                                                                    });
                                                            } else {
                                                                $resultIs(false);
                                                            }
                                                        });
                                                });
                                        } else {
                                            $resultIs(false);
                                        }
                                    });
                            });
                        } else {
                            $resultIs(false);
                        }
                    });
            },
            uninstall: function ($resultIs) {
                var $this = this;
                this.utils.getMinGWPath(function ($mingwPath) {
                    $this.fileSystem.Delete($mingwPath)
                        .Then(function ($success) {
                            $resultIs($success);
                        });
                });
            }
        },
        Msys2: {
            description: "MSYS2",
            info: "MSYS2 is a software distro and building platform for Windows.",
            source: {
                x86: "http://repo.msys2.org/distrib/i686/msys2-base-i686-20161025.tar.xz",
                x64: "http://repo.msys2.org/distrib/x86_64/msys2-base-x86_64-20161025.tar.xz"
            },
            size: 844 * 1024 * 1024,
            requiredPackages: [
                "make", "rsync", "openssh", "sshpass", "mingw-w64-x86_64-binutils", "mingw-w64-x86_64-gcc",
                "mingw-w64-x86_64-make", "mingw-w64-x86_64-gdb"
            ],
            installCondition: function ($resultIs) {
                var $this = this;
                this.utils.getMsysPath(function ($msysPath) {
                    $this.fileSystem.Exists($msysPath + "/msys2.exe")
                        .Then(function ($success) {
                            if ($success) {
                                $this.fileSystem.Exists($msysPath + "/mingw64/bin/g++.exe")
                                    .Then(function ($success) {
                                        if ($success) {
                                            var checkNextPackage = function ($index) {
                                                if ($index < $this.packages.Msys2.requiredPackages.length) {
                                                    var pkg = $this.packages.Msys2.requiredPackages[$index];
                                                    $this.utils.ShellExecute("pacman",
                                                        ["-Qi", pkg], function ($exitCode, $stdout) {
                                                            if ($exitCode === 0 && !StringUtils.Contains($stdout, "error: package '" + pkg +
                                                                "' was not found")) {
                                                                checkNextPackage($index + 1);
                                                            } else {
                                                                $resultIs(true);
                                                            }
                                                        });
                                                } else {
                                                    $resultIs(false);
                                                }
                                            };
                                            checkNextPackage(0);
                                        } else {
                                            $resultIs(true);
                                        }
                                    });
                            } else {
                                $resultIs(true);
                            }
                        });
                });
            },
            install: function ($resultIs) {
                var $this = this;
                var updateMingw = function () {
                    var updatePackages = function ($callback) {
                        $this.utils.ShellExecute("pacman", [
                            "-Syu", "--noconfirm", "--needed"
                        ].concat($this.packages.Msys2.requiredPackages), function ($exitCode, $stdout) {
                            if ($exitCode === 0) {
                                $callback();
                            } else {
                                $resultIs(false, "Unable to install Mingw packages.");
                            }
                        });
                    };
                    var removePackage = function ($package, $callback) {
                        $this.utils.ShellExecute("pacman", [
                            "-R", $package, "--noconfirm"
                        ], function ($exitCode, $stdout) {
                            if (StringUtils.ContainsIgnoreCase($stdout, "removing " + $package) &&
                                StringUtils.ContainsIgnoreCase($stdout, "100%")) {
                                $callback();
                            } else {
                                $resultIs(false, $stdout);
                            }
                        });
                    };
                    var updateCore = function ($callback) {
                        $this.utils.ShellExecute("pacman", ["-Syuu", "--noconfirm", "--needed"], function ($exitCode, $stdout) {
                            if ($exitCode === 0) {
                                if (StringUtils.ContainsIgnoreCase($stdout, "error: unresolvable package conflicts detected")) {
                                    var match = /are\sin\sconflict.\sRemove\s(.*)\?\s\[y\/N]/gi.exec($stdout);
                                    if (!ObjectValidator.IsEmptyOrNull(match) && match.length >= 2) {
                                        removePackage(match[1], function () {
                                            updateCore($callback);
                                        });
                                    } else {
                                        $resultIs(false, "Can not match stdout of pacman execution while updating MinGW core.");
                                    }
                                } else if (StringUtils.OccurrenceCount($stdout, "there is nothing to do") < 2) {
                                    updateCore($callback);
                                } else if (StringUtils.ContainsIgnoreCase($stdout, "/usr/bin/sh: pacman: command not found")) {
                                    $resultIs(false, "MinGW update failed due to corrupted Msys filesystem.");
                                } else {
                                    updatePackages($callback);
                                }
                            } else {
                                $resultIs(false, "Can not update MinGW core.\n" + $stdout);
                            }
                        });
                    };

                    updateCore(function () {
                        $resultIs(true);
                    });
                };
                var initializeMsys = function () {
                    $this.utils.getMsysPath(function ($msysPath) {
                        $this.fileSystem.CreateDirectory($msysPath + "/tmp")
                            .Then(function () {
                                $this.utils.ShellExecute("exit", [], function ($exitCode, $stdout) {
                                    var match = $stdout.match(/#\s*This\sis\sfirst\sstart\sof\sMSYS2\.\s*#/g);
                                    if ($exitCode === 0 && !ObjectValidator.IsEmptyOrNull(match)) {
                                        updateMingw();
                                    } else {
                                        $this.terminal.Spawn("gcc.exe", ["--version"], $msysPath + "/mingw64/bin")
                                            .Then(function ($exitCode) {
                                                if ($exitCode === 0) {
                                                    updateMingw();
                                                } else {
                                                    $resultIs(false, "Can not initialize Msys.");
                                                }
                                            });
                                    }
                                }, true);
                            });
                    });
                };

                $this.fileSystem.Exists($this.block.source)
                    .Then(function ($success) {
                        if ($success) {
                            $this.utils.getMsysPath(function ($msysPath) {
                                $this.fileSystem.Exists($msysPath)
                                    .Then(function ($success) {
                                        if ($success) {
                                            updateMingw();
                                        } else {
                                            $this.fileSystem.CreateDirectory($msysPath)
                                                .Then(function ($success) {
                                                    if ($success) {
                                                        $this.onChange(true, "Unpacking downloaded archive ...");
                                                        $this.fileSystem.Unpack($this.block.source)
                                                            .Then(function ($path) {
                                                                $this.fileSystem.Exists($path)
                                                                    .Then(function ($success) {
                                                                        if ($success) {
                                                                            $this.onChange(true, "Preparing installation folder ...");
                                                                            $this.fileSystem.Delete($msysPath).Then(function ($success) {
                                                                                if ($success) {
                                                                                    $this.fileSystem.Copy($path, $msysPath).Then(function ($success) {
                                                                                        if ($success) {
                                                                                            $this.fileSystem.Delete($path).Then(function () {
                                                                                                $this.onChange(true, "Starting installation ...");
                                                                                                initializeMsys();
                                                                                            });
                                                                                        } else {
                                                                                            $resultIs(false);
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    $resultIs(false);
                                                                                }
                                                                            });
                                                                        } else {
                                                                            $resultIs(false);
                                                                        }
                                                                    });
                                                            });
                                                    } else {
                                                        $resultIs(false);
                                                    }
                                                });
                                        }
                                    });
                            });
                        } else {
                            $resultIs(false);
                        }
                    });
            },
            uninstall: function ($resultIs) {
                var $this = this;
                this.utils.getMsysPath(function ($msysPath) {
                    $this.fileSystem.Delete($msysPath)
                        .Then(function ($success) {
                            $resultIs($success);
                        });
                });
            }
        }
    }
});
