/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { WebServiceClientType } from "../../../../../../../source/typescript/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { BaseConnector, IConnectorOptions } from "../../../../../../../source/typescript/Io/Oidis/Services/Primitives/BaseConnector.js";

export class MockBaseConnector extends BaseConnector {
    public static initCalled = false;

    public getOptions() : IConnectorOptions {
        return this.options;
    }

    protected init() {
        MockBaseConnector.initCalled = true;
        // suppressed client creation
    }
}

export class MockConnectorAlpha extends MockBaseConnector {
    public something : string;

    constructor($something : string) {
        super(<IConnectorOptions>{suppressInit: true});
        this.something = $something;

        this.init();
    }
}

export class MockConnectorBeta extends MockBaseConnector {

    protected resolveDefaultOptions($options : IConnectorOptions) : IConnectorOptions {
        const options : IConnectorOptions = super.resolveDefaultOptions($options);
        options.clientType = WebServiceClientType.FORWARD_CLIENT;
        return options;
    }
}

export class ConnectorConstructionTest extends UnitTestRunner {
    public testCtorDefault() : void {
        const instance : MockBaseConnector = new MockBaseConnector();

        assert.deepEqual(instance.getOptions(), <IConnectorOptions>{suppressInit: false, maxReconnectCount: 0, reconnect: false});
        assert.equal(MockBaseConnector.initCalled, true);
    }

    public testCtorOptions() : void {
        const instance : MockBaseConnector = new MockBaseConnector({});

        assert.deepEqual(instance.getOptions(), <IConnectorOptions>{suppressInit: false, maxReconnectCount: 0, reconnect: false});
        assert.equal(MockBaseConnector.initCalled, true);
    }

    public testCtorOptionsSuppressInit() : void {
        const instance : MockBaseConnector = new MockBaseConnector({suppressInit: true});

        assert.deepEqual(instance.getOptions(), <IConnectorOptions>{suppressInit: true, maxReconnectCount: 0, reconnect: false});
        assert.equal(MockBaseConnector.initCalled, false);
    }

    public testCtorOptionsFull() : void {
        const instance : MockBaseConnector = new MockBaseConnector(<IConnectorOptions>{
            clientType               : WebServiceClientType.HUB_CONNECTOR,
            maxReconnectCount        : 5,
            reconnect                : true,
            serverConfigurationSource: "unknown-hub-location",
            suppressInit             : true
        });

        assert.deepEqual(instance.getOptions(), {
            clientType               : WebServiceClientType.HUB_CONNECTOR,
            maxReconnectCount        : -1,
            reconnect                : true,
            serverConfigurationSource: "unknown-hub-location",
            suppressInit             : true
        });
        assert.equal(MockBaseConnector.initCalled, false);
    }

    public testCtorReconnectArg() : void {
        const instance : MockBaseConnector = new MockBaseConnector(true);

        // not sure why this assert needs stringify because data printed are the same with the same order, to be checked by debugger
        assert.deepEqual(JSON.stringify(instance.getOptions()), JSON.stringify({
            maxReconnectCount: -1,
            reconnect        : true,
            suppressInit     : false
        }));
        assert.equal(MockBaseConnector.initCalled, true);
    }

    public testCtorFullArgs() : void {
        const instance : MockBaseConnector = new MockBaseConnector(true, "some-url", WebServiceClientType.REST_CONNECTOR);

        // not sure why this assert needs stringify because data printed are the same with the same order, to be checked by debugger
        assert.deepEqual(instance.getOptions(), <IConnectorOptions>{
            clientType               : WebServiceClientType.REST_CONNECTOR,
            maxReconnectCount        : -1,
            reconnect                : true,
            serverConfigurationSource: "some-url",
            suppressInit             : false
        });
        assert.equal(MockBaseConnector.initCalled, true);
    }

    public testAlphaOverride() : void {
        const instance : MockConnectorAlpha = new MockConnectorAlpha("hello");

        assert.deepEqual(instance.getOptions(), <IConnectorOptions>{
            maxReconnectCount: 0,
            reconnect        : false,
            suppressInit     : true
        });
        assert.equal(instance.something, "hello");
        assert.equal(MockBaseConnector.initCalled, true);
    }

    public testBetaOverride() : void {
        const instance : MockConnectorBeta = new MockConnectorBeta();

        assert.deepEqual(instance.getOptions(), <IConnectorOptions>{
            clientType       : WebServiceClientType.FORWARD_CLIENT,
            maxReconnectCount: 0,
            reconnect        : false,
            suppressInit     : false
        });
        assert.equal(MockBaseConnector.initCalled, true);
    }

    protected setUp() : void {
        MockBaseConnector.initCalled = false;
    }
}
