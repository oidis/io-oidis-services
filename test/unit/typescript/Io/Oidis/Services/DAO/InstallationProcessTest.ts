/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import {
    FileSystemHandlerConnector
} from "../../../../../../../source/typescript/Io/Oidis/Services/Connectors/FileSystemHandlerConnector.js";
import { InstallationRecipeDAO } from "../../../../../../../source/typescript/Io/Oidis/Services/DAO/InstallationRecipeDAO.js";
import { InstallationProgressCode } from "../../../../../../../source/typescript/Io/Oidis/Services/Enums/InstallationProgressCode.js";
import { InstallationProtocolType } from "../../../../../../../source/typescript/Io/Oidis/Services/Enums/InstallationProtocolType.js";
import {
    InstallationProgressEventArgs
} from "../../../../../../../source/typescript/Io/Oidis/Services/Events/Args/InstallationProgressEventArgs.js";

export class InstallationProcessTest extends UnitTestRunner {
    protected fileSystem : FileSystemHandlerConnector;
    private installDao : InstallationRecipeDAO;
    private installScript : string;
    private forceChain : boolean;
    private chain : string[];

    constructor() {
        super();
        this.setScript("test/resource/data/Io/Oidis/Services/Configuration/BaseInstallationRecipe.jsonp");
        this.setChain([
            // "Netfx4Full",
            // "VisualStudio2015",
            // "VisualCppBuildTools",
            // "VCRedist2012",
            // "VCRedist2015",
            // "Java",
            // "Git",
            // "Python",
            // "Ruby",
            // "Cmake",
            // "Mingw",
            // "Msys2",
            "PortableGit"
            // "PortableCMake"
        ]);
    }

    public __IgnoretestValidate() : IUnitTestRunnerPromise {
        this.timeoutLimit(-1);
        return ($done : () => void) : void => {
            const testGitPath : string = this.getAbsoluteRoot() + "/test/InstallationProcess/git/cmd/git.exe";
            this.getDao().getConfigurationInstance().utils.getGitPath = ($callback : ($path : string) => void) : void => {
                LogIt.Debug("Using overridden getGitPath {0}", testGitPath);
                $callback(testGitPath);
            };
            this.getDao().getEvents().setOnComplete(() : void => {
                this.fileSystem.Exists(testGitPath).Then(($status : boolean) : void => {
                    assert.ok(!$status);
                    $done();
                });
            });
            this.runProtocol(InstallationProtocolType.VALIDATE);
        };
    }

    protected before() : IUnitTestRunnerPromise {
        this.installDao = new InstallationRecipeDAO();
        this.installDao.setConfigurationPath(this.installScript);
        this.installDao.getEvents().setOnStart(() : void => {
            LogIt.Debug("Running installation recipe version \"{0}\".", this.installDao.getConfigurationVersion());
        });
        this.installDao.getEvents().setOnChange(($eventArgs : InstallationProgressEventArgs) : void => {
            if ($eventArgs.ProgressCode() === InstallationProgressCode.DOWNLOAD_START) {
                LogIt.Debug("Download started.");
            }
            if ($eventArgs.ProgressCode() !== InstallationProgressCode.DOWNLOAD_CHANGE) {
                if ($eventArgs.ProgressCode() === InstallationProgressCode.STEP_START) {
                    LogIt.Debug("Step {0}, {1}/{2}", $eventArgs.Message(), $eventArgs.CurrentValue() + "", $eventArgs.RangeEnd() + "");
                } else {
                    if ($eventArgs.CurrentValue() > -1) {
                        LogIt.Debug("{0}, {1}/{2}", $eventArgs.Message(), $eventArgs.CurrentValue() + "", $eventArgs.RangeEnd() + "");
                    } else {
                        LogIt.Debug($eventArgs.Message());
                    }
                }
            }
        });
        this.installDao.getEvents().setOnComplete(() : void => {
            LogIt.Debug("Installation has finished.");
        });
        this.installDao.getEvents().setOnError(($eventArgs : ErrorEventArgs) : void => {
            LogIt.Debug("ERROR: {0}", $eventArgs.Message());
        });

        return ($done : () => void) : void => {
            /// TODO: handle async error in async before method in UnitTestRunner or add at least some execution timeout
            // this.installDao.Load(LanguageType.EN, () : void => {
            //     if (ObjectValidator.IsEmptyOrNull(this.installDao.getChain()) || this.forceChain) {
            //         this.installDao.getStaticConfiguration().getChain = () : string[] => {
            //             return this.chain;
            //         };
            //     }
            //     this.fileSystem = this.installDao.getConfigurationInstance().utils.fileSystem;
            //     $done();
            // });
            $done();
        };
    }

    protected tearDown() : void {
        this.initSendBox();
    }

    protected setScript($path : string) : void {
        if (!ObjectValidator.IsEmptyOrNull($path)) {
            this.installScript = $path;
        }
    }

    protected setChain($value : string[], $force : boolean = false) : void {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.chain = $value;
            this.forceChain = $force;
        }
    }

    protected getDao() : InstallationRecipeDAO {
        return this.installDao;
    }

    protected runProtocol($type : InstallationProtocolType) : void {
        this.installDao.RunInstallationChain($type);
    }
}
