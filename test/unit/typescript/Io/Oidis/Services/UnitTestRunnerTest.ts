/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "./UnitTestRunner.js";

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { IVBoxSession } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";

export class UnitTestRunnerTest extends UnitTestRunner {

    public __IgnoretestVBox() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.vbox
                .Start("Clean Win 7")
                .Then(($success : boolean, $session : IVBoxSession) : void => {
                    if ($success) {
                        LogIt.Info("Hooray, virtual machine is running");
                        $session
                            .Copy(
                                "C:/Windows/System32/notepad.exe",
                                "C:/Users/Guest/AppData/Local/Temp/notepad.exe")
                            .Then(($success : boolean, $session : IVBoxSession) : void => {
                                if ($success) {
                                    LogIt.Info("File copy successful");
                                    $session
                                        .Execute("C:/Windows/System32/notepad.exe")
                                        .Then(($success : boolean) : void => {
                                            if ($success) {
                                                LogIt.Info("File execution successful");
                                                $done();
                                            } else {
                                                LogIt.Error("File execution failed");
                                            }
                                        });
                                } else {
                                    LogIt.Error("File copy failed");
                                }
                            });
                    } else {
                        LogIt.Error("Sorry, bad luck :-)");
                    }
                });
        };
    }
}
